'use strict';

const JM = require('json-mapper');
const util = require('../../util');

const visitNoteResponseCsvMapper = JM.makeConverter({
    "threadVisitId": function(i) {
        if (i && i.threadVisitId) {
            return i.threadVisitId;
        }
        return "";
    },    
	"studyId": function(i) {
        if (i && i.studyId) {
            return i.studyId;
        }
        return "";
    },
    "siteId": function(i) {
        if (i && i.siteId) {
            return i.siteId;
        }
        return "";
    },
    "siteName": function(i) {
        if (i && i.siteName) {
            return i.siteName;
        }
        return "";
    },
    "ThreadParticipantID": function(i) {
        if (i && i.participantId) {
            return i.participantId;
        }
        return "";
    },
    "ParticipantID": function(i) {
        if (i && i.userDefinedParticipantId) {
            return i.userDefinedParticipantId;
        }
        return "";
    },
   
    "truClinicVisitId": function(i) {
        if (i && i.truClinicVisitId) {
            return i.truClinicVisitId;
        }
        return "";
    }
});

exports.visitNoteResponseCsvMapper = visitNoteResponseCsvMapper;