'use strict';

const JM = require('json-mapper');
const moment = require('moment');
const util = require('../../util');

const activityResponseCsvMapper = JM.makeConverter({
    "responseId": function(i) {
        if (i && i.id) {
            return i.id;
        }
        return "";
    },
    "studyId": function(i) {
        if (i && i.studyId) {
            return i.studyId;
        }
        return "";
    },
    "siteId": function(i) {
        if (i && i.siteId) {
            return i.siteId;
        }
        return "";
    },
    "siteName": function(i) {
        if (i && i.siteName) {
            return i.siteName;
        }
        return "";
    },
    "ThreadParticipantID": function(i) {
        if (i && i.participantId) {
            return i.participantId;
        }
        return "";
    },
    "ParticipantID": function(i) {
        if (i && i.userDefinedParticipantId) {
            return i.userDefinedParticipantId;
        }
        return "";
    },
    "activityId": function(i) {
        if (i && i.activityId) {
            return i.activityId;
        }
        return "";
    },
    "activityName": function(i) {
        if (i && i.activityName) {
            return i.activityName;
        }
        return "";
    },
    "timeTaken": function(i) {
        if (i && i.timeTaken) {
            return i.timeTaken;
        }
        return "";
    },
    "startTime": function(i) {
        if (i && i.startTime) {
            return (i.startTime) ? util.changeDateFormat(util.utcToGmt(i.startTime), 'YYYY/MM/DD hh:mm:ss.000  A') : '';
        }
        return "";
    },
    "endTime": function(i) {
        if (i && i.endTime) {
            return (i.endTime) ? util.changeDateFormat(util.utcToGmt(i.endTime), 'YYYY/MM/DD hh:mm:ss.000 A') : '';
        }
        return "";
    },
    "startTimeLocal": function(i) {
        if (i && i.participantLocalStartTime) {
            return (i && i.participantLocalStartTime && i.participantLocalTimeOffset) ? util.stringDateFormatter(i.participantLocalStartTime, i.participantLocalTimeOffset, 'MM/DD/YYYY HH:mm:ss') : '';
            // return (i.participantLocalStartTime) ? util.changeDateFormat(i.participantLocalStartTime, 'YYYY/MM/DD hh:mm:ss A') : '';
        }
        return "";
    },
    "endTimeLocal": function(i) {
        if (i && i.participantLocalEndTime) {
            return (i && i.participantLocalEndTime && i.participantLocalTimeOffset) ? util.stringDateFormatter(i.participantLocalEndTime, i.participantLocalTimeOffset, 'MM/DD/YYYY HH:mm:ss') : '';
        }
        return "";
    },
    "localTimeOffset": function(i) {
        if (i && i.participantLocalTimeOffset) {
            return (i.participantLocalTimeOffset) ? i.participantLocalTimeOffset : '';
        }
        return "";
    },
    "status": function(i) {
        if (i && i.status) {
            return i.status;
        }
        return "";
    },
    "right_eye_score": function(i) {
        if (i && i.rightEyeScore && i.examType && (i.examType === 'ContrastAcuityExamResults' || i.examType === 'VisualAcuityExamResults')) {
            return i.rightEyeScore;
        }
        return;
    },
    "left_eye_score": function(i) {
        if (i && i.leftEyeScore && i.examType && (i.examType === 'ContrastAcuityExamResults' || i.examType === 'VisualAcuityExamResults')) {
            return i.leftEyeScore;
        }
        return;
    }
});

exports.activityResponseCsvMapper = activityResponseCsvMapper;