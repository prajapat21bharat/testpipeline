'use strict';

const JM = require('json-mapper');
const util = require('../../util');

const validicUserProfileCsvMapper = JM.makeConverter({

    "ThreadParticipantID": function (i) {
        if (i && i.participantId !== 'undefined') {
            return i.participantId;
        }
        // if (i && i.participantId !== 'undefined') {
        //     return i.participantId;
        // }
        return "";
    },
    "ParticipantID": function (i) {
        if (i && i.userDefinedParticipantId !== 'undefined') {
            return i.userDefinedParticipantId;
        }
        return "";
    },
    "Study ID": function (i) {
        if (i && i.studyId !== 'undefined') {
            return i.studyId;
        }
        return "";
    },
    "Site ID": function (i) {
        if (i && i.siteId !== 'undefined') {
            return i.siteId;
        }
        return "";
    },
    "Site Name": function (i) {
        if (i && i.siteName !== 'undefined') {
            return i.siteName;
        }
        return "";
    },
    "Device Type": function (i) {
        if (i && i.deviceType !== 'undefined') {
            return i.deviceType;
        }
        return "";
    },
    "Connected_at": function (i) {
        if (i && i.connectedAt !== 'undefined') {
            return i.connectedAt;
        }
        return "";
    },
    "Last_Processed_at": function (i) {
        if (i && i.LastProcessedAt !== 'undefined') {
            return i.LastProcessedAt;
        }
        return "";
    }

});

exports.validicUserProfileCsvMapper = validicUserProfileCsvMapper;