'use strict';

const JM = require('json-mapper');
const util = require('../../util');

const validicCsvMapper = JM.makeConverter({

    "ThreadParticipantID": function(i) {
        if (i && i.participantId !== 'undefined') {
            return i.participantId;
        }
        return "";
    },
    "ParticipantID": function(i) {
        if (i && i.userDefinedParticipantId !== 'undefined') {
            return i.userDefinedParticipantId;
        }
        return "";
    },
    "Study ID": function(i) {
        if (i && i.studyId !== 'undefined') {
            return i.studyId;
        }
        return "";
    },
    "Site ID": function(i) {
        if (i && i.siteId !== 'undefined') {
            return i.siteId;
        }
        return "";
    },
    "Site Name": function(i) {
        if (i && i.siteName !== 'undefined') {
            return i.siteName;
        }
        return "";
    },
    "Start date": function(i) {
        if (i && i.dataCaptureStartTime !== 'undefined') {
            return util.changeDateFormat(i.dataCaptureStartTime, 'MM/DD/YYYY HH:mm:ss');
        }
        return "";
    },
    "End Date": function(i) {
        if (i && i.dataCaptureEndTime !== 'undefined') {
            return util.changeDateFormat(i.dataCaptureEndTime, 'MM/DD/YYYY HH:mm:ss');
        }
        return "";
    },
    "Summaries": function(i) {
        if (i && i.dataObjectType && i.dataObjectType.toLowerCase() === 'summaries') {
            return `Summaries/${i.id}.json`;
        } else if (i && i.Summaries && i.Summaries !== 'undefined' && i.Summaries !== '') { return `Summaries/${i.Summaries}.json`; } else { return ''; }
    },
    "Workouts": function(i) {
        if (i && i.dataObjectType && i.dataObjectType.toLowerCase() === 'workouts') {
            return `Workouts/${i.id}.json`;
        } else if (i && i.Workouts && i.Workouts !== 'undefined' && i.Workouts !== '') { return `Workouts/${i.Workouts}.json`; } else { return ''; }
    },
    "Measurements": function(i) {
        if (i && i.dataObjectType && i.dataObjectType.toLowerCase() === 'measurements') {
            return `Measurements/${i.id}.json`;
        } else if (i && i.Measurements && i.Measurements !== 'undefined' && i.Measurements !== '') { return `Measurements/${i.Measurements}.json`; } else { return ''; }
    },
    "Nutrition": function(i) {
        if (i && i.dataObjectType && i.dataObjectType.toLowerCase() === 'nutrition') {
            return `Nutrition/${i.id}.json`;
        } else if (i && i.Nutrition && i.Nutrition !== 'undefined' && i.Nutrition !== '') { return `Nutrition/${i.Nutrition}.json`; } else { return ''; }
    },
    "Sleep": function(i) {
        if (i && i.dataObjectType && i.dataObjectType.toLowerCase() === 'sleep') {
            return `Sleep/${i.id}.json`;
        } else if (i && i.Sleep && i.Sleep !== 'undefined' && i.Sleep !== '') { return `Sleep/${i.Sleep}.json`; } else { return ''; }
    }

});

exports.validicCsvMapper = validicCsvMapper;