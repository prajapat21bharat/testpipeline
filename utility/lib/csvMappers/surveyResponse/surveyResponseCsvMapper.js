'use strict';

const JM = require('json-mapper');
const util = require('../../util');

const surveyResponseCsvMapper = JM.makeConverter({
    surveyId: function(i) {
        if (i && i.surveyId) {
            return i.surveyId;
        }
        return "";
    },
    studyId: function(i) {
        if (i && i.studyId) {
            return i.studyId;
        }
        return "";
    },
    siteId: function(i) {
        if (i && i.siteId) {
            return i.siteId;
        }
        return "";
    },
    siteName: function(i) {
        if (i && i.siteName) {
            return i.siteName;
        }
        return "";
    },
    ThreadParticipantID: function(i) {
        if (i && i.participantId) {
            return i.participantId;
        }
        return "";
    },
    ParticipantID: function(i) {
        if (i && i.userDefinedParticipantId) {
            return i.userDefinedParticipantId;
        }
        return "";
    },
    questionId: function(i) {
        if (i && i.questionId) {
            return i.questionId;
        }
        return "";
    },
    optionId: function(i) {
        if (i && i.optionId) {
            return i.optionId;
        }
        return "";
    },
    answerText: function(i) {
        if (i && i.answerText) {
            return i.answerText;
        }
        return "";
    },
    questionType: function(i) {
        if (i && i.questionType) {
            return i.questionType;
        }
        return "";
    },
    isSkipped: function(i) {
        if (i && i.isSkipped) {
            return i.isSkipped;
        } else {
            return "FALSE";
        }
    },
    timeTaken: function(i) {
        if (i && i.timeTaken) {
            return i.timeTaken;
        } else if (i && (i.timeTaken || i.timeTaken === 0)) {
            if (i.timeTaken === 0)
                return i.timeTaken;
        }
        return "";
    },
    startTime: function(i) {
        if (i && i.startTime) {
            return util.changeDateFormat(i.startTime, "MM/DD/YYYY hh:mm:ss.000 A");
        }
        return "";
    },
    endTime: function(i) {
        if (i && i.endTime) {
            return util.changeDateFormat(i.endTime, "MM/DD/YYYY hh:mm:ss.000 A");
        }
        return "";
    },
    startTimeLocal: function(i) {
        if (i && i.startTimeLocal) {
            return i.startTimeLocal;
        }
        return "";
    },
    endTimeLocal: function(i) {
        if (i && i.endTimeLocal) {
            return i.endTimeLocal;
        }
        return "";
    },
    localTimeOffset: function(i) {
        if (i && i.localTimeOffset) {
            return i.localTimeOffset;
        }
        return "+00:00";
    },
    status: function(i) {
        if (i && i.status) {
            return i.status;
        }
        return "";
    },
    deviceType: function(i) {
        if (i && i.deviceType) {
            return i.deviceType;
        }
        return "";
    },
    surveyResponseTrackerId: function(i) {
        if (i && i.surveyResponseTrackerId) {
            return i.surveyResponseTrackerId;
        }
        return "";
    },
    app_language_id: function(i) {
        if (i && i.appLanguageId) {
            return i.appLanguageId;
        }
        return "";
    },
    display_language: function(i) {
        if (i && i.displayLanguage) {
            return i.displayLanguage;
        }
        return "";
    },
    question_text: function(i) {
        if (i && i.questionText) {
            return i.questionText;
        }
        return "";
    },
    display_question_text: function(i) {
        if (i && i.displayQuestionText) {
            return i.displayQuestionText;
        }
        return "";
    },
    display_answer_text: function(i) {
        if (i && i.displayAnswerText) {
            return i.displayAnswerText;
        }
        return "";
    },
});

exports.surveyResponseCsvMapper = surveyResponseCsvMapper;