'use strict';

const JM = require('json-mapper');
const util = require('../../util');

const feedbackCsvMapper = JM.makeConverter({
    studyId: function(i) {
        if (i && i.studyId !== 'undefined') {
            return i.studyId;
        }
        return "";
    },
    siteId: function(i) {
        if (i && i.siteId !== 'undefined') {
            return i.siteId;
        }
        return "";
    },
    siteName: function(i) {
        if (i && i.siteName !== 'undefined') {
            return i.siteName;
        }
        return "";
    },
    ThreadParticipantID: function(i) {
        if (i && i.participantId !== 'undefined') {
            return i.participantId;
        }
        return "";
    },
    ParticipantID: function(i) {
        if (i && i.userDefinedParticipantId !== 'undefined') {
            return i.userDefinedParticipantId;
        }
        return "";
    },
    questionId: function(i) {
        if (i && i.questionId !== 'undefined') {
            return i.questionId;
        }
        return "";
    },
    answerText: function(i) {
        if (i && i.answerText !== 'undefined') {
            return i.answerText;
        }
        return "";
    },
    responseTime: function(i) {
        if (i && i.responseTime !== 'undefined') {
            return util.changeDateFormat(i.responseTime, "MM/DD/YYYY hh:mm:ss.000 A");
        }
        return "";
    },
    questionType: function(i) {
        if (i && i.questionType !== 'undefined') {
            return i.questionType;
        }
        return "";
    },
    app_language_id: function(i) {
        if (i && i.appLanguageId !== 'undefined') {
            return i.appLanguageId;
        }
        return "";
    },
    display_language: function(i) {
        if (i && i.displayLanguage !== 'undefined') {
            return i.displayLanguage;
        }
        return "";
    },
    question_text: function(i) {
        if (i && i.questionText !== 'undefined') {
            return i.questionText;
        }
        return "";
    },
    display_question_text: function(i) {
        if (i && i.displayQuestionText !== 'undefined') {
            return i.displayQuestionText;
        }
        return "";
    },
    display_answer_text: function(i) {
        if (i && i.displayAnswerText !== 'undefined') {
            return i.displayAnswerText;
        }
        return "";
    },
});

exports.feedbackCsvMapper = feedbackCsvMapper;