'use strict';
const { activityResponseCsvMapper } = require('./activityResponse/activityResponseCsvMapper');
// const { edcCsvMapper } = require('./participant/edcCsvMapper');
// const { edcUnschduledPacketCsvMapper } = require('./participant/edcUnschduledPacketCsvMapper');
const { feedbackCsvMapper } = require('./feedback/feedbackCsvMapper');
const { surveyResponseCsvMapper } = require('./surveyResponse/surveyResponseCsvMapper');
const { participantRegistrationCsvMapper } = require('./participantRegistrationData/participantRegistrationCsvMapper');
const { healthResponseCsvMapper } = require('./healthResponse/healthResponseCsvMapper');
const { consentOnBoardingEventCsvMapper } = require('./onBoarding/consentOnBoardingEventCsvMapper');
const { onBoardingCsvMapper } = require('./onBoarding/onBoardingCsvMapper');
const { participantOnBoardingEventCsvMapper } = require('./onBoarding/participantOnBoardingEventCsvMapper');
const { truClinicResponseCsvMapper } = require('./truClinicResponse/truClinicResponseCsvMapper');
const { visitNoteResponseCsvMapper } = require('./visitNoteResponse/visitNoteResponseCsvMapper');
const { validicCsvMapper } = require('./validic/validicCsvMapper');
const { validicUserProfileCsvMapper } = require('./validic/validicUserProfileCsvMapper');
// const { visitNoteCsvMapper } = require('./participant/visitNoteCsvMapper');
const { edcResponseCsvMapper } = require('./edcResponseCsvMapper/edcResponseCsvMapper');

exports.map = {
    activityResponseCsvMapper,
    // edcCsvMapper,
    // edcUnschduledPacketCsvMapper,
    consentOnBoardingEventCsvMapper,
    onBoardingCsvMapper,
    participantOnBoardingEventCsvMapper,
    feedbackCsvMapper,
    healthResponseCsvMapper,
    surveyResponseCsvMapper,
    participantRegistrationCsvMapper,
    truClinicResponseCsvMapper,
    validicCsvMapper,
    validicUserProfileCsvMapper,
    visitNoteResponseCsvMapper,
    edcResponseCsvMapper
};