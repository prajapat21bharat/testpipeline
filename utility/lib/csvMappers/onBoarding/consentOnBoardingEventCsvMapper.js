'use strict';

const JM = require('json-mapper');
const util = require('../../util');

const consentOnBoardingEventCsvMapper = JM.makeConverter({

    id: function(i) {
        if (i && i.id) {
            return i.id;
        }
        return "";
    },
    studyId: function(i) {
        if (i && i.studyId) {
            return i.studyId;
        }
        return "";
    },
    studyName: function(i) {
        if (i && i.studyName) {
            return i.studyName;
        }
        return "";
    },
    siteId: function(i) {
        if (i && i.siteId) {
            return i.siteId;
        }
        return "";
    },
    siteName: function(i) {
        if (i && i.siteName) {
            return i.siteName;
        }
        return "";
    },
    ThreadParticipantID: function(i) {
        if (i && i.participantId) {
            return i.participantId;
        }
        return "";
    },
    ParticipantID: function(i) {
        if (i && i.ParticipantID !== null && i.userDefinedParticipantId) {
            return i.userDefinedParticipantId;
        }
        return "";
    },
    cohortId: function(i) {
        if (i && i.cohortId) {
            return i.cohortId;
        }
        return "";
    },
    cohortName: function(i) {
        if (i && i.cohortName) {
            return i.cohortName;
        }
        return "";
    },
    consentId: function(i) {
        if (i && i.consentId) {
            return i.consentId;
        }
        return "";
    },
    consentName: function(i) {
        if (i && i.consentName) {
            return i.consentName;
        }
        return "";
    },
    consentedTime: function(i) {
        if (i && i.consentedTime) {
            // i.consentedTime = util.changeDateToIsoFormat(i.consentedTime);
            // return i.consentedTime;
            let endTimeString = util.changeDateToIsoFormat(i.consentedTime);
            let endTime = (endTimeString && endTimeString.length > 10) ?
                endTimeString.slice(0, -1) + " 0000" : '';
            return (endTime);
        }
        return "";
    },
    localConsentedTime: function(i) {
        if (i && i.localConsentedTime && i.offset) {
            // i.localConsentedTime = util.changeDateFormat(i.localConsentedTime, 'MM/DD/YYYY HH:mm');
            return util.stringDateFormatter(i.localConsentedTime, i.offset, 'MM/DD/YYYY HH:mm:ss');
        }
        return "";
    },
    offset: function(i) {
        if (i && i.offset) {
            return i.offset;
        }
        return "";
    }
});

exports.consentOnBoardingEventCsvMapper = consentOnBoardingEventCsvMapper;