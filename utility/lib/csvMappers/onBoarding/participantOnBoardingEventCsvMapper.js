'use strict';

const JM = require('json-mapper');
const util = require('../../util');

const participantOnBoardingEventCsvMapper = JM.makeConverter({

    id: function(i) {
        if (i && i.id) {
            return i.id;
        }
        return "";
    },
    studyId: function(i) {
        if (i && i.studyId) {
            return i.studyId;
        }
        return "";
    },
    siteId: function(i) {
        if (i && i.siteId) {
            return i.siteId;
        }
        return "";
    },
    siteName: function(i) {
        if (i && i.siteName) {
            return i.siteName;
        }
        return "";
    },
    ThreadParticipantID: function(i) {
        if (i && i.participantId) {
            return i.participantId;
        }
        return "";
    },
    ParticipantID: function(i) {
        if (i && i.ParticipantID !== null && i.userDefinedParticipantId) {
            return i.userDefinedParticipantId;
        }
        return "";
    },
    registeredTime: function(i) {
        if (i && i.registeredTime) {
            return i.registeredTime;
        }
        return "";
    },
    localRegistrationTime: function(i) {
        if (i && i.localRegistrationTime && i.offset) {
            // return (i.localRegistrationTime) ? util.utcToGmt(util.changeDateToUtc(i.localRegistrationTime), 'MM/DD/YYYY hh:mm:ss A') : '';
            if (util.stringDateFormatter(i.localRegistrationTime, i.offset) !== 'Invalid date') {
                return util.stringDateFormatter(i.localRegistrationTime, i.offset, 'MM/DD/YYYY HH:mm:ss');
            } else {
                let endTime = (i.localRegistrationTime && i.localRegistrationTime.length > 10) ?
                    i.localRegistrationTime.slice(0, -5) + "" : '';
                return util.stringDateFormatter(endTime, i.offset, 'MM/DD/YYYY HH:mm:ss')
            }
        }
        return "";
    },
    emailVerifiedTime: function(i) {
        if (i && i.emailVerifiedTime) {
            return i.emailVerifiedTime;
        }
        return "";
    },
    consentedTime: function(i) {
        if (i && i.consentedTime) {
            let endTimeString = util.changeDateToIsoFormat(i.consentedTime);
            let endTime = (endTimeString && endTimeString.length > 10) ?
                endTimeString.slice(0, -1) + " 0000" : '';
            return (endTime);
        }
        return "";
    },
    localConsentedTime: function(i) {
        if (i && i.localConsentedTime) {
            return util.stringDateFormatter(i.localConsentedTime, i.offset, 'MM/DD/YYYY HH:mm:ss');
        }
        return "";
    },
    offset: function(i) {
        if (i && i.offset) {
            return i.offset;
        }
        return "";
    }
});

exports.participantOnBoardingEventCsvMapper = participantOnBoardingEventCsvMapper;