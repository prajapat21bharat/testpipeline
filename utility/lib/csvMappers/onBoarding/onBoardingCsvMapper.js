'use strict';

const JM = require('json-mapper');
const util = require('../../util');

const onBoardingCsvMapper = JM.makeConverter({

    deviceId: function(i) {
        if (i && i.deviceId !== 'undefined') {
            return i.deviceId;
        }
        return "";
    },
    studyId: function(i) {
        if (i && i.studyId !== 'undefined') {
            return i.studyId;
        }
        return "";
    },
    siteId: function(i) {
        if (i && i.siteId !== 'undefined') {
            return i.siteId;
        }
        return "";
    },
    siteName: function(i) {
        if (i && i.siteName !== 'undefined') {
            return i.siteName;
        }
        return "";
    },
    ThreadParticipantID: function(i) {
        if (i && i.participantId !== 'undefined') {
            return i.participantId;
        }
        return "";
    },
    ParticipantID: function(i) {
        if (i && i.userDefinedParticipantId !== null && i.userDefinedParticipantId !== 'undefined') {
            return i.userDefinedParticipantId;
        }
        return "";
    },
    questionId: function(i) {
        if (i && i.questionId !== 'undefined') {
            return i.questionId;
        }
        return "";
    },
    questionType: function(i) {
        if (i && i.questionType !== 'undefined') {
            return i.questionType;
        }
        return "";
    },
    type: function(i) {
        if (i && i.type !== 'undefined') {
            return i.type;
        }
        return "";
    },
    questionBody: function(i) {
        if (i && i.questionBody !== 'undefined') {
            return i.questionBody;
        }
        return "";
    },
    questionAnswer: function(i) {
        if (i && i.questionAnswer !== 'undefined') {
            return i.questionAnswer;
        }
        return "";
    },
    createdTime: function(i) {
        if (i && i.createdTime !== 'undefined') {
            let createdTimeString = util.getISO8601(util.changeDateToIsoFormat(i.createdTime));
            let createTime = (createdTimeString && createdTimeString.length > 10) ?
                createdTimeString.slice(0, -4) + "0000" : '';
            return (createTime);
        }
        return "";
    },
    app_language_id: function(i) {
        if (i && i.appLanguageId !== 'undefined') {
            return i.appLanguageId;
        }
        return "";
    },
    display_language: function(i) {
        if (i && i.displayLanguage !== 'undefined') {
            return i.displayLanguage;
        }
        return "";
    },
    display_question_text: function(i) {
        if (i && i.displayQuestionBody !== 'undefined') {
            return i.displayQuestionBody;
        }
        return "";
    },
    display_answer_text: function(i) {
        if (i && i.displayQuestionAnswer !== 'undefined') {
            return i.displayQuestionAnswer;
        }
        return "";
    }
});

exports.onBoardingCsvMapper = onBoardingCsvMapper;