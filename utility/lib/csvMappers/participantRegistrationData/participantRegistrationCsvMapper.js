'use strict';

const JM = require('json-mapper');
const util = require('../../util');

const participantRegistrationCsvMapper = JM.makeConverter({
    ThreadParticipantID: function(i) {
        if (i && i.id) {
            return i.id;
        }
        return "";
    },
    ParticipantID: function(i) {
        if (i && i.userDefinedParticipantId) {
            return i.userDefinedParticipantId;
        }
        return "";
    },
    studyId: function(i) {
        if (i && i.studyId) {
            return i.studyId;
        }
        return "";
    },
    siteId: function(i) {
        if (i && i.siteId) {
            return i.siteId;
        }
        return "";
    },
    siteName: function(i) {
        if (i && i.siteName) {
            return i.siteName;
        }
        return "";
    },
    email: function(i) {
        if (i && i.email && i.email !== 'undefined') {
            return i.email;
        }
        if (i.isPi && i.isPi === true)
            return "";
        else
            return;
    },
    firstName: function(i) {
        if (i && i.firstName && i.firstName !== 'undefined') {
            return i.firstName;
        }
        if (i.isPi && i.isPi === true)
            return "";
        else
            return;
    },
    lastName: function(i) {
        if (i && i.lastName && i.lastName !== 'undefined') {
            return i.lastName;
        }
        if (i.isPi && i.isPi === true)
            return "";
        else
            return;
    },
    phoneNumber: function(i) {
        if (i && i.phoneNumber && i.phoneNumber !== 'undefined') {
            return i.phoneNumber;
        }
        if (i.isPi && i.isPi === true)
            return "";
        else
            return;
    },
    nonMobilePhoneNumber: function(i) {
        if (i && i.nonMobilePhoneNumber && i.nonMobilePhoneNumber !== 'undefined') {
            return i.nonMobilePhoneNumber;
        }
        if (i.isPi && i.isPi === true)
            return "";
        else
            return;
    },
    address: function(i) {
        if (i && i.address && i.address !== 'undefined') {
            return i.address;
        }
        if (i.isPi && i.isPi === true)
            return "";
        else
            return;
    },
    city: function(i) {
        if (i && i.city && i.city !== 'undefined') {
            return i.city;
        }
        if (i.isPi && i.isPi === true)
            return "";
        else
            return;
    },
    state: function(i) {
        if (i && i.state && i.state !== 'undefined') {
            return i.state;
        }
        if (i.isPi && i.isPi === true)
            return "";
        else
            return;
    },
    zipcode: function(i) {
        if (i && i.zipcode && i.zipcode !== 'undefined') {
            return i.zipcode;
        }
        if (i.isPi && i.isPi === true)
            return "";
        else
            return;
    },
    status: function(i) {
        if (i && i.status) {
            return i.status;
        }
        return "";
    },
    invitationDate: function(i) {
        if (i && i.invitationDate) {
            return (i.invitationDate) ? util.changeDateFormat(util.utcToGmt(i.invitationDate), 'YYYY/MM/DD hh:mm:ss A') : '';
        }
        return "";
    },
    registrationDate: function(i) {
        if (i && i.registrationDate) {
            return (i.registrationDate) ? util.changeDateFormat(util.utcToGmt(i.registrationDate), 'YYYY/MM/DD hh:mm:ss A') : '';
        }
        return "";
    },
    localRegistrationDate: function(i) {
        if (i && i.localRegistrationDate) {
            // return (i.registrationDate) ? util.changeDateFormat(new Date(util.getGmtTime(new Date(`${i.registrationDate}`))), 'MM/DD/YYYY hh:mm:ss A') : '';
            // return (i.registrationDate) ? util.changeDateFormat(i.registrationDate, 'MM/DD/YYYY hh:mm:ss A') : '';
            if (i.offset) {
                return (i.localRegistrationDate) ? util.stringDateFormatter(i.localRegistrationDate, i.offset, 'MM/DD/YYYY hh:mm:ss A') : '';
            } else {
                return (i.localRegistrationDate) ? util.stringDateFormatter(i.localRegistrationDate, '+00:00', 'MM/DD/YYYY hh:mm:ss A') : '';
            }
        } else if (i && i.registrationDate) {
            return (i.registrationDate) ? util.changeDateFormat(util.utcToGmt(i.registrationDate), 'MM/DD/YYYY hh:mm:ss A') : '';
        }
        return "";
    },
    WithdrawalDate: function(i) {
        if (i && i.withdrawDate) {
            return (i.withdrawDate) ? util.changeDateFormat(i.withdrawDate, 'MM/DD/YYYY hh:mm:ss A') : '';
        }
        return "";
    },
    localWithdrawalDate: function(i) {
        if (i && i.withdrawDate && i.localWithdrawDate) {
            if (i.offset) {
                return (i.localWithdrawDate) ? util.stringDateFormatter(i.localWithdrawDate, i.offset, 'MM/DD/YYYY hh:mm:ss A') : '';
            } else {
                return (i.localWithdrawDate) ? util.stringDateFormatter(i.localWithdrawDate, '+00:00', 'MM/DD/YYYY hh:mm:ss A') : '';
            }
        } else {
            return (i.withdrawDate) ? util.changeDateFormat(util.getGmtTime(i.withdrawDate), 'MM/DD/YYYY hh:mm:ss A') : '';
        }
        return "";
    },
    offset: function(i) {
        if (i && i.status && (i.status === 'INVITED' || i.status === 'NOTINVITED')) {
            return "";
        } else if (i && i.offset) {
            return i.offset;
        }
        return "+00:00";
    },
    DisqualificationDate: function(i) {
        if (i && i.disqualifyDate) {
            return util.changeDateFormat(i.disqualifyDate, 'MM/DD/YYYY hh:mm:ss A');
        }
        return "";
    },
    DisqualificationReason: function(i) {
        if (i && i.disqualificationReason) {
            return i.disqualificationReason;
        }
        return "";

    },
    country: function(i) {
        if (i && i.country) {
            return i.country;
        }
        return "";

    },
    language: function(i) {
        if (i && i.language) {
            return i.language;
        }
        return "";

    },
    gender: function(i) {
        if (i && i.gender) {
            return i.gender;
        }
        return "";
    },
    ageRange: function(i) {
        if (i && i.ageRange) {
            return i.ageRange;
        }
        return "";
    },
    howDidYouHear: function(i) {
        if (i && i.howDidYouHear) {
            return i.howDidYouHear;
        }
        return "";
    },
    dob: function(i) {
        if (i && i.dob) {
            return i.dob;
        }
        return "";
    },
    app_language_id: function(i) {
        if (i && i.app_language_id) {
            return i.app_language_id;
        }
        return "";
    },
    display_language: function(i) {
        if (i && i.display_language) {
            return i.display_language;
        }
        return "";
    }
});

exports.participantRegistrationCsvMapper = participantRegistrationCsvMapper;