'use strict';

const JM = require('json-mapper');
const util = require('../../util');

const healthResponseCsvMapper = JM.makeConverter({
    responseId: function(i) {
        if (i && i.responseId) {
            return i.responseId;
        }
        return "";
    },
    studyId: function(i) {
        if (i && i.studyId) {
            return i.studyId;
        }
        return "";
    },
    siteId: function(i) {
        if (i && i.siteId) {
            return i.siteId;
        }
        return "";
    },
    siteName: function(i) {
        if (i && i.siteName) {
            return i.siteName;
        }
        return "";
    },
    ThreadParticipantID: function(i) {
        if (i && i.participantId) {
            return i.participantId;
        }
        return "";
    },
    ParticipantID: function(i) {
        if (i && i.userDefinedParticipantId) {
            return i.userDefinedParticipantId;
        }
        return "";
    },
    responseDate: function(i) {
        if (i && i.responseDate) {
            return util.changeDateFormat(i.responseDate, "MM/DD/YYYY");
        }
        return "";
    }
});

exports.healthResponseCsvMapper = healthResponseCsvMapper;