'use strict';

const JM = require('json-mapper');
const util = require('../../util');

const edcResponseCsvMapper = JM.makeConverter({
    responseId: function(i) {
        if (i && i.responseId) {
            return i.responseId;
        }
        return "";
    },
    unscheduledPacketName: function(i) {
        if (i && i.unscheduledPacketName) {
            return i.unscheduledPacketName;
        }
        return "";
    },
    unscheduledPacketId: function(i) {
        if (i && i.unscheduledPacketId) {
            return i.unscheduledPacketId;
        }
        return "";
    },
    studyId: function(i) {
        if (i && i.studyId) {
            return i.studyId;
        }
        return "";
    },
    siteId: function(i) {
        if (i && i.siteId) {
            return i.siteId;
        }
        return "";
    },
    siteName: function(i) {
        if (i && i.SiteName) {
            return i.SiteName;
        }
        return "";
    },
    ThreadParticipantID: function(i) {
        if (i && i.participantId) {
            return i.participantId;
        }
        return "";
    },
    ParticipantID: function(i) {
        if (i && i.userDefinedParticipantId) {
            return i.userDefinedParticipantId;
        }
        return "";
    },
    piId: function(i) {
        if (i && i.piId) {
            return i.piId;
        }
        return "";
    },
    piName: function(i) {
        if (i && i.piName) {
            return i.piName;
        }
        return "";
    },
    formId: function(i) {
        if (i && i.formId) {
            return i.formId;
        }
        return "";
    },
    formName: function(i) {
        if (i && i.formName) {
            return i.formName;
        }
        return "";
    },
    formVersionId: function(i) {
        if (i && i.formVersion) {
            return i.formVersion;
        }
        return "";
    },
    milestone: function(i) {
        if (i && i.milestone) {
            return i.milestone;
        } else if (`${i.milestone}` === `0`) {
            return 0;
        }
        return "";
    },
    questionId: function(i) {
        if (i && i.questionId) {
            return i.questionId;
        }
        return "";
    },
    questionText: function(i) {
        if (i && i.questionBody) {
            return i.questionBody;
        }
        return "";
    },
    answerText: function(i) {
        if (i && i.answerText) {
            return i.answerText;
        }
        return "";
    },
    weight: function(i) {
        if (i && i.weight) {
            return i.weight;
        }
        return "";
    },
    comments: function(i) {
        if (i && i.comments) {
            return i.comments;
        }
        return "";
    },
    submittedTime: function(i) {
        if (i && i.submittedTime) {
            return util.changeDateFormat(util.utcToGmt(i.submittedTime), 'YYYY/MM/DD HH:mm:ss') //i.submittedTime;
        }
        return "";
    },
    completedBy: function(i) {
        if (i && i.completedBy) {
            return i.completedBy;
        }
        return "";
    },
    fieldName: function(i) {
        if (i && i.fieldName) {
            return i.fieldName;
        }
        return "";
    }
});

exports.edcResponseCsvMapper = edcResponseCsvMapper;