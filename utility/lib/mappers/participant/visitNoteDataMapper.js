'use strict';

const JM = require('json-mapper');
const uuid = require('uuidv4');

const visitNoteDataMapper = JM.makeConverter({
    id: function(i) {
        return uuid();
    },
    thread_visit_id: function(i) {
        if (i && i.thread_visit_id !== 'undefined') {
            return i.thread_visit_id;
        }
        return;
    },
    participant_id: function(i) {
        if (i && i.participant_id !== 'undefined') {
            return i.participant_id;
        }
        return;
    },
    reponses: function(i) {
        if (i && i.reponses !== 'undefined') {
            return i.reponses;
        }
        return;
    },
    study_id: function(i) {
        if (i && i.study_id !== 'undefined') {
            return i.study_id;
        }
        return;
    },
    tru_clinic_visit_id: function(i) {
        if (i && i.tru_clinic_visit_id !== 'undefined') {
            return i.tru_clinic_visit_id;
        }
        return;
    },
    created_by: function(i) {
        if (i && i.created_by !== 'undefined') {
            return i.created_by;
        }
        return;
    },
    created_time: function(i) {
        if (i && i.created_time !== 'undefined') {
            return i.created_time;
        }
        return;
    },
    modified_by: function(i) {
        if (i && i.modified_by !== 'undefined') {
            return i.modified_by;
        }
        return;
    },
    modified_time: function(i) {
        if (i && i.modified_time !== 'undefined') {
            return i.modified_time;
        }
        return;
    }
});

exports.visitNoteDataMapper = visitNoteDataMapper;