'use strict';

const JM = require('json-mapper');
const uuid = require('uuidv4');

const participantMapper = JM.makeConverter({
    id: function(i) {
        // if (i && i.data && i.data.id !== 'undefined') {
        //     return i.data.id;
        // }
        return uuid();
    },
    account_deleted: function(i) {
        if (i && i.data && i.data.account_deleted !== 'undefined') {
            return i.data.account_deleted;
        }
        return;
    },
    account_enabled: function(i) {
        if (i && i.data && i.data.account_enabled !== 'undefined') {
            return i.data.account_enabled;
        }
        return;
    },
    account_locked: function(i) {
        if (i && i.data && i.data.account_locked !== 'undefined') {
            return i.data.account_locked;
        }
        return;
    },
    attempts: function(i) {
        if (i && i.data && i.data.attempts !== 'undefined') {
            return i.data.attempts;
        }
        return;
    },
    consent_status: function(i) {
        if (i && i.data && i.data.consent_status !== 'undefined') {
            return i.data.consent_status;
        }
        return;
    },
    created_by: function(i) {
        if (i && i.data && i.data.created_by !== 'undefined') {
            return i.data.created_by;
        }
        return;
    },
    created_time: function(i) {
        if (i && i.data && i.data.created_time !== 'undefined') {
            return i.data.created_time;
        }
        return;
    },
    credentials_non_expired: function(i) {
        if (i && i.data && i.data.credentials_non_expired !== 'undefined') {
            return i.data.credentials_non_expired;
        }
        return;
    },
    email: function(i) {
        if (i && i.data && i.data.email !== 'undefined') {
            return i.data.email;
        }
        return;
    },
    invitation_date: function(i) {
        if (i && i.data && i.data.invitation_date !== 'undefined') {
            return i.data.invitation_date;
        }
        return;
    },
    invite_process: function(i) {
        if (i && i.data && i.data.invite_process !== 'undefined') {
            return i.data.invite_process;
        }
        return;
    },
    modified_by: function(i) {
        if (i && i.data && i.data.modified_by !== 'undefined') {
            return i.data.modified_by;
        }
        return;
    },
    modified_time: function(i) {
        if (i && i.data && i.data.modified_time !== 'undefined') {
            return i.data.modified_time;
        }
        return;
    },
    notes: function(i) {
        if (i && i.data && i.data.notes !== 'undefined') {
            return i.data.notes;
        }
        return;
    },
    otp_verified: function(i) {
        if (i && i.data && i.data.otp_verified !== 'undefined') {
            return i.data.otp_verified;
        }
        return;
    },
    password: function(i) {
        if (i && i.data && i.data.password !== 'undefined') {
            return i.data.password;
        }
        return;
    },
    password_expiry_date: function(i) {
        if (i && i.data && i.data.password_expiry_date !== 'undefined') {
            return i.data.password_expiry_date;
        }
        return;
    },
    profile_image: function(i) {
        if (i && i.data && i.data.profile_image !== 'undefined') {
            return i.data.profile_image;
        }
        return;
    },
    reference_id: function(i) {
        if (i && i.data && i.data.reference_id !== 'undefined') {
            return i.data.reference_id;
        }
        return;
    },
    reference_source: function(i) {
        if (i && i.data && i.data.reference_source !== 'undefined') {
            return i.data.reference_source;
        }
        return;
    },
    registration_date: function(i) {
        if (i && i.data && i.data.registration_date !== 'undefined') {
            return i.data.registration_date;
        }
        return;
    },
    registration_process: function(i) {
        if (i && i.data && i.data.registration_process !== 'undefined') {
            return i.data.registration_process;
        }
        return;
    },
    registration_status: function(i) {
        if (i && i.data && i.data.registration_status !== 'undefined') {
            return i.data.registration_status;
        }
        return;
    },
    role: function(i) {
        if (i && i.data && i.data.role !== 'undefined') {
            return i.data.role;
        }
        return;
    },
    status: function(i) {
        if (i && i.data && i.data.status !== 'undefined') {
            return i.data.status;
        }
        return;
    },
    study_id: function(i) {
        if (i && i.data && i.data.study_id !== 'undefined') {
            return i.data.study_id;
        }
        return;
    },
    study_invite_code: function(i) {
        if (i && i.data && i.data.study_invite_code !== 'undefined') {
            return i.data.study_invite_code;
        }
        return;
    },
    validic_access_token: function(i) {
        if (i && i.data && i.data.validic_access_token !== 'undefined') {
            return i.data.validic_access_token;
        }
        return;
    },
    validic_id: function(i) {
        if (i && i.data && i.data.validic_id !== 'undefined') {
            return i.data.validic_id;
        }
        return;
    },
    my_chart_account_id: function(i) {
        if (i && i.data && i.data.my_chart_account_id !== 'undefined') {
            return i.data.my_chart_account_id;
        }
        return;
    },
    my_chart_account_type: function(i) {
        if (i && i.data && i.data.my_chart_account_type !== 'undefined') {
            return i.data.my_chart_account_type;
        }
        return;
    },
    patient_id: function(i) {
        if (i && i.data && i.data.patient_id !== 'undefined') {
            return i.data.patient_id;
        }
        return;
    },
    patient_id_type: function(i) {
        if (i && i.data && i.data.patient_id_type !== 'undefined') {
            return i.data.patient_id_type;
        }
        return;
    },
    site_id: function(i) {
        if (i && i.data && i.data.site_id !== 'undefined') {
            return i.data.site_id;
        }
        return;
    },
    latest_used_device: function(i) {
        if (i && i.data && i.data.latest_used_device !== 'undefined') {
            return i.data.latest_used_device;
        }
        return;
    },
    disqualify_reason: function(i) {
        if (i && i.data && i.data.disqualify_reason !== 'undefined') {
            return i.data.disqualify_reason;
        }
        return;
    },
    user_defined_participant_id: function(i) {
        if (i && i.data && i.data.user_defined_participant_id !== 'undefined') {
            return i.data.user_defined_participant_id;
        }
        return;
    }
});

exports.participantMapper = participantMapper;