'use strict';

const JM = require('json-mapper');
const uuid = require('uuidv4');

const surveyResponseDataMapper = JM.makeConverter({
    id: function(i) {
        return uuid();
    },
    answer_text: function(i) {
        if (i && i.answer_text !== 'undefined') {
            return i.answer_text;
        }
        return;
    },
    end_time: function(i) {
        if (i && i.end_time !== 'undefined') {
            return i.end_time;
        }
        return;
    },
    is_skipped: function(i) {
        if (i && i.is_skipped !== 'undefined') {
            return i.is_skipped;
        }
        return;
    },
    option_id: function(i) {
        if (i && i.option_id !== 'undefined') {
            return i.option_id;
        }
        return;
    },
    participant_id: function(i) {
        if (i && i.participant_id !== 'undefined') {
            return i.participant_id;
        }
        return;
    },
    question_id: function(i) {
        if (i && i.question_id !== 'undefined') {
            return i.question_id;
        }
        return;
    },
    question_type: function(i) {
        if (i && i.question_type !== 'undefined') {
            return i.question_type;
        }
        return;
    },
    start_time: function(i) {
        if (i && i.start_time !== 'undefined') {
            return i.start_time;
        }
        return;
    },
    status: function(i) {
        if (i && i.status !== 'undefined') {
            return i.status;
        }
        return;
    },
    study_code: function(i) {
        if (i && i.study_code !== 'undefined') {
            return i.study_code;
        }
        return;
    },
    study_id: function(i) {
        if (i && i.study_id !== 'undefined') {
            return i.study_id;
        }
        return;
    },
    survey_id: function(i) {
        if (i && i.survey_id !== 'undefined') {
            return i.survey_id;
        }
        return;
    },
    time_taken: function(i) {
        if (i && i.time_taken !== 'undefined') {
            return i.time_taken;
        }
        return;
    }
});

exports.surveyResponseDataMapper = surveyResponseDataMapper;