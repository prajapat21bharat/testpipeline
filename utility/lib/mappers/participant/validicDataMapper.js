'use strict';

const JM = require('json-mapper');
const uuid = require('uuidv4');

const validicDataMapper = JM.makeConverter({
    id: function(i) {
        return uuid();
    },
    data: function(i) {
        if (i && i.data !== 'undefined') {
            return i.data;
        }
        return;
    },
    data_capture_end_time: function(i) {
        if (i && i.data_capture_end_time !== 'undefined') {
            return i.data_capture_end_time;
        }
        return;
    },
    data_capture_start_time: function(i) {
        if (i && i.data_capture_start_time !== 'undefined') {
            return i.data_capture_start_time;
        }
        return;
    },
    data_object_type: function(i) {
        if (i && i.data_object_type !== 'undefined') {
            return i.data_object_type;
        }
        return;
    },
    participant_id: function(i) {
        if (i && i.participant_id !== 'undefined') {
            return i.participant_id;
        }
        return;
    },
    study_id: function(i) {
        if (i && i.study_id !== 'undefined') {
            return i.study_id;
        }
        return;
    },
    site_id: function(i) {
        if (i && i.site_id !== 'undefined') {
            return i.site_id;
        }
        return;
    }
});

exports.validicDataMapper = validicDataMapper;