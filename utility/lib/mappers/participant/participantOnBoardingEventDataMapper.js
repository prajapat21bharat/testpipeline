'use strict';

const JM = require('json-mapper');
const uuid = require('uuidv4');

const participantOnBoardingEventDataMapper = JM.makeConverter({
    id: function(i) {
        return uuid();
    },
    study_id: function(i) {
        if (i && i.study_id !== 'undefined') {
            return i.study_id;
        }
        return;
    },
    participant_id: function(i) {
        if (i && i.participant_id !== 'undefined') {
            return i.participant_id;
        }
        return;
    },
    registered_time: function(i) {
        if (i && i.registered_time !== 'undefined') {
            return i.registered_time;
        }
        return;
    },
    email_verified_time: function(i) {
        if (i && i.email_verified_time !== 'undefined') {
            return i.email_verified_time;
        }
        return;
    },
    consented_time: function(i) {
        if (i && i.consented_time !== 'undefined') {
            return i.consented_time;
        }
        return;
    }
});

exports.participantOnBoardingEventDataMapper = participantOnBoardingEventDataMapper;