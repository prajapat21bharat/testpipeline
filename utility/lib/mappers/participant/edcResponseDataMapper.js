'use strict';

const JM = require('json-mapper');
const uuid = require('uuidv4');

const edcResponseDataMapper = JM.makeConverter({
    id: function(i) {
        return uuid();
    },
    response_s3_key: function(i) {
        if (i && i.response_s3_key !== 'undefined') {
            return i.response_s3_key;
        }
        return;
    },
    form_id: function(i) {
        if (i && i.form_id !== 'undefined') {
            return i.form_id;
        }
        return;
    },
    form_version_id: function(i) {
        if (i && i.form_version_id !== 'undefined') {
            return i.form_version_id;
        }
        return;
    },
    study_id: function(i) {
        if (i && i.study_id !== 'undefined') {
            return i.study_id;
        }
        return;
    },
    participant_id: function(i) {
        if (i && i.participant_id !== 'undefined') {
            return i.participant_id;
        }
        return;
    },
    edc_response_version_id: function(i) {
        if (i && i.edc_response_version_id !== 'undefined') {
            return i.edc_response_version_id;
        }
        return;
    },
    created_at: function(i) {
        if (i && i.created_at !== 'undefined') {
            return i.created_at;
        }
        return;
    },
    submitted_at: function(i) {
        if (i && i.submitted_at !== 'undefined') {
            return i.submitted_at;
        }
        return;
    },
    state: function(i) {
        if (i && i.state !== 'undefined') {
            return i.state;
        }
        return;
    },
    milestone: function(i) {
        if (i && i.milestone !== 'undefined') {
            return i.milestone;
        }
        return;
    },
    created_by: function(i) {
        if (i && i.created_by !== 'undefined') {
            return i.created_by;
        }
        return;
    }
});

exports.edcResponseDataMapper = edcResponseDataMapper;