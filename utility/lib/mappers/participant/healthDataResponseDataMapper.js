'use strict';

const JM = require('json-mapper');
const uuid = require('uuidv4');

const healthDataResponseDataMapper = JM.makeConverter({
    id: function(i) {
        return uuid();
    },
    study_id: function(i) {
        if (i && i.study_id !== 'undefined') {
            return i.study_id;
        }
        return;
    },
    participant_id: function(i) {
        if (i && i.participant_id !== 'undefined') {
            return i.participant_id;
        }
        return;
    },
    response_date: function(i) {
        if (i && i.response_date !== 'undefined') {
            return i.response_date;
        }
        return;
    },
    response_data: function(i) {
        if (i && i.response_data !== 'undefined') {
            return i.response_data;
        }
        return;
    },
    created_time: function(i) {
        if (i && i.created_time !== 'undefined') {
            return i.created_time;
        }
        return;
    }
});

exports.healthDataResponseDataMapper = healthDataResponseDataMapper;