'use strict';

const JM = require('json-mapper');
const uuid = require('uuidv4');

const participantAppointmentDataMapper = JM.makeConverter({
    id: function(i) {
        return uuid();
    },
    day_of_visit: function(i) {
        if (i && i.day_of_visit !== 'undefined') {
            return i.day_of_visit;
        }
        return;
    },
    is_cancelled: function(i) {
        if (i && i.is_cancelled !== 'undefined') {
            return i.is_cancelled;
        }
        return;
    },
    participant_id: function(i) {
        if (i && i.participant_id !== 'undefined') {
            return i.participant_id;
        }
        return;
    },
    tru_clinic_appointment_time_stamp: function(i) {
        if (i && i.tru_clinic_appointment_time_stamp !== 'undefined') {
            return i.tru_clinic_appointment_time_stamp;
        }
        return;
    },
    tru_clinic_visit_url: function(i) {
        if (i && i.tru_clinic_visit_url !== 'undefined') {
            return i.tru_clinic_visit_url;
        }
        return;
    },
    visit_type: function(i) {
        if (i && i.visit_type !== 'undefined') {
            return i.visit_type;
        }
        return;
    },
    study_id: function(i) {
        if (i && i.study_id !== 'undefined') {
            return i.study_id;
        }
        return;
    },
    tru_clinic_appointment_id: function(i) {
        if (i && i.tru_clinic_appointment_id !== 'undefined') {
            return i.tru_clinic_appointment_id;
        }
        return;
    },
    completion_window: function(i) {
        if (i && i.completion_window !== 'undefined') {
            return i.completion_window;
        }
        return;
    },
    created_time: function(i) {
        if (i && i.created_time !== 'undefined') {
            return i.created_time;
        }
        return;
    },
    modified_time: function(i) {
        if (i && i.modified_time !== 'undefined') {
            return i.modified_time;
        }
        return;
    },
    is_captured: function(i) {
        if (i && i.is_captured !== 'undefined') {
            return i.is_captured;
        }
        return;
    },
    is_pi_initiated: function(i) {
        if (i && i.is_pi_initiated !== 'undefined') {
            return i.is_pi_initiated;
        }
        return;
    }
});

exports.participantAppointmentDataMapper = participantAppointmentDataMapper;