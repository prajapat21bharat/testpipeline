'use strict';

const JM = require('json-mapper');
const uuid = require('uuidv4');

const onBoardingResponseDataMapper = JM.makeConverter({
    id: function(i) {
        return uuid();
    },
    created_time: function(i) {
        if (i && i.created_time !== 'undefined') {
            return i.created_time;
        }
        return;
    },
    device_id: function(i) {
        if (i && i.device_id !== 'undefined') {
            return i.device_id;
        }
        return;
    },
    participant_id: function(i) {
        if (i && i.participant_id !== 'undefined') {
            return i.participant_id;
        }
        return;
    },
    question_answer: function(i) {
        if (i && i.question_answer !== 'undefined') {
            return i.question_answer;
        }
        return;
    },
    question_body: function(i) {
        if (i && i.question_body !== 'undefined') {
            return i.question_body;
        }
        return;
    },
    question_id: function(i) {
        if (i && i.question_id !== 'undefined') {
            return i.question_id;
        }
        return;
    },
    question_type: function(i) {
        if (i && i.question_type !== 'undefined') {
            return i.question_type;
        }
        return;
    },
    study_id: function(i) {
        if (i && i.study_id !== 'undefined') {
            return i.study_id;
        }
        return;
    },
    type: function(i) {
        if (i && i.type !== 'undefined') {
            return i.type;
        }
        return;
    }
});

exports.onBoardingResponseDataMapper = onBoardingResponseDataMapper;