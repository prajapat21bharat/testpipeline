'use strict';

const JM = require('json-mapper');

const studySiteMapper = JM.makeConverter({
    study_id: function(i) {
        if (i && i.data && i.data.study_id !== 'undefined') {
            return i.data.study_id;
        }
        return;
    },
    site_id: function(i) {
        if (i && i.data && i.data.site_id !== 'undefined') {
            return i.data.site_id;
        }
        return;
    },
    is_default: function(i) {
        if (i && i.data && i.data.is_default !== 'undefined') {
            return i.data.is_default;
        }
        return;
    }
});

exports.studySiteMapper = studySiteMapper;