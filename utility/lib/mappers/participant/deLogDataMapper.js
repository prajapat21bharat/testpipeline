'use strict';

const JM = require('json-mapper');
const uuid = require('uuid/v4');
console.log(uuid());
const deLogDataMapper = JM.makeConverter({
    id: function(i) {
        return uuid();
    },
    study_id: function(i) {
        if (i && i.study_id !== 'undefined') {
            return i.study_id;
        }
        return;
    },
    de_sftp_meta_id: function(i) {
        if (i && i.de_sftp_meta_id !== 'undefined') {
            return i.de_sftp_meta_id;
        }
        return;
    },
    trigger_datetime: function(i) {
        if (i && i.trigger_datetime !== 'undefined') {
            return i.trigger_datetime;
        }
        return;
    },
    completed_datetime: function(i) {
        if (i && i.completed_datetime !== 'undefined') {
            return i.completed_datetime;
        }
        return;
    },
    message: function(i) {
        if (i && i.message !== 'undefined') {
            return i.message;
        }
        return;
    },
    status: function(i) {
        if (i && i.status !== 'undefined') {
            return i.status;
        }
        return;
    },
    aws_job_id: function(i) {
        if (i && i.aws_job_id !== 'undefined') {
            return i.aws_job_id;
        }
        return;
    },
    aws_log_path: function(i) {
        if (i && i.aws_log_path !== 'undefined') {
            return i.aws_log_path;
        }
        return;
    },
    type: function(i) {
        if (i && i.type !== 'undefined') {
            return i.type;
        }
        return;
    },
    s3_location: function(i) {
        if (i && i.s3_location !== 'undefined') {
            return i.s3_location;
        }
        return;
    },
    triggered_by: function(i) {
        if (i && i.triggered_by !== 'undefined') {
            return i.triggered_by;
        }
        return;
    }
});

exports.deLogDataMapper = deLogDataMapper;