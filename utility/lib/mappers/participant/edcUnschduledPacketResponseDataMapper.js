'use strict';

const JM = require('json-mapper');
const uuid = require('uuidv4');

const edcUnschduledPacketResponseDataMapper = JM.makeConverter({
    id: function(i) {
        return uuid();
    },
    responseS3Key: function(i) {
        if (i && i.responseS3Key !== 'undefined') {
            return i.responseS3Key;
        }
        return;
    },
    study_id: function(i) {
        if (i && i.study_id !== 'undefined') {
            return i.study_id;
        }
        return;
    },
    participant_id: function(i) {
        if (i && i.participant_id !== 'undefined') {
            return i.participant_id;
        }
        return;
    },
    state: function(i) {
        if (i && i.state !== 'undefined') {
            return i.state;
        }
        return;
    },
    created_at: function(i) {
        if (i && i.created_at !== 'undefined') {
            return i.created_at;
        }
        return;
    },
    submitted_at: function(i) {
        if (i && i.submitted_at !== 'undefined') {
            return i.submitted_at;
        }
        return;
    },
    edc_unsch_pack_id: function(i) {
        if (i && i.edc_unsch_pack_id !== 'undefined') {
            return i.edc_unsch_pack_id;
        }
        return;
    },
    edc_unsch_pack_ver_id: function(i) {
        if (i && i.edc_unsch_pack_ver_id !== 'undefined') {
            return i.edc_unsch_pack_ver_id;
        }
        return;
    },
    edc_unscheduled_packet_response_version_id: function(i) {
        if (i && i.edc_unscheduled_packet_response_version_id !== 'undefined') {
            return i.edc_unscheduled_packet_response_version_id;
        }
        return;
    },
    created_by: function(i) {
        if (i && i.created_by !== 'undefined') {
            return i.created_by;
        }
        return;
    }
});

exports.edcUnschduledPacketResponseDataMapper = edcUnschduledPacketResponseDataMapper;