'use strict';

const JM = require('json-mapper');
const uuid = require('uuidv4');

const validicProfileDataMapper = JM.makeConverter({
    id: function(i) {
        return uuid();
    },
    profile_data: function(i) {
        if (i && i.profile_data !== 'undefined') {
            return i.profile_data;
        }
        return;
    },
    data_capture_time: function(i) {
        if (i && i.data_capture_time !== 'undefined') {
            return i.data_capture_time;
        }
        return;
    },
    participant_id: function(i) {
        if (i && i.participant_id !== 'undefined') {
            return i.participant_id;
        }
        return;
    },
    study_id: function(i) {
        if (i && i.study_id !== 'undefined') {
            return i.study_id;
        }
        return;
    },
    site_id: function(i) {
        if (i && i.site_id !== 'undefined') {
            return i.site_id;
        }
        return;
    }
});

exports.validicProfileDataMapper = validicProfileDataMapper;