'use strict';

const JM = require('json-mapper');
const uuid = require('uuidv4');

const clientConfigMapper = JM.makeConverter({
    id: function(i) {
        return uuid();
    },
    domain_name: function(i) {
        if (i && i.data && i.data.domain_name !== 'undefined') {
            return i.data.domain_name;
        }
        return;
    },
    driver_class_name: function(i) {
        if (i && i.data && i.data.driver_class_name !== 'undefined') {
            return i.data.driver_class_name;
        }
        return;
    },
    initialize: function(i) {
        if (i && i.data && i.data.initialize !== 'undefined') {
            return i.data.initialize;
        }
        return;
    },
    name: function(i) {
        if (i && i.data && i.data.name !== 'undefined') {
            return i.data.name;
        }
        return;
    },
    region: function(i) {
        if (i && i.data && i.data.region !== 'undefined') {
            return i.data.region;
        }
        return;
    },
    s3_bucket: function(i) {
        if (i && i.data && i.data.s3_bucket !== 'undefined') {
            return i.data.s3_bucket;
        }
        return;
    },
    client_type: function(i) {
        if (i && i.data && i.data.client_type !== 'undefined') {
            return i.data.client_type;
        }
        return;
    },
    mobile_analytics_cognito_id: function(i) {
        if (i && i.data && i.data.mobile_analytics_cognito_id !== 'undefined') {
            return i.data.mobile_analytics_cognito_id;
        }
        return;
    },
    mobile_analytics_app_id: function(i) {
        if (i && i.data && i.data.mobile_analytics_app_id !== 'undefined') {
            return i.data.mobile_analytics_app_id;
        }
        return;
    },
    adverse_event_target_emails: function(i) {
        if (i && i.data && i.data.adverse_event_target_emails !== 'undefined') {
            return i.data.adverse_event_target_emails;
        }
        return;
    },
    mobile_schedule_notification_duration: function(i) {
        if (i && i.data && i.data.mobile_schedule_notification_duration !== 'undefined') {
            return i.data.mobile_schedule_notification_duration;
        }
        return;
    },
    // firebase_server_url: function(i) {
    //     if (i && i.data && i.data.firebase_server_url !== 'undefined') {
    //         return i.data.firebase_server_url;
    //     }
    //     return;
    // },
    // firebase_server_key: function(i) {
    //     if (i && i.data && i.data.firebase_server_key !== 'undefined') {
    //         return i.data.firebase_server_key;
    //     }
    //     return;
    // },
    // firebase_service_json: function(i) {
    //     if (i && i.data && i.data.firebase_service_json !== 'undefined') {
    //         return i.data.firebase_service_json;
    //     }
    //     return;
    // },
    dt_transfer_protocol: function(i) {
        if (i && i.data && i.data.dt_transfer_protocol !== 'undefined') {
            return i.data.dt_transfer_protocol;
        }
        return;
    }
});

exports.clientConfigMapper = clientConfigMapper;