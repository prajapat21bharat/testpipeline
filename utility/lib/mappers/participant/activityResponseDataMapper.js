'use strict';

const JM = require('json-mapper');
const uuid = require('uuidv4');

const activityResponseDataMapper = JM.makeConverter({
    id: function(i) {
        return uuid();
    },
    activity_id: function(i) {
        if (i && i.activity_id !== 'undefined') {
            return i.activity_id;
        }
        return;
    },
    activity_name: function(i) {
        if (i && i.activity_name !== 'undefined') {
            return i.activity_name;
        }
        return;
    },
    end_time: function(i) {
        if (i && i.end_time !== 'undefined') {
            return i.end_time;
        }
        return;
    },
    participant_id: function(i) {
        if (i && i.participant_id !== 'undefined') {
            return i.participant_id;
        }
        return;
    },
    response_data: function(i) {
        if (i && i.response_data !== 'undefined') {
            return i.response_data;
        }
        return;
    },
    start_time: function(i) {
        if (i && i.start_time !== 'undefined') {
            return i.start_time;
        }
        return;
    },
    status: function(i) {
        if (i && i.status !== 'undefined') {
            return i.status;
        }
        return;
    },
    study_code: function(i) {
        if (i && i.study_code !== 'undefined') {
            return i.study_code;
        }
        return;
    },
    study_id: function(i) {
        if (i && i.study_id !== 'undefined') {
            return i.study_id;
        }
        return;
    },
    time_taken: function(i) {
        if (i && i.time_taken !== 'undefined') {
            return i.time_taken;
        }
        return;
    },
    activity_key: function(i) {
        if (i && i.activity_key !== 'undefined') {
            return i.activity_key;
        }
        return;
    }
});

exports.activityResponseDataMapper = activityResponseDataMapper;