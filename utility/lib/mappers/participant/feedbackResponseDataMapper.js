'use strict';

const JM = require('json-mapper');
const uuid = require('uuidv4');

const feedbackResponseDataMapper = JM.makeConverter({
    id: function(i) {
        return uuid();
    },
    answer_text: function(i) {
        if (i && i.answer_text !== 'undefined') {
            return i.answer_text;
        }
        return;
    },
    participant_id: function(i) {
        if (i && i.participant_id !== 'undefined') {
            return i.participant_id;
        }
        return;
    },
    question_id: function(i) {
        if (i && i.question_id !== 'undefined') {
            return i.question_id;
        }
        return;
    },
    study_id: function(i) {
        if (i && i.study_id !== 'undefined') {
            return i.study_id;
        }
        return;
    },
    response_time: function(i) {
        if (i && i.response_time !== 'undefined') {
            return i.response_time;
        }
        return;
    },
    question_type: function(i) {
        if (i && i.question_type !== 'undefined') {
            return i.question_type;
        }
        return;
    }
});

exports.feedbackResponseDataMapper = feedbackResponseDataMapper;