'use strict';
const { activityResponseDataMapper } = require('./participant/activityResponseDataMapper');
const { deLogDataMapper } = require('./participant/deLogDataMapper');
const { edcResponseDataMapper } = require('./participant/edcResponseDataMapper');
const { edcUnschduledPacketResponseDataMapper } = require('./participant/edcUnschduledPacketResponseDataMapper');
const { feedbackResponseDataMapper } = require('./participant/feedbackResponseDataMapper');
const { healthDataResponseDataMapper } = require('./participant/healthDataResponseDataMapper');
const { onBoardingResponseDataMapper } = require('./participant/onBoardingResponseDataMapper');
const { participantAppointmentDataMapper } = require('./participant/participantAppointmentDataMapper');
const { participantOnBoardingEventDataMapper } = require('./participant/participantOnBoardingEventDataMapper');
const { surveyResponseDataMapper } = require('./participant/surveyResponseDataMapper');
const { validicDataMapper } = require('./participant/validicDataMapper');
const { validicProfileDataMapper } = require('./participant/validicProfileDataMapper');
const { visitNoteDataMapper } = require('./participant/visitNoteDataMapper');

exports.map = {
    activityResponseDataMapper,
	deLogDataMapper,
    edcResponseDataMapper,
    edcUnschduledPacketResponseDataMapper,
    feedbackResponseDataMapper,
    healthDataResponseDataMapper,
    onBoardingResponseDataMapper,
    participantAppointmentDataMapper,
    participantOnBoardingEventDataMapper,
    surveyResponseDataMapper,
    validicDataMapper,
    validicProfileDataMapper,
    visitNoteDataMapper
};