'use strict';

// const AWS = require('aws-sdk');

const awsXRay = require('aws-xray-sdk');
const AWS = awsXRay.captureAWS(require('aws-sdk'));

const s3 = new AWS.S3();
const client_secrets_manager = new AWS.SecretsManager();
const fs = require('fs');


async function getSignedUrl(file, bucketName, timeInMins, clientId) {
    try {
        let signedUrlExpireSeconds;
        // await updateAwsConfig(clientId);
        AWS.config.update({
            region: process.env.REGION //awsregion
        });
        if (timeInMins) {
            signedUrlExpireSeconds = timeInMins * 1000 * 60;
            console.log('timeInMins', file, signedUrlExpireSeconds);
        } else {
            signedUrlExpireSeconds = 24 * 60 * 1000 * 60; // 24 hours
        }

        var params = { Bucket: bucketName, Key: file, Expires: Number(signedUrlExpireSeconds) };
        var url = s3.getSignedUrl('getObject', params);
        return url;
    } catch (err) {

    }
}

async function getAwsSecreteValue(clientId, parameter) {
    const environment = process.env.ENVIRONMENT.replace(/^\w/, c => c.toUpperCase());

    const params = {
        SecretId: (clientId) ? `${environment}/${clientId}/${parameter}` : `${environment}/${parameter}`, // name of secrete
        // VersionStage: 'AWSPENDING',  //optional
    };
    console.log('params', params);
    const data = await client_secrets_manager.getSecretValue(params).promise();
    const secretString = JSON.parse(data['SecretString']);

    console.log('RETRIEVED PENDING SECRET');
    return secretString;
}

async function checkBucketExists(bucket) {
    const options = {
        Bucket: bucket,
    };
    try {
        const bucketHead = await s3.headBucket(options).promise();
        return true;
    } catch (error) {
        if (error.statusCode === 404) {
            return false;
        }
        console.log('bucketHead error', error);
        throw error;
    }
};

async function doesObjectExists(keyPath, s3BucketName, clientId) {
    try {
        AWS.config.update({
            region: process.env.REGION //awsregion
        });
        const params = {
            Bucket: s3BucketName,
            Key: keyPath //if any sub folder-> path/of/the/folder.ext
        }
        const headCode = await s3.headObject(params).promise();
        if (headCode) {
            return true;
        }
    } catch (headErr) {
        if (headErr.code === 'NotFound') {
            // Handle no object on cloud here
            return false;
        }
    }
}

async function readS3Object(keyPath, s3BucketName) {
    try {
        AWS.config.update({
            region: process.env.REGION
        });
        const getParams = {
            Bucket: s3BucketName,
            Key: keyPath
        };
        console.log("getParams", getParams);
        const headCode = await s3.headObject(getParams).promise();
        if (headCode) {
            //Fetch or read data from aws s3
            const s3BinaryData = await s3.getObject(getParams).promise();

            var fileContents = s3BinaryData.Body.toString();
            var json = JSON.parse(fileContents);
            // console.log(json.schedules);

            // let buff = new Buffer(s3BinaryData.Body, 'base64');
            // const s3Data = buff.toString();
            return json;
        } else {
            return false;
        }
    } catch (error) {
        if (error.code === 'NotFound') {
            // Handle no object on cloud here
            return false;
        }
        console.log(error);
    }
}


async function getAwsSecreteByKey(secreteKey) {
    const environment = process.env.ENVIRONMENT.replace(/^\w/, c => c.toUpperCase());

    const params = {
        SecretId: secreteKey
    };
    console.log('params', params);
    const data = await client_secrets_manager.getSecretValue(params).promise();
    const secretString = JSON.parse(data['SecretString']);

    console.log('RETRIEVED PENDING SECRET');
    return secretString;
}

async function downloadMultipleS3Files(bucketName, studyId, executionId) {
    console.log('downloadMultipleS3Files called');
    return new Promise((resolve, reject) => {
        var AdmZip = require('adm-zip');
        var params = {
            Bucket: bucketName,
            Delimiter: '/',
            Prefix: `studies/${studyId}/tmp/${studyId}_${executionId}/`
                // MaxKeys: 2
        };
        console.log('Listing object', params);
        s3.listObjects(params, async function(err, data) {
            if (err) {
                console.log('!!!!!!!!!!!!!!!!!!!!!!');
                console.log(err, err.stack); // an error occurred
            } else {
                console.log('list object done', data);
                if (data && data.Contents && data.Contents.length > 0) {
                    console.log('file found on S3');
                    const s3Items = data.Contents;
                    let count = 1;
                    s3Items.forEach(async(s3Item) => {
                        console.log('inside items.');
                        if (!fs.existsSync(`/tmp/${studyId}_${executionId}/`)) {
                            fs.mkdirSync(`/tmp/${studyId}_${executionId}/`);
                        }
                        const key = s3Item.Key;
                        params.Key = key;
                        const fileName = key.split('/').pop();
                        console.log('fileName', fileName); // successful response
                        const options = {
                            Bucket: bucketName,
                            Key: key
                        };
                        var zip = new AdmZip();
                        let file = zip.writeZip(`/tmp/${studyId}_${executionId}/${fileName}`);
                        console.log('checking binary files');
                        const s3BinaryData = await s3.getObject(options).promise();
                        let buff = new Buffer.from(s3BinaryData.Body, 'base64');
                        zip = new AdmZip(buff);
                        file = zip.writeZip(`/tmp/${studyId}_${executionId}/${fileName}`);
                        if (count === data.Contents.length) {
                            resolve(true);
                        }
                        count++;
                    });
                } else {
                    console.log('no data available');
                }
            }
        });
    })
}

async function deleteFolder(s3BucketName, folderName) {
    console.log('=== deleteFolder called ===', s3BucketName, folderName);
    var params = {
        Bucket: s3BucketName,
        Prefix: folderName
    };

    s3.listObjects(params, function(err, data) {
        console.log(`Listing Objects in Folder`);
        if (err) {
            console.log('Delete bucket folder list error', err);
        }

        if (data.Contents.length == 0) {
            console.log(`${folderName} Folder is empty`);
        }

        params = { Bucket: s3BucketName };
        params.Delete = { Objects: [] };

        data.Contents.forEach(function(content) {
            params.Delete.Objects.push({ Key: content.Key });
        });

        s3.deleteObjects(params, function(err, data) {
            if (err) {
                console.log('Erro occured in delete s3 folder', err);
            } else {
                console.log(`Folder ${folderName} Deleted Successfully`);
            }
        });
    });
}

async function uploadFile(s3BucketName, fileName, fileContent, type) {
    return new Promise(async(resolve, rejct) => {
        const params = { Bucket: s3BucketName, Key: fileName, Body: fileContent };
        if (type) {
            params.ContentType = type;
        }
        // s3.putObject(params, function(err, data) {
        //     if (err) {
        //         console.log('Unable to Save file on S3', err);
        //         rejct(false);
        //     } else {
        //         console.log(`Successfully uploaded data to S3`);
        //         resolve(true);
        //     }
        // });
        try {
            const stored = await s3.upload(params).promise();
            console.log(`Successfully uploaded data to S3`);
            resolve(true);
        } catch (err) {
            console.log('Unable to Save file on S3', err);
            rejct(false);
        }
    });
}

async function uploadFileWithSigenedUrl(s3BucketName, fileName, fileContent, type) {
    return new Promise((resolve, rejct) => {
        const params = { Bucket: s3BucketName, Key: fileName, Body: fileContent, Expires: 604800 };
        if (type) {
            params.ContentType = type;
        }
        s3.createPresignedPost(params, function(err, data) {
            if (err) {
                console.log('Unable to Save file on S3', err);
                rejct(false);
            } else {
                console.log(`Successfully uploaded data to S3`);
                resolve(true);
            }
        });
    });
}

async function sftpUpload(clientId, pathToFile, fileName, secreteKey) {
    return new Promise(async(resolve, reject) => {
        try {
            let secrete;
            if (secreteKey) {
                secrete = await getAwsSecreteByKey(secreteKey);
                if (secrete instanceof Error) {
                    console.log('Unable to fetch SFTP Details', secrete);
                    reject(secrete);
                }
            }
            if (secrete && secrete.host && secrete.port && secrete.username && secrete.password) {
                console.log('connecting sftp client', secreteKey);
                let binaryFile = fs.createReadStream(`${pathToFile}`);
                let remote = `${secrete.transferPath}`;

                var Client = require('ssh2').Client;
                var connSettings = {
                    host: secrete.host,
                    port: secrete.port,
                    username: secrete.username,
                    password: secrete.password,
                };

                var conn = new Client();

                conn.on('error', function(err) {
                    console.log('SSH - Connection Error: ', err);
                    reject(err);
                });

                conn.on('end', function() {
                    console.log('SSH - Connection Closed');
                });

                conn.on('ready', function() {
                    // code to work with SSH
                    conn.sftp(function(err, sftp) {
                        if (err) {
                            console.log('error ocuured in sftp', err);
                            reject(err);
                        } else {
                            console.log('0 Error we can proceed');
                            var readStream = fs.createReadStream(pathToFile); // "path-to-local-file.txt"
                            var writeStream = sftp.createWriteStream(`${remote}/${fileName}`); //"path-to-remote-file.txt"

                            writeStream.on('close', function() {
                                console.log("- file transferred succesfully");
                                resolve(true);
                            });

                            writeStream.on('end', function() {
                                console.log("sftp connection closed");
                                conn.close();
                                resolve(true);
                            });
                            // initiate transfer of file
                            readStream.pipe(writeStream);
                        }
                    });

                });
                conn.connect(connSettings);
            } else {
                console.log('unable to upload file on sftp. sftp details not found', secreteKey);
                reject('unable to upload file on sftp. sftp details not found');
            }
        } catch (error) {
            console.log('Error occured while fetching sftp details', error);
            reject(error);
        }
    });
}

async function getSsmValueByKey(key, clientId) {
    return new Promise(function(resolve, reject) {
        const ssm = new AWS.SSM({ apiVersion: '2014-11-06', region: process.env.REGION });
        AWS.config = {
            region: process.env.REGION
        };
        var params = {
            Name: `${key}`,
            WithDecryption: true
        };
        ssm.getParameter(params, function(err, data) {
            if (err) {
                console.log('Error occured while fetching SSM Parameter', err);
                reject(err);
            }
            if (data) {
                resolve(data);
            }
        });
    });
}

exports.getSignedUrl = getSignedUrl;
exports.getAwsSecreteValue = getAwsSecreteValue;
exports.checkBucketExists = checkBucketExists;
exports.doesObjectExists = doesObjectExists;
exports.readS3Object = readS3Object;
exports.downloadMultipleS3Files = downloadMultipleS3Files;
exports.deleteFolder = deleteFolder;
exports.uploadFile = uploadFile;
exports.uploadFileWithSigenedUrl = uploadFileWithSigenedUrl;
exports.getAwsSecreteByKey = getAwsSecreteByKey;
exports.sftpUpload = sftpUpload;
exports.getSsmValueByKey = getSsmValueByKey;