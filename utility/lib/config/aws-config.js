'use strict';

const { getAwsParameters } = require('./../aws-parameter');
const awsCredentials = {};

function getAWSConfig(clientId) {
    const paramNames = ['pooldata'];

    return getAwsParameters(paramNames, 'thread', clientId)
        .then((paramObject) => {
            const poolData = paramObject.pooldata.split(",");

            awsCredentials.AWS_REGION = poolData[0]; //awsregion
            awsCredentials.USER_POOL_ID = poolData[1]; //poolid
            awsCredentials.USER_POOL_CLIENT_ID = poolData[2]; //poolclientid
            awsCredentials.USER_POOL_ARN = poolData[3]; //poolarn
            return awsCredentials;
        })
        .catch((err) => {
            throw err;
        });
}

exports.getAWSConfig = getAWSConfig;