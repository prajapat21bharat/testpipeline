'use strict'

const moment = require('moment-timezone');

const nanoid = require('nanoid');
const generate = require('nanoid/generate');

const jwtDecode = require('jwt-decode');

function changeDateFormat(date, format = 'YYYY-MM-DD HH:mm:ss A') {
    return moment(date).format(format);
}

function changeDateToIsoFormat(date) {
    return moment(date).toISOString();
}

function changeDateToUtc(date) {
    return moment.utc(date).valueOf();
}

function convertToEpoch(date) {
    return moment(date).unix();
}

function getLocalDate(date, offset, format = "YYYY-MM-DD HH:mm:ss") {
    let operator = offset[0];
    let offsetObj = offset.split(":");
    let hours = offsetObj[0].replace(/[+-]/g, "");
    let minutes = offsetObj[1];
    let duration = moment.duration({ 'hours': hours, 'minutes': minutes, 'seconds': 0 });
    let localDate;
    if (operator === '+')
        localDate = moment(date).add(duration);
    else
        localDate = moment(date).subtract(duration);

    return localDate.format(format);
}

function dateDifferenceInDays(startDate, endDate) {
    return new Promise((resolve, reject) => {
        const start = new Date(startDate);
        const end = new Date(endDate);

        const diffTime = Math.abs(end.getTime() - start.getTime());
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        console.log(diffDays); // 155
        resolve(diffDays);
    });
}

function getDates(start, end) {
    var start = moment(start)
    var end = moment(end);

    var list = [];

    for (var current = start; current <= end; current.add(28, 'd')) {
        list.push(current.format("YYYY-MM-DD"))
    }
    return list;
}

function getAccountIdFromArn(arn) {
    let accountId;
    const arnArray = arn.split(":");
    if (arnArray && arnArray.length > 0 && arnArray[4]) {
        accountId = arnArray[4];
    }
    return accountId;
}

function generateMixedUniqueCode() {
    return nanoid(6);
}

function generateUniqueCode() {
    // syntax:- param-1: leters, number, symbol param-2: length of code
    return generate('1234567890abcdefghijklmnoprstuvwxyz', 6);
}

function decodeJwtToken(token) {
    let decodedToken;
    decodedToken = jwtDecode(token);
    console.log(decodedToken);
    return decodedToken
}

function getKeys(data) {
    const headers = [];
    Object.keys(data).forEach(item => {
        headers.push({ id: item, title: item })
    });
    return headers;
}

function utcToGmt(dateTime, format = 'YYYY-MM-DD HH:mm:ss') {
    var localDate = new Date(dateTime);
    return moment.utc(localDate).format(format);
}

function getGmtTime(inputDateTime) {
    const date = new Date(inputDateTime);
    const gmtDate = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
    return changeDateFormat(gmtDate);
}

function getMetaKeyValues(inputData) {
    let keyPair = {};
    keyPair.key = inputData.field_name;
    keyPair.value = inputData.field_value;
    return keyPair;
}

function getISO8601(dateTime) {
    // ISO 8601 time zone // dateTime should be in format(2019-11-02T08:56:22.480Z)
    return moment(new Date(dateTime)).utc().format("YYYY-MM-DDTHH:mm:ss.SSSZZ")
}

function getExcelKeys(data) {
    const headers = [];
    Object.keys(data).forEach(item => {
        headers.push({ header: item, key: item })
    });
    return headers;
}

function replaceChars(data, replacedWith) {
    return data.replace(/ /g, replacedWith);
}

function getTimeDifference(startTime, endTime) {
    var diff = moment.duration(moment(endTime).diff(moment(startTime)));
    var minutes = parseInt(diff.asMinutes());
    return minutes;
}

function getActivityTypes(activityEnum, task) {
    for (let prop in activityEnum) {
        if (object.hasOwnProperty(prop)) {
            if (object[prop] === task)
                return prop;
            else
                return "";
        }
    }
}

function convertTime12to24(time) {
    var hours = Number(time.match(/^(\d+)/)[1]);
    var minutes = Number(time.match(/:(\d+)/)[1]);
    var AMPM = time.match(/\s(.*)$/)[1];
    if (AMPM == "PM" && hours < 12) hours = hours + 12;
    if (AMPM == "AM" && hours == 12) hours = hours - 12;
    var sHours = hours.toString();
    var sMinutes = minutes.toString();
    if (hours < 10) sHours = "0" + sHours;
    if (minutes < 10) sMinutes = "0" + sMinutes;
    var timestring = `${sHours}H${sMinutes}M`
    return timestring;
}

function includeNotNull(obj) {
    for (var propName in obj) {
        if (obj[propName] === null || obj[propName] === undefined) {
            delete obj[propName];
        }
    }
    return obj;
}

function removeLastCommafromString(str) {
    str = str.replace(/,\s*$/, "");
    return str;
}

function toTitleCase(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);;
}

function createLog(event, message, data) {
    let output = {
        event,
        environment: process.env.ENVIRONMENT,
        message,
        info: data && data.stack ? data.stack : data && data.message ? data.message : data
    }
    console.log(JSON.stringify(output));
}

function getExecutionIdFromArn(str) {
    var arnArray = str.split(":");
    return arnArray[arnArray.length - 1]
}

function removeObjectByAttr(arr, attr, value) {
    var i = arr.length;
    while (i--) {
        if (arr[i] &&
            arr[i].hasOwnProperty(attr) &&
            (arguments.length > 2 && arr[i][attr] === value)) {
            arr.splice(i, 1);
        }
    }
    return arr;
}

function removeSpecialChars(string, regex) {
    var nonUft8String = string.replace(/[^a-zA-Z_0-9-]/g, "");
    // return nonUft8String.replace(/__/g, "_–_");
    return nonUft8String;
}

function createSurveyFolderName(string, regex) {
    let nameWithoudSpace = string.replace(/ /g, '_');
    var nonUft8String = nameWithoudSpace.replace(/[^a-zA-Z_0-9-–]/g, "");
    // return nonUft8String.replace(/__/g, "_–_");
    return nonUft8String;
}

function stringDateFormatter(datestr, offset, format = 'MM/DD/YYYY HH:mm') {
    var formattedDate = moment.utc(datestr).utcOffset(offset).format(format);
    return formattedDate;
}

function removeBackSlashes(string) {
    return string.replace(/\/$/, "");
}

exports.changeDateFormat = changeDateFormat;
exports.changeDateToIsoFormat = changeDateToIsoFormat;
exports.changeDateToUtc = changeDateToUtc;
exports.convertToEpoch = convertToEpoch;
exports.dateDifferenceInDays = dateDifferenceInDays;
exports.getDates = getDates;
exports.getAccountIdFromArn = getAccountIdFromArn;
exports.generateMixedUniqueCode = generateMixedUniqueCode;
exports.generateUniqueCode = generateUniqueCode;
exports.decodeJwtToken = decodeJwtToken;
exports.getKeys = getKeys;
exports.replaceChars = replaceChars;
exports.utcToGmt = utcToGmt;
exports.getGmtTime = getGmtTime;
exports.getMetaKeyValues = getMetaKeyValues;
exports.getISO8601 = getISO8601;
exports.getExcelKeys = getExcelKeys;
exports.getTimeDifference = getTimeDifference;
exports.getActivityTypes = getActivityTypes;
exports.convertTime12to24 = convertTime12to24;
exports.includeNotNull = includeNotNull;
exports.removeLastCommafromString = removeLastCommafromString;
exports.toTitleCase = toTitleCase;
exports.createLog = createLog;
exports.getExecutionIdFromArn = getExecutionIdFromArn;
exports.removeObjectByAttr = removeObjectByAttr;
exports.removeSpecialChars = removeSpecialChars;
exports.createSurveyFolderName = createSurveyFolderName;
exports.getLocalDate = getLocalDate;
exports.stringDateFormatter = stringDateFormatter;
exports.removeBackSlashes = removeBackSlashes;