'use strict';

// const AWS = require('aws-sdk');

const awsXRay = require('aws-xray-sdk');
const AWS = awsXRay.captureAWS(require('aws-sdk'));

//const { uuid } = require('uuidv4');
const stepfunctions = new AWS.StepFunctions({ region: process.env.REGION });
var sqs = new AWS.SQS({ region: process.env.REGION });
var lambda = new AWS.Lambda({ region: process.env.REGION });

const _ = require('lodash');

async function createStateMachine(machineName, roleArn) {
    return new Promise((resolve, reject) => {
        console.log('inside create step function');
        var params = {
            definition: `{
                "Comment": "Step functions which triggers outside processing to fetch data from validic",
                "StartAt": "TaskLambda",
                "States": {
                    "TaskLambda": {
                        "Type": "Task",
                        "Resource": "${roleArn}",
                        "End": true
                    }
                }
            }`,
            /* required */
            name: machineName,
            /* required */
            roleArn: process.env.ROLE_ARN,
            /* required */
        };

        stepfunctions.createStateMachine(params).promise().then((result) => {
            console.log('create step function executed successfully');
            console.log('create step function result', result);
            resolve(result);
        }).catch((error) => {
            console.log('error occured in start Execution', error);
            reject(error); // Bypass error for next call
        });
    });
}

async function executeStepFunction(params) {
    return new Promise((resolve, reject) => {
        console.log('inside step function execute');
        stepfunctions.startExecution(params).promise().then((result) => {
            console.log('Step function executed successfully');
            console.log('Step function result', result);
            const response = {
                statusCode: 200,
                body: JSON.stringify({
                    message: 'Step function worked',
                    result: result
                }),
                title: "Thread-Jobs"
            };
            // resolve(true);
            resolve(response);
        }).catch((error) => {
            console.log('error occured in start Execution', error);
            reject(error); // Bypass error for next call
        });
    });
}

async function triggerStepFunction(inputParams, machineName,executionId) {
    return new Promise((resolve, reject) => {
        console.log('callStepFunction');
        console.log('Fetching the list of available State Machines');

        stepfunctions
            .listStateMachines({})
            .promise()
            .then(async(listStateMachines) => {
                console.log('Searching for the step function', listStateMachines);

                const stateMachines = listStateMachines.stateMachines;
                const stateMachineName = _.result(
                    _.find(stateMachines, function(i) {
                        return i.name === machineName;
                    }),
                    'name'
                );
                const stateMachineArn = _.result(
                    _.find(stateMachines, function(i) {
                        return i.name === machineName;
                    }),
                    'stateMachineArn'
                );
                console.log('stateMachineName', stateMachineName, 'stateMachineArn', stateMachineArn);
                if (stateMachineName && stateMachineArn) {
                    // let executionId = uuid();
                    console.log('executionId', executionId);

                    var params = {
                        stateMachineArn: stateMachineArn,
                        input: JSON.stringify(inputParams),
                        name: executionId
                    };

                    console.log('Start execution');
                    console.log('Starting Step execution');
                    const stepResult = await executeStepFunction(params);
                    // return stepResult;
                    resolve(stepResult);
                } else {
                    console.log('State machine not found');
                    // return false;
                    reject('State machine not found');
                }
            })
            .catch((error) => {
                console.log('Error occured in step function', error);
                // return false;
                reject(error);
            });
    })

}

async function sqsCreateQueue(queueName) {
    return new Promise((resolve, reject) => {
        console.log('sqs create queue called');
        var params = {
            QueueName: queueName,
            Attributes: {
                // 'DelaySeconds': '60',
                // 'MessageRetentionPeriod': '43200',
                'VisibilityTimeout': '43200'
            }
        };

        sqs.createQueue(params, function(err, data) {
            if (err) {
                console.log("Error", err);
                reject(err);
            } else {
                console.log("Successfully created Queue");
                // console.log("Success", data.QueueUrl);
                resolve(data);
            }
        });
    });
}

async function sqsGetQueueUrl(queueName, accountId) {
    return new Promise((resolve, reject) => {
        console.log('sqs send message called');
        var params = {
            QueueName: queueName,
            /* required */
            QueueOwnerAWSAccountId: accountId
        };
        sqs.getQueueUrl(params, function(err, data) {
            if (err) {
                console.log(err, err.stack); // an error occurred
                reject(err);
            } else {
                console.log('getQueue url successfull', data); // successful response
                resolve(data);
            }
        });
    });
}

async function sqsSendMessage(params) {
    return new Promise((resolve, reject) => {
        console.log('sqs send message called');
        sqs.sendMessage(params, function(err, data) {
            if (err) {
                console.log('error:', 'Fail Send Message' + err);

                reject(err);
            } else {
                // console.log('data:', data.MessageId);
                console.log("SQS Send Message Successfull");
                resolve(data);
            }
        });
    });
}

async function getLambdaDetails(lambdaName) {
    return new Promise((resolve, reject) => {
        console.log('get Lambda Details called');
        var params = {
            FunctionName: `${lambdaName}`
                // Qualifier: "1"
        };
        lambda.getFunction(params, function(err, data) {
            if (err) {
                console.log('error occured in get lambda details', err, err.stack); // an error occurred
                reject(err);
            } else {
                console.log('lamda result');
                resolve(data);
            }
        });
    });
}

async function listEventSourceMappings(eventArn, lambdaName) {
    return new Promise((resolve, reject) => {
        console.log('list Event Source Mapping called');
        var params = {
            EventSourceArn: eventArn,
            FunctionName: lambdaName,
        };
        lambda.listEventSourceMappings(params, function(err, data) {
            if (err) {
                console.log('error occured in get lambda events source', err, err.stack); // an error occurred
                reject(err);
            } else {
                console.log('lamda result');
                resolve(data);
            }
        });
    });
}

async function createEventSourceMapping(lambdaArn, lambdaFunction) {
    return new Promise((resolve, reject) => {
        console.log('create Event Source Mapping called');
        var params = {
            EventSourceArn: `${lambdaArn}`,
            /* required */
            FunctionName: lambdaFunction,
            /* required */
            BatchSize: '10',
            Enabled: true
        };
        lambda.createEventSourceMapping(params, function(err, data) {
            if (err) {
                console.log('Error occured in lambda add events ', err, err.stack); // an error occurred
                reject(err);
            } else {
                console.log('create Event Source Mapping successfull'); // successful response
                resolve(data);
            }
        });
    });
}

async function updateEventSourceMapping(uuid, lambdaFunction) {
    return new Promise((resolve, reject) => {
        console.log('Update Event Source Mapping called');
        var params = {
            UUID: uuid,
            /* required */
            FunctionName: lambdaFunction,
            Enabled: true
        };
        lambda.updateEventSourceMapping(params, function(err, data) {
            if (err) {
                console.log('Error occured in lambda Update events ', err, err.stack); // an error occurred
                reject(err);
            } else {
                console.log('Update Event Source Mapping successfull'); // successful response
                resolve(data);
            }
        });
    });
}

exports.createStateMachine = createStateMachine;
exports.triggerStepFunction = triggerStepFunction;
exports.sqsSendMessage = sqsSendMessage;
exports.sqsCreateQueue = sqsCreateQueue;
exports.sqsGetQueueUrl = sqsGetQueueUrl;
exports.getLambdaDetails = getLambdaDetails;
exports.listEventSourceMappings = listEventSourceMappings;
exports.createEventSourceMapping = createEventSourceMapping;
exports.updateEventSourceMapping = updateEventSourceMapping;