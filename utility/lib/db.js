'use strict';

const { connection } = require('./connection');
const _ = require('lodash');

const { getActivityDataModel } = require('./models/activity');
const { getActivityResponseDataModel } = require('./models/reponse/activityResponse');
const { getClientConfigModel } = require('./models/clientConfig');
const { getCohortModel } = require('./models/cohort');
const { getConsentConfigurationModel } = require('./models/consentConfiguration');
const { getDeLogDataModel } = require('./models/deLog');
const { getEdcResponseDataModel } = require('./models/edcResponse');
const { getEmailTemplateDataModel } = require('./models/emailTemplate');
const { getEdcUnschduledPacketResponseDataModel } = require('./models/edcUnschduledPacketResponse');
const { getFeedbackResponseDataModel } = require('./models/reponse/feedbackResponse');
const { getHealthDataResponseDataModel } = require('./models/healthDataResponse');
const { getOnBoardingResponseDataModel } = require('./models/onBoardingResponse');
const { getParticipantModel } = require('./models/participant');
const { getParticipantAppointmentDataModel } = require('./models/participantAppointment');
const { getPiParticipantAppointmentDataModel } = require('./models/piParticipantAppointment');
const { getParticipantMetaModel } = require('./models/participantMeta');
const { getParticipantOnBoardingEventDataModel } = require('./models/participantOnBoardingEvent');
const { getParticipantOnBoardingConsentEventDataModel } = require('./models/ParticipantOnBoardingConsentEvent');
const { getParticipantStatusHistoryModel } = require('./models/participantStatusHistory');
const { getSiteModel } = require('./models/site');
const { getStudyMetaDataModel } = require('./models/studyMetaData');
const { getStudySiteModel } = require('./models/studySite');
const { getSurveyResponseDataModel } = require('./models/reponse/surveyResponse');
const { getSurveyResponseTrackerModel } = require('./models/reponse/surveyResponseTracker');
const { getVisitNoteDataModel } = require('./models/reponse/visitNote');
const { getUserDataModel } = require('./models/userData');
const { getTelehealthSessionDataModel } = require('./models/telehealthSessionData');
const { getStudyFeatureModel } = require('./models/studyFeature');
const { getClientFeatureModel } = require('./models/clientFeature');
const { getEdcResponseVersionDataModel } = require('./models/edcResponseVersion');
const { getEdcUnschduledPacketResponseVersionDataModel } = require('./models/edcUnschduledPacketResponseVersion');
const { getEdcUnscheduledPacketDataModel } = require('./models/edcUnscheduledPacket');
const { getSequelize } = require('./models/sequelize');
const { getValidicDataModel } = require('./models/validicData');
const { getValidicProfileDataModel } = require('./models/validicProfileData');
const { getDeSftpMetaModel } = require('./models/deSftpMeta');
const { getLanguageModel } = require('./models/language');
const { getParticipantLanguageModel } = require('./models/participantLanguage');

let models = {};

class DB {
    async init(clientId, dbName) {
        console.log('init called in db.js', clientId);
        const sequelizeConnection = await connection(clientId, dbName);

        console.log('sequelizeConnection called in db.js');
        return sequelizeConnection
            .authenticate()
            .then(() => {
                console.log('sequelizeConnection inside');
                // Connection has been established successfully
                this.initModels(sequelizeConnection);
                this.sequelizeConnection = sequelizeConnection;
                return this.getModels();
            })
            .catch((err) => {
                // Unable to connect to the database
                console.log('sequelizeConnection authenticate error', err);
                Promise.reject(err);
            });
    }

    initModels(conn) {
        models = {
            Activity: getActivityDataModel(conn),
            ActivityResponse: getActivityResponseDataModel(conn),
            ClientConfig: getClientConfigModel(conn),
            Cohort: getCohortModel(conn),
            ConsentConfiguration: getConsentConfigurationModel(conn),
            DeLog: getDeLogDataModel(conn),
            EdcResponse: getEdcResponseDataModel(conn),
            EmailTemplate: getEmailTemplateDataModel(conn),
            EdcUnschduledPacketResponse: getEdcUnschduledPacketResponseDataModel(conn),
            FeedbackResponse: getFeedbackResponseDataModel(conn),
            HealthDataResponse: getHealthDataResponseDataModel(conn),
            OnBoardingResponse: getOnBoardingResponseDataModel(conn),
            Participant: getParticipantModel(conn),
            ParticipantAppointment: getParticipantAppointmentDataModel(conn),
            PiParticipantAppointment: getPiParticipantAppointmentDataModel(conn),
            ParticipantMeta: getParticipantMetaModel(conn),
            ParticipantOnBoardingEvent: getParticipantOnBoardingEventDataModel(conn),
            ParticipantOnBoardingConsentEvent: getParticipantOnBoardingConsentEventDataModel(conn),
            ParticipantStatusHistory: getParticipantStatusHistoryModel(conn),
            Sites: getSiteModel(conn),
            StudyMetaData: getStudyMetaDataModel(conn),
            StudySite: getStudySiteModel(conn),
            SurveyResponse: getSurveyResponseDataModel(conn),
            SurveyResponseTracker: getSurveyResponseTrackerModel(conn),
            VisitNote: getVisitNoteDataModel(conn),
            UserData: getUserDataModel(conn),
            TelehealthSessionData: getTelehealthSessionDataModel(conn),
            StudyFeature: getStudyFeatureModel(conn),
            ClientFeature: getClientFeatureModel(conn),
            EdcResponseVersion: getEdcResponseVersionDataModel(conn),
            EdcUnschduledPacketResponseVersion: getEdcUnschduledPacketResponseVersionDataModel(conn),
            EdcUnscheduledPacket: getEdcUnscheduledPacketDataModel(conn),
            Sequelize_: getSequelize(conn),
            ValidicData: getValidicDataModel(conn),
            ValidicProfileData: getValidicProfileDataModel(conn),
            DeSftpMeta: getDeSftpMetaModel(conn),
            Language: getLanguageModel(conn),
            ParticipantLanguage: getParticipantLanguageModel(conn)
        };
        return models;
    }

    getModels() {
        return models;
    }
    async sequelizeConn(clientId, dbName) {
        const sequelizeConnection = await connection(clientId, dbName);
        return sequelizeConnection;
    }
    findAll(query = {}, include = [], model, attributes = [], order = ['id', 'ASC']) {
        if (_.isArray(order[0])) {
            order = order

        } else {
            order = [
                order
            ]
        }
        if (attributes && attributes.length > 0) {
            return model
                .findAll({
                    attributes,
                    where: query,
                    include,
                    order: order,
                    // limit: 20
                })
                .then((result) => {
                    return result;
                })
                .catch((err) => {
                    throw err;
                });
        }
        return model
            .findAll({
                where: query,
                include,
                order: order
            })
            .then((result) => {
                return result;
            })
            .catch((err) => {
                throw err;
            });
    }

    findOne(query = {}, include = [], model, order = []) {
        return model
            .findOne({
                where: query,
                include,
                order
            })
            .then((result) => {
                return result;
            })
            .catch((err) => {
                throw err;
            });
    }


    findOneWithOrderBy(query = {}, include = [], model, order = ['id', 'ASC']) {
        return model
            .findOne({
                where: query,
                include,
                order: [
                    order
                ],
            })
            .then((result) => {
                return result;
            })
            .catch((err) => {
                throw err;
            });
    }

    //fetch data with attributes
    findOneWithAttr(query = {}, include = [], model, attributes = [], order = []) {
        if (attributes && attributes.length > 0) {
            return model
                .findOne({
                    attributes,
                    where: query,
                    include,
                    order
                })
                .then((result) => {
                    return result;
                })
                .catch((err) => {
                    throw err;
                });
        } else {
            return model
                .findOne({
                    where: query,
                    include,
                    order
                })
                .then((result) => {
                    return result;
                })
                .catch((err) => {
                    throw err;
                });
        }
    }

    // Create new record
    create(payload, model) {
        return model
            .create(payload)
            .then((result) => {
                return result;
            })
            .catch((err) => {
                throw err;
            });
    }

    // Delete record by id
    deleteById(id, model) {
        return model.findByPk(id).then((row) => {
            if (row) {
                return row
                    .destroy()
                    .then((result) => {
                        return result;
                    })
                    .catch((err) => {
                        throw err;
                    });
            }
            throw new Error('Record Not Found', 404);
        });
    }

    // Create bulk records
    bulkCreate(payload = [], model) {
        return model
            .bulkCreate(payload, { returning: true })
            .then((result) => {
                return result;
            })
            .catch((err) => {
                throw err;
            });
    }

    findByPk(id, model) {
        return model
            .findByPk(id)
            .then((result) => {
                return result;
            })
            .catch((err) => {
                throw err;
            });
    }

    // Update record by id
    updateById(id, payload, model) {
        return model.findByPk(id).then((row) => {
            if (row) {
                return row
                    .update(payload)
                    .then((result) => {
                        return result;
                    })
                    .catch((err) => {
                        throw err;
                    });
            }
            throw new Error('Record Not Found', 404);
        });
    }

    // raw query
    rawQuery(sqlQuery, model) {
        return model.query(sqlQuery, { type: model.QueryTypes.SELECT }).then(function(result) {
            return result;
        }).catch((error) => {
            return error;
        });
    }
    async rawQueryWithConn(clientId, sqlQuery, model, dbName = '') {
            var Sequelize = require('sequelize');
            var sequelize = await this.sequelizeConn(clientId, dbName);
            return sequelize.query(sqlQuery, { type: Sequelize.QueryTypes.SELECT }).then(function(result) {
                return result;
            }).catch((error) => {
                return error;
            });
        }
        // Close DB Connection
    closeDbConnection() {
        console.log('=== connection closed === ');
        if (this.sequelizeConnection) {
            this.sequelizeConnection.close();
        }
    }

    findAllWithGroup(query = {}, include = [], model, attributes = [], order = ['id', 'ASC'], groupBy = []) {
        if (attributes && attributes.length > 0) {
            return model
                .findAll({
                    attributes,
                    where: query,
                    include,
                    group: groupBy,
                    order: [
                        order
                    ],
                    // limit: 20
                })
                .then((result) => {
                    return result;
                })
                .catch((err) => {
                    throw err;
                });
        }
        return model
            .findAll({
                where: query,
                include,
                group: groupBy,
                order: [
                    order
                ]
            })
            .then((result) => {
                return result;
            })
            .catch((err) => {
                throw err;
            });
    }
}

module.exports = DB;