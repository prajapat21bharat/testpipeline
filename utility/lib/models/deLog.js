'use strict';

const Sequelize = require('sequelize');

function getDeLogDataModel(_sequelize) {
    const deLog = _sequelize.define(
        'de_log', {
            id: {
                type: Sequelize.DataTypes.UUID,
                field: 'id',
                allowNull: false,
                primaryKey: true,
            },
            study_id: {
                type: Sequelize.STRING,
                field: 'study_id',
                allowNull: true
            },
            de_sftp_meta_id: {
                type: Sequelize.STRING,
                field: 'de_sftp_meta_id',
                allowNull: true
            },
            trigger_datetime: {
                type: Sequelize.DATE,
                field: 'trigger_datetime',
                allowNull: true
            },
            completed_datetime: {
                type: Sequelize.DATE,
                field: 'completed_datetime',
                allowNull: true
            },
            message: {
                type: Sequelize.STRING,
                field: 'message',
                allowNull: true
            },
            status: {
                type: Sequelize.STRING,
                field: 'status',
                allowNull: true
            },
            aws_job_id: {
                type: Sequelize.STRING,
                field: 'aws_job_id',
                allowNull: true
            },
            aws_log_path: {
                type: Sequelize.STRING,
                field: 'aws_log_path',
                allowNull: true
            },
            type: {
                type: Sequelize.STRING,
                field: 'type',
                allowNull: true
            },
            s3_location: {
                type: Sequelize.STRING,
                field: 's3_location',
                allowNull: true
            },
            triggered_by: {
                type: Sequelize.STRING,
                field: 'triggered_by',
                allowNull: true
            }
        }, {
            // schema: 'public',
            tableName: 'de_log',
            timestamps: false
        }
    );

    return deLog;
}

module.exports.getDeLogDataModel = getDeLogDataModel;