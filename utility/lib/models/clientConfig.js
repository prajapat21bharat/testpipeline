'use strict';

const Sequelize = require('sequelize');

function getClientConfigModel(_sequelize) {
    const ClientConfig = _sequelize.define(
        'client_config', {
            id: {
                type: Sequelize.DataTypes.UUID,
                field: 'id',
                allowNull: false,
                primaryKey: true,
            },
            domain_name: {
                type: Sequelize.STRING,
                field: 'domain_name',
                allowNull: true
            },
            driver_class_name: {
                type: Sequelize.STRING,
                field: 'driver_class_name',
                allowNull: true
            },
            initialize: {
                type: Sequelize.BOOLEAN,
                field: 'initialize',
                allowNull: false
            },
            name: {
                type: Sequelize.STRING,
                field: 'name',
                allowNull: true
            },
            region: {
                type: Sequelize.STRING,
                field: 'region',
                allowNull: true
            },
            s3_bucket: {
                type: Sequelize.STRING,
                field: 's3_bucket',
                allowNull: true
            },
            client_type: {
                type: Sequelize.STRING,
                field: 'client_type',
                allowNull: true
            },
            mobile_analytics_cognito_id: {
                type: Sequelize.STRING,
                field: 'mobile_analytics_cognito_id',
                allowNull: true
            },
            mobile_analytics_app_id: {
                type: Sequelize.STRING,
                field: 'mobile_analytics_app_id',
                allowNull: true
            },
            adverse_event_target_emails: {
                type: Sequelize.STRING,
                field: 'adverse_event_target_emails',
                allowNull: true
            },
            mobile_schedule_notification_duration: {
                type: Sequelize.INTEGER,
                field: 'mobile_schedule_notification_duration',
                allowNull: false
            },
            // firebase_server_url: {
            //     type: Sequelize.STRING,
            //     field: 'firebase_server_url',
            //     allowNull: false
            // },
            // firebase_server_key: {
            //     type: Sequelize.STRING,
            //     field: 'firebase_server_key',
            //     allowNull: false
            // },
            // firebase_service_json: {
            //     type: Sequelize.STRING,
            //     field: 'firebase_service_json',
            //     allowNull: false
            // },
            dt_transfer_protocol: {
                type: Sequelize.STRING,
                field: 'dt_transfer_protocol',
                allowNull: true
            }
        }, {
            // schema: 'public',
            tableName: 'client_config',
            timestamps: false
        }
    );

    return ClientConfig;
}

module.exports.getClientConfigModel = getClientConfigModel;