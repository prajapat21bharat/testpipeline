'use strict';

const Sequelize = require('sequelize');

function getParticipantAppointmentDataModel(_sequelize) {
    const participantAppointment = _sequelize.define(
        'participant_appointment', {
            id: {
                type: Sequelize.DataTypes.UUID,
                field: 'id',
                allowNull: false,
                primaryKey: true,
            },
            day_of_visit: {
                type: Sequelize.INTEGER,
                field: 'day_of_visit',
                allowNull: true
            },
            is_cancelled: {
                type: Sequelize.BOOLEAN,
                field: 'is_cancelled',
                allowNull: true
            },
            participant_id: {
                type: Sequelize.STRING,
                field: 'participant_id',
                allowNull: true
            },
            tru_clinic_appointment_time_stamp: {
                type: Sequelize.STRING,
                field: 'tru_clinic_appointment_time_stamp',
                allowNull: true
            },
            tru_clinic_visit_url: {
                type: Sequelize.STRING,
                field: 'tru_clinic_visit_url',
                allowNull: true
            },
            visit_type: {
                type: Sequelize.STRING,
                field: 'visit_type',
                allowNull: true
            },
            study_id: {
                type: Sequelize.STRING,
                field: 'study_id',
                allowNull: true
            },
            tru_clinic_appointment_id: {
                type: Sequelize.STRING,
                field: 'tru_clinic_appointment_id',
                allowNull: true
            },
            completion_window: {
                type: Sequelize.INTEGER,
                field: 'completion_window',
                allowNull: true
            },
            created_time: {
                type: Sequelize.DATE,
                field: 'created_time',
                allowNull: true
            },
            modified_time: {
                type: Sequelize.DATE,
                field: 'modified_time',
                allowNull: true
            },
            is_captured: {
                type: Sequelize.BOOLEAN,
                field: 'is_captured',
                allowNull: true
            },
            is_pi_initiated: {
                type: Sequelize.BOOLEAN,
                field: 'is_pi_initiated',
                allowNull: true
            }
        }, {
            // schema: 'public',
            tableName: 'participant_appointment',
            timestamps: false
        }
    );

    return participantAppointment;
}

module.exports.getParticipantAppointmentDataModel = getParticipantAppointmentDataModel;