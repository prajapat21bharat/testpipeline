'use strict';

const Sequelize = require('sequelize');

function getEdcUnscheduledPacketDataModel(_sequelize) {
    const edcUnscheduledPacket = _sequelize.define(
        'edc_unscheduled_packet', {
        id: {
            type: Sequelize.DataTypes.UUID,
            field: 'id',
            allowNull: false,
            primaryKey: true,
        },
        packet_name: {
            type: Sequelize.STRING,
            field: 'packet_name',
            allowNull: true
        },
        study_id: {
            type: Sequelize.STRING,
            field: 'study_id',
            allowNull: true
        },
        created_by: {
            type: Sequelize.STRING,
            field: 'created_by',
            allowNull: true
        },
        created_at: {
            type: Sequelize.DATE,
            field: 'created_at',
            allowNull: true
        },
        modified_by: {
            type: Sequelize.STRING,
            field: 'modified_by',
            allowNull: true
        },
        modified_at: {
            type: Sequelize.DATE,
            field: 'modified_at',
            allowNull: true
        },
        edc_unscheduled_packet_version_id: {
            type: Sequelize.STRING,
            field: 'edc_unscheduled_packet_version_id',
            allowNull: true
        }
    }, {
        // schema: 'public',
        tableName: 'edc_unscheduled_packet',
        timestamps: false
    }
    );

    return edcUnscheduledPacket;
}

module.exports.getEdcUnscheduledPacketDataModel = getEdcUnscheduledPacketDataModel;