'use strict';

const Sequelize = require('sequelize');
//const { getParticipantModel } = require('./participant');
function getTelehealthSessionDataModel(_sequelize) {
    const telehealthSessionData = _sequelize.define(
        'telehealth_session', {
        id: {
            type: Sequelize.DataTypes.UUID,
            field: 'id',
            allowNull: false,
            primaryKey: true,
        },
        study_id: {
            type: Sequelize.STRING,
            field: 'study_id',
            allowNull: true
        },
        site_id: {
            type: Sequelize.STRING,
            field: 'site_id',
            allowNull: true
        },
        participant_id: {
            type: Sequelize.STRING,
            field: 'participant_id',
            allowNull: true
        },
        pi_id: {
            type: Sequelize.STRING,
            field: 'pi_id',
            allowNull: true
        },
        session_id: {
            type: Sequelize.STRING,
            field: 'session_id',
            allowNull: true
        },
        session_status: {
            type: Sequelize.STRING,
            field: 'session_status',
            allowNull: true
        },
        visit_type: {
            type: Sequelize.STRING,
            field: 'visit_type',
            allowNull: true
        },
        appointment_id: {
            type: Sequelize.STRING,
            field: 'appointment_id',
            allowNull: true
        },
        completion_window: {
            type: Sequelize.INTEGER(11),
            field: 'completion_window',
            allowNull: true
        },
        is_captured: {
            type: Sequelize.STRING,
            field: 'is_captured',
            allowNull: true
        },
        is_cancelled: {
            type: Sequelize.STRING,
            field: 'is_cancelled',
            allowNull: true
        },
        initiated_by: {
            type: Sequelize.STRING,
            field: 'initiated_by',
            allowNull: true
        },
        start_time: {
            type: Sequelize.DATE,
            field: 'start_time',
            allowNull: true
        },
        end_time: {
            type: Sequelize.DATE,
            field: 'end_time',
            allowNull: true
        },
        cancelled_by: {
            type: Sequelize.STRING,
            field: 'cancelled_by',
            allowNull: true
        },
        created_time: {
            type: Sequelize.DATE,
            field: 'created_time',
            allowNull: true
        },
        modified_time: {
            type: Sequelize.DATE,
            field: 'modified_time',
            allowNull: true
        }
    }, {
        // schema: 'public',
        tableName: 'telehealth_session',
        timestamps: false
    }
    );
    // const Participant = getParticipantModel(_sequelize);
    // piParticipantAppointment.belongsTo(Participant, { foreignKey: 'participant_id', sourceKey: 'id' });
    return telehealthSessionData;
}

module.exports.getTelehealthSessionDataModel = getTelehealthSessionDataModel;