'use strict';

const Sequelize = require('sequelize');

function getValidicProfileDataModel(_sequelize) {
    const ValidicProfileData = _sequelize.define(
        'validic_user_profile', {

        id: {
            type: Sequelize.DataTypes.UUID,
            field: 'id',
            allowNull: false,
            primaryKey: true,
        },
        profileData: {
            type: Sequelize.TEXT('long'),
            field: 'profile_data',
            allowNull: true
        },
        dataCaptureTime: {
            type: Sequelize.STRING,
            field: 'data_capture_time',
            allowNull: true
        },
        participantId: {
            type: Sequelize.STRING,
            field: 'participant_id',
            allowNull: true
        },
        studyId: {
            type: Sequelize.STRING,
            field: 'study_id',
            allowNull: true
        },
        siteId: {
            type: Sequelize.STRING,
            field: 'site_id',
            allowNull: true
        }
    }, {
        // schema: 'public',
        tableName: 'validic_user_profile',
        timestamps: false
    }
    );

    return ValidicProfileData;
}

module.exports.getValidicProfileDataModel = getValidicProfileDataModel;