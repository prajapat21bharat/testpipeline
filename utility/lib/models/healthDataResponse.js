'use strict';

const Sequelize = require('sequelize');

function getHealthDataResponseDataModel(_sequelize) {
    const healthDataResponse = _sequelize.define(
        'health_data_response', {
            id: {
                type: Sequelize.DataTypes.UUID,
                field: 'id',
                allowNull: false,
                primaryKey: true,
            },
            studyId: {
                type: Sequelize.STRING,
                field: 'study_id',
                allowNull: true
            },
            participantId: {
                type: Sequelize.STRING,
                field: 'participant_id',
                allowNull: true
            },
            responseDate: {
                type: Sequelize.STRING,
                field: 'response_date',
                allowNull: true
            },
            responseData: {
                type: Sequelize.TEXT('long'),
                field: 'response_data',
                allowNull: true
            },
            createdTime: {
                type: Sequelize.STRING,
                field: 'created_time',
                allowNull: true
            },
            siteId: {
                type: Sequelize.STRING,
                field: 'site_id',
                allowNull: true
            }
        }, {
            // schema: 'public',
            tableName: 'health_data_response',
            timestamps: false
        }
    );

    return healthDataResponse;
}

module.exports.getHealthDataResponseDataModel = getHealthDataResponseDataModel;