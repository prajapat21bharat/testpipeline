'use strict';

const Sequelize = require('sequelize');

const { getParticipantOnBoardingConsentEventDataModel } = require('./ParticipantOnBoardingConsentEvent');
// const { getConsentConfigurationModel } = require('./consentConfiguration');
// const { getCohortModel } = require('./cohort');
// const { getStudyMetaDataModel } = require('./studyMetaData');
// const { getSiteModel } = require('./site');

function getParticipantOnBoardingEventDataModel(_sequelize) {
    const participantOnBoardingEvent = _sequelize.define(
        'participant_on_boarding_event', {
        id: {
            type: Sequelize.DataTypes.UUID,
            field: 'id',
            allowNull: false,
            primaryKey: true,
        },
        studyId: {
            type: Sequelize.STRING,
            field: 'study_id',
            allowNull: true
        },
        participantId: {
            type: Sequelize.STRING,
            field: 'participant_id',
            allowNull: true
        },
        registeredTime: {
            type: Sequelize.STRING,
            field: 'registered_time',
            allowNull: true
        },
        emailVerifiedTime: {
            type: Sequelize.STRING,
            field: 'email_verified_time',
            allowNull: true
        },
        consentedTime: {
            type: Sequelize.DATE,
            field: 'consented_time',
            allowNull: true
        },
        siteId: {
            type: Sequelize.STRING,
            field: 'site_id',
            allowNull: true
        },
        localRegistrationTime: {
            type: Sequelize.STRING,
            field: 'local_registration_time',
            allowNull: true
        },
        localConsentedTime: {
            type: Sequelize.STRING,
            field: 'local_consented_time',
            allowNull: true
        },
        offset: {
            type: Sequelize.STRING,
            field: 'offset',
            allowNull: true
        }
    }, {
        // schema: 'public',
        tableName: 'participant_on_boarding_event',
        timestamps: false
    }
    );

    const ParticipantOnBoardingConsentEvent = getParticipantOnBoardingConsentEventDataModel(_sequelize);
    // const ConsentConfiguration = getConsentConfigurationModel(_sequelize);
    // const Cohort = getCohortModel(_sequelize);
    // const StudyMetaData = getStudyMetaDataModel(_sequelize);
    // const Site = getSiteModel(_sequelize);

    participantOnBoardingEvent.hasMany(ParticipantOnBoardingConsentEvent, { foreignKey: 'participant_on_boarding_event_id', targetKey: 'id' });
    // ConsentConfiguration.hasMany(SurveyResponse, { foreignKey: 'consent_id', targetKey: 'id' });
    // Cohort.hasMany(SurveyResponse, { foreignKey: 'survey_response_tracker_id', targetKey: 'id' });
    // StudyMetaData.hasMany(SurveyResponse, { foreignKey: 'id', targetKey: 'study_id' });
    // Site.hasMany(SurveyResponse, { foreignKey: 'id', targetKey: 'site_id' });

    return participantOnBoardingEvent;
}

module.exports.getParticipantOnBoardingEventDataModel = getParticipantOnBoardingEventDataModel;