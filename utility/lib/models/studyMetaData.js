'use strict';

const Sequelize = require('sequelize');
// const uuid = require('uuidv4');

function getStudyMetaDataModel(_sequelize) {
    const StudyMetaData = _sequelize.define(
        'study_meta_data', {
            id: {
                type: Sequelize.DataTypes.UUID,
                field: 'id',
                allowNull: false,
                primaryKey: true,
                // defaultValue: uuid(),
            },
            android: {
                type: Sequelize.BOOLEAN,
                field: 'android',
                allowNull: false,
            },
            app_download_path: {
                type: Sequelize.STRING,
                field: 'app_download_path',
                allowNull: false
            },
            created_by: {
                type: Sequelize.STRING,
                field: 'created_by',
                allowNull: false
            },
            created_time: {
                type: Sequelize.STRING,
                field: 'created_time',
                allowNull: false,

            },
            customer_id: {
                type: Sequelize.STRING,
                field: 'customer_id',
                allowNull: false
            },
            digital_health: {
                type: Sequelize.STRING,
                field: 'digital_health',
                allowNull: false
            },
            document_location: {
                type: Sequelize.STRING,
                field: 'document_location',
                allowNull: false,

            },
            expected_patients: {
                type: Sequelize.STRING,
                field: 'expected_patients',
                allowNull: false
            },
            first_patient_enroll_date: {
                type: Sequelize.STRING,
                field: 'first_patient_enroll_date',
                allowNull: false
            },
            first_patient_last_date: {
                type: Sequelize.STRING,
                field: 'first_patient_last_date',
                allowNull: false,

            },
            ios: {
                type: Sequelize.BOOLEAN,
                field: 'ios',
                allowNull: false
            },
            last_patient_enroll_date: {
                type: Sequelize.STRING,
                field: 'last_patient_enroll_date',
                allowNull: false
            },
            last_patient_last_date: {
                type: Sequelize.STRING,
                field: 'last_patient_last_date',
                allowNull: false,

            },
            modified_by: {
                type: Sequelize.STRING,
                field: 'modified_by',
                allowNull: false
            },
            modified_time: {
                type: Sequelize.STRING,
                field: 'modified_time',
                allowNull: false
            },
            name: {
                type: Sequelize.STRING,
                field: 'name',
                allowNull: false,

            },
            onboarding_type: {
                type: Sequelize.STRING,
                field: 'onboarding_type',
                allowNull: false
            },
            stage: {
                type: Sequelize.STRING,
                field: 'stage',
                allowNull: false
            },
            status: {
                type: Sequelize.STRING,
                field: 'status',
                allowNull: false,

            },
            support: {
                type: Sequelize.STRING,
                field: 'support',
                allowNull: false
            },
            target_patient: {
                type: Sequelize.STRING,
                field: 'target_patient',
                allowNull: false
            },
            type: {
                type: Sequelize.STRING,
                field: 'type',
                allowNull: false,

            },
            end_date: {
                type: Sequelize.STRING,
                field: 'end_date',
                allowNull: false
            },
            app_name: {
                type: Sequelize.STRING,
                field: 'app_name',
                allowNull: false,

            },
            condensed_app_name: {
                type: Sequelize.STRING,
                field: 'condensed_app_name',
                allowNull: false
            },
            study_consent_agreement_path: {
                type: Sequelize.STRING,
                field: 'study_consent_agreement_path',
                allowNull: false
            },
            continuous_study: {
                type: Sequelize.BOOLEAN,
                field: 'continuous_study',
                allowNull: false,
            },
            web: {
                type: Sequelize.BOOLEAN,
                field: 'web',
                allowNull: false
            },
            study_end_date: {
                type: Sequelize.STRING,
                field: 'study_end_date',
                allowNull: false
            },
            study_start_date: {
                type: Sequelize.STRING,
                field: 'study_start_date',
                allowNull: false
            },
            sub_type: {
                type: Sequelize.STRING,
                field: 'sub_type',
                allowNull: false
            },
            client_id: {
                type: Sequelize.STRING,
                field: 'client_id',
                allowNull: false
            },
            is_internal_organizational_approved: {
                type: Sequelize.BOOLEAN,
                field: 'is_internal_organizational_approved',
                allowNull: false
            },
            is_irb_ec_approved: {
                type: Sequelize.BOOLEAN,
                field: 'is_irb_ec_approved',
                allowNull: false
            },
            validic_integration: {
                type: Sequelize.BOOLEAN,
                field: 'validic_integration',
                allowNull: false
            },
            marketing: {
                type: Sequelize.STRING,
                field: 'marketing',
                allowNull: false
            },
            thread_marketing_support: {
                type: Sequelize.STRING,
                field: 'thread_marketing_support',
                allowNull: false
            },
            marketing_site_url: {
                type: Sequelize.STRING,
                field: 'marketing_site_url',
                allowNull: false
            },
            current_url: {
                type: Sequelize.STRING,
                field: 'current_url',
                allowNull: false
            },
            route_name: {
                type: Sequelize.STRING,
                field: 'route_name',
                allowNull: false
            },
            epic_integration: {
                type: Sequelize.BOOLEAN,
                field: 'epic_integration',
                allowNull: false
            },
            epic_base_url: {
                type: Sequelize.STRING,
                field: 'epic_base_url',
                allowNull: false
            },
            tru_clinic_integration: {
                type: Sequelize.BOOLEAN,
                field: 'tru_clinic_integration',
                allowNull: false
            },
            tru_clinic_api_key: {
                type: Sequelize.STRING,
                field: 'tru_clinic_api_key',
                allowNull: false
            },
            tru_clinic_waiting_room: {
                type: Sequelize.STRING,
                field: 'tru_clinic_waiting_room',
                allowNull: false
            },
            tru_clinic_pi_endpoint: {
                type: Sequelize.STRING,
                field: 'tru_clinic_pi_endpoint',
                allowNull: false
            },
        }, {
            // schema: 'public',
            tableName: 'study_meta_data',
            timestamps: false
        }
    );

    return StudyMetaData;
}

module.exports.getStudyMetaDataModel = getStudyMetaDataModel;