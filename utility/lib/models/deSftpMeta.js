'use strict';

const Sequelize = require('sequelize');

function getDeSftpMetaModel(_sequelize) {
    const deSftpMeta = _sequelize.define(
        'de_sftp_meta', {
            id: {
                type: Sequelize.DataTypes.UUID,
                field: 'id',
                allowNull: false,
                primaryKey: true,
            },
            study_id: {
                type: Sequelize.STRING,
                field: 'study_id',
                allowNull: true
            },
            sftp_id: {
                type: Sequelize.STRING,
                field: 'sftp_id',
                allowNull: true
            },
            frequency_cron: {
                type: Sequelize.STRING,
                field: 'frequency_cron',
                allowNull: false
            },
            timezone: {
                type: Sequelize.STRING(45),
                field: 'timezone',
                allowNull: true
            },
            created_by: {
                type: Sequelize.STRING,
                field: 'created_by',
                allowNull: true
            },
            secret_manager_key: {
                type: Sequelize.STRING(500),
                field: 'secret_manager_key',
                allowNull: true
            },
            enabled: {
                type: Sequelize.TINYINT(1),
                field: 'enabled',
                allowNull: true
            },
            sftp_version: {
                type: Sequelize.INTEGER(11),
                field: 'sftp_version',
                allowNull: true
            },
            created_at: {
                type: Sequelize.DATE,
                field: 'created_at',
                allowNull: true
            }
        }, {
            // schema: 'public',
            tableName: 'de_sftp_meta',
            timestamps: false
        }
    );

    return deSftpMeta;
}

module.exports.getDeSftpMetaModel = getDeSftpMetaModel;