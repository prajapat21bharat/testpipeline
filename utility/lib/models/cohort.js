'use strict';

const Sequelize = require('sequelize');

function getCohortModel(_sequelize) {
    const cohort = _sequelize.define(
        'cohort', {
            id: {
                type: Sequelize.DataTypes.UUID,
                field: 'id',
                allowNull: false,
                primaryKey: true,
            },
            cohortId: {
                type: Sequelize.STRING,
                field: 'cohort_id',
                allowNull: true
            },
            cohortName: {
                type: Sequelize.STRING,
                field: 'cohort_name',
                allowNull: true
            },
            createdTime: {
                type: Sequelize.DATE,
                field: 'created_time',
                allowNull: true
            },
            status: {
                type: Sequelize.BOOLEAN,
                field: 'status',
                allowNull: true
            },
            cohortcol: {
                type: Sequelize.STRING,
                field: 'cohortcol',
                allowNull: true
            }

        }, {
            tableName: 'cohort',
            timestamps: false
        }
    );

    return cohort;
}

module.exports.getCohortModel = getCohortModel;