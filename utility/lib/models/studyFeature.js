'use strict';

const Sequelize = require('sequelize');
// const uuid = require('uuidv4');

function getStudyFeatureModel(_sequelize) {
    const studyFeature = _sequelize.define(
        'study_feature', {
        id: {
            type: Sequelize.DataTypes.INTEGER,
            field: 'id',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
            // defaultValue: uuid(),
        },
        study_meta_data_id: {
            type: Sequelize.STRING,
            field: 'study_meta_data_id',
            allowNull: true
        },
        feature_name: {
            type: Sequelize.STRING,
            field: 'feature_name',
            allowNull: true
        },
        feature_key: {
            type: Sequelize.STRING,
            field: 'feature_key',
            allowNull: true
        },
        description: {
            type: Sequelize.STRING,
            field: 'description',
            allowNull: true
        },
        is_enabled: {
            type: Sequelize.BOOLEAN,
            field: 'is_enabled',
            allowNull: true
        }
    }, {
        // schema: 'public',
        tableName: 'study_feature',
        timestamps: false
    }
    );

    return studyFeature;
}

module.exports.getStudyFeatureModel = getStudyFeatureModel;