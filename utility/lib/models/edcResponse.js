'use strict';

const Sequelize = require('sequelize');
const { getEdcResponseVersionDataModel } = require('./edcResponseVersion');

function getEdcResponseDataModel(_sequelize) {
    const edcResponse = _sequelize.define(
        'edc_response', {
        id: {
            type: Sequelize.DataTypes.UUID,
            field: 'id',
            allowNull: false,
            primaryKey: true,
        },
        response_s3_key: {
            type: Sequelize.TEXT('long'),
            field: 'response_s3_key',
            allowNull: true
        },
        form_id: {
            type: Sequelize.STRING,
            field: 'form_id',
            allowNull: true
        },
        form_version_id: {
            type: Sequelize.STRING,
            field: 'form_version_id',
            allowNull: true
        },
        study_id: {
            type: Sequelize.STRING,
            field: 'study_id',
            allowNull: true
        },
        participant_id: {
            type: Sequelize.STRING,
            field: 'participant_id',
            allowNull: true
        },
        edc_response_version_id: {
            type: Sequelize.STRING,
            field: 'edc_response_version_id',
            allowNull: true
        },
        created_at: {
            type: Sequelize.DATE,
            field: 'created_at',
            allowNull: true
        },
        submitted_at: {
            type: Sequelize.DATE,
            field: 'submitted_at',
            allowNull: true
        },
        state: {
            type: Sequelize.STRING,
            field: 'state',
            allowNull: true
        },
        milestone: {
            type: Sequelize.INTEGER,
            field: 'milestone',
            allowNull: true
        },
        created_by: {
            type: Sequelize.STRING,
            field: 'created_by',
            allowNull: true
        }
    }, {
        // schema: 'public',
        tableName: 'edc_response',
        timestamps: false
    }
    );

    //return edcResponse;
    const edcResponseVersion = getEdcResponseVersionDataModel(_sequelize);

    edcResponse.belongsTo(edcResponseVersion, { foreignKey: 'edc_response_version_id', targetKey: 'id' });

    return edcResponse;
}

module.exports.getEdcResponseDataModel = getEdcResponseDataModel;