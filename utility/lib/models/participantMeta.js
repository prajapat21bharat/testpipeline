'use strict';

const Sequelize = require('sequelize');
// const uuid = require('uuidv4');

// const { getParticipantModel } = require('./Participant');

function getParticipantMetaModel(_sequelize) {
    const ParticipantMeta = _sequelize.define(
        'participant_meta', {
            id: {
                type: Sequelize.DataTypes.UUID,
                field: 'id',
                allowNull: false,
                primaryKey: true,
                // defaultValue: uuid(),
            },
            field_name: {
                type: Sequelize.STRING,
                field: 'field_name',
                allowNull: true
            },
            field_type: {
                type: Sequelize.STRING,
                field: 'field_type',
                allowNull: true
            },
            field_value: {
                type: Sequelize.TEXT,
                field: 'field_value',
                allowNull: true
            },
            pid: {
                type: Sequelize.STRING,
                field: 'pid',
                allowNull: true
            }
        }, {
            // schema: 'public',
            tableName: 'participant_meta',
            timestamps: false
        }
    );

    // const Participant = getParticipantModel(_sequelize);

    // ParticipantMeta.belongsTo(Participant, { foreignKey: 'pid', targetKey: 'id' });
    return ParticipantMeta;
}

module.exports.getParticipantMetaModel = getParticipantMetaModel;