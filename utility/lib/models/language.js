'use strict';

const Sequelize = require('sequelize');

function getLanguageModel(_sequelize) {
    const Language = _sequelize.define(
        'language', {
        language_id: {
            type: Sequelize.DataTypes.UUID,
            field: 'language_id',
            allowNull: false,
            primaryKey: true,
        },

        'name': {
            type: Sequelize.STRING,
            field: 'name',
            allowNull: true
        },
        'display_name': {
            type: Sequelize.STRING,
            field: 'display_name',
            allowNull: true
        },
        'language_culture': {
            type: Sequelize.STRING,
            field: 'language_culture',
            allowNull: true
        },
        'Active': {
            type: Sequelize.INTEGER,
            field: 'Active',
            allowNull: true
        },
        'sort_Order': {
            type: Sequelize.INTEGER,
            field: 'sort_Order',
            allowNull: true
        },
        'created_by': {
            type: Sequelize.STRING,
            field: 'created_by',
            allowNull: true
        },
        'created_time': {
            type: Sequelize.DATE,
            field: 'created_time',
            allowNull: true
        },
        'is_default': {
            type: Sequelize.INTEGER,
            field: 'is_default',
            allowNull: true
        },
        'language_name': {
            type: Sequelize.STRING,
            field: 'language_name',
            allowNull: true
        }
    }, {
        tableName: 'language',
        timestamps: false
    }
    );

    return Language;
}

module.exports.getLanguageModel = getLanguageModel;