'use strict';

const Sequelize = require('sequelize');
// const uuid = require('uuidv4');

function getClientFeatureModel(_sequelize) {
    const clientFeature = _sequelize.define(
        'client_feature', {
        id: {
            type: Sequelize.DataTypes.INTEGER(11),
            field: 'id',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
            // defaultValue: uuid(),
        },
        client_config_id: {
            type: Sequelize.STRING,
            field: 'client_config_id',
            allowNull: true
        },
        feature_name: {
            type: Sequelize.STRING,
            field: 'feature_name',
            allowNull: true
        },
        feature_key: {
            type: Sequelize.STRING,
            field: 'feature_key',
            allowNull: true
        },
        description: {
            type: Sequelize.STRING,
            field: 'description',
            allowNull: true
        },
        is_enabled: {
            type: Sequelize.BOOLEAN,
            field: 'is_enabled',
            allowNull: true
        }
    }, {
        // schema: 'public',
        tableName: 'client_feature',
        timestamps: false
    }
    );

    return clientFeature;
}

module.exports.getClientFeatureModel = getClientFeatureModel;