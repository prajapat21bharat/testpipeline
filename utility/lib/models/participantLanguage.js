'use strict';

const Sequelize = require('sequelize');

function getParticipantLanguageModel(_sequelize) {
    const ParticipantLanguage = _sequelize.define(
        'participant_language', {
        id: {
            type: Sequelize.DataTypes.UUID,
            field: 'id',
            allowNull: false,
            primaryKey: true,
        },
        participant_id: {
            type: Sequelize.STRING,
            field: 'participant_id',
            allowNull: true
        },
        language_id: {
            type: Sequelize.STRING,
            field: 'language_id',
            allowNull: true
        },
        active: {
            type: Sequelize.INTEGER,
            field: 'active',
            allowNull: true
        }
    }, {
        tableName: 'participant_language',
        timestamps: false
    }
    );

    return ParticipantLanguage;
}

module.exports.getParticipantLanguageModel = getParticipantLanguageModel;