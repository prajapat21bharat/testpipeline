'use strict';

const Sequelize = require('sequelize');

function getEmailTemplateDataModel(_sequelize) {
    const emailTemplate = _sequelize.define(
        'email_template', {
            id: {
                type: Sequelize.DataTypes.UUID,
                field: 'id',
                allowNull: false,
                primaryKey: true,
            },
            client_id: {
                type: Sequelize.STRING,
                field: 'client_id',
                allowNull: true
            },
            template_id: {
                type: Sequelize.STRING,
                field: 'template_id',
                allowNull: true
            },
            template_type: {
                type: Sequelize.STRING,
                field: 'template_type',
                allowNull: true
            }
        }, {
            // schema: 'public',
            tableName: 'email_template',
            timestamps: false
        }
    );

    return emailTemplate;
}

module.exports.getEmailTemplateDataModel = getEmailTemplateDataModel;