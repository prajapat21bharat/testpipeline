'use strict';

const Sequelize = require('sequelize');
const { getStudyMetaDataModel } = require('./studyMetaData');

function getStudySiteModel(_sequelize) {
    const StudySite = _sequelize.define(
        'study_site', {
            study_id: {
                type: Sequelize.STRING,
                field: 'study_id',
                allowNull: false,
                // primaryKey: true
            },
            site_id: {
                type: Sequelize.STRING,
                field: 'site_id',
                allowNull: false
            },
            is_default: {
                type: Sequelize.BOOLEAN,
                field: 'is_default',
                allowNull: false
            }
        }, {
            // schema: 'public',
            tableName: 'study_site',
            timestamps: false
        }
    );

    StudySite.removeAttribute('id');
    const StudyMetaData = getStudyMetaDataModel(_sequelize);

    StudySite.belongsTo(StudyMetaData, { foreignKey: 'study_id', targetKey: 'id' });
    return StudySite;
}

module.exports.getStudySiteModel = getStudySiteModel;