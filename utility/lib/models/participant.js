'use strict';

const Sequelize = require('sequelize');
// const uuid = require('uuidv4');

const { getParticipantMetaModel } = require('./participantMeta');

function getParticipantModel(_sequelize) {
    const Participant = _sequelize.define(
        'participant', {
            id: {
                type: Sequelize.DataTypes.UUID,
                field: 'id',
                allowNull: false,
                primaryKey: true,
                // defaultValue: uuid(),
            },
            account_deleted: {
                type: Sequelize.BOOLEAN,
                field: 'account_deleted',
                allowNull: false
            },
            account_enabled: {
                type: Sequelize.BOOLEAN,
                field: 'account_enabled',
                allowNull: false
            },
            account_locked: {
                type: Sequelize.BOOLEAN,
                field: 'account_locked',
                allowNull: false
            },
            attempts: {
                type: Sequelize.STRING,
                field: 'attempts',
                allowNull: false
            },
            consent_status: {
                type: Sequelize.BOOLEAN,
                field: 'consent_status',
                allowNull: true
            },
            createdBy: {
                type: Sequelize.STRING,
                field: 'created_by',
                allowNull: true
            },
            createdTime: {
                type: Sequelize.STRING,
                field: 'created_time',
                allowNull: true
            },
            credentials_non_expired: {
                type: Sequelize.BOOLEAN,
                field: 'credentials_non_expired',
                allowNull: false
            },
            email: {
                type: Sequelize.STRING,
                field: 'email',
                allowNull: false,
                // validate: {
                //     isEmail: true
                // }
            },
            invitationDate: {
                type: Sequelize.STRING,
                field: 'invitation_date',
                allowNull: true
            },
            invite_process: {
                type: Sequelize.STRING,
                field: 'invite_process',
                allowNull: false
            },
            modified_by: {
                type: Sequelize.STRING,
                field: 'modified_by',
                allowNull: true
            },
            modified_time: {
                type: Sequelize.STRING,
                field: 'modified_time',
                allowNull: true
            },
            notes: {
                type: Sequelize.STRING,
                field: 'notes',
                allowNull: true
            },
            otp_verified: {
                type: Sequelize.BOOLEAN,
                field: 'otp_verified',
                allowNull: false
            },
            password: {
                type: Sequelize.STRING,
                field: 'password',
                allowNull: true
            },
            password_expiry_date: {
                type: Sequelize.STRING,
                field: 'password_expiry_date',
                allowNull: true
            },
            profileImage: {
                type: Sequelize.STRING,
                field: 'profile_image',
                allowNull: true
            },
            referenceId: {
                type: Sequelize.STRING,
                field: 'reference_id',
                allowNull: true
            },
            referenceSource: {
                type: Sequelize.STRING,
                field: 'reference_source',
                allowNull: true
            },
            registrationDate: {
                type: Sequelize.STRING,
                field: 'registration_date',
                allowNull: true
            },
            registration_process: {
                type: Sequelize.STRING,
                field: 'registration_process',
                allowNull: false
            },
            registration_status: {
                type: Sequelize.BOOLEAN,
                field: 'registration_status',
                allowNull: true
            },
            role: {
                type: Sequelize.STRING,
                field: 'role',
                allowNull: true
            },
            status: {
                type: Sequelize.STRING,
                field: 'status',
                allowNull: true
            },
            study_id: {
                type: Sequelize.STRING,
                field: 'study_id',
                allowNull: true
            },
            study_invite_code: {
                type: Sequelize.STRING,
                field: 'study_invite_code',
                allowNull: true
            },
            validicAccessToken: {
                type: Sequelize.STRING,
                field: 'validic_access_token',
                allowNull: true
            },
            validicId: {
                type: Sequelize.STRING,
                field: 'validic_id',
                allowNull: true
            },
            my_chart_account_id: {
                type: Sequelize.STRING,
                field: 'my_chart_account_id',
                allowNull: true
            },
            my_chart_account_type: {
                type: Sequelize.STRING,
                field: 'my_chart_account_type',
                allowNull: true
            },
            patient_id: {
                type: Sequelize.STRING,
                field: 'patient_id',
                allowNull: true
            },
            patient_id_type: {
                type: Sequelize.STRING,
                field: 'patient_id_type',
                allowNull: true
            },
            site_id: {
                type: Sequelize.STRING,
                field: 'site_id',
                allowNull: true
            },
            latest_used_device: {
                type: Sequelize.STRING,
                field: 'latest_used_device',
                allowNull: true
            },
            disqualificationReason: {
                type: Sequelize.STRING,
                field: 'disqualify_reason',
                allowNull: true
            },
            user_defined_participant_id: {
                type: Sequelize.STRING,
                field: 'user_defined_participant_id',
                allowNull: true
            }
        }, {
            // schema: 'public',
            tableName: 'participant',
            timestamps: false
        }
    );

    const ParticipantMeta = getParticipantMetaModel(_sequelize);

    Participant.hasMany(ParticipantMeta, { foreignKey: 'pid', targetKey: 'id' });

    return Participant;
}

module.exports.getParticipantModel = getParticipantModel;