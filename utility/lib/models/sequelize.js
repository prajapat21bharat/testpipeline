'use strict';

const Sequelize = require('sequelize');

function getSequelize(_sequelize) {
    return _sequelize;
}

module.exports.getSequelize = getSequelize;