'use strict';

const Sequelize = require('sequelize');
// const uuid = require('uuidv4');

function getParticipantStatusHistoryModel(_sequelize) {
    const ParticipantStatusHistory = _sequelize.define(
        'participant_status_history', {
            id: {
                type: Sequelize.DataTypes.UUID,
                field: 'id',
                allowNull: false,
                primaryKey: true,
                // defaultValue: uuid(),
            },
            participant_id: {
                type: Sequelize.STRING(45),
                field: 'participant_id',
                allowNull: true
            },
            study_id: {
                type: Sequelize.STRING(45),
                field: 'study_id',
                allowNull: true
            },
            old_status: {
                type: Sequelize.STRING,
                field: 'old_status',
                allowNull: true
            },
            new_status: {
                type: Sequelize.STRING,
                field: 'new_status',
                allowNull: true
            },
            modified_time: {
                type: Sequelize.NOW,
                field: 'modified_time',
                allowNull: true
            },
            modified_by: {
                type: Sequelize.STRING,
                field: 'modified_by',
                allowNull: true
            },
            participant_local_time: {
                type: Sequelize.STRING,
                field: 'participant_local_time',
                allowNull: true
            },
            participant_local_time_offset: {
                type: Sequelize.STRING,
                field: 'participant_local_time_offset',
                allowNull: true
            }
        }, {
            // schema: 'public',
            tableName: 'participant_status_history',
            timestamps: false
        }
    );

    return ParticipantStatusHistory;
}

module.exports.getParticipantStatusHistoryModel = getParticipantStatusHistoryModel;