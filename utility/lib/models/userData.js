'use strict';

const Sequelize = require('sequelize');
// const uuid = require('uuidv4');

function getUserDataModel(_sequelize) {
    const User = _sequelize.define(
        'user', {
        id: {
            type: Sequelize.DataTypes.UUID,
            field: 'id',
            allowNull: false,
            primaryKey: true,
            // defaultValue: uuid(),
        },
        account_enabled: {
            type: Sequelize.STRING,
            field: 'account_enabled',
            allowNull: true
        },
        account_expired: {
            type: Sequelize.STRING,
            field: 'account_expired',
            allowNull: true
        },
        account_locked: {
            type: Sequelize.STRING,
            field: 'account_locked',
            allowNull: true
        },
        attempts: {
            type: Sequelize.INTEGER,
            field: 'attempts',
            allowNull: true
        },
        created_at: {
            type: Sequelize.DATE,
            field: 'created_at',
            allowNull: true
        },
        created_by: {
            type: Sequelize.STRING,
            field: 'created_by',
            allowNull: true
        },
        credentials_non_expired: {
            type: Sequelize.STRING,
            field: 'credentials_non_expired',
            allowNull: true
        },
        date_of_birth: {
            type: Sequelize.BIGINT,
            field: 'date_of_birth',
            allowNull: true
        },
        first_name: {
            type: Sequelize.STRING,
            field: 'first_name',
            allowNull: true
        },
        last_name: {
            type: Sequelize.STRING,
            field: 'last_name',
            allowNull: true
        },
        modified_at: {
            type: Sequelize.DATE,
            field: 'modified_at',
            allowNull: true
        },
        modified_by: {
            type: Sequelize.STRING,
            field: 'modified_by',
            allowNull: true
        },
        password: {
            type: Sequelize.STRING,
            field: 'password',
            allowNull: true
        },
        password_expiry_date: {
            type: Sequelize.BIGINT,
            field: 'password_expiry_date',
            allowNull: true
        },
        patient_research_study_code: {
            type: Sequelize.STRING,
            field: 'patient_research_study_code',
            allowNull: true
        },
        phone_number: {
            type: Sequelize.STRING,
            field: 'phone_number',
            allowNull: true
        },
        pin_expiry: {
            type: Sequelize.DATE,
            field: 'pin_expiry',
            allowNull: true
        },
        previous_passwords: {
            type: Sequelize.STRING,
            field: 'previous_passwords',
            allowNull: true
        },
        profile_image: {
            type: Sequelize.BLOB("long"),
            field: 'profile_image',
            allowNull: true
        },
        status: {
            type: Sequelize.STRING,
            field: 'status',
            allowNull: true
        },
        user_role: {
            type: Sequelize.STRING,
            field: 'user_role',
            allowNull: true
        },
        username: {
            type: Sequelize.STRING,
            field: 'username',
            allowNull: true
        },
        work_number: {
            type: Sequelize.STRING,
            field: 'work_number',
            allowNull: true
        },
        profile_completed: {
            type: Sequelize.STRING,
            field: 'profile_completed',
            allowNull: true
        },
        otp_verified: {
            type: Sequelize.STRING,
            field: 'otp_verified',
            allowNull: true
        },
        on_boarding: {
            type: Sequelize.STRING,
            field: 'on_boarding',
            allowNull: true
        },
        client_id: {
            type: Sequelize.STRING,
            field: 'client_id',
            allowNull: true
        },
        site_id: {
            type: Sequelize.STRING,
            field: 'site_id',
            allowNull: true
        }
    }, {
        // schema: 'public',
        tableName: 'user',
        timestamps: false
    }
    );

    return User;
}

module.exports.getUserDataModel = getUserDataModel;