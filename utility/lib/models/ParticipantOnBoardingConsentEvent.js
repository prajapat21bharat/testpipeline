'use strict';

const Sequelize = require('sequelize');

function getParticipantOnBoardingConsentEventDataModel(_sequelize) {
    const participantOnBoardingConsentEvent = _sequelize.define(
        'participant_on_boarding_consent_event', {

            id: {
                type: Sequelize.DataTypes.UUID,
                field: 'Id',
                allowNull: false,
                primaryKey: true,
            },
            participantOnBoardingEventId: {
                type: Sequelize.STRING,
                field: 'participant_on_boarding_event_id',
                allowNull: true
            },
            cohortId: {
                type: Sequelize.STRING,
                field: 'cohort_Id',
                allowNull: true
            },
            consentId: {
                type: Sequelize.STRING,
                field: 'consent_Id',
                allowNull: true
            },
            consentStartTime: {
                type: Sequelize.DATE,
                field: 'consent_start_time',
                allowNull: true
            },
            consentEndTime: {
                type: Sequelize.DATE,
                field: 'consent_end_time',
                allowNull: true
            },
            participantLocalConsentStartTime: {
                type: Sequelize.STRING,
                field: 'participant_local_consent_start_time',
                allowNull: true
            },
            participantLocalConsentEndTime: {
                type: Sequelize.STRING,
                field: 'participant_local_consent_end_time',
                allowNull: true
            },
            participantLocalConsentTimeOffset: {
                type: Sequelize.STRING,
                field: 'participant_local_consent_time_offset',
                allowNull: true
            }
        }, {
            // schema: 'public',
            tableName: 'participant_on_boarding_consent_event',
            timestamps: false
        }
    );
    
    return participantOnBoardingConsentEvent;
}

module.exports.getParticipantOnBoardingConsentEventDataModel = getParticipantOnBoardingConsentEventDataModel;