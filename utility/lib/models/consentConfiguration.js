'use strict';

const Sequelize = require('sequelize');

function getConsentConfigurationModel(_sequelize) {
    const consentConfiguration = _sequelize.define(
        'consent_configuration', {
            id: {
                type: Sequelize.DataTypes.UUID,
                field: 'id',
                allowNull: false,
                primaryKey: true,
            },
            consentId: {
                type: Sequelize.STRING,
                field: 'consent_id',
                allowNull: true
            },
            studyId: {
                type: Sequelize.STRING,
                field: 'study_id',
                allowNull: true
            },
            consentName: {
                type: Sequelize.STRING,
                field: 'consent_name',
                allowNull: true
            },
            consentBucketLocation: {
                type: Sequelize.STRING,
                field: 'consent_bucket_location',
                allowNull: true
            },
            createdTime: {
                type: Sequelize.DATE,
                field: 'created_time',
                allowNull: true
            },
            status: {
                type: Sequelize.BOOLEAN,
                field: 'status',
                allowNull: true
            }

        }, {
            tableName: 'consent_configuration',
            timestamps: false
        }
    );

    return consentConfiguration;
}

module.exports.getConsentConfigurationModel = getConsentConfigurationModel;