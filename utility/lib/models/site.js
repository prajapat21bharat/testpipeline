'use strict';

const Sequelize = require('sequelize');
// const uuid = require('uuidv4');

function getSiteModel(_sequelize) {
    const Site = _sequelize.define(
        'site', {
            id: {
                type: Sequelize.DataTypes.UUID,
                field: 'id',
                allowNull: false,
                primaryKey: true,
                // defaultValue: uuid(),
            },
            createdBy: {
                type: Sequelize.STRING,
                field: 'created_by',
                allowNull: true
            },
            createdTime: {
                type: Sequelize.STRING,
                field: 'created_time',
                allowNull: true
            },
            modifiedBy: {
                type: Sequelize.STRING,
                field: 'modified_by',
                allowNull: true
            },
            modifiedTime: {
                type: Sequelize.STRING,
                field: 'modified_time',
                allowNull: true
            },
            name: {
                type: Sequelize.STRING,
                field: 'name',
                allowNull: true
            },
            status: {
                type: Sequelize.STRING,
                field: 'status',
                allowNull: true
            },
            type: {
                type: Sequelize.STRING,
                field: 'type',
                allowNull: true
            },
            piId: {
                type: Sequelize.STRING,
                field: 'pi_id',
                allowNull: true
            },
            siteId: {
                type: Sequelize.STRING,
                field: 'site_id',
                allowNull: true
            },
            address: {
                type: Sequelize.STRING,
                field: 'address',
                allowNull: true
            },
            zipcode: {
                type: Sequelize.STRING,
                field: 'zipcode',
                allowNull: true
            },
            country: {
                type: Sequelize.STRING,
                field: 'country',
                allowNull: true
            },
            phoneNumber: {
                type: Sequelize.STRING,
                field: 'phone_number',
                allowNull: true
            }
        }, {
            // schema: 'public',
            tableName: 'site',
            timestamps: false
        }
    );

    return Site;
}

module.exports.getSiteModel = getSiteModel;