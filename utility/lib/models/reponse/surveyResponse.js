'use strict';

const Sequelize = require('sequelize');

function getSurveyResponseDataModel(_sequelize) {
    const surveyResponse = _sequelize.define(
        'survey_response', {
            id: {
                type: Sequelize.DataTypes.UUID,
                field: 'id',
                allowNull: false,
                primaryKey: true,
            },
            answerText: {
                type: Sequelize.TEXT('long'),
                field: 'answer_text',
                allowNull: true
            },
            endTime: {
                type: Sequelize.DATE,
                field: 'end_time',
                allowNull: true
            },
            isSkipped: {
                type: Sequelize.BOOLEAN,
                field: 'is_skipped',
                allowNull: true
            },
            optionId: {
                type: Sequelize.STRING,
                field: 'option_id',
                allowNull: true
            },
            participantId: {
                type: Sequelize.STRING,
                field: 'participant_id',
                allowNull: true
            },
            questionId: {
                type: Sequelize.STRING,
                field: 'question_id',
                allowNull: true
            },
            questionType: {
                type: Sequelize.STRING,
                field: 'question_type',
                allowNull: true
            },
            startTime: {
                type: Sequelize.DATE,
                field: 'start_time',
                allowNull: true
            },
            status: {
                type: Sequelize.STRING,
                field: 'status',
                allowNull: true
            },
            studyCode: {
                type: Sequelize.STRING,
                field: 'study_code',
                allowNull: true
            },
            studyId: {
                type: Sequelize.STRING,
                field: 'study_id',
                allowNull: true
            },
            surveyId: {
                type: Sequelize.STRING,
                field: 'survey_id',
                allowNull: true
            },
            timeTaken: {
                type: Sequelize.STRING,
                field: 'time_taken',
                allowNull: true
            },
            siteId: {
                type: Sequelize.STRING,
                field: 'site_id',
                allowNull: true
            },
            surveyResponseTrackerId: {
                type: Sequelize.STRING,
                field: 'survey_response_tracker_id',
                allowNull: true
            },
            deviceType: {
                type: Sequelize.STRING,
                field: 'device_type',
                allowNull: true
            },
            startTimeLocal: {
                type: Sequelize.STRING,
                field: 'participant_local_start_time',
                allowNull: true
            },
            endTimeLocal: {
                type: Sequelize.STRING,
                field: 'participant_local_end_time',
                allowNull: true
            },
            localTimeOffset: {
                type: Sequelize.STRING,
                field: 'participant_local_time_offset',
                allowNull: true
            },
            appLanguageId: {
                type: Sequelize.STRING,
                field: 'app_language_id',
                allowNull: true
            },
            displayLanguage: {
                type: Sequelize.STRING,
                field: 'display_language',
                allowNull: true
            },
            questionText: {
                type: Sequelize.STRING,
                field: 'question_text',
                allowNull: true
            },
            displayQuestionText: {
                type: Sequelize.STRING,
                field: 'display_question_text',
                allowNull: true
            },
            displayAnswerText: {
                type: Sequelize.STRING,
                field: 'display_answer_text',
                allowNull: true
            }
        }, {
            // schema: 'public',
            tableName: 'survey_response',
            timestamps: false
        }
    );

    return surveyResponse;
}

module.exports.getSurveyResponseDataModel = getSurveyResponseDataModel;