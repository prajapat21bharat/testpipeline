'use strict';

const Sequelize = require('sequelize');

function getActivityResponseDataModel(_sequelize) {
    const activityResponse = _sequelize.define(
        'activity_response', {
            id: {
                type: Sequelize.DataTypes.UUID,
                field: 'id',
                allowNull: false,
                primaryKey: true,
            },
            activityId: {
                type: Sequelize.STRING,
                field: 'activity_id',
                allowNull: true
            },
            activityName: {
                type: Sequelize.STRING,
                field: 'activity_name',
                allowNull: true
            },
            endTime: {
                type: Sequelize.DATE,
                field: 'end_time',
                allowNull: true
            },
            participantId: {
                type: Sequelize.STRING,
                field: 'participant_id',
                allowNull: true
            },
            responseData: {
                type: Sequelize.TEXT,
                field: 'response_data',
                allowNull: true
            },
            startTime: {
                type: Sequelize.DATE,
                field: 'start_time',
                allowNull: true
            },
            status: {
                type: Sequelize.STRING,
                field: 'status',
                allowNull: true
            },
            studyCode: {
                type: Sequelize.STRING,
                field: 'study_code',
                allowNull: true
            },
            studyId: {
                type: Sequelize.STRING,
                field: 'study_id',
                allowNull: true
            },
            timeTaken: {
                type: Sequelize.STRING,
                field: 'time_taken',
                allowNull: true
            },
            activityKey: {
                type: Sequelize.STRING,
                field: 'activity_key',
                allowNull: true
            },
            siteId: {
                type: Sequelize.STRING,
                field: 'site_id',
                allowNull: true
            },
            participantLocalStartTime: {
                type: Sequelize.STRING,
                field: 'participant_local_start_time',
                allowNull: true
            },
            participantLocalEndTime: {
                type: Sequelize.STRING,
                field: 'participant_local_end_time',
                allowNull: true
            },
            participantLocalTimeOffset: {
                type: Sequelize.STRING,
                field: 'participant_local_time_offset',
                allowNull: true
            }
        }, {
            // schema: 'public',
            tableName: 'activity_response',
            timestamps: false
        }
    );

    return activityResponse;
}

module.exports.getActivityResponseDataModel = getActivityResponseDataModel;