'use strict';

const Sequelize = require('sequelize');

function getFeedbackResponseDataModel(_sequelize) {
    const feedbackResponse = _sequelize.define(
        'feedback_response', {
            id: {
                type: Sequelize.DataTypes.UUID,
                field: 'id',
                allowNull: false,
                primaryKey: true,
            },
            answerText: {
                type: Sequelize.STRING,
                field: 'answer_text',
                allowNull: true
            },
            participantId: {
                type: Sequelize.STRING,
                field: 'participant_id',
                allowNull: true
            },
            questionId: {
                type: Sequelize.STRING,
                field: 'question_id',
                allowNull: true
            },
            studyId: {
                type: Sequelize.STRING,
                field: 'study_id',
                allowNull: true
            },
            responseTime: {
                type: Sequelize.DATE,
                field: 'response_time',
                allowNull: true
            },
            questionType: {
                type: Sequelize.STRING,
                field: 'question_type',
                allowNull: true
            },
            siteId: {
                type: Sequelize.STRING,
                field: 'site_id',
                allowNull: true
            },
            appLanguageId: {
                type: Sequelize.STRING,
                field: 'app_language_id',
                allowNull: true
            },
            displayLanguage: {
                type: Sequelize.STRING,
                field: 'display_language',
                allowNull: true
            },
            questionText: {
                type: Sequelize.STRING,
                field: 'question_text',
                allowNull: true
            },
            displayQuestionText: {
                type: Sequelize.STRING,
                field: 'display_question_text',
                allowNull: true
            },
            displayAnswerText: {
                type: Sequelize.STRING,
                field: 'display_answer_text',
                allowNull: true
            }
        }, {
            // schema: 'public',
            tableName: 'feed_back_response',
            timestamps: false
        }
    );

    return feedbackResponse;
}

module.exports.getFeedbackResponseDataModel = getFeedbackResponseDataModel;