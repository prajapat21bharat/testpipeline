'use strict';

const Sequelize = require('sequelize');
// const { getSurveyResponseDataModel } = require('./surveyResponse');

function getSurveyResponseTrackerModel(_sequelize) {
    const surveyResponseTracker = _sequelize.define(
        'survey_response_tracker', {
            id: {
                type: Sequelize.DataTypes.UUID,
                field: 'id',
                allowNull: false,
                primaryKey: true,
            },
            survey_id: {
                type: Sequelize.STRING,
                field: 'survey_id',
                allowNull: true
            },
            study_id: {
                type: Sequelize.STRING,
                field: 'study_id',
                allowNull: true
            },
            participant_id: {
                type: Sequelize.STRING,
                field: 'participant_id',
                allowNull: true
            },
            response_time: {
                type: Sequelize.DATE,
                field: 'response_time',
                allowNull: true
            }
        }, {
            // schema: 'public',
            tableName: 'survey_response_tracker',
            timestamps: false
        }
    );

    // const SurveyResponse = getSurveyResponseDataModel(_sequelize);

    // surveyResponseTracker.hasMany(SurveyResponse, { foreignKey: 'survey_response_tracker_id', targetKey: 'id' });
    return surveyResponseTracker;
}

module.exports.getSurveyResponseTrackerModel = getSurveyResponseTrackerModel;