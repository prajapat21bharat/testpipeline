'use strict';

const Sequelize = require('sequelize');

function getHealthResponseDataModel(_sequelize) {
    const healthResponse = _sequelize.define(
        'health_data_response', {
            id: {
                type: Sequelize.DataTypes.UUID,
                field: 'id',
                allowNull: false,
                primaryKey: true,
            },
            studyId: {
                type: Sequelize.STRING,
                field: 'study_id',
                allowNull: true
            },
            participantId: {
                type: Sequelize.STRING,
                field: 'participant_id',
                allowNull: true
            },
            response_date: {
                type: Sequelize.STRING,
                field: 'response_date',
                allowNull: true
            },
            response_data: {
                type: Sequelize.STRING,
                field: 'response_data',
                allowNull: true
            },
            created_time: {
                type: Sequelize.STRING,
                field: 'created_time',
                allowNull: true
            },
            site_id: {
                type: Sequelize.STRING,
                field: 'site_id',
                allowNull: true
            }
        }, {
            // schema: 'public',
            tableName: 'health_data_response',
            timestamps: false
        }
    );

    return healthResponse;
}

module.exports.getHealthResponseDataModel = getHealthResponseDataModel;