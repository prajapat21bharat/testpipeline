'use strict';

const Sequelize = require('sequelize');

function getVisitNoteDataModel(_sequelize) {
    const visitNote = _sequelize.define(
        'visit_note', {
            // id: {
            //     type: Sequelize.DataTypes.UUID,
            //     field: 'id',
            //     allowNull: false,
            //     primaryKey: true,
            // },
            threadVisitId: {
                type: Sequelize.STRING,
                field: 'thread_visit_id',
                allowNull: true
            },
            participantId: {
                type: Sequelize.STRING,
                field: 'participant_id',
                allowNull: true
            },
            reponses: {
                type: Sequelize.TEXT('long'),
                field: 'reponses',
                allowNull: true
            },
            studyId: {
                type: Sequelize.STRING,
                field: 'study_id',
                allowNull: true
            },
            truClinicVisitId: {
                type: Sequelize.STRING,
                field: 'tru_clinic_visit_id',
                allowNull: true
            },
            createdBy: {
                type: Sequelize.STRING,
                field: 'created_by',
                allowNull: true
            },
            createdTime: {
                type: Sequelize.DATE,
                field: 'created_time',
                allowNull: true
            },
            modifiedBy: {
                type: Sequelize.STRING,
                field: 'modified_by',
                allowNull: true
            },
            modifiedTime: {
                type: Sequelize.DATE,
                field: 'modified_time',
                allowNull: true
            },
            siteId: {
                type: Sequelize.STRING,
                field: 'site_id',
                allowNull: true
            },
        }, {
            // schema: 'public',
            tableName: 'visit_note',
            timestamps: false
        }
    );

    visitNote.removeAttribute('id');
    return visitNote;
}

module.exports.getVisitNoteDataModel = getVisitNoteDataModel;