'use strict';

const Sequelize = require('sequelize');

function getEdcResponseVersionDataModel(_sequelize) {
    const edcResponseVersion = _sequelize.define(
        'edc_response_version', {
        id: {
            type: Sequelize.DataTypes.UUID,
            field: 'id',
            allowNull: false,
            primaryKey: true,
        },
        edc_response_id: {
            type: Sequelize.STRING,
            field: 'edc_response_id',
            allowNull: true
        },
        version: {
            type: Sequelize.INTEGER,
            field: 'version',
            allowNull: true
        },
        response_s3_version: {
            type: Sequelize.TEXT,
            field: 'response_s3_version',
            allowNull: true
        },
        response_s3_key: {
            type: Sequelize.STRING,
            field: 'response_s3_key',
            allowNull: true
        },
        parent_id: {
            type: Sequelize.STRING,
            field: 'parent_id',
            allowNull: true
        },
        created_by: {
            type: Sequelize.DATE,
            field: 'created_by',
            allowNull: true
        },
        created_at: {
            type: Sequelize.DATE,
            field: 'created_at',
            allowNull: true
        },
        state: {
            type: Sequelize.STRING,
            field: 'state',
            allowNull: true
        },
        start_date: {
            type: Sequelize.DATE,
            field: 'start_date',
            allowNull: true
        },
        end_date: {
            type: Sequelize.DATE,
            field: 'end_date',
            allowNull: true
        },
        primary_reason: {
            type: Sequelize.TEXT,
            field: 'primary_reason',
            allowNull: true
        },
        reason_comment: {
            type: Sequelize.TEXT,
            field: 'reason_comment',
            allowNull: true
        },
        verification_status: {
            type: Sequelize.STRING,
            field: 'verification_status',
            allowNull: true
        },
        approve_status: {
            type: Sequelize.STRING,
            field: 'approve_status',
            allowNull: true
        },
        site_id: {
            type: Sequelize.STRING,
            field: 'site_id',
            allowNull: true
        },
        data: {
            type: Sequelize.TEXT,
            field: 'data',
            allowNull: true
        }
    }, {
        // schema: 'public',
        tableName: 'edc_response_version',
        timestamps: false
    }
    );

    return edcResponseVersion;
}

module.exports.getEdcResponseVersionDataModel = getEdcResponseVersionDataModel;