'use strict';

const Sequelize = require('sequelize');

function getValidicDataModel(_sequelize) {
    const ValidicData = _sequelize.define(
        'validic_data', {
            id: {
                type: Sequelize.DataTypes.UUID,
                field: 'id',
                allowNull: false,
                primaryKey: true,
            },
            dataCaptureStartTime: {
                type: Sequelize.DATE,
                field: 'data_capture_start_time',
                allowNull: true
            },
            dataCaptureEndTime: {
                type: Sequelize.DATE,
                field: 'data_capture_end_time',
                allowNull: true
            },
            participantId: {
                type: Sequelize.STRING,
                field: 'participant_id',
                allowNull: true
            },
            studyId: {
                type: Sequelize.STRING,
                field: 'study_id',
                allowNull: true
            },
            dataObjectType: {
                type: Sequelize.STRING,
                field: 'data_object_type',
                allowNull: true
            },
            data: {
                type: Sequelize.TEXT('long'),
                field: 'data',
                allowNull: true
            },
            siteId: {
                type: Sequelize.STRING,
                field: 'site_id',
                allowNull: true
            }
        }, {
            // schema: 'public',
            tableName: 'validic_data',
            timestamps: false
        }
    );

    return ValidicData;
}

module.exports.getValidicDataModel = getValidicDataModel;