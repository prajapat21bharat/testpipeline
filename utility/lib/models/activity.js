'use strict';

const Sequelize = require('sequelize');

// const { getActivityResponseDataModel } = require('./reponse/activityResponse');

function getActivityDataModel(_sequelize) {
    const Activity = _sequelize.define(
        'activity', {
            id: {
                type: Sequelize.DataTypes.UUID,
                field: 'id',
                allowNull: false,
                primaryKey: true,
            },
            name: {
                type: Sequelize.STRING,
                field: 'name',
                allowNull: true
            },
            type: {
                type: Sequelize.STRING,
                field: 'type',
                allowNull: true
            }
        }, {
            // schema: 'public',
            tableName: 'activity',
            timestamps: false
        }
    );

    // const ActivityResponse = getActivityResponseDataModel(_sequelize);

    // Activity.hasMany(ActivityResponse, { foreignKey: 'activityId', targetKey: 'id' });
    return Activity;
}

module.exports.getActivityDataModel = getActivityDataModel;