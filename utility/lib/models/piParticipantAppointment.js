'use strict';

const Sequelize = require('sequelize');
//const { getParticipantModel } = require('./participant');
function getPiParticipantAppointmentDataModel(_sequelize) {
    const piParticipantAppointment = _sequelize.define(
        'pi_participant_appointment', {
        id: {
            type: Sequelize.DataTypes.UUID,
            field: 'id',
            allowNull: false,
            primaryKey: true,
        },
        pi_id: {
            type: Sequelize.STRING,
            field: 'pi_id',
            allowNull: true
        },
        participant_id: {
            type: Sequelize.STRING,
            field: 'participant_id',
            allowNull: true
        },
        site_id: {
            type: Sequelize.STRING,
            field: 'site_id',
            allowNull: true
        },
        study_id: {
            type: Sequelize.STRING,
            field: 'study_id',
            allowNull: true
        },
        visit_id: {
            type: Sequelize.STRING,
            field: 'visit_id',
            allowNull: true
        },
        appointment_date: {
            type: Sequelize.DATE,
            field: 'appointment_date',
            allowNull: true
        },
        start_time: {
            type: Sequelize.DATE,
            field: 'start_time',
            allowNull: true
        },
        end_time: {
            type: Sequelize.DATE,
            field: 'end_time',
            allowNull: true
        },
        pi_availability_id: {
            type: Sequelize.STRING,
            field: 'pi_availability_id',
            allowNull: true
        },
        pi_availability_slot: {
            type: Sequelize.STRING,
            field: 'pi_availability_slot',
            allowNull: true
        },
        status: {
            type: Sequelize.STRING,
            field: 'status',
            allowNull: true
        },
        created_by: {
            type: Sequelize.STRING,
            field: 'created_by',
            allowNull: true
        },
        created_time: {
            type: Sequelize.DATE,
            field: 'created_time',
            allowNull: true
        },
        modified_time: {
            type: Sequelize.DATE,
            field: 'modified_time',
            allowNull: true
        },
        modified_by: {
            type: Sequelize.STRING,
            field: 'modified_by',
            allowNull: true
        },
        type: {
            type: Sequelize.STRING,
            field: 'type',
            allowNull: true
        },
        visit_name: {
            type: Sequelize.STRING,
            field: 'visit_name',
            allowNull: true
        },
        task_instanceuuid: {
            type: Sequelize.STRING,
            field: 'task_instanceuuid',
            allowNull: true
        }
    }, {
        // schema: 'public',
        tableName: 'pi_participant_appointment',
        timestamps: false
    }
    );
    // const Participant = getParticipantModel(_sequelize);
    // piParticipantAppointment.belongsTo(Participant, { foreignKey: 'participant_id', sourceKey: 'id' });
    return piParticipantAppointment;
}

module.exports.getPiParticipantAppointmentDataModel = getPiParticipantAppointmentDataModel;