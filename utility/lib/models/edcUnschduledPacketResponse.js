'use strict';

const Sequelize = require('sequelize');
const { getEdcUnschduledPacketResponseVersionDataModel } = require('./edcUnschduledPacketResponseVersion');
function getEdcUnschduledPacketResponseDataModel(_sequelize) {
    const edcUnschduledPacketResponse = _sequelize.define(
        'edc_unscheduled_packet_response', {
        id: {
            type: Sequelize.DataTypes.UUID,
            field: 'id',
            allowNull: false,
            primaryKey: true,
        },
        response_s3_key: {
            type: Sequelize.TEXT('long'),
            field: 'response_s3_key',
            allowNull: true
        },
        study_id: {
            type: Sequelize.STRING,
            field: 'study_id',
            allowNull: true
        },
        participant_id: {
            type: Sequelize.STRING,
            field: 'participant_id',
            allowNull: true
        },
        state: {
            type: Sequelize.STRING,
            field: 'state',
            allowNull: true
        },
        createdAt: {
            type: Sequelize.DATE,
            field: 'created_at',
            allowNull: true
        },
        submitted_at: {
            type: Sequelize.DATE,
            field: 'submitted_at',
            allowNull: true
        },
        edc_unsch_pack_id: {
            type: Sequelize.STRING,
            field: 'edc_unsch_pack_id',
            allowNull: true
        },
        edc_unsch_pack_ver_id: {
            type: Sequelize.STRING,
            field: 'edc_unsch_pack_ver_id',
            allowNull: true
        },
        edc_unscheduled_packet_response_version_id: {
            type: Sequelize.STRING,
            field: 'edc_unscheduled_packet_response_version_id',
            allowNull: true
        },
        created_by: {
            type: Sequelize.STRING,
            field: 'created_by',
            allowNull: true
        }
    }, {
        // schema: 'public',
        tableName: 'edc_unscheduled_packet_response',
        timestamps: false
    }
    );
    const edcUnschduledPacketResponseVersion = getEdcUnschduledPacketResponseVersionDataModel(_sequelize);

    edcUnschduledPacketResponse.belongsTo(edcUnschduledPacketResponseVersion, { foreignKey: 'edc_unscheduled_packet_response_version_id', targetKey: 'id' });
    return edcUnschduledPacketResponse;
}

module.exports.getEdcUnschduledPacketResponseDataModel = getEdcUnschduledPacketResponseDataModel;