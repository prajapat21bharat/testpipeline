'use strict';

const Sequelize = require('sequelize');

function getOnBoardingResponseDataModel(_sequelize) {
    const onBoardingResponse = _sequelize.define(
        'on_boarding_response', {
        id: {
            type: Sequelize.DataTypes.UUID,
            field: 'id',
            allowNull: false,
            primaryKey: true,
        },
        createdTime: {
            type: Sequelize.TEXT('long'),
            field: 'created_time',
            allowNull: true
        },
        deviceId: {
            type: Sequelize.STRING,
            field: 'device_id',
            allowNull: true
        },
        participantId: {
            type: Sequelize.STRING,
            field: 'participant_id',
            allowNull: true
        },
        questionAnswer: {
            type: Sequelize.STRING,
            field: 'question_answer',
            allowNull: true
        },
        questionBody: {
            type: Sequelize.STRING,
            field: 'question_body',
            allowNull: true
        },
        questionId: {
            type: Sequelize.STRING,
            field: 'question_id',
            allowNull: true
        },
        questionType: {
            type: Sequelize.STRING,
            field: 'question_type',
            allowNull: true
        },
        studyId: {
            type: Sequelize.STRING,
            field: 'study_id',
            allowNull: true
        },
        type: {
            type: Sequelize.STRING,
            field: 'type',
            allowNull: true
        },
        // userDefinedParticipantId: {
        //     type: Sequelize.STRING,
        //     field: 'userDefinedParticipantId',
        //     allowNull: true
        // },
        siteId: {
            type: Sequelize.STRING,
            field: 'site_id',
            allowNull: true
        },
        // siteName: {
        //     type: Sequelize.STRING,
        //     field: 'siteName',
        //     allowNull: true
        // },
        appLanguageId: {
            type: Sequelize.STRING,
            field: 'app_language_id',
            allowNull: true
        },
        displayLanguage: {
            type: Sequelize.STRING,
            field: 'display_language',
            allowNull: true
        },
        displayQuestionBody: {
            type: Sequelize.STRING,
            field: 'display_question_body',
            allowNull: true
        },
        displayQuestionAnswer: {
            type: Sequelize.STRING,
            field: 'display_question_answer',
            allowNull: true
        },

    }, {
        // schema: 'public',
        tableName: 'on_boarding_response',
        timestamps: false
    }
    );

    return onBoardingResponse;
}

module.exports.getOnBoardingResponseDataModel = getOnBoardingResponseDataModel;