'use strict';

const axios = require('axios');
const constants = require('../../../constants').constants;

async function getTokenizerSessionToken() {
    return new Promise((resolve, reject) => {
        console.log('inside getTokenizerSessionToken');
        let url = `${constants.tokenizer.Url}`;
        const sessionPayload = {
            APIKey: constants.tokenizer.APIKey,
            Policy: constants.tokenizer.Policy,
            PolicyPassword: constants.tokenizer.PolicyPassword
        }

        const instance = axios.create({
            baseURL: url,
            headers: {
                'Content-Type': 'application/json'
            }
        });

        return instance
            .post('/createsession', sessionPayload)
            .then((response) => {
                resolve(response.data.SessionToken);
            })
            .catch((error) => {
                console.log(error);
            });
    })
}

async function deTokenize(payload) {
    return new Promise((resolve, reject) => {
        console.log('inside deTokenize');
        let url = `${constants.tokenizer.Url}`;

        const instance = axios.create({
            baseURL: url,
            headers: {
                'Content-Type': 'application/json'
            }
        });

        return instance
            .post('/detokenize', payload)
            .then((response) => {
                resolve(response.data.Value);
            })
            .catch((error) => {
                console.log(error);
            });
    })
}


exports.getTokenizerSessionToken = getTokenizerSessionToken;
exports.deTokenize = deTokenize;