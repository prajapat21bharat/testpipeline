'use strict';

const sgMail = require('@sendgrid/mail');
const AWS = require('aws-sdk');

const client_secrets_manager = new AWS.SecretsManager();

async function getSendGridApiKey() {
    try {
        const environment = process.env.ENVIRONMENT.replace(/^\w/, c => c.toUpperCase());

        const params = {
            SecretId: `${environment}/Sendgrid`, // name of secrete
            // VersionStage: 'AWSPENDING',  //optional
        };
        const data = await client_secrets_manager.getSecretValue(params).promise();
        const secretString = JSON.parse(data['SecretString']);

        console.log('RETRIEVED PENDING Sendgrid');
        return secretString.apiKey;
    } catch (error) {
        console.log('Sendgrid error', error);
    }
}

async function sendMail(mailObject, substitutions, templateId) {
    try {
        const sendGridKey = await getSendGridApiKey();
        sgMail.setApiKey(sendGridKey);

        sgMail.setSubstitutionWrappers('{{', '}}'); // Configure the substitution tag wrappers globally
        sgMail.setSubstitutionWrappers('-', '-');

        const msg = {
            to: mailObject.to,
            from: mailObject.from,
            subject: mailObject.subject,
            templateId: templateId,
            substitutions: substitutions,
        };

        const mailResponse = sgMail.send(msg);

        return mailResponse;
    } catch (error) {
        console.log('mail error', error);
        return false;
    }
}

exports.sendMail = sendMail;