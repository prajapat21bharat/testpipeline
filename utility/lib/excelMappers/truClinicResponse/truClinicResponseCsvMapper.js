'use strict';

const JM = require('json-mapper');
const util = require('../../util');

const truClinicResponseCsvMapper = JM.makeConverter({
    "STUDY ID": function(i) {
        if (i && i.study_id) {
            return i.study_id;
        }
        return "";
    },
    "PRINCIPAL INVESTIGATOR NAME": function(i) {
        if (i && i.piName) {
            return i.piName;
        }
        return "";
    },
    "PRINCIPAL INVESTIGATOR ID": function(i) {
        if (i && i.piId) {
            return i.piId;
        }
        return "";
    },
    "SITE ID": function(i) {
        if (i && i.siteId) {
            return i.siteId;
        }
        return "";
    },
    "SITE NAME": function(i) {
        if (i && i.siteName) {
            return i.siteName;
        }
        return "";
    },
    "THREAD PARTICIPANT ID": function(i) {
        if (i && i.participant_id) {
            return i.participant_id;
        }
        return "";
    },
    "PARTICIPANT ID": function(i) {
        if (i && i.userDefinedParticipantId) {
            return i.userDefinedParticipantId;
        }
        return "";
    },
    "SCHEDULED VISIT DATE & TIME": function(i) {
        if (i && i.tru_clinic_appointment_time_stamp) {

            // const appointmentTime = util.changeDateFormat(util.getGmtTime(i.tru_clinic_appointment_time_stamp, 'YYYY-MM-DD HH:mm:ss'), 'YYYY-MM-DD HH:mm:ss');
            const appointmentTime = (i.tru_clinic_appointment_time_stamp) ? util.changeDateFormat(util.utcToGmt(i.tru_clinic_appointment_time_stamp), 'YYYY-MM-DD HH:mm:ss') : '';

            //changeDateFormat(date, format = 'YYYY-MM-DD HH:mm:ss A')
            return (i.tru_clinic_appointment_time_stamp) ? util.changeDateFormat(util.utcToGmt(i.tru_clinic_appointment_time_stamp), 'YYYY-MM-DD HH:mm:ss.0') : '';
        }
        return "";
    },
    "VISIT CANCELLED": function(i) {
        if (i && i.is_cancelled) {
            return (i.is_cancelled) ? "Y" : "N";
        }
        return "N";
    },
    "VISIT TYPE": function(i) {
        if (i && i.visit_type) {
            return i.visit_type;
        }
        return "";
    },
    "VISIT STUDY DAY": function(i) {
        if (i.completion_window) {
            if (!i.day_of_visit) {
                return 0;
            } else {
                return i.day_of_visit
            }
        } else if (i.day_of_visit) {
            return i.day_of_visit
        } else {
            return "";
        }
    },
    "VISIT COMPLETION WINDOW": function(i) {
        if (i && i.completion_window) {
            return i.completion_window;
        }
        return "";
    }
});

exports.truClinicResponseCsvMapper = truClinicResponseCsvMapper;