'use strict';

const JM = require('json-mapper');
const util = require('../../util');

const telehealthExcelMapper = JM.makeConverter({
    StudyID: function (i) {
        if (i && i.study_id) {
            return i.study_id;
        }
        return "";
    },
    THREADParticipantID: function (i) {
        if (i && i.participant_id) {
            return i.participant_id;
        }
        return "";
    },
    ParticipantID: function (i) {
        if (i && i.userDefinedParticipantId) {
            return i.userDefinedParticipantId;
        }
        return "";
    },
    SiteName: function (i) {
        if (i && i.SiteName) {
            return i.SiteName;
        }
        return "";
    },
    SiteID: function (i) {
        if (i && i.siteId) {
            return i.siteId;
        }
        return "";
    },
    PIName: function (i) {
        if (i && i.piName) {
            return i.piName;
        }
        return "";
    },
    AppointmentType: function (i) {
        if (i && i.type) {
            return i.type;
        }
        return "";
    },
    AppointmentStatus: function (i) {
        if (i && i.status) {
            if (i.status && i.status.toLowerCase() === 'notstarted') {
                return "Upcoming";
            } else if (i.status && i.status.toLowerCase() === 'complete') {
                return "Completed";
            } else {
                return i.status;
            }
        }
        return "";
    },
    ScheduledStartTime: function (i) {
        if (i && i.start_time && i.type.toLowerCase() !== 'unscheduled') {
            let startTimeString = util.getISO8601(util.changeDateToIsoFormat(i.start_time));
            let startTime = (startTimeString && startTimeString.length > 10) ?
                startTimeString.slice(0, -5) + " 0000" : '';
            return (startTime);
        }
        return "";
    },
    ScheduledEndTime: function (i) {
        if (i && i.end_time && i.type.toLowerCase() !== 'unscheduled') {
            let endTimeString = util.getISO8601(util.changeDateToIsoFormat(i.end_time));
            let endTime = (endTimeString && endTimeString.length > 10) ?
                endTimeString.slice(0, -5) + " 0000" : '';
            return (endTime);
        }
        return "";
    },
    Scheduleduration: function (i) {
        if (i && i.start_time && i.end_time && i.type.toLowerCase() !== 'unscheduled') {
            return "" + util.getTimeDifference(i.start_time, i.end_time); //i.end_time;
        } else {
            return "N/A";
        }
    },
    CallStartTime: function (i) {
        if (i && i.session_start_time) {
            //return util.changeDateToIsoFormat(i.session_start_time); //i.session_start_time;
            let sessionStartTimeString = util.getISO8601(util.changeDateToIsoFormat(i.session_start_time));
            let sessionStartTime = (sessionStartTimeString && sessionStartTimeString.length > 10) ?
                sessionStartTimeString.slice(0, -5) + " 0000" : '';
            return (sessionStartTime);
        }
        return "";
    },
    CallEndTime: function (i) {
        if (i && i.session_end_time && i.status && i.status.toLowerCase() === 'complete') {
            //return util.changeDateToIsoFormat(i.session_end_time); //i.session_end_time;
            let sessionEndTimeString = util.getISO8601(util.changeDateToIsoFormat(i.session_end_time));
            let sessionEndTime = (sessionEndTimeString && sessionEndTimeString.length > 10) ?
                sessionEndTimeString.slice(0, -5) + " 0000" : '';
            return (sessionEndTime);
        }
        return "";
    },
    Actualduration: function (i) {
        if (i && i.session_start_time && i.session_end_time && i.status && i.status.toLowerCase() === 'complete') {
            return "" + util.getTimeDifference(i.session_start_time, i.session_end_time); //i.end_time;
        } else {
            return "";
        }
    },
    ConductedByName: function (i) {
        if (i && i.piName && i.status && i.status.toLowerCase() === 'complete') {
            return i.piName;
        }
        return "";
    },
    ConductedByRole: function (i) {
        if (i && i.user_role && i.status && i.status.toLowerCase() === 'complete') {
            return i.user_role;
        }
        return "";
    }
});
exports.telehealthExcelMapper = telehealthExcelMapper;