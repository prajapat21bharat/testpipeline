'use strict';
const { telehealthExcelMapper } = require('./telehealth/telehealthExcelMapper');
const { truClinicResponseCsvMapper } = require('./truClinicResponse/truClinicResponseCsvMapper');
exports.map = {
    telehealthExcelMapper,
    truClinicResponseCsvMapper
};