'use strict';

// need to change this

const Sequelize = require('sequelize');
// const AWS = require('aws-sdk');

const awsXRay = require('aws-xray-sdk');
const AWS = awsXRay.captureAWS(require('aws-sdk'));
const params = {
    region: process.env.REGION
}
const clientSecretsManager = new AWS.SecretsManager(params);

let sequelizeConnection;
const Op = Sequelize.Op;
const operatorsAliases = {
    $eq: Op.eq,
    $ne: Op.ne,
    $gte: Op.gte,
    $gt: Op.gt,
    $lte: Op.lte,
    $lt: Op.lt,
    $not: Op.not,
    $in: Op.in,
    $notIn: Op.notIn,
    $is: Op.is,
    $like: Op.like,
    $notLike: Op.notLike,
    $iLike: Op.iLike,
    $notILike: Op.notILike,
    $regexp: Op.regexp,
    $notRegexp: Op.notRegexp,
    $iRegexp: Op.iRegexp,
    $notIRegexp: Op.notIRegexp,
    $between: Op.between,
    $notBetween: Op.notBetween,
    $overlap: Op.overlap,
    $contains: Op.contains,
    $contained: Op.contained,
    $adjacent: Op.adjacent,
    $strictLeft: Op.strictLeft,
    $strictRight: Op.strictRight,
    $noExtendRight: Op.noExtendRight,
    $noExtendLeft: Op.noExtendLeft,
    $and: Op.and,
    $or: Op.or,
    $any: Op.any,
    $all: Op.all,
    $values: Op.values,
    $col: Op.col
};

async function connection(clientId, dbName) {
    console.log('clientId', clientId, 'stage', process.env.ENVIRONMENT);
    let local = false;
    let dataBaseName;
    try {
        const stage = process.env.ENVIRONMENT.replace(/^\w/, c => c.toUpperCase());
        console.log('SEC', `${stage}/${clientId}/MySQL`);
        const params = {
            SecretId: `${stage}/${clientId}/MySQL`, // name of secrete
            // VersionStage: 'AWSPENDING',  //optional
        };
        if (!local) {
            const data = await clientSecretsManager.getSecretValue(params).promise();
            const secretString = JSON.parse(data['SecretString']);

            console.log('RETRIEVED PENDING SECRET');
            dataBaseName = secretString.dbname;
            if (dbName) {
                dataBaseName = dbName;
            }
            sequelizeConnection = new Sequelize(dataBaseName, secretString.username, secretString.password, {
                host: secretString.host,
                port: secretString.port,
                dialect: secretString.engine,
                operatorsAliases
            })
        } else {
            console.log('Connecting Local DB');
            dataBaseName = 'research';

            if (dbName) {
                dataBaseName = dbName;
            }
            sequelizeConnection = new Sequelize(dataBaseName, 'root', '', {
                host: 'localhost',
                port: 3306,
                dialect: 'mysql',
                operatorsAliases
            })
        }
        return Promise.resolve(sequelizeConnection);
    } catch (error) {
        console.log('sql connection error', error);
    }
}

exports.connection = connection;