'use strict';

// const AWS = require('aws-sdk');

const awsXRay = require('aws-xray-sdk');
const AWS = awsXRay.captureAWS(require('aws-sdk'));

function getAwsParameters(paramNames, serviceName, clientId) {
    const ssm = new AWS.SSM({ apiVersion: '2014-11-06' });
    const completeNames = [];
    const siteName = 'api'

    paramNames.map((name) => {
        // This should be done as the naming in the AWS SSM parameter store has the format
        // specified above
        if (clientId) {
            console.log('\r\n', '/' + serviceName + '/' + siteName + '/' + process.env.ENVIRONMENT + '/' + clientId + '/' + name);
            completeNames.push('/' + serviceName + '/' + siteName + '/' + process.env.ENVIRONMENT + '/' + clientId + '/' + name);
        } else {
            completeNames.push('/' + serviceName + '/' + siteName + '/' + process.env.ENVIRONMENT + '/' + name);
        }
    });
    const params = {
        Names: completeNames,
        WithDecryption: true
    };
    const paramObject = {};

    return ssm
        .getParameters(params)
        .promise()
        .then((p) => {
            let paramItem = null;

            for (let i = 0; i < p.Parameters.length; i++) {
                paramItem = p.Parameters[i];
                // This is formating the returned parameters object from AWS SSM parameter store
                // so it will be generic to be used in lambda function
                let newName;
                if (clientId) {
                    newName = paramItem.Name.replace(
                        '/' + serviceName + '/' + siteName + '/' + process.env.ENVIRONMENT + '/' + clientId + '/',
                        ''
                    ).trim();
                } else {
                    newName = paramItem.Name.replace(
                        '/' + serviceName + '/' + siteName + '/' + process.env.ENVIRONMENT + '/',
                        ''
                    ).trim();
                }

                paramObject[newName] = paramItem.Value;
                // Do something
            }
            return paramObject;
        })
        .catch((err) => {
            console.log('error occured from getAwsParameters', err);
            throw err;
        });
}

exports.getAwsParameters = getAwsParameters;