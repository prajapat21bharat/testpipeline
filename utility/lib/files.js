'use strict';
const fs = require('fs');
const mkdirp = require('mkdirp');
const path = require('path');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
var rimraf = require("rimraf");
const Excel = require('exceljs');
const s3Services = require('./s3Services');
var AdmZip = require('adm-zip');
var extract = require('extract-zip');


async function createJsonFile(filePath, content) {
    console.log('createJsonFile called');
    const data = JSON.stringify(content);
    const isFile = await checkAndCreateFile(filePath);
    return fs.writeFile(filePath, data, () => {});
}

async function createJsonFileWithoutSlashes(filePath, content) {
    console.log('createJsonFileWithoutSlashes called');
    const data = content;
    const isFile = await checkAndCreateFile(filePath);
    return fs.writeFile(filePath, data, () => {});
}

async function checkAndCreateFile(filePath) {
    console.log("checkAndCreateFile called");
    return new Promise((resolve, reject) => {
        mkdirp(path.dirname(filePath), '0777', function(err) {
            if (err) {
                reject(err);
            }
            resolve(true);
        });
    })
}

async function createCsvFile(filePath, headers, content) {
    console.log('createCsvFile called');
    // const data = JSON.stringify(content);
    const isFile = await checkAndCreateFile(filePath);
    const csvWriter = createCsvWriter({
        path: filePath,
        header: headers
    });

    const data = content;

    return csvWriter
        .writeRecords(data).then((filedata) => {
            console.log('The CSV file was written successfully');
            return true;
        });
}

function removeFile(directoryPath) {
    rimraf(directoryPath, function() { console.log("Files Deleted Successfully !"); });
}

async function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
async function createZip(folderToRead, zipPath, whereToSaveZip) {
    return new Promise((resolve, reject) => {
        console.log("folderToRead", folderToRead, "\r\n", "zipPath", zipPath);
        var zip = new AdmZip();
        fs.readdir(folderToRead, function(err, files) {
            console.log('Files:', files);
            //handling error
            if (err) {
                console.log('Unable to scan directory: ', err);
                reject(err);
            }
            zip.addLocalFolder(whereToSaveZip);
            zip.writeZip(zipPath);
            resolve(true)
        });
    });
}
async function unzipFiles(studyId, executionId, fileName) {
    return new Promise((resolve, reject) => {
        // var extract = require('extract-zip');
        if (!fs.existsSync(`/tmp/${studyId}_${executionId}/data`)) {
            console.log(`/tmp/${studyId}_${executionId}/data Does not exist. Creating it...`);
            fs.mkdirSync(`/tmp/${studyId}_${executionId}/data`);
        }
        extract(`/tmp/${studyId}_${executionId}/${fileName}`, { dir: `/tmp/${studyId}_${executionId}/data/` }, function(err) {
            if (err) {
                console.log('Error in temp file extraction', err);
                reject(reject);
            } else {
                console.log('Extracted successfully.');
                resolve(true);
            }
        })
    })
}

async function extractAllFiles(studyId, executionId) {
    console.log('extractAllFiles called');
    return new Promise((resolve, reject) => {
        let zipFilePath = `/tmp/${studyId}_${executionId}`;
        fs.readdir(zipFilePath, async function(err, files) {
            console.log('Files:', files);
            resolve(files);
            /*
            let index = 1;
            files.forEach((file) => {
                console.log('file:', file);
                // var zip = new AdmZip(`/tmp/${studyId}_${executionId}/${file}`);
                // zip.extractAllTo(`/tmp/${studyId}_${executionId}/data/`, true, true);
                if (!fs.existsSync(`/tmp/${studyId}_${executionId}/data`)) {
                    console.log(`/tmp/${studyId}_${executionId}/data Does not exist. Creating it...`);
                    fs.mkdirSync(`/tmp/${studyId}_${executionId}/data`);
                }
                console.log(`/tmp/${studyId}_${executionId}/data Created Successfully.`);

                await unzipFiles(studyId, executionId, file);

                if (index === files.length) {
                    resolve(true);
                }
                index++;
            })*/
        })

        //     console.log('Files:', files);
        //     files.forEach(async(file) => {
        //         console.log('file:', file);
        //         let buff = fs.readFileSync(`/tmp/${studyId}_${executionId}/${file}`);
        //         var zip = new AdmZip(buff);
        //         console.log('zip', zip);
        //         // zip.readFileAsync(`/tmp/${studyId}_${executionId}/${file}`);
        //         // zip.extractAllToAsync(`/tmp/${studyId}_${executionId}/data/`, false, true);
        //         var zipEntries = zip.getEntries();
        //         zip.extractAllToAsync(`/tmp/${studyId}_${executionId}/data/`, false)
        //     })
    });
}

function readFile(filePath) {
    var fileStream = fs.createReadStream(filePath);
    return fileStream;
}

async function createExcelFile(filePath, headers, content, worksheet) {
    console.log("createExcelFile called");
    // const data = JSON.stringify(content);
    try {
        const isFile = await checkAndCreateFile(filePath);
        var options = {
            filename: filePath,
            useStyles: true, // Default
            useSharedStrings: true // Default
        };

        var workbook = new Excel.stream.xlsx.WorkbookWriter(options);
        //var workbook = new Excel.Workbook();
        var sheet = workbook.addWorksheet(worksheet);
        var worksheet = workbook.getWorksheet(worksheet);

        worksheet.columns = headers;
        //console.log("content", content);
        for (const data of content) {
            worksheet.addRow(data);
        }
        worksheet.commit(); // Need to commit the changes to the worksheet
        let finished = await workbook.commit();
        if (finished) { // Finish the workbook
            return true;
        } else {
            return false;
        }
    } catch (error) {
        console.log("Error in importing data in excel file -", error);
    }
}

async function uploadFilestoTemp(studyId, localFolderPath, zipFilePath, rootFolder, bucketName, ) {
    console.log('====== uploadFilestoTemp called ======');
    console.log(studyId, '***', localFolderPath, '***', zipFilePath, '***', rootFolder, '***', bucketName, '***')
    const zipFile = await createZip(`${localFolderPath}`, `${zipFilePath}`, rootFolder);
    // Convert Zip file to Binary file for S3 Upload
    const binaryZip = readFile(`${zipFilePath}`);
    const bucketPath = `studies/${studyId}/tmp/${zipFilePath}`
        // Uploading Zip File to S3 Bucket
    const isUploadedToS3 = await s3Services.uploadFile(bucketName, bucketPath, binaryZip, 'application/zip');
    console.log("is zipFile Uploaded To S3", isUploadedToS3);
}

exports.createJsonFile = createJsonFile;
exports.createJsonFileWithoutSlashes = createJsonFileWithoutSlashes;
exports.createCsvFile = createCsvFile;
exports.removeFile = removeFile;
exports.createZip = createZip;
exports.extractAllFiles = extractAllFiles;
exports.unzipFiles = unzipFiles;
exports.readFile = readFile;
exports.createExcelFile = createExcelFile;
exports.uploadFilestoTemp = uploadFilestoTemp;
exports.checkAndCreateFile = checkAndCreateFile;