'use strict';
const constants = {
    statusCodes: {
        Success: 200,
        Created: 201,
        NotFound: 404,
        InternalServerError: 500,
        BadRequest: 400,
        Forbidden: 403,
        Unauthorized: 401
    },
    userRoles: {
        admin: 'SystemAdmin',
        researcher: 'Researcher',
        careGiver: 'CareGiver',
        cro: 'Cro',
        threadAdmin: 'ThreadAdmin',
        studyAdmin: 'StudyAdmin',
        sponsor: 'Sponsor',
        participant: 'Participant',
        pi: 'Pi',
        subPi: 'SubPi',
        studyCoordinator: 'StudyCoordinator',
        biostatics: 'Biostatics',
        auditor: 'Auditor',
        clinicalResearchAssociate: 'ClinicalResearchAssociate',
        cra: 'Cra',
        dm: 'Dm',
        reporter: 'Reporter',
        itReporter: 'ITReporter',
        homeHealth: 'HomeHealth',
        quality: 'Quality'
    },
    database: {
        dbResearchResnpnse: 'research_response'
    },
    userConsts: {
        s3Folders: {
            study: 'studies',
            assets: 'assets',
            learnmore: 'learnmore',
            surveys: 'surveys',
            activities: 'activities'
        },
        s3Files: {
            learnmore: 'learn_more',
            schedulerFileName: "tasks_and_schedules"
        }
    },
    email: {
        from: 'no-reply@threadresearch.com',
        sftpFailedFrom: 'alerts@threadresearch.com',
        templateType: {
            dataExport: ''
        },
        subjects: {
            dataExport: 'Study Export: THREAD Platform',
            dataExportFailed: 'Data Transfer Upload Failed'
        },
        templateIds: {
            dataExport: 'ff219972-f319-4507-a680-af718db3b537',
            dataExportError: '25121d19-ecf0-4f1c-9106-48dd776610c9',
            sftpFailed: '32a6f302-4d01-4b06-803c-49966ab59f20'
        }
    },
    enums: {
        deLogs: {
            status: {
                STARTED: 'Started',
                INPROGRESS: 'InProgress',
                SUCCEEDED: 'Succeeded',
                FAILED: 'Failed'
            },
            type: {
                SFTP: 'SFTP',
                DATAEXPORT: 'DATAEXPORT',
                USERACCESS: 'USERACCESS',
                MINISITE: 'MINISITE',
                APPLICANTSITE: 'APPLICANTSITE'
            }
        },
        scheduleTypes: {
            ONCE: "once",
            DAILY: "daily",
            WEEKLY: "weekly",
            MONTHLY: "monthly",
            YEARLY: "yearly",
            MILESTONE: "milestone",
            CUSTOM: "custom"
        },
        activityTypes: {
            FingerTapping: "Finger Tapping",
            NineHolePeg: "Nine Hole Peg",
            GaitAndBalance: "Gait and Balance",
            Pasat: "PASAT",
            FitnessTask: "Fitness Task",
            SpatialMemory: "Spatial Memory",
            TimeWalk: "Time Walk",
            ToneAudioMetry: "Tone Audiometry",
            ReactionTime: "Reaction Time",
            TowerOfHanoi: "Tower Of Hanoi",
            MirSpirometer: "MirSpirometerFVC",
            PefManeuver: "MirSpirometerFEV1",
            VisualAcuityExam: "VisualAcuityExam",
            ContrastAcuityExam: "ContrastAcuityExam",
            SkinHealthActivity: "Skin Health Activity",
            FacialVideoAnalysis: "Facial Video Analysis"
        },
        questionFieldType: {
            formHeader: "formHeader",
            formDropdown: "formDropdown",
            formParagraph: "formParagraph",
            formCheckbox: "formCheckbox",
            formSinglechoice: "formSinglechoice",
            formTextNumeric: "formTextNumeric",
            formTextArea: "formTextArea",
            formTable: "formTable",
            formScale: "formScale",
            formDateTime: "formDateTime"
        },
        fixedValues: {
            QUESTION: 'QUESTION'
        }

    },
    time: {
        oneWeek: '30 * 2 * 24 * 7'
    },
    participant: {
        status: {
            ACTIVE: 'ACTIVE',
            DISQUALIFIED: 'DISQUALIFIED',
            INVITED: 'INVITED',
            NOTINVITED: 'NOTINVITED',
            REGISTERED: 'REGISTERED',
            VERIFIED: 'VERIFIED',
            WITHDRAWSTUDY: 'WITHDRAWSTUDY'
        }
    },
    user: {
        status: {
            Active: 'Active',
            InActive: 'Inactive',
            Pending: 'Pending',
            Invited: 'Invited',
            NotInvited: 'NotInvited'
        }
    },
    ACCEPTED_STATE: 'SUBMITTED',
    exportAreas: {
        activity: 'activity-response',
        edc: 'edc-response',
        feedback: 'feedback-response',
        health: 'health-data',
        onBoardingEvents: 'on-boarding-events',
        onBoardingResponse: 'on-boarding-response',
        survey: 'survey-response',
        telehealth: 'telehealth-appointment',
        trueClinic: 'true-clinic-appointment',
        validic: 'validic',
        visitNote: 'visit-note',
    },
    tempExportFolder: 'data-export',
    researchDbTables: {
        deSftpMeta: 'de_sftp_meta'
    },
    tokenizer: {
        Url: 'https://demo.nxt-security.com/api/services/evtservice/v2',
        APIKey: 'aa767d5ab284459d8dd6661c044da7b6d3o6n2e83ji5',
        PolicyPassword: 'FcVDoIe9ckuUJODvc5U7f7EIW',
        Policy: 'fullaccess',
        TokenName: 'persisttoken'
    }


}

exports.constants = constants;