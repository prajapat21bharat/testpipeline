'use strict';

// const AWS = require('aws-sdk');

const awsXRay = require('aws-xray-sdk');
const AWS = awsXRay.captureAWS(require('aws-sdk'));

const constants = require('../../constants').constants;
const { util, db, files, s3Services, csvMappers, mappers, sendgridMail } = require('../../utility');
const dbObj = new db();
const dbName = constants.database.dbResearchResnpnse;

const _ = require('lodash');

let entityModels;

function checkIfSiteTeamMember(userRole) {
    if (!userRole ||
        userRole.toLowerCase() === constants.userRoles.pi.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.subPi.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.studyCoordinator.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.homeHealth.toLowerCase()) {
        return true;
    } else {
        return false;
    }
}

async function getStudyDetails(event, studyId) {
    util.createLog(event, `getStudyDetails called`);
    try {
        let filter = {
            id: studyId,
        };
        const studyDetails = await dbObj.findOne(filter, [], entityModels.StudyMetaData);
        return studyDetails;
    } catch (error) {
        util.createLog(event, `Error in getStudyDetails : `, error);
        return error;
    }
}

async function getSiteDetails(event, siteId) {
    util.createLog(event, `getSiteDetails called`);
    try {
        let filter = {
            id: siteId
        };
        const site = await dbObj.findOne(filter, [], entityModels.Sites);
        return site;
    } catch (error) {
        util.createLog(event, `Error in getSiteDetails : `, error);
        return error;
    }
}

async function getClientConfig(event, clientId) {
    util.createLog(event, `getClientConfig called`);
    try {
        let filter = {
            id: clientId
        };
        const clientConfig = await dbObj.findOne(filter, [], entityModels.ClientConfig);
        return clientConfig;
    } catch (error) {
        util.createLog(event, `Error in getClientConfig : `, error);
        return error;
    }
}

async function getSFTPDetails(event, studyId) {
    util.createLog(event, `getSFTPDetails called`);
    try {
        let filter = {
            study_id: studyId,
        };
        const sftpDetails = await dbObj.findOneWithOrderBy(filter, [], entityModels.DeSftpMeta, ['sftp_version', 'DESC']);
        return sftpDetails;
    } catch (error) {
        util.createLog(event, `Error in getSFTPDetails : `, error)
        return error;
    }
}

async function createDeLog(event, clientId, studyId, logType, loggedInUserId) {
    util.createLog(event, `createDeLog called`);
    try {
        let deSftpMetaId = null;
        if (logType === 'SFTP') {
            const SFTPSecreteManagerDetails = await getSFTPDetails(event, studyId);
            if (SFTPSecreteManagerDetails instanceof Error) {
                util.createLog(event, `getSFTPDetails Error: `, SFTPSecreteManagerDetails);
                await updateDeLogFailed(event, clientId, deLog, SFTPSecreteManagerDetails);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Upload file on sftp'
                    }),
                };
            }
            util.createLog(event, `getSFTPDetails fetched successfully `);
            deSftpMetaId = (SFTPSecreteManagerDetails && SFTPSecreteManagerDetails.id) ? SFTPSecreteManagerDetails.id : null;
        }

        const data = {
            study_id: studyId,
            de_sftp_meta_id: deSftpMetaId,
            trigger_datetime: util.changeDateToUtc(new Date()),
            completed_datetime: null,
            message: null,
            status: constants.enums.deLogs.status.STARTED,
            aws_job_id: (event && event.executionId) ? event.executionId : null,
            aws_log_path: null,
            type: logType,
            s3_location: null,
            triggered_by: loggedInUserId ? loggedInUserId : null
        };
        const payload = mappers.map.deLogDataMapper(data);

        const deLog = await dbObj.create(payload, entityModels.DeLog);
        return deLog;
    } catch (error) {
        util.createLog(event, `Error in createDeLog : `, error);
        return error;
    }
}

async function changeDeLogStatus(event, clientId, logId, payload) {
    util.createLog(event, `changeDeLogStatus called`);
    try {
        const deLog = await dbObj.updateById(logId, payload, entityModels.DeLog);
        return deLog;
    } catch (error) {
        util.createLog(event, `Error in changeDeLogStatus : `, error);
        return error;
    }
}
async function updateDeLogFailed(event, clientId, deLog) {
    // Update Delog Status
    const payloadFailed = { status: constants.enums.deLogs.status.FAILED }
    util.createLog(event, `deLog status ${payloadFailed}`);
    const delogFailed = await changeDeLogStatus(event, clientId, deLog.id, payloadFailed);
    if (delogFailed instanceof Error) {
        return {
            statusCode: constants.statusCodes.InternalServerError,
            body: JSON.stringify({
                error: 'Error occured in Updating deLog'
            }),
        };
    }
}

var findByStudyIdAndType = async (event, clientId, studyId, type, defaultSideId) => {
    try {
        const models = await dbObj.init(clientId, dbName);
        let filter = {
            studyId: studyId,
            type: type
        };
        const onBoardingResponse = await dbObj.findAll(filter, [], models.OnBoardingResponse);
        return onBoardingResponse;
    } catch (error) {
        util.createLog(event, `Error in findByStudyIdAndType `, error);
        return error;
    }
}

var findByStudyIdAndTypeAndDefaultSiteId = async (event, clientId, studyId, string, defaultSiteId) => {
    try {
        const models = await dbObj.init(clientId, dbName);
        let filter = {
            studyId: studyId,
            type: string,
            siteId: defaultSiteId
            // $or: [{ siteId: defaultSiteId }, { siteId: null }]
        };
        const onBoardingResponse = await dbObj.findAll(filter, [], models.OnBoardingResponse);
        return onBoardingResponse;
    } catch (error) {
        util.createLog(event, `Error in findByStudyIdAndTypeAndDefaultSiteId `, error);
        return error;
    }
}

var findByStudyIdAndTypeAndSiteId = async (event, clientId, studyId, string, loggedInUserSiteId) => {
    try {
        const models = await dbObj.init(clientId, dbName);
        let filter = {
            studyId: studyId,
            type: string,
            siteId: loggedInUserSiteId
            // $or: [{ siteId: loggedInUserSiteId }, { siteId: null }]
        };
        const onBoardingResponse = await dbObj.findAll(filter, [], models.OnBoardingResponse);
        return onBoardingResponse;
    } catch (error) {
        util.createLog(event, `Error in findByStudyIdAndTypeAndSiteId `, error);
        return error;
    }
}

var createOnBoardingEligibilityResponsesCsvFile = async (event, onBoardingResponses, eligibilityResponsesBaseUrl) => { //, studySitesMap , participantUserDefinedIdMap
    const OnBoardingEligibilityResponses = await createCsvPayload(event, onBoardingResponses);

    // Getting Header for CSV File
    const headers = await util.getKeys(OnBoardingEligibilityResponses[0]);

    // Creating CSV File
    const isCsvCreated = await files.createCsvFile(`${eligibilityResponsesBaseUrl}/response.csv`, headers, OnBoardingEligibilityResponses);
    if (isCsvCreated instanceof Error) {
        return {
            statusCode: constants.statusCodes.BadRequest,
            body: JSON.stringify({
                error: 'Not able to create json'
            }),
        };
    }
    // CSVExportUtils.exportOnBoardingResponsesToCSV(onBoardingResponses, csvFile, studySitesMap);//, participantUserDefinedIdMap
}

async function createCsvPayload(event, data) {
    util.createLog(event, `Creating CSV Payload`);
    const payload = [];
    for (const OnBoardingEligibilityResponses of data) {
        const mappedOnBoardingEligibilityResponses = csvMappers.map.onBoardingCsvMapper(OnBoardingEligibilityResponses);
        payload.push(mappedOnBoardingEligibilityResponses);
    }
    return payload;
}

var createOnBoardingResponsesJsonFile = async (event, onBoardingResponses, onBoardingResponsesBaseUrl) => {
    //, studySitesMap , participantUserDefinedIdMap

    for (let data of onBoardingResponses) {
        data.appLanguageId = data.appLanguageId ? data.appLanguageId : '';
        data.displayLanguage = data.displayLanguage ? data.displayLanguage : '';
        data.displayQuestionBody = data.displayQuestionBody ? data.displayQuestionBody : '';
        data.displayQuestionAnswer = data.displayQuestionAnswer ? data.displayQuestionAnswer : '';
        data.userDefinedParticipantId = data.userDefinedParticipantId ? data.userDefinedParticipantId : null;
    }

    // Creating Json File
    const isJsonCreated = await files.createJsonFile(`${onBoardingResponsesBaseUrl}/response.json`, onBoardingResponses);
    if (isJsonCreated instanceof Error) {
        return {
            statusCode: constants.statusCodes.BadRequest,
            body: JSON.stringify({
                error: 'Not able to create json'
            }),
        };
    }
    util.createLog(event, `Json File ${onBoardingResponsesBaseUrl}/response.json Created Successfully`);
}

var getUserDefinedParticipantId = async (event, ParticipantID) => {
    try {
        let filter = {
            id: ParticipantID
        };
        if (ParticipantID !== null) {
            const participantId = await dbObj.findOne(filter, [], entityModels.Participant);
            if (participantId && participantId.user_defined_participant_id) {
                return participantId.user_defined_participant_id;
            } else {
                return '';
            }
        } else {
            return null;
        }
    } catch (error) {
        util.createLog(event, `Error in getUserDefinedParticipantId : `, error);
        return error;
    }
}

async function getStudyDefaultSite(event, studyId) {
    try {
        let filter = {
            study_id: studyId,
            is_default: 1
        };
        const defaultSite = await dbObj.findOne(filter, [], entityModels.StudySite);
        return defaultSite;
    } catch (error) {
        util.createLog(event, `Error in getStudyDefaultSite : `, error);
        return error;
    }
}

async function getStudySitesMap(event, studyId) {
    util.createLog(event, `getStudySiteMap called`);
    try {
        let filter = {
            study_id: studyId
        };
        const studySites = await dbObj.findAll(filter, [], entityModels.StudySite, [], ['study_id', 'ASC']); //await dbObj.findOne(filter, [], entityModels.StudySite);
        return studySites;
    } catch (error) {
        util.createLog(event, `Error in getStudySitesMap: `, error);
        return error;
    }
}
async function validateStudy(event, studyId, clientId) {
    try {
        let filter = {
            id: studyId,
            client_id: clientId
        };
        const study = await dbObj.findOne(filter, [], entityModels.StudyMetaData);
        return study;
    } catch (error) {
        util.createLog(event, `Error in validateStudy: `, error);
        return error;
    }
}
module.exports.export = async event => {
    let deLogId;
    try {
        util.createLog(event, `Lambda on-boarding-response execution started`);
        let sftpUpload = false;
        let clientId, studyId, zipFileNameWithTimeStamp;
        let loggedInUserId = (event && event.triggeredBy && event.triggeredBy.user && event.triggeredBy.user.id) ? event.triggeredBy.user.id : null;
        let hasAreas = [];
        if (event && event.areas) {
            hasAreas = event.areas.split(',');
        }
        //Checked area to execute onboardingResponse lambda
        if (hasAreas && hasAreas.length > 0 && hasAreas.includes(constants.exportAreas.onBoardingResponse) === false) {
            util.createLog(event, `Skipped lambda on-boarding-response`)
            return;
        }
        if (event && event.clientId) {
            clientId = event.clientId;
        } else {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: 'Missing clientId'
                }),
            };
        }
        // checking for sftpupload parameter in payload
        if (event && event.sftpUpload && event.sftpUpload === true) {
            sftpUpload = event.sftpUpload;
        }
        // checking for studyId in paylod
        if (event && event.studyId) {
            studyId = event.studyId;
            util.createLog(event, `studyId is `, studyId);
        } else {
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    message: 'Bad Request',
                    error: 'studyId is required'
                }),
            };
        }
        const models = await dbObj.init(clientId);
        entityModels = models;
        const validateStudyDetails = await validateStudy(event, studyId, clientId);
        if (!validateStudyDetails || validateStudyDetails.length === 0) {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: "Invalid study details or Unauthorized access"
                })
            };
        }
        const loggedInUserRole = (event && event.triggeredBy && event.triggeredBy.user && event.triggeredBy.user.role) ? event.triggeredBy.user.role : null;
        const loggedInUserSiteIds = event.siteIds;
        let loggedInUserSiteId;
        const isSiteTeamMember = checkIfSiteTeamMember(loggedInUserRole);
        if (isSiteTeamMember) {
            util.createLog(event, `Site team member logged in`);
            if (loggedInUserSiteIds && loggedInUserSiteIds.length > 0) {
                loggedInUserSiteId = loggedInUserSiteIds[0]
            } else {
                throw 'User is not associated with any site';
            }
        }
        // if (!isSiteTeamMember) {
        //     return {
        //         statusCode: constants.statusCodes.Unauthorized,
        //         body: JSON.stringify({
        //             message: 'User is not authorized user'
        //         }),
        //     };
        // }

        
        // if (!loggedInUserSiteIds || loggedInUserSiteIds.length < 0) {
        //     // return {
        //     //     statusCode: constants.statusCodes.Unauthorized,
        //     //     body: JSON.stringify({
        //     //         message: 'User is not associated with any site'
        //     //     }),
        //     // };
        //     const studyDefaultSite = await getStudyDefaultSite(event, studyId);
        //     if (studyDefaultSite && studyDefaultSite.is_default) {
        //         loggedInUserSiteId = studyDefaultSite.site_id;
        //     }
        // } else {
        //     loggedInUserSiteId = loggedInUserSiteIds[0];
        // }
        // const loggedInUserSiteId = null;

        util.createLog(event, `User is associated with site: loggedInUserSiteId ${loggedInUserSiteId}, LoggedIn user role is: ${loggedInUserRole}`);

        const studyDetails = await getStudyDetails(event, studyId);
        if (studyDetails instanceof Error) {
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Study Details not found'
                }),
            };
        }

        // Creating deLog
        const logType = (sftpUpload === true) ? constants.enums.deLogs.type.SFTP : constants.enums.deLogs.type.DATAEXPORT;
        const deLog = await createDeLog(event, clientId, studyId, logType, loggedInUserId);
        if (deLog instanceof Error) {
            util.createLog(event, `unable to insert de log record: ${deLog}`);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Not able to create Delog'
                }),
            };
        }
        deLogId = deLog.id;
        util.createLog(event, `deLog: ${deLog.id}`);

        var defaultSiteId;
        let studySitesMap = await getStudySitesMap(event, studyId);
        if (studySitesMap && studySitesMap.length > 0) {
            for (let site of studySitesMap) {
                if (site.is_default && site.is_default === true) {
                    defaultSiteId = site.site_id;
                }
            }
        }

        // Fetching site details
        const siteDetails = await getSiteDetails(event, (loggedInUserSiteId) ? loggedInUserSiteId : defaultSiteId);
        if (siteDetails instanceof Error) {
            await updateDeLogFailed(event, clientId, deLog);
            util.createLog(event, `deLog failed ${deLog.id}`)
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Site Details not found'
                }),
            };
        }
        const siteId = siteDetails.siteId;

        // Fetching client configuration details
        const clientConfig = await getClientConfig(event, clientId);
        if (clientConfig instanceof Error) {

            await updateDeLogFailed(event, clientId, deLog);
            util.createLog(event, `deLog failed ${deLog.id}`)

            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'Client Config not found'
                }),
            };
        }
        const bucketName = clientConfig.s3_bucket;
        const domain = clientConfig.domain_name;
        if (!bucketName) {
            await updateDeLogFailed(event, clientId, deLog);
            util.createLog(event, `deLog failed ${deLog.id}`)
            util.createLog(event, `S3 Bucket not Found`);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'S3 Bucket not Found'
                }),
            };
        }

        let executionId = util.generateUniqueCode();
        if (event.executionId) {
            executionId = event.executionId;
        }
        let studyBaseUrl = `/tmp/${constants.tempExportFolder}/${studyId}_${executionId}`;
        if (event.studyBaseUrl) {
            studyBaseUrl = event.studyBaseUrl;
        }
        let zipFileName = studyId;
        if (studyDetails && studyDetails.name) {
            util.createLog(event, `Study Name ${studyDetails.name}`)
            zipFileName = studyDetails.name;
        }
        util.createLog(event, `zipFileName `, zipFileName);
        const dateNow = new Date();
        const timeStamp = util.changeDateFormat(dateNow, 'YYYYMMDDHHmm');

        zipFileNameWithTimeStamp = `${zipFileName}_${timeStamp}`;
        if (event.zipFileNameWithTimeStamp) {
            zipFileNameWithTimeStamp = event.zipFileNameWithTimeStamp;
        }
        util.createLog(event, `zipFileNameWithTimeStamp `, zipFileNameWithTimeStamp);
        const onBoardingStudyBaseUrl = studyBaseUrl + "/onBoarding";

        // Update Delog Status
        const payloadInProgress = { status: constants.enums.deLogs.status.INPROGRESS, message: 'on-boarding-response' }
        util.createLog(event, `deLog status `, payloadInProgress);
        const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
        if (delogInProgress instanceof Error) {
            util.createLog(event, `delogInProgress Error  `, delogInProgress);
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'Error occured in Updating deLog'
                }),
            };
        }

        //#region OnBoardingEligibilityResponses
        var studyOnBoardingEligibilityResponses = [];

        if (!loggedInUserSiteId) { //For all user other than site team members
            util.createLog(event, `For non-default sites site team members`);
            studyOnBoardingEligibilityResponses = await findByStudyIdAndType(event, clientId, studyId, "eligibility", defaultSiteId);
        } else if (defaultSiteId && defaultSiteId != null && defaultSiteId === loggedInUserSiteId) {
            //For default site's site team members this is for handling null values of site_is column
            util.createLog(event, `defaultSiteId && defaultSiteId === loggedInUserSiteId`);
            studyOnBoardingEligibilityResponses = await findByStudyIdAndTypeAndDefaultSiteId(event, clientId, studyId, "eligibility", defaultSiteId);
        } else {//For non-default site's site team members
            util.createLog(event, `loggedInUserSiteId`);
            studyOnBoardingEligibilityResponses = await findByStudyIdAndTypeAndSiteId(event, clientId, studyId, "eligibility", loggedInUserSiteId);
        }

        if (studyOnBoardingEligibilityResponses instanceof Error) {
            await updateDeLogFailed(event, clientId, deLog);
            util.createLog(event, `deLog failed ${deLog.id}`)
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'error in study On-Boarding Eligibility Responses'
                }),
            };
        }

        if (studyOnBoardingEligibilityResponses && studyOnBoardingEligibilityResponses.length > 0) {
            var studyOnBoardingEligibilityResponsesArr = [];

            for (const data of studyOnBoardingEligibilityResponses) {
                const siteInfo = await getSiteDetails(event, data.siteId ? data.siteId : siteDetails.id);
                data.dataValues["siteId"] = siteInfo.siteId; //siteDetails.siteId;
                data.dataValues["siteName"] = siteInfo.name; //siteDetails.name;

                data.dataValues["userDefinedParticipantId"] = await getUserDefinedParticipantId(event, data.dataValues.participantId);
                studyOnBoardingEligibilityResponsesArr.push(data.dataValues)
            }

            var eligibilityResponsesBaseUrl = studyBaseUrl + "/onBoarding/responses/eligibility";
            await createOnBoardingEligibilityResponsesCsvFile(event, studyOnBoardingEligibilityResponsesArr, eligibilityResponsesBaseUrl); //, studySitesMap , participantUserDefinedIdMap

            util.createLog(event, `Finished creating OnBoarding Eligibility responses csv file for study ${studyId}`)
            await createOnBoardingResponsesJsonFile(event, studyOnBoardingEligibilityResponsesArr, eligibilityResponsesBaseUrl); //, studySitesMap, participantUserDefinedIdMap
            util.createLog(event, `Finished creating OnBoarding Eligibility responses Json file for study ${studyId}`);
        } else {
            util.createLog(event, `No OnBoarding Eligibility responses data to export for studyId `, studyId);
        }
        //#endregion

        //#region OnBoardingEConsentResponses
        var studyOnBoardingEConsentResponses = [];
        if (!loggedInUserSiteId) { //For all user other than site team members
            util.createLog(event, `OnBoardingEConsentResponses For non-default sites site team members`);
            studyOnBoardingEConsentResponses = await findByStudyIdAndType(event, clientId, studyId, "econsent", defaultSiteId);
        } else if (defaultSiteId && defaultSiteId != null && defaultSiteId === loggedInUserSiteId) {
            //For default site's site team members this is for handling null values of site_is column
            util.createLog(event, `OnBoardingEConsentResponses defaultSiteId && defaultSiteId === loggedInUserSiteId`);
            studyOnBoardingEConsentResponses = await findByStudyIdAndTypeAndDefaultSiteId(event, clientId, studyId, "econsent", defaultSiteId);
        } else {//For non-default site's site team members
            util.createLog(event, `OnBoardingEConsentResponses loggedInUserSiteId`);
            studyOnBoardingEConsentResponses = await findByStudyIdAndTypeAndSiteId(event, clientId, studyId, "econsent", loggedInUserSiteId);
        }

        if (studyOnBoardingEConsentResponses instanceof Error) {
            await updateDeLogFailed(event, clientId, deLog);
            util.createLog(event, `deLog failed ${deLog.id}`)
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'error in study On-Boarding E-Consent Responses'
                }),
            };
        }

        if (studyOnBoardingEConsentResponses && studyOnBoardingEConsentResponses.length > 0) {
            var studyOnBoardingEConsentResponsesArr = [];

            for (const data of studyOnBoardingEConsentResponses) {
                const siteInfo = await getSiteDetails(event, data.siteId ? data.siteId : siteDetails.id);
                data.dataValues["siteId"] = siteInfo.siteId; //siteDetails.siteId;
                data.dataValues["siteName"] = siteInfo.name; //siteDetails.name;

                data.dataValues["userDefinedParticipantId"] = await getUserDefinedParticipantId(event, data.dataValues.participantId);
                studyOnBoardingEConsentResponsesArr.push(data.dataValues);
            }

            var econsentResponsesBaseUrl = studyBaseUrl + "/onBoarding/responses/econsent";
            await createOnBoardingEligibilityResponsesCsvFile(event, studyOnBoardingEConsentResponsesArr, econsentResponsesBaseUrl); //, studySitesMap, participantUserDefinedIdMap

            util.createLog(event, `Finished creating OnBoarding eConsent responses csv file for study ${studyId}`)
            await createOnBoardingResponsesJsonFile(event, studyOnBoardingEConsentResponsesArr, econsentResponsesBaseUrl); //, studySitesMap, participantUserDefinedIdMap
            util.createLog(event, `Finished creating OnBoarding eConsent responses Json file for study ${studyId}`);
            const payloadInProgress = {
                status: constants.enums.deLogs.status.SUCCEEDED,
                message: 'OnBoarding eConsent Data export Done',
                completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
            }
            util.createLog(event, `deLog status `, payloadInProgress);
            const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
            if (delogInProgress instanceof Error) {
                util.createLog(event, `delogInProgress Error  `, delogInProgress);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Updating deLog'
                    }),
                };
            }
        } else {
            util.createLog(event, `No OnBoarding eConsent responses data to export for studyId `, studyId);

            // Update Delog Status
            const payloadInProgress = {
                status: constants.enums.deLogs.status.SUCCEEDED,
                message: 'No Data for OnBoarding eConsent Export',
                completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
            }
            util.createLog(event, `deLog status `, payloadInProgress);
            const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
            if (delogInProgress instanceof Error) {
                util.createLog(event, `delogInProgress Error  `, delogInProgress);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Updating deLog'
                    }),
                };
            }
        }
        //#endregion

        if ((studyOnBoardingEConsentResponses && studyOnBoardingEConsentResponses.length > 0) || (studyOnBoardingEligibilityResponses && studyOnBoardingEligibilityResponses.length > 0)) {
            //#region create zip file onBoarding
            util.createLog(event, `uploading files to s3 in temp folder`);
            // Creating Zip File
            const zipFile = await files.createZip(`${onBoardingStudyBaseUrl}`, `${studyBaseUrl}/onBoarding.zip`, studyBaseUrl);
            // Convert Zip file to Binary file for S3 Upload
            const binaryZip = files.readFile(`${studyBaseUrl}/onBoarding.zip`);

            // Uploading Zip File to S3 Bucket
            const isUploadedToS3 = await s3Services.uploadFile(bucketName, `studies/${studyId}/tmp/${studyId}_${executionId}/onBoarding.zip`, binaryZip, 'application/zip');
            util.createLog(event, `is zipFile Uploaded To S3 `, isUploadedToS3);
            if (isUploadedToS3) {
                util.createLog(event, `Deleting Temp Files`);
                // files.removeFile(studyBaseUrl);
                // Update Delog Status
                const payloadInProgress = {
                    status: constants.enums.deLogs.status.SUCCEEDED,
                    message: 'TeleHealth Data export Done',
                    completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
                }
                util.createLog(event, `deLog status `, payloadInProgress);
                const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
                if (delogInProgress instanceof Error) {
                    util.createLog(event, `delogInProgress Error  `, delogInProgress);
                    return {
                        statusCode: constants.statusCodes.InternalServerError,
                        body: JSON.stringify({
                            error: 'Error occured in Updating deLog'
                        }),
                    };
                }
            }
        }
        //#endregion
        return {
            statusCode: constants.statusCodes.Success,
            body: JSON.stringify({
                success: 'true on-boarding-response',
            },
                null,
                2
            ),
        };

    } catch (error) {
        util.createLog(event, `Error in exporting on-boarding-response data : `, error);
        console.log('Error', error, 'deLogId', deLogId);

        const payloadFailed = {
            status: constants.enums.deLogs.status.FAILED,
            message: error && error.stack ? error.stack : error && error.message ? error.message : 'failed',
            completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss'),
        }

        const isStatusUpdated = await changeDeLogStatus(event, event.clientId, deLogId, payloadFailed);
        throw error;
    }
};