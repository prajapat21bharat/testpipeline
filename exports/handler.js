'use strict';
const constants = require('../constants').constants;
const { util, db, files, s3Services, csvMappers, stepFunctions } = require('../utility');
const dbObj = new db();
let entityModels;
async function getStudySiteDetails(studyId) {
    try {
        let filter = {
            study_id: studyId,
        };
        const studySite = await dbObj.findOne(filter, [{
            model: entityModels.StudyMetaData,
            attributes: ['id', 'name', 'onboarding_type', 'status', 'app_name', 'client_id']
        }], entityModels.StudySite);
        return studySite;
    } catch (error) {
        console.log('error', error);
        return error;
    }
}

async function getClientConfig(clientId) {
    console.log(" ========= getClientConfig called ========= ");
    try {
        let filter = {
            id: clientId
        };
        const clientConfig = await dbObj.findOne(filter, [], entityModels.ClientConfig);
        return clientConfig;
    } catch (error) {
        console.log('error', error);
        return error;
    }
}

module.exports.exportData = async(event, context) => {
    try {
        // var executionId = util.getExecutionIdFromArn(event.Execution);
        util.createLog(event);
        if (!event.headers || !event.headers.Authorization) {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: 'Unauthorized'
                }),
            };
        }

        if (!event.headers || !event.headers.clientId) {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: 'Missing clientId'
                }),
            };
        }
        const clientId = event.headers.clientId;

        if (!event.queryStringParameters || !event.queryStringParameters.studyId) {
            console.log(' ========= studyId is required ========= ');
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    message: 'Bad Request',
                    error: 'studyId is required'
                }),
            };
        }
        const studyId = event.queryStringParameters.studyId;
        console.log(' ========= studyId', studyId, ' ========= ');

        console.log(' ========= clientId from lambda ', clientId, ' ========= ');
        const models = await dbObj.init(clientId);
        entityModels = models;
        console.log(' ========= models from lambda ', entityModels, ' ========= ');

        const sites = await getStudySiteDetails(studyId);
        console.log(' ========= site details:', JSON.stringify(sites), ' ========= ');

        const executionId = util.generateUniqueCode();
        let studyBaseUrl = `/tmp/${constants.tempExportFolder}/${studyId}_${executionId}`;

        let zipFileName = studyId;
        if (sites && sites.study_meta_datum && sites.study_meta_datum.name) {
            console.log(' ========= Study Name:', sites.study_meta_datum.name, ' ========= ');
            zipFileName = sites.study_meta_datum.name;
        }
        const dateNow = new Date();
        const timeStamp = util.changeDateFormat(dateNow, 'YYYYMMDDHHmm');
        const zipFileNameWithTimeStamp = `${zipFileName}_${timeStamp}`;

        // Fetching client configuration details
        const clientConfig = await getClientConfig(clientId);
        if (clientConfig instanceof Error) {
            console.log(clientConfig);
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'Client Config not found'
                }),
            };
        }
        const bucketName = clientConfig.s3_bucket;
        const domain = clientConfig.domain_name;
        if (!bucketName) {
            console.log(' ========= S3 Bucket not Found ========= ');
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'S3 Bucket not Found'
                }),
            };
        }

        var inputParams = { clientId: clientId, bucket: bucketName, domainName: domain, executionId: executionId, studyBaseUrl: studyBaseUrl, zipFileNameWithTimeStamp: zipFileNameWithTimeStamp, queryStringParameters: event.queryStringParameters, authorization: event.headers.Authorization, }
            // const stepResult = await stepFunctions.triggerStepFunction(inputParams, `data-export-${process.env.ENVIRONMENT}`);
            // if (stepResult instanceof Error) {
            //     console.log(stepResult);
            //     return {
            //         statusCode: constants.statusCodes.InternalServerError,
            //         body: JSON.stringify({
            //             error: 'Internal Server Error',
            //             errorCode: 11
            //         }),
            //     };
            // }

        return {
            body: JSON.stringify({
                    executionId,
                    studyBaseUrl,
                    zipFileNameWithTimeStamp
                },
                null,
                2
            ),
        };
    } catch (error) {
        console.log('error', error);
        return {
            statusCode: constants.statusCodes.InternalServerError,
            body: JSON.stringify({
                    error: error,
                },
                null,
                2
            ),
        };
    }
};