'use strict';

// const AWS = require('aws-sdk');

const awsXRay = require('aws-xray-sdk');
const AWS = awsXRay.captureAWS(require('aws-sdk'));

const constants = require('../../constants').constants;
const { util, db, files, s3Services, csvMappers, mappers, sendgridMail } = require('../../utility');
const dbObj = new db();
const dbName = constants.database.dbResearchResnpnse;

const _ = require('lodash');

let entityModels, responseEntityModels;

function checkIfSiteTeamMember(userRole) {
    if (!userRole ||
        userRole.toLowerCase() === constants.userRoles.pi.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.subPi.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.studyCoordinator.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.homeHealth.toLowerCase()) {
        return true;
    } else {
        return false;
    }
}

async function getStudyDetails(event, studyId) {
    util.createLog(event, `getStudyDetails called`);
    try {
        let filter = {
            id: studyId,
        };
        const studyDetails = await dbObj.findAll(filter, [], entityModels.StudyMetaData);
        return studyDetails;
    } catch (error) {
        util.createLog(event, `getStudyDetails Error: `, error);
        return error;
    }
}

async function getStudySiteDetails(event, studyId) {
    util.createLog(event, `getStudySiteDetails called`);
    try {
        let filter = {
            study_id: studyId,
        };
        const studySiteDetails = await dbObj.findAll(filter, [], entityModels.StudySite, '', ['study_id', 'ASC']);
        return studySiteDetails;
    } catch (error) {
        util.createLog(event, `getStudySiteDetails Error: `, error);
        return error;
    }
}

async function getSiteDetails(event, siteId) {
    util.createLog(event, `getSiteDetails called`);
    try {
        let filter = {
            id: siteId
        };
        const site = await dbObj.findOne(filter, [], entityModels.Sites);
        return site;
    } catch (error) {
        util.createLog(event, `getSiteDetails Error: `, error);
        return error;
    }
}

async function getSurveyResponseByStudyId(event, clientId, trackerIds) {
    util.createLog(event, `getSurveyResponseByStudyId called`);
    try {
        let filter = {
            survey_response_tracker_id: { $in: trackerIds },
        };
        const surveyResponse = await dbObj.findAll(filter, [], responseEntityModels.SurveyResponse, [], [
            ['survey_id', 'ASC'],
            ['survey_response_tracker_id', 'ASC'],
            ['participant_id', 'ASC']
        ]);
        return surveyResponse;
    } catch (error) {
        util.createLog(event, `getSurveyResponseByStudyId Error: `, error);
        return error;
    }
}

async function getSurveyResponseByStudyIdAndDefaultSiteId(event, clientId, trackerIds, defaultSiteId) {
    util.createLog(event, `getSurveyResponseByStudyIdAndDefaultSiteId called, trackerIds: ${trackerIds}`);
    try {
        let filter = {
            survey_response_tracker_id: { $in: trackerIds },
            site_id: defaultSiteId
        };
        const surveyResponse = await dbObj.findAll(filter, [], responseEntityModels.SurveyResponse, [], [
            ['survey_id', 'ASC'],
            ['survey_response_tracker_id', 'ASC'],
            ['participant_id', 'ASC']
        ]);
        return surveyResponse;
    } catch (error) {
        util.createLog(event, `getSurveyResponseByStudyIdAndDefaultSiteId Error: `, error);
        return error;
    }
}

async function getSurveyResponseByStudyIdAndSiteId(event, clientId, trackerIds, loggedInUserSiteId) {
    util.createLog(event, `getSurveyResponseByStudyIdAndSiteId called`);
    try {
        let filter = {
            survey_response_tracker_id: { $in: trackerIds },
            site_id: loggedInUserSiteId
        };
        const surveyResponse = await dbObj.findAll(filter, [], responseEntityModels.SurveyResponse, [], [
            ['survey_id', 'ASC'],
            ['survey_response_tracker_id', 'ASC'],
            ['participant_id', 'ASC']
        ]);
        return surveyResponse;
    } catch (error) {
        util.createLog(event, `getSurveyResponseByStudyIdAndSiteId Error: `, error);
        return error;
    }
}

async function getClientConfig(event, clientId) {
    util.createLog(event, `getClientConfig called`);
    try {
        let filter = {
            id: clientId
        };
        const clientConfig = await dbObj.findOne(filter, [], entityModels.ClientConfig);
        return clientConfig;
    } catch (error) {
        util.createLog(event, `getClientConfig Error: `, error);
        return error;
    }
}

async function getSurveyResponseTrackers(event, clientId, studyId) {
    util.createLog(event, `getSurveyResponseTrackers called`);
    try {
        let filter = {
            study_id: studyId
        };
        const responseTrackers = await dbObj.findAll(filter, [], responseEntityModels.SurveyResponseTracker, [], [
            ['survey_id', 'ASC'],
            ['id', 'ASC'],
            ['participant_id', 'ASC']
        ]);
        return responseTrackers;
    } catch (error) {
        util.createLog(event, `getSurveyResponseTrackers Error: `, error);
        return error;
    }
}


async function getParticipantDetails(event, participantId) {
    util.createLog(event, `getParticipantDetails called`);
    try {
        let filter = {
            id: participantId
        };
        const participant = await dbObj.findOne(filter, [], entityModels.Participant);
        return participant;
    } catch (error) {
        util.createLog(event, `getParticipantDetails Error: `, error);
        return error;
    }
}

async function downloadSurveyJsonFromS3(event, bucketName, s3Path) {
    util.createLog(event, `get Survey Json from s3 for called`);
    try {
        const s3Object = s3Services.readS3Object(s3Path, bucketName);
        return s3Object;
    } catch (error) {
        util.createLog(event, `downloadSurveyJsonFromS3 Error: `, error);
        return error;
    }
}

async function createCsvPayload(event, data) {
    util.createLog(event, `Creating CSV Payload`);
    const payload = [];
    for (const response of data) {
        const mappedResponse = csvMappers.map.surveyResponseCsvMapper(response);
        payload.push(mappedResponse);
    }
    return payload;
}

async function getSFTPDetails(event, studyId) {
    util.createLog(event, `getSFTPDetails called`);
    try {
        let filter = {
            study_id: studyId,
        };
        const sftpDetails = await dbObj.findOneWithOrderBy(filter, [], entityModels.DeSftpMeta, ['sftp_version', 'DESC']);
        return sftpDetails;
    } catch (error) {
        util.createLog(event, `Error in getSFTPDetails : `, error)
        return error;
    }
}

async function createDeLog(event, clientId, studyId, logType, loggedInUserId) {
    util.createLog(event, `createDeLog called`);
    try {
        let deSftpMetaId = null;
        if (logType === 'SFTP') {
            const SFTPSecreteManagerDetails = await getSFTPDetails(event, studyId);
            if (SFTPSecreteManagerDetails instanceof Error) {
                util.createLog(event, `getSFTPDetails Error: `, SFTPSecreteManagerDetails);
                await updateDeLogFailed(event, clientId, deLog, SFTPSecreteManagerDetails);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Upload file on sftp'
                    }),
                };
            }
            util.createLog(event, `getSFTPDetails fetched successfully `);
            deSftpMetaId = (SFTPSecreteManagerDetails && SFTPSecreteManagerDetails.id) ? SFTPSecreteManagerDetails.id : null;
        }

        const data = {
            study_id: studyId,
            de_sftp_meta_id: deSftpMetaId,
            trigger_datetime: util.changeDateToUtc(new Date()),
            completed_datetime: null,
            message: null,
            status: constants.enums.deLogs.status.STARTED,
            aws_job_id: event.executionId,
            aws_log_path: null,
            type: logType,
            s3_location: null,
            triggered_by: loggedInUserId ? loggedInUserId : null
        };
        const payload = mappers.map.deLogDataMapper(data);

        const deLog = await dbObj.create(payload, entityModels.DeLog);
        return deLog;
    } catch (error) {
        util.createLog(event, `createDeLog Error: `, error);
        return error;
    }
}

async function changeDeLogStatus(event, clientId, logId, payload) {
    util.createLog(event, `changeDeLogStatus called`);
    try {
        const deLog = await dbObj.updateById(logId, payload, entityModels.DeLog);
        return deLog;
    } catch (error) {
        util.createLog(event, `changeDeLogStatus Error: `, error);
        return error;
    }
}

async function getTrackerIdsFromTrackerData(event, trackerData) {
    return new Promise((resolve, reject) => {
        const trackerIds = [];
        const surveyIds = [];
        if (trackerData && trackerData.length > 0) {
            trackerData.forEach(async(survey) => {
                trackerIds.push(survey.id);
                surveyIds.push(survey.survey_id);
            });
        }
        resolve({ trackerId: trackerIds, surveyId: surveyIds });

    });
}
async function validateStudy(event, studyId, clientId) {
    try {
        let filter = {
            id: studyId,
            client_id: clientId
        };
        const study = await dbObj.findOne(filter, [], entityModels.StudyMetaData);
        return study;
    } catch (error) {
        util.createLog(event, `Error in validateStudy: `, error);
        return error;
    }
}

async function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

module.exports.export = async(event, context) => {
    let deLogId;
    try {
        util.createLog(event, `Lambda survey-response Execution Started`);
        let sftpUpload = false;
        let clientId, studyId, zipFileNameWithTimeStamp, authorization;
        let loggedInUserId = (event && event.triggeredBy && event.triggeredBy.user && event.triggeredBy.user.id) ? event.triggeredBy.user.id : null;
        let hasAreas = [];
        if (event && event.areas) {
            hasAreas = event.areas.split(',');
        }
        //Checked area to execute surveyResponse lambda
        if (hasAreas && hasAreas.length > 0 && hasAreas.includes(constants.exportAreas.survey) === false) {
            util.createLog(event, `Skipped lambda survey-response`);
            return;
        }
        if (event && event.clientId) {
            clientId = event.clientId;
        } else {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: 'Missing clientId'
                }),
            };
        }
        // checking for sftpupload parameter in payload
        if (event && event.sftpUpload && event.sftpUpload === true) {
            sftpUpload = event.sftpUpload;
        }
        // checking for studyId in paylod
        if (event && event.studyId) {
            studyId = event.studyId;
            util.createLog(event, `studyId is `, studyId);
        } else {
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    message: 'Bad Request',
                    error: 'studyId is required'
                }),
            };
        }
        const models = await dbObj.init(clientId);
        entityModels = models;
        const responseModels = await dbObj.init(clientId, dbName);
        responseEntityModels = responseModels;
        const validateStudyDetails = await validateStudy(event, studyId, clientId);
        if (!validateStudyDetails || validateStudyDetails.length === 0) {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: "Invalid study details or Unauthorized access"
                })
            };
        }
        const loggedInUserRole = (event && event.triggeredBy && event.triggeredBy.user && event.triggeredBy.user.role) ? event.triggeredBy.user.role : null;
        const loggedInUserSiteIds = event.siteIds;
        let loggedInUserSiteId;
        util.createLog(event, `LoggedIn user role is: ${loggedInUserRole}`);


        const isSiteTeamMember = checkIfSiteTeamMember(loggedInUserRole);
        if (isSiteTeamMember) {
            util.createLog(event, `Site team member logged in`);
            if (loggedInUserSiteIds && loggedInUserSiteIds.length > 0) {
                loggedInUserSiteId = loggedInUserSiteIds[0]
            } else {
                throw 'User is not associated with any site';
            }
        }
        util.createLog(event, `User is associated with site: loggedInUserSiteId ${loggedInUserSiteId}, LoggedIn user role is: ${loggedInUserRole}`);

        // if (!isSiteTeamMember) {
        //     return {
        //         statusCode: constants.statusCodes.Unauthorized,
        //         body: JSON.stringify({
        //             message: 'User is not authorized user'
        //         }),
        //     };
        // }
        // util.createLog(event, `User is authorized username`);

        // if ((!loggedInUserSiteIds || !loggedInUserSiteIds.length > 0) && !loggedInUserRole.toLowerCase() === constants.userRoles.admin.toLowerCase()) {
        //     return {
        //         statusCode: constants.statusCodes.Unauthorized,
        //         body: JSON.stringify({
        //             message: 'User is not associated with any site'
        //         }),
        //     };
        // }
        // // const loggedInUserSiteId = null;

        // let loggedInUserSiteId;
        // if (loggedInUserSiteIds && loggedInUserSiteIds.length > 0) {
        //     loggedInUserSiteId = loggedInUserSiteIds[0];
        //     util.createLog(event, `User is associated with site: loggedInUserSiteId ${loggedInUserSiteId}, LoggedIn user role is: ${loggedInUserRole}`);
        // }

        const studyDetails = await getStudyDetails(event, studyId);
        if (studyDetails instanceof Error) {
            util.createLog(event, `studyDetails Error: ${studyDetails}`);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Study Details not found'
                }),
            };
        }
        util.createLog(event, `studyDetails called successfully`);

        // Creating deLog
        const logType = (sftpUpload === true) ? constants.enums.deLogs.type.SFTP : constants.enums.deLogs.type.DATAEXPORT;
        const deLog = await createDeLog(event, clientId, studyId, logType, loggedInUserId);
        if (deLog instanceof Error) {
            util.createLog(event, `unable to insert de log record: ${deLog}`);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Not able to create Delog'
                }),
            };
        }
        util.createLog(event, `deLog: ${deLog.id}`);
        deLogId = deLog.id;

        const studySiteDetails = await getStudySiteDetails(event, studyId);

        let defaultSiteId;
        for (const studySiteDetail of studySiteDetails) {
            if (studySiteDetail && studySiteDetail.is_default) {
                defaultSiteId = studySiteDetail.site_id;
            }
        }
        util.createLog(event, `defaultSiteId:`, defaultSiteId);

        // Fetching site details
        let siteDetails = await getSiteDetails(event, (loggedInUserSiteId) ? loggedInUserSiteId : defaultSiteId);
        if (siteDetails instanceof Error) {
            util.createLog(event, `siteDetails: `, siteDetails);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Site Details not found'
                }),
            };
        }
        util.createLog(event, `siteDetails called successfully`);

        const siteId = siteDetails.site_id;
        util.createLog(event, `siteId: ${siteId}`);

        // Fetching client configuration details
        const clientConfig = await getClientConfig(event, clientId);
        if (clientConfig instanceof Error) {
            util.createLog(event, `clientConfig Error: `, clientConfig);
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'Client Config not found'
                }),
            };
        }
        const bucketName = clientConfig.s3_bucket;
        const domain = clientConfig.domain_name;
        if (!bucketName) {
            util.createLog(event, `S3 Bucket not Found`);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'S3 Bucket not Found'
                }),
            };
        }
        let executionId = util.generateUniqueCode();
        if (event.executionId) {
            executionId = event.executionId;
        }
        let studyBaseUrl = `/tmp/${constants.tempExportFolder}/${studyId}_${executionId}`;
        if (event.studyBaseUrl) {
            studyBaseUrl = event.studyBaseUrl;
        }
        let zipFileName = studyId;
        if (studyDetails && studyDetails.length > 0 && studyDetails[0].name) {
            util.createLog(event, `Study Name: ${studyDetails[0].name}`);
            zipFileName = studyDetails[0].name;
        }
        util.createLog(event, `zipFileName: ${zipFileName}`);
        const dateNow = new Date();
        const timeStamp = util.changeDateFormat(dateNow, 'YYYYMMDDHHmm');
        zipFileNameWithTimeStamp = `${zipFileName}_${timeStamp}`;
        if (event.zipFileNameWithTimeStamp) {
            zipFileNameWithTimeStamp = event.zipFileNameWithTimeStamp;
        }
        util.createLog(event, `zipFileNameWithTimeStamp: ${zipFileNameWithTimeStamp}`);
        let surveyResponsesBaseUrl = studyBaseUrl;

        // Update Delog Status
        const payloadInProgress = { status: constants.enums.deLogs.status.INPROGRESS, message: 'survey-response' };
        util.createLog(event, `deLog status: ${payloadInProgress}`);
        const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
        if (delogInProgress instanceof Error) {
            util.createLog(event, `delogInProgress Error: `, delogInProgress);
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'Error occured in Updating deLog'
                }),
            };
        }

        let studySurveyResponses, surveyIds;
        if (!loggedInUserSiteId) {
            //For all user other than site team members
            util.createLog(event, `!loggedInUserSiteId`);
            const surveyTrackers = await getSurveyResponseTrackers(event, clientId, studyId);
            console.log('surveyTrackers', surveyTrackers);

            const trackers = await getTrackerIdsFromTrackerData(event, surveyTrackers);
            console.log('trackers', trackers);
            const trackerIds = trackers.trackerId;
            surveyIds = trackers.surveyId;
            util.createLog(event, `trackerIds ${trackerIds}`);
            studySurveyResponses = await getSurveyResponseByStudyId(event, clientId, trackerIds);

        } else if (defaultSiteId && defaultSiteId === loggedInUserSiteId) {
            //For default site's site team members this is for handling null values of site_is column
            util.createLog(event, `defaultSiteId && defaultSiteId === loggedInUserSiteId`);

            const surveyTrackers = await getSurveyResponseTrackers(event, clientId, studyId, defaultSiteId);

            const trackers = await getTrackerIdsFromTrackerData(event, surveyTrackers);
            const trackerIds = trackers.trackerId;
            surveyIds = trackers.surveyId;
            util.createLog(event, `trackerIds ${trackerIds}`);
            studySurveyResponses = await getSurveyResponseByStudyIdAndDefaultSiteId(event, clientId, trackerIds, defaultSiteId);
        } else {
            //For non-default site's site team members
            util.createLog(event, `For non-default sites site team members`);

            const surveyTrackers = await getSurveyResponseTrackers(event, clientId, studyId, loggedInUserSiteId);
            util.createLog(event, `surveyTrackers: ${surveyTrackers}`);

            const trackers = await getTrackerIdsFromTrackerData(event, surveyTrackers);
            const trackerIds = trackers.trackerId;
            surveyIds = trackers.surveyId;
            util.createLog(event, `trackerIds: ${trackerIds}`);

            studySurveyResponses = await getSurveyResponseByStudyIdAndSiteId(event, clientId, trackerIds, loggedInUserSiteId);
        }
        util.createLog(event, `studySurveyResponses fetched successfully. surveyIds: ${surveyIds}`);

        if (studySurveyResponses && studySurveyResponses.length > 0) {
            util.createLog(event, `Study Survey responses size ${studySurveyResponses.length}`);
            let surveyS3Data = [];
            let eproSurvey = false;

            let surveyDataForJson = [];
            let itemCount = 1;
            for (const survey of studySurveyResponses) {
                let surveyId = survey.surveyId;
                const s3Folders = constants.userConsts.s3Folders;
                const s3FileKey = `${s3Folders.study}/${studyId}/${s3Folders.surveys}/${surveyId}.json`;

                // Download Survey Data from S3 Bucket
                const s3SurveyData = await downloadSurveyJsonFromS3(event, bucketName, s3FileKey);
                if (s3SurveyData instanceof Error) {
                    util.createLog(event, `s3SurveyData Error: ${s3SurveyData}`);
                    return {
                        statusCode: constants.statusCodes.InternalServerError,
                        body: JSON.stringify({
                            error: 'Error occured in Data fetch from S3'
                        }),
                    };
                }

                util.createLog(event, `Successfully downloaded survey s3 Data ${surveyId}.json`);
                const surveyDataFromS3 = s3SurveyData;
                const surveyName = (surveyDataFromS3 && surveyDataFromS3.title) ? util.createSurveyFolderName(surveyDataFromS3.title) : surveyId;
                // const surveyName = (surveyDataFromS3.title) ? util.replaceChars(surveyDataFromS3.title, "_") : "";

                util.createLog(event, `surveyName ${surveyName}`);
                if (surveyDataFromS3 && surveyDataFromS3.eproSurvey) {
                    eproSurvey = true;

                    util.createLog(event, `is eproSurvey ${eproSurvey}`);
                } else {
                    eproSurvey = false;
                }
                surveyResponsesBaseUrl = (eproSurvey) ? `${studyBaseUrl}/ePROResponses` : `${studyBaseUrl}/surveyResponses`;

                util.createLog(event, `surveyResponsesBaseUrl ${surveyResponsesBaseUrl}`);
                if (surveyDataFromS3 && surveyDataFromS3.questions) {
                    // Create Json File for Survey data from S3 Learnmore file
                    // const isSurveyIdJson = await files.createJsonFile(`${surveyResponsesBaseUrl}/${surveyName}/${surveyId}.json`, surveyDataFromS3);
                    // if (isSurveyIdJson instanceof Error) {
                    //     util.createLog(event, `isSurveyIdJson Error: `, error);
                    //     return {
                    //         statusCode: constants.statusCodes.BadRequest,
                    //         body: JSON.stringify({
                    //             error: 'Not able to create json'
                    //         }),
                    //     };
                    // }
                    // util.createLog(event, `File Created successfully ${surveyName}/${surveyId}.json`);
                    surveyS3Data.push({ surveyName: surveyName, surveyId: surveyId, data: surveyDataFromS3 })
                }

                let surveyRawData = {};
                surveyRawData = JSON.parse(JSON.stringify(survey))
                const participant = await getParticipantDetails(event, survey.participantId);
                surveyRawData.userDefinedParticipantId = participant.user_defined_participant_id;

                // getting siteDetails
                let siteData;
                if (survey && survey.siteId) {
                    siteData = await getSiteDetails(event, survey.siteId);
                }
                surveyRawData.siteId = (siteData && siteData.siteId) ? siteData.siteId : '';
                surveyRawData.siteName = (siteData && siteData.name) ? siteData.name : '';
                let offset = (surveyRawData && surveyRawData.localTimeOffset) ? surveyRawData.localTimeOffset : '+00:00';

                surveyRawData.localTimeOffset = offset;
                const startTime = util.changeDateToUtc(surveyRawData.startTime);
                const endTime = util.changeDateToUtc(surveyRawData.endTime);
                let startTimeLocal, endTimeLocal;
                if (surveyRawData && surveyRawData.startTimeLocal) {
                    startTimeLocal = util.stringDateFormatter(surveyRawData.startTimeLocal, offset, "YYYY-MM-DD HH:mm:ss");
                } else if (surveyRawData.startTime) {
                    startTimeLocal = util.stringDateFormatter(surveyRawData.startTime, offset, "YYYY-MM-DD HH:mm:ss");
                }

                if (surveyRawData.endTimeLocal) {
                    endTimeLocal = util.stringDateFormatter(surveyRawData.endTimeLocal, offset, "YYYY-MM-DD HH:mm:ss");
                } else if (surveyRawData.endTime) {
                    endTimeLocal = util.stringDateFormatter(surveyRawData.endTime, offset, "YYYY-MM-DD HH:mm:ss");
                }

                surveyRawData.startTime = startTime;
                surveyRawData.endTime = endTime;
                surveyRawData.startTimeLocal = startTimeLocal;
                surveyRawData.endTimeLocal = endTimeLocal;

                const survey_Name = _.result(
                    _.find(surveyS3Data, function(i) {
                        return i.surveyId === survey.surveyId;
                    }),
                    'surveyName'
                );
                if (survey_Name) {
                    surveyRawData.surveyName = survey_Name;
                    let surveyFolder = util.createSurveyFolderName(survey_Name);
                    surveyDataForJson.push(surveyRawData);

                    console.log('surveyDataForJson', survey_Name, surveyDataFromS3.title, surveyDataForJson, );
                    const result = _.filter(surveyDataForJson, (o) => _.includes(o, `${survey_Name}`));
                    util.createLog(event, 'data for csv write', result);
                    if (result && result.length > 0) {
                        if (surveyDataFromS3 && surveyDataFromS3.questions) {
                            // Create Json File for Survey data from S3 Learnmore file
                            const isSurveyIdJson = await files.createJsonFile(`${surveyResponsesBaseUrl}/${survey_Name}/${surveyId}.json`, surveyDataFromS3);
                            if (isSurveyIdJson instanceof Error) {
                                util.createLog(event, `isSurveyIdJson Error: `, error);
                                return {
                                    statusCode: constants.statusCodes.BadRequest,
                                    body: JSON.stringify({
                                        error: 'Not able to create json'
                                    }),
                                };
                            }
                            util.createLog(event, `File Created successfully ${survey_Name}/${surveyId}.json`);
                        }

                        // Creating Json File
                        util.createLog(event, `Creating Json File: ${surveyResponsesBaseUrl}/${surveyFolder}/response.json`);

                        const isJsonCreated = await files.createJsonFile(`${surveyResponsesBaseUrl}/${surveyFolder}/response.json`, result);
                        if (isJsonCreated instanceof Error) {
                            util.createLog(event, `isJsonCreated Error:  `, isJsonCreated);
                            return {
                                statusCode: constants.statusCodes.BadRequest,
                                body: JSON.stringify({
                                    error: 'Not able to create json'
                                }),
                            };
                        }
                        util.createLog(event, `Json File ${surveyResponsesBaseUrl}/${surveyFolder}/response.json Created Successfully`);

                        // Creating CSV payload and mappings for data
                        const mappedSurveyresponse = await createCsvPayload(event, result);

                        // Getting Header for CSV File
                        const headers = util.getKeys(mappedSurveyresponse[0]);

                        // Creating CSV File
                        const isCsvCreated = await files.createCsvFile(`${surveyResponsesBaseUrl}/${surveyFolder}/response.csv`, headers, mappedSurveyresponse);
                        if (isCsvCreated instanceof Error) {
                            util.createLog(event, `isCsvCreated Error: `, isCsvCreated);
                            return {
                                statusCode: constants.statusCodes.BadRequest,
                                body: JSON.stringify({
                                    error: 'Not able to create CSV'
                                }),
                            };
                        }
                        util.createLog(event, `Csv Created ${surveyFolder}/response.csv`);

                        if (itemCount === studySurveyResponses.length) {
                            util.createLog(event, `uploading files to s3 in temp folder studies/${studyId}/tmp/${studyId}_${executionId}/surveyResponses.zip`);
                            // Creating Zip File
                            const zipFile = await files.createZip(`${studyBaseUrl}`, `${studyBaseUrl}/surveyResponses.zip`, studyBaseUrl);
                            // Convert Zip file to Binary file for S3 Upload
                            const binaryZip = files.readFile(`${studyBaseUrl}/surveyResponses.zip`);

                            // Uploading Zip File to S3 Bucket
                            const isUploadedToS3 = await s3Services.uploadFile(bucketName, `studies/${studyId}/tmp/${studyId}_${executionId}/surveyResponses.zip`, binaryZip, 'application/zip');
                            util.createLog(event, `is zipFile Uploaded To S3 `, isUploadedToS3);
                            await timeout(20000);
                            // Update Delog Status
                            const payloadInProgress = {
                                status: constants.enums.deLogs.status.SUCCEEDED,
                                message: 'Survey Data export Done',
                                completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
                            }
                            util.createLog(event, `deLog status `, payloadInProgress);
                            const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
                            if (delogInProgress instanceof Error) {
                                util.createLog(event, `delogInProgress Error  `, delogInProgress);
                                return {
                                    statusCode: constants.statusCodes.InternalServerError,
                                    body: JSON.stringify({
                                        error: 'Error occured in Updating deLog'
                                    }),
                                };
                            }
                        }
                    }
                }
                // }

                itemCount++;
            }
        } else {
            util.createLog(event, `No survey responses to export for studyId `, studyId);
            // Update Delog Status
            const payloadInProgress = {
                status: constants.enums.deLogs.status.SUCCEEDED,
                message: 'No Data for Survey Export',
                completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
            }
            util.createLog(event, `deLog status `, payloadInProgress);
            const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
            if (delogInProgress instanceof Error) {
                util.createLog(event, `delogInProgress Error  `, delogInProgress);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Updating deLog'
                    }),
                };
            }
        }

        return {
            statusCode: constants.statusCodes.Success,
            body: JSON.stringify({
                    success: 'true survey-response'
                },
                null,
                2
            ),
        };
    } catch (error) {
        util.createLog(event, `Error: `, error);
        console.log('Error', error, 'deLogId', deLogId);

        const payloadFailed = {
            status: constants.enums.deLogs.status.FAILED,
            message: error && error.stack ? error.stack : error && error.message ? error.message : 'failed',
            completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss'),
        }

        const isStatusUpdated = await changeDeLogStatus(event, event.clientId, deLogId, payloadFailed);
        throw error;
    } finally {

    }
};