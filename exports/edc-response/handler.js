"use strict";

// const AWS = require('aws-sdk');

const awsXRay = require('aws-xray-sdk');
const AWS = awsXRay.captureAWS(require('aws-sdk'));

const constants = require('../../constants').constants;
const { util, db, files, s3Services, csvMappers, mappers, sendgridMail } = require('../../utility');
const dbObj = new db();
const dbName = constants.database.dbResearchResnpnse;
//const Excel = require('exceljs');

const _ = require('lodash');

let entityModels, responseEntityModels;

function checkIfSiteTeamMember(userRole) {
    if (!userRole ||
        userRole.toLowerCase() === constants.userRoles.pi.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.subPi.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.studyCoordinator.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.homeHealth.toLowerCase()) {
        return true;
    } else {
        return false;
    }
}
async function getStudySitesMap(event, studyId) {
    util.createLog(event, "getStudySiteMap called");
    try {
        let filter = {
            study_id: studyId
        };
        const studySites = await dbObj.findAll(filter, [], entityModels.StudySite, [], ['study_id', 'ASC']); //await dbObj.findOne(filter, [], entityModels.StudySite);
        return studySites;
    } catch (error) {
        util.createLog(event, `Error in getStudySiteMap: `, error);
        return error;
    }
}
async function getStudyFeatures(event, studyId) {
    try {
        let filter = {
            study_meta_data_id: studyId,
            feature_key: 'EDC',
            is_enabled: 1
        };
        const studyFeature = await dbObj.findOne(filter, [], entityModels.StudyFeature);
        return studyFeature;
    } catch (error) {
        util.createLog(event, `Error in getStudyFeatures: `, error);
        return error;
    }
}
async function getClientFeatures(event, clientId) {
    try {
        let filter = {
            client_config_id: clientId,
            feature_key: 'EDC',
            is_enabled: 1
        };
        const clientFeature = await dbObj.findOne(filter, [], entityModels.ClientFeature);
        return clientFeature;
    } catch (error) {
        util.createLog(event, `Error in getClientFeatures: `, error);
        return error;
    }
}
async function getStudyDetails(event, studyId) {
    util.createLog(event, `getStudyDetails Called`);
    try {
        let filter = {
            id: studyId,
        };
        const studyDetails = await dbObj.findOne(filter, [], entityModels.StudyMetaData);
        return studyDetails;
    } catch (error) {
        util.createLog(event, `Error in getStudyDetails: `, error);
        return error;
    }
}

async function getSFTPDetails(event, studyId) {
    util.createLog(event, `getSFTPDetails called`);
    try {
        let filter = {
            study_id: studyId,
        };
        const sftpDetails = await dbObj.findOneWithOrderBy(filter, [], entityModels.DeSftpMeta, ['sftp_version', 'DESC']);
        return sftpDetails;
    } catch (error) {
        util.createLog(event, `Error in getSFTPDetails : `, error)
        return error;
    }
}


async function createDeLog(event, clientId, studyId, logType, loggedInUserId) {
    util.createLog(event, `createDeLog Called`);
    try {

        let deSftpMetaId = null;
        if (logType === 'SFTP') {
            const SFTPSecreteManagerDetails = await getSFTPDetails(event, studyId);
            if (SFTPSecreteManagerDetails instanceof Error) {
                util.createLog(event, `getSFTPDetails Error: `, SFTPSecreteManagerDetails);
                await updateDeLogFailed(event, clientId, deLog, SFTPSecreteManagerDetails);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Upload file on sftp'
                    }),
                };
            }
            util.createLog(event, `getSFTPDetails fetched successfully `);
            deSftpMetaId = (SFTPSecreteManagerDetails && SFTPSecreteManagerDetails.id) ? SFTPSecreteManagerDetails.id : null;
        }

        const data = {
            study_id: studyId,
            de_sftp_meta_id: deSftpMetaId,
            trigger_datetime: util.changeDateToUtc(new Date()),
            completed_datetime: null,
            message: null,
            status: constants.enums.deLogs.status.STARTED,
            aws_job_id: (event && event.executionId) ? event.executionId : null,
            aws_log_path: null,
            type: logType,
            s3_location: null,
            triggered_by: loggedInUserId ? loggedInUserId : null
        };
        const payload = mappers.map.deLogDataMapper(data);
        const deLog = await dbObj.create(payload, entityModels.DeLog);
        return deLog;
    } catch (error) {
        util.createLog(event, `Error in createDeLog: `, error);
        return error;
    }
}

async function changeDeLogStatus(event, clientId, logId, payload) {
    util.createLog(event, `changeDeLogStatus Called`);
    try {
        const deLog = await dbObj.updateById(logId, payload, entityModels.DeLog);
        return deLog;
    } catch (error) {
        util.createLog(event, `Error in changeDeLogStatus: `, error);
        return error;
    }
}
async function getSiteDetails(event, siteId) {
    util.createLog(event, `getSiteDetails Called`);
    try {
        let filter = {
            id: siteId
        };
        const site = await dbObj.findOne(filter, [], entityModels.Sites);
        return site;
    } catch (error) {
        util.createLog(event, `Error in getSiteDetails: `, error);
        return error;
    }
}

async function getClientConfig(event, clientId) {
    util.createLog(event, `getClientConfig Called`);
    try {
        let filter = {
            id: clientId
        };
        const clientConfig = await dbObj.findOne(filter, [], entityModels.ClientConfig);
        return clientConfig;
    } catch (error) {
        util.createLog(event, `Error in getClientConfig: `, error);
        return error;
    }
}
async function getEdcResponseByStudyIdAndStateAndSiteId(event, studyId, stateId, siteId) {
    try {
        let filter = {
            study_id: studyId,
            state: stateId
        };
        const edcResponses = await dbObj.findAll(filter, [{
            model: responseEntityModels.EdcResponseVersion,
            attributes: [
                'site_id',
                'data'
            ]
        }], responseEntityModels.EdcResponse);
        return edcResponses;
    } catch (error) {
        util.createLog(event, `Error in getEdcResponseByStudyIdAndStateAndSiteId: `, error);
        return error;
    }
}
async function getEdcResponseByStudyIdAndStateAndDefaultSiteId(event, studyId, stateId, siteId) {
    try {
        let filter = {
            study_id: studyId,
            state: stateId
        };
        const edcResponses = await dbObj.findAll(filter, [{
            model: responseEntityModels.EdcResponseVersion,
            where: {
                $or: [{
                    site_id: {
                        $eq: siteId
                    }
                },
                {
                    site_id: {
                        $eq: null
                    }
                }
                ]
            },
            attributes: [
                'site_id',
                'data'
            ]
        }], responseEntityModels.EdcResponse);
        return edcResponses;
    } catch (error) {
        util.createLog(event, `Error in getEdcResponseByStudyIdAndStateAndDefaultSiteId: `, error);
        return error;
    }
}
async function getEdcResponseByStudyIdAndLoggedInUserSiteId(event, studyId, stateId, siteId) {
    try {
        let filter = {
            study_id: studyId,
            state: stateId
        };
        const edcResponses = await dbObj.findAll(filter, [{
            model: responseEntityModels.EdcResponseVersion,
            where: { site_id: siteId },
            attributes: [
                'site_id',
                'data'
            ]
        }], responseEntityModels.EdcResponse);
        return edcResponses;
    } catch (error) {
        util.createLog(event, `Error in getEdcResponseByStudyIdAndLoggedInUserSiteId: `, error);
        return error;
    }
}
async function getEdcUnscheduledResponseByStudyIdAndStateAndSiteId(event, studyId, stateId, siteId) {
    try {
        let filter = {
            study_id: studyId,
            state: stateId
        };
        const edcUnscheduledResponses = await dbObj.findAll(filter, [{
            model: responseEntityModels.EdcUnschduledPacketResponseVersion,
            attributes: [
                'site_id',
                'data'
            ]
        }], responseEntityModels.EdcUnschduledPacketResponse);
        return edcUnscheduledResponses;
    } catch (error) {
        util.createLog(event, `Error in getEdcUnscheduledResponseByStudyIdAndStateAndSiteId: `, error);
        return error;
    }
}
async function getEdcUnscheduledResponseByStudyIdAndStateAndDefaultSiteId(event, studyId, stateId, siteId) {
    try {
        let filter = {
            study_id: studyId,
            state: stateId
        };
        const edcUnscheduledResponses = await dbObj.findAll(filter, [{
            model: responseEntityModels.EdcUnschduledPacketResponseVersion,
            where: {
                $or: [{
                    site_id: {
                        $eq: siteId
                    }
                },
                {
                    site_id: {
                        $eq: null
                    }
                }
                ]
            },
            attributes: [
                'site_id',
                'data'
            ]
        }], responseEntityModels.EdcUnschduledPacketResponse);
        return edcUnscheduledResponses;
    } catch (error) {
        util.createLog(event, `Error in getEdcUnscheduledResponseByStudyIdAndStateAndDefaultSiteId: `, error);
        return error;
    }
}
async function getEdcUnscheduledResponseByStudyIdAndLoggedInUserSiteId(event, studyId, stateId, siteId) {
    try {
        let filter = {
            study_id: studyId,
            state: stateId
        };
        const edcUnscheduledResponses = await dbObj.findAll(filter, [{
            model: responseEntityModels.EdcUnschduledPacketResponseVersion,
            where: { site_id: siteId },
            attributes: [
                'site_id',
                'data'
            ]
        }], responseEntityModels.EdcUnschduledPacketResponse);
        return edcUnscheduledResponses;
    } catch (error) {
        util.createLog(event, `Error in getEdcUnscheduledResponseByStudyIdAndLoggedInUserSiteId: `, error);
        return error;
    }
}
async function getEdcUnscheduledPacket(event, packetId) {
    try {
        let filter = {
            id: packetId,
        };
        const edcUnscheduledPacket = await dbObj.findOneWithAttr(filter, [], responseEntityModels.EdcUnscheduledPacket, ["packet_name"]);
        return edcUnscheduledPacket;
    } catch (error) {
        util.createLog(event, `Error in getEdcUnscheduledPacket: `, error);
        return error;
    }
}

function getMultiAnswer(multiAnswer) {
    var answer = '';
    if (multiAnswer && multiAnswer.length > 0) {
        for (let ans of multiAnswer) {
            if (ans.answerText) {
                answer = answer + ans.answerText + ','
            }
        }
    }
    answer = util.removeLastCommafromString(answer);
    return answer;
}

function multiAnswerComment(multiAnswer) {
    var comment = '';
    if (multiAnswer && multiAnswer.length > 0) {
        for (let ans of multiAnswer) {
            if (ans.comment) {
                comment = comment + ans.comment + ','
            }
        }
    }
    comment = util.removeLastCommafromString(comment);
    return comment;
}

function getAnswersOptions(responseModel) {
    var answers = {}
    console.log("responseModel.answers>>>>>>>", responseModel.answers);
    for (let ans of responseModel.answers) {
        let map = {};
        if (ans.questionName.toLowerCase() === constants.enums.questionFieldType.formTable.toLocaleLowerCase() || ans.questionName.toLowerCase() === constants.enums.questionFieldType.formCheckbox.toLocaleLowerCase()) {
            map['ANSWER'] = getMultiAnswer(ans.multiAnswer);
            map['COMMENT'] = multiAnswerComment(ans.multiAnswer);
            answers[ans.questionId] = map;
        } else if (ans.questionName.toLowerCase() === constants.enums.questionFieldType.formDropdown.toLocaleLowerCase() ||
            ans.questionName.toLowerCase() === constants.enums.questionFieldType.formSinglechoice.toLocaleLowerCase() ||
            ans.questionName.toLowerCase() === constants.enums.questionFieldType.formTextNumeric.toLocaleLowerCase() ||
            ans.questionName.toLowerCase() === constants.enums.questionFieldType.formTextArea.toLocaleLowerCase()) {
            map['ANSWER'] = (ans.answerText) ? ans.answerText : '';
            map['COMMENT'] = (ans.comment) ? ans.comment : '';
            answers[ans.questionId] = map;
        } else if (ans.questionName.toLocaleLowerCase() === constants.enums.questionFieldType.formDateTime.toLocaleLowerCase()) {
            var date = '';
            if (ans.date) {
                date = date + ans.date;
            }
            if (ans.time) {
                if (ans.date) {
                    date = date + " ";
                }
                date = date + ans.time;
                if (ans.timezone) {
                    date = date + " " + ans.timezone;
                }
            }
            map['ANSWER'] = date;
            map['COMMENT'] = (ans.comment) ? ans.comment : '';
            answers[ans.questionId] = map;
        } else if (ans.questionName.toLocaleLowerCase() === constants.enums.questionFieldType.formScale.toLocaleLowerCase()) {
            map['WEIGHT'] = (ans.weight) ? ans.weight : '';
            map['ANSWER'] = (ans.answerText) ? ans.answerText : '';
            map['COMMENT'] = (ans.comment) ? ans.comment : '';
            answers[ans.questionId] = map;
        }

    }
    return answers;
}
async function getParticipantDetails(event, participantId) {
    util.createLog(event, `getParticipantDetails called`);
    try {
        let filter = {
            id: participantId
        };
        const participant = await dbObj.findOneWithAttr(filter, [], entityModels.Participant, ["user_defined_participant_id"]);
        return participant;
    } catch (error) {
        util.createLog(event, `Error in getParticipantDetails: `, error);
        return error;
    }
}
async function getUserDetail(event, id) {
    util.createLog(event, `getUserDetail called`);
    try {
        let filter = {
            id: id
        };
        const users = await dbObj.findOneWithAttr(filter, [], entityModels.UserData, ["first_name", "last_name"]);
        return users;
    } catch (error) {
        util.createLog(event, `Error in getUserDetail: `, error);
        return error;
    }
}
async function getPiUserDetail(piId, siteId, userRole) {
    try {
        let filter = {
            id: piId,
            site_id: siteId,
            user_role: userRole
        };
        const piUser = await dbObj.findOneWithAttr(filter, [], entityModels.UserData, ["id", "first_name", "last_name"]);
        return piUser;
    } catch (error) {
        return error;
    }
}
async function createCsvPayload(event, data) {
    util.createLog(event, `Creating CSV Payload Called`);
    const payload = [];
    for (const edcdata of data) {
        const mappedEdcData = csvMappers.map.edcResponseCsvMapper(edcdata);
        payload.push(mappedEdcData);
    }
    return payload;
}
async function getSiteCurrentPI(event, siteId, userRole, clientId, delogId) {
    util.createLog(event, `getSiteCurrentPI called`);
    try {
        let userStatus = [constants.user.status.Active, constants.user.status.Pending];
        let sitePI = null;
        let filter = {
            site_id: siteId,
            user_role: userRole,
            status: { $in: userStatus }
        };
        const users = await dbObj.findAll(filter, [], entityModels.UserData, []);
        if (users && users.length > 1) {
            let delogPayload = {
                status: constants.enums.deLogs.status.FAILED,
                message: 'More than one PI users for site: ' + siteId,
                completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
            }
            await changeDeLogStatus(event, clientId, delogId, delogPayload);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    message: 'Bad Request',
                    error: 'More than one PI users for site: ' + siteId
                }),
            };
        } else if (users && users.length === 0) {
            let filter = {
                site_id: siteId,
                user_role: userRole,
                status: constants.user.status.InActive
            };
            sitePI = await dbObj.findOne(filter, [], entityModels.UserData, ['modified_At', 'DESC']);
            return sitePI;
        } else if (!users) {
            return null;
        } else {
            return users[0];
        }
    } catch (error) {
        util.createLog(event, `Error in getSiteCurrentPI: `, error);
        return error;
    }
}

function getEdcResponseData(event, studyId, formResponseModel, edcResponse, answersOption, field, defaultSiteId, metaTable, packetData, clientId, delogId) {
    return new Promise(async (resolve, reject) => {
        let csvResponse = {};
        csvResponse['responseId'] = formResponseModel.responseModel.responseId;
        csvResponse['studyId'] = studyId;
        let participantId = edcResponse.participant_id;
        if (participantId) {
            csvResponse['participantId'] = participantId;
            // Getting Participant Details
            let participant = await getParticipantDetails(event, participantId);
            if (participant && participant.user_defined_participant_id)
                csvResponse['userDefinedParticipantId'] = participant.user_defined_participant_id;
        }
        csvResponse['formId'] = formResponseModel.formModel.id;
        csvResponse['formVersion'] = formResponseModel.formModel.formVersionId;
        csvResponse['formName'] = formResponseModel.formModel.name;
        csvResponse['milestone'] = formResponseModel.milestone;
        csvResponse['questionId'] = field.id;
        csvResponse['questionBody'] = field.body;
        csvResponse['fieldName'] = field.fieldName;
        if (answersOption && answersOption.hasOwnProperty(field.id) && answersOption[field.id].ANSWER) {
            csvResponse['answerText'] = answersOption[field.id].ANSWER;
        } else {
            csvResponse['answerText'] = "";
        }
        if (answersOption && answersOption.hasOwnProperty(field.id) && answersOption[field.id].WEIGHT) {
            csvResponse['weight'] = answersOption[field.id].WEIGHT;
        } else {
            csvResponse['weight'] = "";
        }
        if (answersOption && answersOption.hasOwnProperty(field.id) && answersOption[field.id].COMMENT) {
            csvResponse['comments'] = answersOption[field.id].COMMENT;
        } else {
            csvResponse['comments'] = "";
        }
        if (edcResponse.edc_unsch_pack_id) {
            csvResponse['unscheduledPacketId'] = edcResponse.edc_unsch_pack_id;
        } else {
            csvResponse['unscheduledPacketId'] = "";
        }
        if (packetData.packet_name) {
            csvResponse['unscheduledPacketName'] = packetData.packet_name;
        } else {
            csvResponse['unscheduledPacketName'] = "";
        }
        csvResponse['submittedTime'] = edcResponse.submitted_at;
        if (edcResponse.created_by) {
            // Getting user detail
            let userDetail = await getUserDetail(event, edcResponse.created_by);
            if (userDetail && userDetail.first_name && userDetail.last_name) {
                csvResponse['completedBy'] = userDetail.first_name + ' ' + userDetail.last_name
            }
        }
        if (!edcResponse.dataValues[metaTable].dataValues['site_id']) {
            if (defaultSiteId) {
                //Get sites details
                let siteDetails = await getSiteDetails(event, defaultSiteId);
                if (siteDetails.siteId) {
                    csvResponse["siteId"] = siteDetails.siteId
                }
                if (siteDetails.name) {
                    csvResponse["SiteName"] = siteDetails.name
                }
                //Get Pi user details
                let piUserData = await getSiteCurrentPI(event, defaultSiteId, constants.userRoles.pi, clientId, delogId)
                if (piUserData && piUserData.id) {
                    csvResponse["piId"] = piUserData.id
                }
                if (piUserData && piUserData.first_name && piUserData.last_name) {
                    csvResponse["piName"] = piUserData.first_name + ' ' + piUserData.last_name;
                }
            }
        } else {
            //Get sites details
            let edcSiteId = edcResponse.dataValues[metaTable].dataValues['site_id'];
            let siteDetails = await getSiteDetails(event, edcSiteId);
            if (siteDetails.siteId) {
                csvResponse["siteId"] = siteDetails.siteId
            }
            if (siteDetails.name) {
                csvResponse["SiteName"] = siteDetails.name
            }
            // Get pi user detail
            let piUserData = await getSiteCurrentPI(event, edcSiteId, constants.userRoles.pi)
            if (piUserData && piUserData.id) {
                csvResponse["piId"] = piUserData.id
            }
            if (piUserData && piUserData.first_name && piUserData.last_name) {
                csvResponse["piName"] = piUserData.first_name + ' ' + piUserData.last_name;
            }
        }
        resolve(csvResponse);
    });
}

async function validateStudy(event, studyId, clientId) {
    try {
        let filter = {
            id: studyId,
            client_id: clientId
        };
        const study = await dbObj.findOne(filter, [], entityModels.StudyMetaData);
        return study;
    } catch (error) {
        util.createLog(event, `Error in validateStudy: `, error);
        return error;
    }
}
module.exports.export = async (event) => {
    let deLogId;
    try {
        util.createLog(event, "Lambda edcResponse execution started");
        console.log('event', event);
        let sftpUpload = false;
        var isEDCEnabled = false;
        let clientId, studyId, zipFileNameWithTimeStamp;
        let loggedInUserId = (event && event.triggeredBy && event.triggeredBy.user && event.triggeredBy.user.id) ? event.triggeredBy.user.id : null;
        let hasAreas = [];
        if (event && event.areas) {
            hasAreas = event.areas.split(',');
        }
        //Checked area to execute edcResponse lambda
        if (hasAreas && hasAreas.length > 0 && hasAreas.includes(constants.exportAreas.edc) === false) {
            util.createLog(event, "Skipped lambda edcResponse execution");
            return;
        }
        //Checking for client id in payload
        if (event && event.clientId) {
            clientId = event.clientId;
        } else {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: 'Missing clientId'
                }),
            };
        }
        // checking for sftpupload parameter in payload
        if (event && event.sftpUpload && event.sftpUpload === true) {
            sftpUpload = event.sftpUpload;
        }
        // checking for studyId in paylod
        if (event && event.studyId) {
            studyId = event.studyId;
            util.createLog(event, `studyId is `, studyId);
        } else {
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    message: 'Bad Request',
                    error: 'studyId is required'
                }),
            };
        }
        const loggedInUserRole = (event && event.triggeredBy && event.triggeredBy.user && event.triggeredBy.user.role) ? event.triggeredBy.user.role : null;
        const loggedInUserSiteIds = event.siteIds;
        let loggedInUserSiteId;
        const isSiteTeamMember = checkIfSiteTeamMember(loggedInUserRole);
        if (isSiteTeamMember) {
            util.createLog(event, `Site team member logged in`);
            if (loggedInUserSiteIds && loggedInUserSiteIds.length > 0) {
                loggedInUserSiteId = loggedInUserSiteIds[0]
            } else {
                throw 'User is not associated with any site';
            }
        }
        util.createLog(event, `User is associated with site: loggedInUserSiteId ${loggedInUserSiteId}, LoggedIn user role is: ${loggedInUserRole}`);
        const models = await dbObj.init(clientId);
        entityModels = models;

        const responseModels = await dbObj.init(clientId, dbName);
        responseEntityModels = responseModels;
        
        const studyDetails = await validateStudy(event, studyId, clientId);
        if (!studyDetails || studyDetails.length === 0) {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: "Invalid study details or Unauthorized access"
                })
            };
        }

        // Get Study Features for study
        let studyFeatureWrapper = await getStudyFeatures(event, studyId);
        if (studyFeatureWrapper && studyFeatureWrapper.is_enabled && studyFeatureWrapper.is_enabled === true) {
            isEDCEnabled = true;
        } else {
            let clientFeatureWrapper = await getClientFeatures(event, clientId);
            if (clientFeatureWrapper && clientFeatureWrapper.is_enabled && clientFeatureWrapper.is_enabled === true) {
                isEDCEnabled = true;
            }
        }

        if (isEDCEnabled === true) {
            const logType = (sftpUpload === true) ? constants.enums.deLogs.type.SFTP : constants.enums.deLogs.type.DATAEXPORT;
            const deLog = await createDeLog(event, clientId, studyId, logType, loggedInUserId);
            if (deLog instanceof Error) {
                util.createLog(event, `unable to insert de log record: `, deLog);
                return {
                    statusCode: constants.statusCodes.BadRequest,
                    body: JSON.stringify({
                        error: 'Not able to create Delog'
                    }),
                };
            }
            util.createLog(event, `deLog: ${deLog.id}`);
            deLogId = deLog.id;
            var defaultSiteId;
            let studySitesMap = await getStudySitesMap(event, studyId);
            if (studySitesMap && studySitesMap.length > 0) {
                for (let site of studySitesMap) {
                    if (site.is_default && site.is_default === true) {
                        defaultSiteId = site.site_id;
                    }
                }
            }
            // Fetching client configuration details
            const clientConfig = await getClientConfig(event, clientId);
            if (clientConfig instanceof Error) {
                util.createLog(event, `clientConfig Error: `, clientConfig)
                let delogPayload = {
                    status: constants.enums.deLogs.status.FAILED,
                    message: 'Client Config not found',
                    completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
                }
                await changeDeLogStatus(event, clientId, deLog.id, delogPayload);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Client Config not found'
                    }),
                };
            }
            const bucketName = clientConfig.s3_bucket;
            const domain = clientConfig.domain_name;
            if (!bucketName) {
                util.createLog(event, `S3 Bucket not Found`);
                let delogPayload = {
                    status: constants.enums.deLogs.status.FAILED,
                    message: 'S3 Bucket not Found',
                    completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
                }
                await changeDeLogStatus(event, clientId, deLog.id, delogPayload);
                return {
                    statusCode: constants.statusCodes.BadRequest,
                    body: JSON.stringify({
                        error: 'S3 Bucket not Found'
                    }),
                };
            }
            let executionId = util.generateUniqueCode();
            if (event.executionId) {
                executionId = event.executionId;
            }
            let studyBaseUrl = `/tmp/${constants.tempExportFolder}/${studyId}_${executionId}`;
            if (event.studyBaseUrl) {
                studyBaseUrl = event.studyBaseUrl;
            }
            let zipFileName = studyId;
            if (studyDetails && studyDetails.name) {
                util.createLog(event, `Study Name ${studyDetails.name}`)
                zipFileName = studyDetails.name;
            }
            util.createLog(event, `zipFileName `, zipFileName);
            const dateNow = new Date();
            const timeStamp = util.changeDateFormat(dateNow, 'YYYYMMDDHHmm');
            zipFileNameWithTimeStamp = `${zipFileName}_${timeStamp}`;
            if (event.zipFileNameWithTimeStamp) {
                zipFileNameWithTimeStamp = event.zipFileNameWithTimeStamp;
            }
            util.createLog(event, `zipFileNameWithTimeStamp `, zipFileNameWithTimeStamp);
            let studyScheduledResponsesCsvFilePath = studyBaseUrl + "/EDC_Data/EDC_Responses";
            let edcResponsesFolder = "/" + studyId + "_" + executionId + "/";
            // Update Delog Status
            const payloadInProgress = { status: constants.enums.deLogs.status.INPROGRESS, message: 'edc-response' }
            util.createLog(event, `deLog status `, payloadInProgress);
            const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
            if (delogInProgress instanceof Error) {
                util.createLog(event, `delogInProgress Error  `, delogInProgress);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Updating deLog'
                    }),
                };
            }
            //Get EDC Data Responses//
            util.createLog(event, `Exporting EDC Responses for study :  ${studyId}`);
            let edcResponse = [];
            let edcScheduledResponses;
            if (!loggedInUserSiteId) {
                //For all user other than site team members
                util.createLog(event, `Edc Scheduled response if !loggedInUserSiteId`);
                edcScheduledResponses = await getEdcResponseByStudyIdAndStateAndSiteId(event, studyId, constants.ACCEPTED_STATE, defaultSiteId);
            } else if (defaultSiteId && defaultSiteId === loggedInUserSiteId) {
                //For default site's site team members this is for handling null values of site_is column
                util.createLog(event, `Edc Scheduled response if defaultSiteId && defaultSiteId === loggedInUserSiteId`);
                edcScheduledResponses = await getEdcResponseByStudyIdAndStateAndDefaultSiteId(event, studyId, constants.ACCEPTED_STATE, defaultSiteId);
            } else { //For non-default site's site team members
                edcScheduledResponses = await getEdcResponseByStudyIdAndLoggedInUserSiteId(event, studyId, constants.ACCEPTED_STATE, loggedInUserSiteId);
            }
            if (edcScheduledResponses && edcScheduledResponses.length > 0) {
                for (let scheduledResponse of edcScheduledResponses) {
                    if (scheduledResponse && scheduledResponse.edc_response_version && scheduledResponse.edc_response_version.data) {
                        let formResponseModel = JSON.parse(scheduledResponse.edc_response_version.data);
                        let answersOption = getAnswersOptions(formResponseModel.responseModel);
                        for (let field of formResponseModel.formModel.fields) {
                            if (field.fieldType.toLocaleLowerCase() === constants.enums.fixedValues.QUESTION.toLocaleLowerCase()) {
                                let edcResponseData = await getEdcResponseData(event, studyId, formResponseModel, scheduledResponse, answersOption, field, defaultSiteId, 'edc_response_version', '', clientId, deLog.id);
                                edcResponse.push(edcResponseData);
                            }
                        }
                    }
                }
            }
            //Get edc unscheduled responses data
            let edcUnscheduledResponses;
            if (!loggedInUserSiteId) {
                //For all user other than site team members
                util.createLog(event, `Edc Unscheduled response if loggedInUserSiteId`);
                edcUnscheduledResponses = await getEdcUnscheduledResponseByStudyIdAndStateAndSiteId(event, studyId, constants.ACCEPTED_STATE, defaultSiteId);
            } else if (defaultSiteId && defaultSiteId === loggedInUserSiteId) {
                //For default site's site team members this is for handling null values of site_is column
                util.createLog(event, `Edc Unscheduled response if defaultSiteId && defaultSiteId === loggedInUserSiteId`);
                edcUnscheduledResponses = await getEdcUnscheduledResponseByStudyIdAndStateAndDefaultSiteId(event, studyId, constants.ACCEPTED_STATE, defaultSiteId);
            } else { //For non-default site's site team members
                edcUnscheduledResponses = await getEdcUnscheduledResponseByStudyIdAndLoggedInUserSiteId(event, studyId, constants.ACCEPTED_STATE, loggedInUserSiteId);
            }
            if (edcUnscheduledResponses && edcUnscheduledResponses.length > 0) {
                //let unscheduledPacketResponse = [];
                for (let unscheduledResponse of edcUnscheduledResponses) {
                    let unscheduledResponseData = JSON.parse(unscheduledResponse.edc_unscheduled_packet_response_version.data);
                    let edcUnscheduledPacket = await getEdcUnscheduledPacket(event, unscheduledResponseData.unscheduledPacketId);
                    for (let packetResponse of unscheduledResponseData.packetFormResponsesModel) {
                        let answersOption = getAnswersOptions(packetResponse.responseModel);
                        for (let field of packetResponse.formModel.fields) {
                            if (field.fieldType.toLocaleLowerCase() === constants.enums.fixedValues.QUESTION.toLocaleLowerCase()) {
                                let edcResponseData = await getEdcResponseData(event, studyId, packetResponse, unscheduledResponse, answersOption, field, defaultSiteId, 'edc_unscheduled_packet_response_version', edcUnscheduledPacket, clientId, deLog.id);
                                edcResponse.push(edcResponseData);
                            }
                        }
                    }
                }
            }
            if (edcResponse && edcResponse.length > 0) {
                // Creating CSV payload and mappings for data
                const mappedEdcData = await createCsvPayload(event, edcResponse);

                // Getting Header for CSV File
                const headers = util.getKeys(mappedEdcData[0]);

                // Creating CSV File
                const isCsvCreated = await files.createCsvFile(`${studyScheduledResponsesCsvFilePath}/EDC_Responses.csv`, headers, mappedEdcData);
                if (isCsvCreated instanceof Error) {
                    util.createLog(event, `Csv create error: `, isCsvCreated);
                    let delogPayload = {
                        status: constants.enums.deLogs.status.FAILED,
                        message: 'Error in creating csv',
                        completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
                    }
                    await changeDeLogStatus(event, clientId, deLog.id, delogPayload);
                    return {
                        statusCode: constants.statusCodes.BadRequest,
                        body: JSON.stringify({
                            error: 'Not able to create CSV'
                        }),
                    };
                }
                util.createLog(event, `uploading files to s3 in temp folder`);
                // Creating Zip File
                const zipFile = await files.createZip(`${studyScheduledResponsesCsvFilePath}`, `${studyBaseUrl}/edcResponse.zip`, studyBaseUrl);
                // Convert Zip file to Binary file for S3 Upload
                const binaryZip = files.readFile(`${studyBaseUrl}/edcResponse.zip`);

                // Uploading Zip File to S3 Bucket
                const isUploadedToS3 = await s3Services.uploadFile(bucketName, `studies/${studyId}/tmp/${studyId}_${executionId}/edcResponse.zip`, binaryZip, 'application/zip');
                util.createLog(event, `is zipFile Uploaded To S3 `, isUploadedToS3);
                if (isUploadedToS3) {
                    util.createLog(event, `Deleting Temp Files`);
                    // files.removeFile(studyBaseUrl);
                }

                // Update Delog Status
                const payloadInProgress = {
                    status: constants.enums.deLogs.status.SUCCEEDED,
                    message: 'EDC Data export Done',
                    completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
                }
                util.createLog(event, `deLog status `, payloadInProgress);
                const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
                if (delogInProgress instanceof Error) {
                    util.createLog(event, `delogInProgress Error  `, delogInProgress);
                    return {
                        statusCode: constants.statusCodes.InternalServerError,
                        body: JSON.stringify({
                            error: 'Error occured in Updating deLog'
                        }),
                    };
                }
            } else {

                // Update Delog Status
                const payloadInProgress = {
                    status: constants.enums.deLogs.status.SUCCEEDED,
                    message: 'No Data for EDC Export',
                    completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
                }
                util.createLog(event, `deLog status `, payloadInProgress);
                const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
                if (delogInProgress instanceof Error) {
                    util.createLog(event, `delogInProgress Error  `, delogInProgress);
                    return {
                        statusCode: constants.statusCodes.InternalServerError,
                        body: JSON.stringify({
                            error: 'Error occured in Updating deLog'
                        }),
                    };
                }
            }
            util.createLog(event, `Finished Exporting EDC Responses Data`);
            return {
                statusCode: 200,
                body: JSON.stringify({
                    success: "EdcResponse created successfully"
                })
            };
        }
    } catch (error) {
        util.createLog(event, `Error in exporting edc data: `, error);
        console.log('Error', error, 'deLogId', deLogId);

        const payloadFailed = {
            status: constants.enums.deLogs.status.FAILED,
            message: error && error.stack ? error.stack : error && error.message ? error.message : 'failed',
            completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss'),
        }

        const isStatusUpdated = await changeDeLogStatus(event, event.clientId, deLogId, payloadFailed);
        throw error;
    } finally {

    }
};