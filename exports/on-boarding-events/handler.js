'use strict';

// const AWS = require('aws-sdk');

const awsXRay = require('aws-xray-sdk');
const AWS = awsXRay.captureAWS(require('aws-sdk'));

const constants = require('../../constants').constants;
const { util, db, files, s3Services, csvMappers, mappers, sendgridMail } = require('../../utility');
const dbObj = new db();
const dbName = constants.database.dbResearchResnpnse;

const _ = require('lodash');

let entityModels, responeEntityModels;


function checkIfSiteTeamMember(userRole) {
    if (!userRole ||
        userRole.toLowerCase() === constants.userRoles.pi.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.subPi.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.studyCoordinator.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.homeHealth.toLowerCase()) {
        return true;
    } else {
        return false;
    }
}

async function getStudyDetails(event, studyId) {
    util.createLog(event, `getStudyDetails called`);
    try {
        let filter = {
            id: studyId,
        };
        const studyDetails = await dbObj.findOne(filter, [], entityModels.StudyMetaData);
        return studyDetails;
    } catch (error) {
        util.createLog(event, `Error in getStudyDetails: `, error);
        return error;
    }
}

async function getSiteDetails(event, siteId) {
    util.createLog(event, `getSiteDetails called`);
    try {
        let filter = {
            id: siteId
        };
        const site = await dbObj.findOne(filter, [], entityModels.Sites);
        return site;
    } catch (error) {
        util.createLog(event, `Error in getSiteDetails: `, error);
        return error;
    }
}

async function getClientConfig(event, clientId) {
    util.createLog(event, `getClientConfig called`);
    try {
        let filter = {
            id: clientId
        };
        const clientConfig = await dbObj.findOne(filter, [], entityModels.ClientConfig);
        return clientConfig;
    } catch (error) {
        util.createLog(event, `Error in getClientConfig: `, error);
        return error;
    }
}

async function getSFTPDetails(event, studyId) {
    util.createLog(event, `getSFTPDetails called`);
    try {
        let filter = {
            study_id: studyId,
        };
        const sftpDetails = await dbObj.findOneWithOrderBy(filter, [], entityModels.DeSftpMeta, ['sftp_version', 'DESC']);
        return sftpDetails;
    } catch (error) {
        util.createLog(event, `Error in getSFTPDetails : `, error)
        return error;
    }
}

async function createDeLog(event, clientId, studyId, logType, loggedInUserId) {
    util.createLog(event, `createDeLog called`);
    try {
        let deSftpMetaId = null;
        if (logType === 'SFTP') {
            const SFTPSecreteManagerDetails = await getSFTPDetails(event, studyId);
            if (SFTPSecreteManagerDetails instanceof Error) {
                util.createLog(event, `getSFTPDetails Error: `, SFTPSecreteManagerDetails);
                await updateDeLogFailed(event, clientId, deLog, SFTPSecreteManagerDetails);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Upload file on sftp'
                    }),
                };
            }
            util.createLog(event, `getSFTPDetails fetched successfully `);
            deSftpMetaId = (SFTPSecreteManagerDetails && SFTPSecreteManagerDetails.id) ? SFTPSecreteManagerDetails.id : null;
        }
        const data = {
            study_id: studyId,
            de_sftp_meta_id: deSftpMetaId,
            trigger_datetime: util.changeDateToUtc(new Date()),
            completed_datetime: null,
            message: null,
            status: constants.enums.deLogs.status.STARTED,
            aws_job_id: (event && event.executionId) ? event.executionId : null,
            aws_log_path: null,
            type: logType,
            s3_location: null,
            triggered_by: loggedInUserId ? loggedInUserId : null
        };
        const payload = mappers.map.deLogDataMapper(data);

        const deLog = await dbObj.create(payload, entityModels.DeLog);
        return deLog;
    } catch (error) {
        util.createLog(event, `Error in createDeLog: `, error);
        return error;
    }
}

async function changeDeLogStatus(event, clientId, logId, payload) {
    util.createLog(event, `changeDeLogStatus called`)
    try {
        const deLog = await dbObj.updateById(logId, payload, entityModels.DeLog);
        return deLog;
    } catch (error) {
        util.createLog(event, `Error in changeDeLogStatus: `, error);
        return error;
    }
}

async function updateDeLogFailed(event, clientId, deLog) {
    // Update Delog Status
    const payloadFailed = { status: constants.enums.deLogs.status.FAILED }
    util.createLog(event, `deLog status: ${payloadFailed}`)
    const delogFailed = await changeDeLogStatus(event, clientId, deLog.id, payloadFailed);
    if (delogFailed instanceof Error) {
        return {
            statusCode: constants.statusCodes.InternalServerError,
            body: JSON.stringify({
                error: 'Error occured in Updating deLog'
            }),
        };
    }
}

var findByStudyId = async (event, clientId, studyId, defaultSiteId) => {
    try {
        let filter = {
            studyId: studyId
        };
        const participantOnBoardingEvent = await dbObj.findAll(filter, [], responeEntityModels.ParticipantOnBoardingEvent);

        for (const data of participantOnBoardingEvent) {
            data.dataValues.siteId = data.dataValues.siteId ? data.dataValues.siteId : defaultSiteId;

            data.dataValues.offset = data.dataValues.offset ? data.dataValues.offset : '+00:00';

            data.dataValues.localConsentedTime = data.dataValues.localConsentedTime ? data.dataValues.localConsentedTime : data.dataValues.consentedTime;

            data.dataValues.localRegistrationTime = data.dataValues.localRegistrationTime ? data.dataValues.localRegistrationTime : data.dataValues.registeredTime;
        }

        return participantOnBoardingEvent;
    } catch (error) {
        util.createLog(event, `Error in findByStudyId : `, error);
        return error;
    }
}

var findByStudyIdAndDefaultSiteId = async (event, clientId, studyId, defaultSiteId) => {
    try {
        let filter = {
            studyId: studyId,
            site_id: defaultSiteId
            // $or: [{ site_id: defaultSiteId }, { site_id: null }]
        };
        const participantOnBoardingEvent = await dbObj.findAll(filter, [], responeEntityModels.ParticipantOnBoardingEvent);

        for (const data of participantOnBoardingEvent) {
            data.dataValues.siteId = data.dataValues.siteId ? data.dataValues.siteId : defaultSiteId;

            data.dataValues.offset = data.dataValues.offset ? data.dataValues.offset : '+00:00';

            data.dataValues.localConsentedTime = data.dataValues.localConsentedTime ? data.dataValues.localConsentedTime : data.dataValues.consentedTime;

            data.dataValues.localRegistrationTime = data.dataValues.localRegistrationTime ? data.dataValues.localRegistrationTime : data.dataValues.registeredTime;
        }
        return participantOnBoardingEvent;
    } catch (error) {
        util.createLog(event, `Error in findByStudyIdAndDefaultSiteId : `, error);
        return error;
    }
}

var findByStudyIdAndSiteId = async (event, clientId, studyId, loggedInUserSiteId) => {
    try {
        let filter = {
            studyId: studyId,
            siteId: loggedInUserSiteId
        };
        const participantOnBoardingEvent = await dbObj.findAll(filter, [], responeEntityModels.ParticipantOnBoardingEvent);

        for (const data of participantOnBoardingEvent) {
            data.dataValues.offset = data.dataValues.offset ? data.dataValues.offset : '+00:00';

            data.dataValues.localConsentedTime = data.dataValues.localConsentedTime ? data.dataValues.localConsentedTime : data.dataValues.consentedTime;

            data.dataValues.localRegistrationTime = data.dataValues.localRegistrationTime ? data.dataValues.localRegistrationTime : data.dataValues.registeredTime;
        }

        return participantOnBoardingEvent;
    } catch (error) {
        util.createLog(event, `Error in findByStudyIdAndSiteId : `, error);
        return error;
    }
}

var getUserDefinedParticipantId = async (event, ParticipantID) => {
    try {
        let filter = {
            id: ParticipantID
        };
        if (ParticipantID !== null) {
            const participantId = await dbObj.findOne(filter, [], entityModels.Participant);
            return (participantId && participantId.user_defined_participant_id) ? participantId.user_defined_participant_id : ''
        } else {
            return null;
        }
    } catch (error) {
        util.createLog(event, `Error in getUserDefinedParticipantId : `, error);
        return error;
    }
}

var createParticipantEventsDataResponsesCsvFile = async (event, participantEventsData, participantEventsDataResponsesBaseUrl) => {
    const ParticipantEventsDataResponses = await createCsvPayload(event, participantEventsData);

    // Getting Header for CSV File
    const headers = await util.getKeys(ParticipantEventsDataResponses[0]);

    // Creating CSV File
    const isCsvCreated = await files.createCsvFile(`${participantEventsDataResponsesBaseUrl}/timings.csv`, headers, ParticipantEventsDataResponses);
    if (isCsvCreated instanceof Error) {
        return {
            statusCode: constants.statusCodes.BadRequest,
            body: JSON.stringify({
                error: 'Not able to create json'
            }),
        };
    }
}

async function createCsvPayload(event, data) {
    util.createLog(event, `Creating CSV Payload`)
    const payload = [];
    for (const participantEventsData of data) {
        const mappedParticipantEventsData = csvMappers.map.participantOnBoardingEventCsvMapper(participantEventsData);
        payload.push(mappedParticipantEventsData);
    }
    return payload;
}

var createParticipantEventsDataResponsesJsonFile = async (event, participantEventsData, participantEventsDataResponsesBaseUrl) => {

    for (let data of participantEventsData) {
        var localRegistrationTimeTemp;
        if (util.stringDateFormatter(data.localRegistrationTime, data.offset) !== 'Invalid date') {
            localRegistrationTimeTemp = util.stringDateFormatter(data.localRegistrationTime, data.offset, 'YYYY-MM-DD HH:mm:ss');
        } else {
            let endTime = (data.localRegistrationTime && data.localRegistrationTime.length > 10) ?
                data.localRegistrationTime.slice(0, -5) + "" : '';
            localRegistrationTimeTemp = util.stringDateFormatter(endTime, data.offset, 'YYYY-MM-DD HH:mm:ss')
        }
        data.localRegistrationTime = localRegistrationTimeTemp;
        data.consentedTime = util.changeDateToUtc(data.consentedTime);

        if (util.stringDateFormatter(data.localConsentedTime, data.offset) !== 'Invalid date')
            data.localConsentedTime = util.stringDateFormatter(data.localConsentedTime, data.offset, 'YYYY-MM-DD HH:mm:ss');
        else
            data.localConsentedTime = null;

        data.userDefinedParticipantId = data.userDefinedParticipantId ? data.userDefinedParticipantId : null;
    }

    const isJsonCreated = await files.createJsonFile(`${participantEventsDataResponsesBaseUrl}/timings.json`, participantEventsData);
    if (isJsonCreated instanceof Error) {
        return {
            statusCode: constants.statusCodes.BadRequest,
            body: JSON.stringify({
                error: 'Not able to create json'
            }),
        };
    }
    util.createLog(event, `Json File ${participantEventsDataResponsesBaseUrl}/timings.json Created Successfully`);

}

var findParticipantOnBoardingConsentEventRepository = async (event, clientId, studyId, siteID) => {

    try {
        var whereStr;
        if (siteID)
            whereStr = `E.study_id = '${studyId}' AND E.site_id='${siteID}'`
        else
            whereStr = `E.study_id = '${studyId}'`

        var query = `SELECT DISTINCT E.id, E.study_id AS studyId, SM.name AS studyName, E.site_id AS siteId, S.name AS siteName, E.participant_id AS participantId,
        CE.cohort_id AS cohortId, C.cohort_name AS cohortName, CE.consent_id AS consentId, CC.consent_name AS consentName, CE.consent_end_time AS consentedTime,
        IF(participant_local_consent_end_time IS NOT NULL, participant_local_consent_end_time, consent_end_time) AS localConsentedTime,
        IF(participant_local_consent_time_offset IS NULL, '+00:00', participant_local_consent_time_offset) as offset
        FROM research_response.participant_on_boarding_event E
        INNER JOIN research_response.participant_on_boarding_consent_event CE ON CE.participant_on_boarding_event_id = E.id
        INNER JOIN research.consent_configuration CC ON CC.consent_id = CE.consent_Id
        INNER JOIN research.cohort C ON C.cohort_id = CE.cohort_Id
        INNER JOIN research.study_meta_data SM ON SM.id = E.study_id
        INNER JOIN research.site S ON S.id = E.site_id
        WHERE ${whereStr} ORDER BY CE.consent_end_time`;
        const participantOnBoardingEvent = await dbObj.rawQueryWithConn(clientId, query, responeEntityModels.ParticipantOnBoardingEvent, dbName);
        return participantOnBoardingEvent;

        // let filter = {
        //     studyId: studyId,
        // };
        // const participantOnBoardingEvent = await dbObj.findAll(filter, [{
        //     model: responeEntityModels.ParticipantOnBoardingConsentEvent,
        //     order: ['participant_on_boarding_consent_event.consent_end_time', 'ASC'],
        //     attributes: ['cohort_id', 'consent_id', 'consent_end_time']
        // }], responeEntityModels.ParticipantOnBoardingEvent);
        // var participantOnBoardingEventArr = [];
        // for (const data of participantOnBoardingEvent) {
        //     data.dataValues.participantLocalConsentEndTime = data.dataValues.participantLocalConsentEndTime ? data.dataValues.participantLocalConsentEndTime : data.dataValues.consentEndTime;

        //     data.dataValues.participantLocalConsentTimeOffset = data.dataValues.participantLocalConsentTimeOffset ? data.dataValues.participantLocalConsentTimeOffset : '+00:00';

        //     let filter3 = { id: data.dataValues.studyId };
        //     const studyMetaData = await dbObj.findOne(filter3, [], entityModels.StudyMetaData);
        //     data.dataValues["studyName"] = studyMetaData.name;

        //     if (data.dataValues.participant_on_boarding_consent_events && data.dataValues.participant_on_boarding_consent_events.length > 0) {
        //         for (var metaData of data.dataValues.participant_on_boarding_consent_events) {

        //             let filter1 = { consentId: metaData.dataValues.consent_id };
        //             // data.dataValues.participant_on_boarding_consent_events[0]
        //             var consentConfiguration = await dbObj.findOne(filter1, [], entityModels.ConsentConfiguration);
        //             participantOnBoardingEventObj["consentId"] = consentConfiguration.consentId;
        //             participantOnBoardingEventObj["consentName"] = consentConfiguration.consentName;

        //             let filter2 = { cohortId: metaData.dataValues.cohort_id };
        //             // data.dataValues.participant_on_boarding_consent_events[0]
        //             var cohort = await dbObj.findOne(filter2, [], entityModels.Cohort);
        //             participantOnBoardingEventObj["cohortId"] = cohort.cohortId;
        //             participantOnBoardingEventObj["cohortName"] = cohort.cohortName;

        //             console.log("participantOnBoardingEventObj ?????", JSON.stringify(participantOnBoardingEventObj));
        //             participantOnBoardingEventArr.push(participantOnBoardingEventObj);
        //             console.log("participantOnBoardingEventArr ?????", JSON.stringify(participantOnBoardingEventArr));
        //             participantOnBoardingEventObj = {};

        //         }
        //     }

        //     // let filter4 = { id: data.dataValues.siteId };
        //     // const site = await dbObj.findOne(filter4, [], entityModels.Sites);
        //     // data.dataValues["siteName"] = site.name;

        // }



        // return participantOnBoardingEventArr;
    } catch (error) {
        util.createLog(event, `Error in findParticipantOnBoardingConsentEvent : `, error)
        return error;
    }
}

var createConsentEventsDataResponsesCsvFile = async (event, consentOnBoardingEvent, consentOnBoardingEventBaseUrl) => {
    const ConsentOnBoardingEventData = await createCsvPayloadConsentEvents(event, consentOnBoardingEvent);
    if (ConsentOnBoardingEventData && ConsentOnBoardingEventData.length > 0) {
        // Getting Header for CSV File
        const headers = await util.getKeys(ConsentOnBoardingEventData[0]);
        // Creating CSV File
        const isCsvCreated = await files.createCsvFile(`${consentOnBoardingEventBaseUrl}/consent_timings.csv`, headers, ConsentOnBoardingEventData);
        if (isCsvCreated instanceof Error) {
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Not able to create json'
                }),
            };
        }
    }
}

async function createCsvPayloadConsentEvents(event, data) {
    util.createLog(event, `Creating CSV Payload Consent Events`)
    const payload = [];
    for (const consentOnBoardingEvent of data) {
        const mappedConsentOnBoardingEvent = csvMappers.map.consentOnBoardingEventCsvMapper(consentOnBoardingEvent);
        payload.push(mappedConsentOnBoardingEvent);
    }
    return payload;
}

var createConsentEventsDataResponsesJsonFile = async (event, consentOnBoardingEvent, consentOnBoardingEventBaseUrl) => {
    if (consentOnBoardingEvent && consentOnBoardingEvent.length > 0) {

        for (let data of consentOnBoardingEvent) {
            let endTimeString = util.changeDateToIsoFormat(data.consentedTime);
            let endTime = (endTimeString && endTimeString.length > 10) ?
                endTimeString.slice(0, -1) + " 0000" : '';
            data.consentedTime = endTime;
            data.localConsentedTime = util.stringDateFormatter(data.localConsentedTime, data.offset, 'YYYY-MM-DD HH:mm:ss');
        }

        const isJsonCreated = await files.createJsonFile(`${consentOnBoardingEventBaseUrl}/consent_timings.json`, consentOnBoardingEvent);
        if (isJsonCreated instanceof Error) {
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Not able to create json'
                }),
            };
        }
        util.createLog(event, `Json File ${consentOnBoardingEventBaseUrl}/consent_timings.json Created Successfully`);
    } else {
        util.createLog(event, `No data available for consent_timings.json`);
    }
}

async function getStudyDefaultSite(event, studyId) {
    try {
        let filter = {
            study_id: studyId,
            is_default: 1
        };
        const defaultSite = await dbObj.findOne(filter, [], entityModels.StudySite);
        return defaultSite;
    } catch (error) {
        util.createLog(event, `Error in getStudyDefaultSite : `, error)
        return error;
    }
}

async function getStudySitesMap(event, studyId) {
    util.createLog(event, `getStudySiteMap called`);
    try {
        let filter = {
            study_id: studyId
        };
        const studySites = await dbObj.findAll(filter, [], entityModels.StudySite, [], ['study_id', 'ASC']); //await dbObj.findOne(filter, [], entityModels.StudySite);
        return studySites;
    } catch (error) {
        util.createLog(event, `Error in getStudySitesMap: `, error);
        return error;
    }
}
async function validateStudy(event, studyId, clientId) {
    try {
        let filter = {
            id: studyId,
            client_id: clientId
        };
        const study = await dbObj.findOne(filter, [], entityModels.StudyMetaData);
        return study;
    } catch (error) {
        util.createLog(event, `Error in validateStudy: `, error);
        return error;
    }
}
module.exports.export = async event => {
    let deLogId;
    try {
        util.createLog(event, `Lambda on-boarding-events execution started`);
        let sftpUpload = false;
        let clientId, studyId, zipFileNameWithTimeStamp;
        let loggedInUserId = (event && event.triggeredBy && event.triggeredBy.user && event.triggeredBy.user.id) ? event.triggeredBy.user.id : null;
        let hasAreas = [];
        if (event && event.areas) {
            hasAreas = event.areas.split(',');
        }
        //Checked area to execute onboardingEvents lambda
        if (hasAreas && hasAreas.length > 0 && hasAreas.includes(constants.exportAreas.onBoardingEvents) === false) {
            util.createLog(event, `Lambda Skipped lambda on-boarding-events`);
            return;
        }
        //Checking for client id in payload
        if (event && event.clientId) {
            clientId = event.clientId;
        } else {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: 'Missing clientId'
                }),
            };
        }
        // checking for sftpupload parameter in payload
        if (event && event.sftpUpload && event.sftpUpload === true) {
            sftpUpload = event.sftpUpload;
        }
        // checking for studyId in paylod
        if (event && event.studyId) {
            studyId = event.studyId;
            util.createLog(event, `studyId is `, studyId);
        } else {
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    message: 'Bad Request',
                    error: 'studyId is required'
                }),
            };
        }
        const models = await dbObj.init(clientId);
        entityModels = models;

        const responseModels = await dbObj.init(clientId, dbName);
        responeEntityModels = responseModels;

        const validateStudyDetails = await validateStudy(event, studyId, clientId);
        if (!validateStudyDetails || validateStudyDetails.length === 0) {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: "Invalid study details or Unauthorized access"
                })
            };
        }
        const loggedInUserRole = (event && event.triggeredBy && event.triggeredBy.user && event.triggeredBy.user.role) ? event.triggeredBy.user.role : null;
        const loggedInUserSiteIds = event.siteIds;
        let loggedInUserSiteId;

        const isSiteTeamMember = checkIfSiteTeamMember(loggedInUserRole);
        if (isSiteTeamMember) {
            util.createLog(event, `Site team member logged in`);
            if (loggedInUserSiteIds && loggedInUserSiteIds.length > 0) {
                loggedInUserSiteId = loggedInUserSiteIds[0]
            } else {
                throw 'User is not associated with any site';
            }
        }
        // if (!isSiteTeamMember) {
        //     return {
        //         statusCode: constants.statusCodes.Unauthorized,
        //         body: JSON.stringify({
        //             message: 'User is not authorized user'
        //         }),
        //     };
        // }

        // let loggedInUserSiteId;
        // if (!loggedInUserSiteIds || loggedInUserSiteIds.length < 0) {
        //     // return {
        //     // statusCode: constants.statusCodes.Unauthorized,
        //     // body: JSON.stringify({
        //     //     message: 'User is not associated with any site'
        //     // }),
        //     // };
        //     const studyDefaultSite = await getStudyDefaultSite(event, studyId);
        //     if (studyDefaultSite && studyDefaultSite.is_default) {
        //         loggedInUserSiteId = studyDefaultSite.site_id;
        //     }
        // } else {
        //     loggedInUserSiteId = loggedInUserSiteIds[0];
        // }
        // const loggedInUserSiteId = null;

        util.createLog(event, `User is associated with site: loggedInUserSiteId ${loggedInUserSiteId}, LoggedIn user role is: ${loggedInUserRole}`);

        const studyDetails = await getStudyDetails(event, studyId);
        if (studyDetails instanceof Error) {
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'study Details not found'
                }),
            };
        }
        // Creating deLog
        const logType = (sftpUpload === true) ? constants.enums.deLogs.type.SFTP : constants.enums.deLogs.type.DATAEXPORT;
        const deLog = await createDeLog(event, clientId, studyId, logType, loggedInUserId);
        if (deLog instanceof Error) {
            util.createLog(event, `unable to insert de log record: ${deLog}`);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Not able to create Delog'
                }),
            };
        }
        deLogId = deLog.id;
        util.createLog(event, `deLog: ${deLog.id}`);
        var defaultSiteId;
        let studySitesMap = await getStudySitesMap(event, studyId);
        if (studySitesMap && studySitesMap.length > 0) {
            for (let site of studySitesMap) {
                if (site.is_default && site.is_default === true) {
                    defaultSiteId = site.site_id;
                }
            }
        }


        // Fetching client configuration details
        const clientConfig = await getClientConfig(event, clientId);
        if (clientConfig instanceof Error) {

            await updateDeLogFailed(event, clientId, deLog);
            util.createLog(event, `deLog failed ${deLog.id}`);
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'Client Config not found'
                }),
            };
        }
        const bucketName = clientConfig.s3_bucket;
        const domain = clientConfig.domain_name;
        if (!bucketName) {
            await updateDeLogFailed(event, clientId, deLog);
            util.createLog(event, `deLog failed ${deLog.id}`);
            util.createLog(event, `S3 Bucket not Found`);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'S3 Bucket not Found'
                }),
            };
        }

        let executionId = util.generateUniqueCode();
        if (event.executionId) {
            executionId = event.executionId;
        }
        let studyBaseUrl = `/tmp/${constants.tempExportFolder}/${studyId}_${executionId}`;
        if (event.studyBaseUrl) {
            studyBaseUrl = event.studyBaseUrl;
        }
        let zipFileName = studyId;
        if (studyDetails && studyDetails.name) {
            util.createLog(event, `Study Name ${studyDetails.name}`)
            zipFileName = studyDetails.name;
        }
        util.createLog(event, `zipFileName `, zipFileName);
        const dateNow = new Date();
        const timeStamp = util.changeDateFormat(dateNow, 'YYYYMMDDHHmm');

        zipFileNameWithTimeStamp = `${zipFileName}_${timeStamp}`;
        if (event.zipFileNameWithTimeStamp) {
            zipFileNameWithTimeStamp = event.zipFileNameWithTimeStamp;
        }
        util.createLog(event, `zipFileNameWithTimeStamp `, zipFileNameWithTimeStamp);
        const onBoardingStudyBaseUrl = studyBaseUrl + "/onBoarding";

        // Update Delog Status
        const payloadInProgress = { status: constants.enums.deLogs.status.INPROGRESS, message: 'on-boarding-events' }
        util.createLog(event, `deLog status `, payloadInProgress);
        const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
        if (delogInProgress instanceof Error) {
            util.createLog(event, `delogInProgress Error  `, delogInProgress);
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'Error occured in Updating deLog'
                }),
            };
        }

        var participantEventsDataResponses = [];
        if (!loggedInUserSiteId) { //For non-default site's site team members
            util.createLog(event, `For non-default sites site team members`);
            participantEventsDataResponses = await findByStudyId(event, clientId, studyId, defaultSiteId);
        } else if (defaultSiteId && defaultSiteId != null && defaultSiteId === loggedInUserSiteId) {
            //For default site's site team members this is for handling null values of site_is column
            util.createLog(event, `defaultSiteId && defaultSiteId === loggedInUserSiteId`);
            participantEventsDataResponses = await findByStudyIdAndDefaultSiteId(event, clientId, studyId, defaultSiteId);
        } else {
            //For all user other than site team members
            util.createLog(event, `loggedInUserSiteId`);
            participantEventsDataResponses = await findByStudyIdAndSiteId(event, clientId, studyId, loggedInUserSiteId);
        }

        if (participantEventsDataResponses instanceof Error) {
            await updateDeLogFailed(event, clientId, deLog);
            util.createLog(event, `deLog failed ${deLog.id}`)
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'error in Participant Events Data Responses'
                }),
            };
        }

        if (participantEventsDataResponses && participantEventsDataResponses.length > 0) {

            var participantEventsDataResponsesArr = [];

            // Fetching site details
            const siteDetails = await getSiteDetails(event, (loggedInUserSiteId) ? loggedInUserSiteId : defaultSiteId);
            if (siteDetails instanceof Error) {
                util.createLog(event, `deLog siteDetails : `, siteDetails);
                return {
                    statusCode: constants.statusCodes.BadRequest,
                    body: JSON.stringify({
                        error: 'Site Details not found'
                    }),
                };
            }

            const siteId = siteDetails.siteId;
            util.createLog(event, `siteId : ${siteId}`);
            for (const data of participantEventsDataResponses) {
                // data.dataValues["siteId"] = siteDetails.siteId;
                // data.dataValues["siteName"] = siteDetails.name;
                const siteInfo = await getSiteDetails(event, data.siteId ? data.siteId : siteDetails.id);
                data.dataValues["siteId"] = siteInfo.siteId;
                data.dataValues["siteName"] = siteInfo.name;

                data.dataValues["userDefinedParticipantId"] = await getUserDefinedParticipantId(event, data.dataValues.participantId);
                participantEventsDataResponsesArr.push(data.dataValues)
            }

            const participantEventsDataResponsesBaseUrl = studyBaseUrl + "/onBoarding/events/";
            await createParticipantEventsDataResponsesCsvFile(event, participantEventsDataResponsesArr, participantEventsDataResponsesBaseUrl); //, studySitesMap
            util.createLog(event, `Finished creating OnBoarding events Json file for study ${studyId}`)
            await createParticipantEventsDataResponsesJsonFile(event, participantEventsDataResponsesArr, participantEventsDataResponsesBaseUrl); //, studySitesMap
            util.createLog(event, `Finished creating OnBoarding Eligibility responses Json file for study ${studyId}`)

            var tempSiteId;
            if (!loggedInUserSiteId) {
                tempSiteId = null;
            } else if (defaultSiteId && defaultSiteId != null && defaultSiteId === loggedInUserSiteId) {
                tempSiteId = defaultSiteId;
            } else {
                tempSiteId = loggedInUserSiteId;
            }
            const listConsentOnBoardingEvent = await findParticipantOnBoardingConsentEventRepository(event, clientId, studyId, tempSiteId);
            var consentOnBoardingEventArr = [];

            if (listConsentOnBoardingEvent instanceof Error) {
                await updateDeLogFailed(event, clientId, deLog);
                util.createLog(event, `deLog failed ${deLog.id}`)
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'error in Consent On-Boarding Event'
                    }),
                };
            }

            for (const data of listConsentOnBoardingEvent) {
                if (data) {
                    const siteInfo = await getSiteDetails(event, data.siteId ? data.siteId : siteDetails.id);
                    data.siteId = siteInfo.siteId;
                    data.siteName = siteInfo.name;
                    // data["siteName"] = data.siteName; //siteDetails.name;

                    data["userDefinedParticipantId"] = await getUserDefinedParticipantId(event, data.participantId);

                    data["cohortId"] = data.cohortId ? data.cohortId : ''; //(data.dataValues.participant_on_boarding_consent_events && data.dataValues.participant_on_boarding_consent_events.length > 0) data.dataValues.participant_on_boarding_consent_events[0].dataValues.cohort_id
                    data["cohortName"] = data.cohortName;
                    data["consentId"] = data.consentId ? data.consentId : ''; //(data.dataValues.participant_on_boarding_consent_events && data.dataValues.participant_on_boarding_consent_events.length > 0) data.dataValues.participant_on_boarding_consent_events[0].dataValues.consent_id
                    data["consentName"] = data.consentName;
                    consentOnBoardingEventArr.push(data);
                }
            }

            await createConsentEventsDataResponsesCsvFile(event, consentOnBoardingEventArr, participantEventsDataResponsesBaseUrl); //, studySitesMap
            util.createLog(event, `Finished creating OnBoarding consent events csv file for study ${studyId}`)
            await createConsentEventsDataResponsesJsonFile(event, consentOnBoardingEventArr, participantEventsDataResponsesBaseUrl); //, studySitesMap
            util.createLog(event, `Finished creating OnBoarding consent events Json file for study ${studyId}`);

            util.createLog(event, `uploading files to s3 in temp folder`);
            // Creating Zip File
            const zipFile = await files.createZip(`${onBoardingStudyBaseUrl}`, `${studyBaseUrl}/onBoardingEvent.zip`, studyBaseUrl);
            // Convert Zip file to Binary file for S3 Upload
            const binaryZip = files.readFile(`${studyBaseUrl}/onBoardingEvent.zip`);

            // Uploading Zip File to S3 Bucket
            const isUploadedToS3 = await s3Services.uploadFile(bucketName, `studies/${studyId}/tmp/${studyId}_${executionId}/onBoardingEvent.zip`, binaryZip, 'application/zip');
            util.createLog(event, `is zipFile Uploaded To S3 `, isUploadedToS3);
            if (isUploadedToS3) {
                util.createLog(event, `Deleting Temp Files`);
                // files.removeFile(studyBaseUrl);
            }

            // Update Delog Status
            const payloadInProgress = {
                status: constants.enums.deLogs.status.SUCCEEDED,
                message: 'onBoardingEvent Data export Done',
                completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
            }
            util.createLog(event, `deLog status `, payloadInProgress);
            const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
            if (delogInProgress instanceof Error) {
                util.createLog(event, `delogInProgress Error  `, delogInProgress);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Updating deLog'
                    }),
                };
            }
        } else {
            util.createLog(event, `No OnBoarding Event responses to export for studyId`);
            // Update Delog Status
            const payloadInProgress = {
                status: constants.enums.deLogs.status.SUCCEEDED,
                message: 'No Data for OnBoarding Event Export',
                completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
            }
            util.createLog(event, `deLog status `, payloadInProgress);
            const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
            if (delogInProgress instanceof Error) {
                util.createLog(event, `delogInProgress Error  `, delogInProgress);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Updating deLog'
                    }),
                };
            }
        }

        //#endregion

        return {
            statusCode: constants.statusCodes.Success,
            body: JSON.stringify({
                success: 'true on-boarding-event',
            },
                null,
                2
            ),
        };
    } catch (error) {
        util.createLog(event, `Error in exporting edc data: `, error);
        console.log('Error', error, 'deLogId', deLogId);

        const payloadFailed = {
            status: constants.enums.deLogs.status.FAILED,
            message: error && error.stack ? error.stack : error && error.message ? error.message : 'failed',
            completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss'),
        }

        const isStatusUpdated = await changeDeLogStatus(event, event.clientId, deLogId, payloadFailed);
        throw error;
    } finally {

    }
};