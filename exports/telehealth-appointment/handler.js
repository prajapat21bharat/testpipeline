"use strict";

// const AWS = require('aws-sdk');

const awsXRay = require('aws-xray-sdk');
const AWS = awsXRay.captureAWS(require('aws-sdk'));

const constants = require('../../constants').constants;
const { util, db, files, s3Services, csvMappers, mappers, sendgridMail, excelMappers } = require('../../utility');
const dbObj = new db();
const dbName = constants.database.dbResearchResnpnse;

const _ = require('lodash');

let entityModels, responseEntityModels;

function checkIfSiteTeamMember(userRole) {
    if (!userRole ||
        userRole.toLowerCase() === constants.userRoles.pi.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.subPi.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.studyCoordinator.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.homeHealth.toLowerCase()) {
        return true;
    } else {
        return false;
    }
}
async function getStudyDetails(event, studyId) {
    util.createLog(event, `getStudyDetails called`);
    try {
        let filter = {
            id: studyId,
        };
        const studyDetails = await dbObj.findOne(filter, [], entityModels.StudyMetaData);
        return studyDetails;
    } catch (error) {
        util.createLog(event, `Error in getStudyDetails: `, error);
        return error;
    }
}

async function getSiteDetails(event, siteId) {
    util.createLog(event, `getSiteDetails called`);
    try {
        let filter = {
            id: siteId
        };
        const site = await dbObj.findOne(filter, [], entityModels.Sites);
        return site;
    } catch (error) {
        util.createLog(event, `Error in getSiteDetails: `, error);
        return error;
    }
}

async function getClientConfig(event, clientId) {
    util.createLog(event, `getClientConfig called`);
    try {
        let filter = {
            id: clientId
        };
        const clientConfig = await dbObj.findOne(filter, [], entityModels.ClientConfig);
        return clientConfig;
    } catch (error) {
        util.createLog(event, `Error in getClientConfig: `, error);
        return error;
    }
}

async function getSFTPDetails(event, studyId) {
    util.createLog(event, `getSFTPDetails called`);
    try {
        let filter = {
            study_id: studyId,
        };
        const sftpDetails = await dbObj.findOneWithOrderBy(filter, [], entityModels.DeSftpMeta, ['sftp_version', 'DESC']);
        return sftpDetails;
    } catch (error) {
        util.createLog(event, `Error in getSFTPDetails : `, error)
        return error;
    }
}

async function createDeLog(event, clientId, studyId, logType, loggedInUserId) {
    util.createLog(event, `createDeLog Called`);
    try {
        let deSftpMetaId = null;
        if (logType === 'SFTP') {
            const SFTPSecreteManagerDetails = await getSFTPDetails(event, studyId);
            if (SFTPSecreteManagerDetails instanceof Error) {
                util.createLog(event, `getSFTPDetails Error: `, SFTPSecreteManagerDetails);
                await updateDeLogFailed(event, clientId, deLog, SFTPSecreteManagerDetails);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Upload file on sftp'
                    }),
                };
            }
            util.createLog(event, `getSFTPDetails fetched successfully `);
            deSftpMetaId = (SFTPSecreteManagerDetails && SFTPSecreteManagerDetails.id) ? SFTPSecreteManagerDetails.id : null;
        }

        const data = {
            study_id: studyId,
            de_sftp_meta_id: deSftpMetaId,
            trigger_datetime: util.changeDateToUtc(new Date()),
            completed_datetime: null,
            message: null,
            status: constants.enums.deLogs.status.STARTED,
            aws_job_id: (event && event.executionId) ? event.executionId : null,
            aws_log_path: null,
            type: logType,
            s3_location: null,
            triggered_by: loggedInUserId ? loggedInUserId : null
        };
        const payload = mappers.map.deLogDataMapper(data);
        const deLog = await dbObj.create(payload, entityModels.DeLog);
        return deLog;
    } catch (error) {
        util.createLog(event, `Error in createDeLog: `, error);
        return error;
    }
}

async function changeDeLogStatus(event, clientId, logId, payload) {
    util.createLog(event, `changeDeLogStatus called`);
    try {
        const deLog = await dbObj.updateById(logId, payload, entityModels.DeLog);
        return deLog;
    } catch (error) {
        util.createLog(event, `Error in changeDeLogStatus: `, error);
        return error;
    }
}
async function getTelehealthResponseByStudyIdAndSiteId(event, studyId, siteId, clientId) {
    try {
        let filter = {
            study_id: studyId,
            site_id: siteId
        };
        const telehealthResponse = await dbObj.findAll(filter, [], responseEntityModels.PiParticipantAppointment, [], [
            ["start_time", "ASC"]
        ]);
        return telehealthResponse;
    } catch (error) {
        util.createLog(event, `Error in getTelehealthResponseByStudyIdAndSiteId: `, error);
        return error;
    }
}
async function getTelehealthResponseByStudyId(event, studyId, clientId) {
    try {
        let filter = {
            study_id: studyId
        };
        const telehealthResponse = await dbObj.findAll(filter, [], responseEntityModels.PiParticipantAppointment, [], [
            ["start_time", "ASC"]
        ]);
        return telehealthResponse;
    } catch (error) {
        util.createLog(event, `Error in getTelehealthResponseByStudyId: `, error);
        return error;
    }
}
async function getStudySiteDetails(event, studyId) {
    try {
        let filter = {
            study_id: studyId
            // site_id: sitId
        };
        const studySite = await dbObj.findAll(
            filter, [{
                model: entityModels.StudyMetaData,
                attributes: [
                    "id",
                    "name",
                    "onboarding_type",
                    "status",
                    "app_name",
                    "client_id"
                ]
            }],
            entityModels.StudySite, [], [
            ["study_id", "ASC"]
        ]
        );
        return studySite;
    } catch (error) {
        util.createLog(event, `Error in getStudySiteDetails: `, error);
        return error;
    }
}
async function getStudyDefaultSite(event, studyId) {
    try {
        let filter = {
            study_id: studyId,
            is_default: 1
        };
        const defaultSite = await dbObj.findOne(filter, [], entityModels.StudySite);
        return defaultSite;
    } catch (error) {
        util.createLog(event, `Error in getStudyDefaultSite: `, error);
        return error;
    }
}
async function getParticipantDetails(event, participantId) {
    util.createLog(event, `getParticipantDetails called`);
    try {
        let filter = {
            id: participantId
        };
        const participant = await dbObj.findOneWithAttr(filter, [], entityModels.Participant, ["user_defined_participant_id"]);
        return participant;
    } catch (error) {
        util.createLog(event, `Error in getParticipantDetails: `, error);
        return error;
    }
}
async function getPiUserDetail(event, piId) {
    util.createLog(event, `getPiUserDetail called`);
    try {
        let filter = {
            id: piId
        };
        const user = await dbObj.findOneWithAttr(filter, [], entityModels.UserData, ["first_name", "last_name", "user_role"]);
        return user;
    } catch (error) {
        util.createLog(event, `Error in getPiUserDetail: `, error);
        return error;
    }
}
async function getTelehealthSessionData(event, id, clientId) {
    util.createLog(event, `getTelehealthSessionData called`);
    try {
        let filter = {
            appointment_id: id
        };
        const telehealthSessionData = await dbObj.findOneWithAttr(filter, [], responseEntityModels.TelehealthSessionData, ["start_time", "end_time"]);
        return telehealthSessionData;
    } catch (error) {
        util.createLog(event, `Error in getTelehealthSessionData: `, error);
        return error;
    }
}
async function createExcelPayload(event, data) {
    util.createLog(event, `Creating Excel Payload`);
    const payload = [];
    for (const telehealth of data) {
        const mappedTelehealth = excelMappers.map.telehealthExcelMapper(telehealth);
        payload.push(mappedTelehealth);
    }
    return payload;
}
async function downloadTelehealthScheduleFromS3(event, studyId, bucketName) {
    let studyFolder = constants.userConsts.s3Folders.study;
    let schedulerFileName = constants.userConsts.s3Files.schedulerFileName;
    let s3Path;
    try {
        util.createLog(event, `Download task and schedule data from S3`);
        s3Path = `${studyFolder}/${studyId}/${schedulerFileName}.json`;
        const s3Object = await s3Services.readS3Object(s3Path, bucketName);
        return s3Object;
    } catch (error) {
        util.createLog(event, `Error in downloadTelehealthScheduleFromS3: `, error);
        return error;
    }
}

function setDuration(startDay, endDay) {
    let duration = [{ startDay: startDay, endDay: endDay }];
    return duration;
}

function convertOldScehdule(event, schedule) {
    util.createLog(event, `Schedule of custom type found--" + schedule.scheduleId`);
    util.createLog(event, `Started converion of oldschedule json to latest json`);
    if (schedule.startDay != null && schedule.endDay != null) { // when get first  time
        util.createLog(event, `StartDay and EndDay not null creating duration object`);
        let duration = setDuration(schedule.startDay, schedule.endDay)
        schedule.startDay = null;
        schedule.endDay = null;
        // convert old notifications
        if (schedule.reminders) {
            var notifications = [];
            for (const reminder of schedule.reminders) {
                let notification = {};
                notification['notificationText'] = reminder.message;
                let dayString = (reminder.day < 0) ? 0 : reminder.day; // updated this to  change dayString just based on reminder day
                let delayString = "P" + dayString + "D";
                if (reminder.time) {
                    let timeOfDay = reminder.time
                    let delayStringModified = delayString + util.convertTime12to24(timeOfDay);
                    notification['delay'] = delayStringModified
                } else {
                    notification['delay'] = delayString
                }
                notification['notificationText'] = reminder.message;
                notifications.push(notification);
            }
            schedule.notifications = notifications;
        }
        if (duration && (schedule.duration == null || !schedule.duration)) { // first time :Duration object is updated in
            util.createLog(event, `Start day and end day are null no duration object present`);
            schedule.duration = duration
            util.createLog(event, `End of conversion of old schedule json to latest json`);
        }
    }
}

function getDetailsFromSchedule(event, schedule, scheduleObj) {
    let scheduleTypes = constants.enums.scheduleTypes;
    if (schedule.scheduleType === "milestone") {
        scheduleObj['milestones'] = schedule.milestones;
        scheduleObj['duration'] = setDuration(schedule.startDay, schedule.endDay)
    } else if (schedule.scheduleType === scheduleTypes.CUSTOM) {
        convertOldScehdule(event, schedule);
        // for new updated schedules
        if (schedule.duration) {
            if (schedule.allowUntillDuration) {
                let durationLength = schedule.duration.length - 1;
                schedule.duration[durationLength].endDay = null;
            }
            scheduleObj['duration'] = schedule.duration;
        } else {
            if (schedule.startDay && schedule.endDay) {
                scheduleObj['duration'] = setDuration(schedule.startDay, schedule.endDay)
            }
        }
        util.createLog(event, `Schedular duration exists no change`);
    }
    scheduleObj['frequency'] = schedule.frequency;
    scheduleObj['completionWindow'] = schedule.completionDuration;
    if (schedule.reminders) {
        schedule.reminders.forEach(function (element) {
            if (!element["delayType"])
                element.delayType = null;
        });
        scheduleObj['reminders'] = schedule.reminders;
    }
    if (schedule.callReminders) {
        schedule.callReminders.forEach(function (element) {
            if (!element["delayType"])
                element.delayType = null;
        });
        scheduleObj['callReminders'] = schedule.callReminders
    };
    scheduleObj['allowMultipleTimes'] = schedule.allowMultiReponseInCompletionWindow;
    scheduleObj['showDailySummary'] = (schedule.showDailySummary) ? true : false;
    scheduleObj['allowUntillDuration'] = schedule.allowUntillDuration;
}

function convertToObj(event, schedule) {
    let scheduleObj = {};
    scheduleObj['type'] = schedule.scheduleType
    for (const task of schedule.tasks) {
        scheduleObj['taskId'] = task.taskID
        scheduleObj['taskType'] = task.taskType
        scheduleObj['taskTitle'] = task.taskTitle
        scheduleObj['advanceScheduleDays'] = schedule.advanceScheduleDays
        if (task.studyPhysicianNumber) {
            scheduleObj['studyPhysicianNumber'] = task.studyPhysicianNumber
            scheduleObj['countryCode'] = task.countryCode ? task.countryCode : ''
            scheduleObj['phoneNumber'] = task.phoneNumber ? task.phoneNumber : ''
        }
        if (task.taskClassName && (task.taskClassName.toLowerCase() === "truclinic" || task.taskClassName.toLowerCase() === "telehealth")) {
            scheduleObj['taskKey'] = task.taskClassName
        } else if (task.taskClassName) {
            try {
                let activityEnum = constants.enums.activityTypes;
                scheduleObj['taskKey'] = util.getActivityTypes(activityEnum, task.taskClassName);
            } catch (error) {
                util.createLog(event, `Error in taskClassName `, error);
            }
        }
        getDetailsFromSchedule(event, schedule, scheduleObj);
    }
    return scheduleObj;
}
async function validateStudy(event, studyId, clientId) {
    try {
        let filter = {
            id: studyId,
            client_id: clientId
        };
        const study = await dbObj.findOne(filter, [], entityModels.StudyMetaData);
        return study;
    } catch (error) {
        util.createLog(event, `Error in validateStudy: `, error);
        return error;
    }
}
async function getStudySitesMap(event, studyId) {
    util.createLog(event, `getStudySiteMap called`);
    try {
        let filter = {
            study_id: studyId
        };
        const studySites = await dbObj.findAll(filter, [], entityModels.StudySite, [], ['study_id', 'ASC']); //await dbObj.findOne(filter, [], entityModels.StudySite);
        return studySites;
    } catch (error) {
        util.createLog(event, `Error in getStudySitesMap: `, error);
        return error;
    }
}
module.exports.export = async (event, context) => {
    let deLogId;
    try {
        util.createLog(event, "Lambda telehealth-appointment execution started");
        let sftpUpload = false;
        let clientId, studyId, zipFileNameWithTimeStamp;

        let loggedInUserId = (event && event.triggeredBy && event.triggeredBy.user && event.triggeredBy.user.id) ? event.triggeredBy.user.id : null;
        let hasAreas = [];
        if (event && event.areas) {
            hasAreas = event.areas.split(',');
        }
        //Checked area to execute telehealth lambda
        if (hasAreas && hasAreas.length > 0 && hasAreas.includes(constants.exportAreas.telehealth) === false) {
            util.createLog(event, `Skipped lambda telehealthAppoinment`);
            return;
        }
        //Checking for client id in payload
        if (event && event.clientId) {
            clientId = event.clientId;
        } else {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: 'Missing clientId'
                }),
            };
        }
        // checking for sftpupload parameter in payload
        if (event && event.sftpUpload && event.sftpUpload === true) {
            sftpUpload = event.sftpUpload;
        }
        // checking for studyId in paylod
        if (event && event.studyId) {
            studyId = event.studyId;
            util.createLog(event, `studyId is `, studyId);
        } else {
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    message: 'Bad Request',
                    error: 'studyId is required'
                }),
            };
        }
        const loggedInUserRole = (event && event.triggeredBy && event.triggeredBy.user && event.triggeredBy.user.role) ? event.triggeredBy.user.role : null;
        const loggedInUserSiteIds = event.siteIds;
        let loggedInUserSiteId;
        const isSiteTeamMember = checkIfSiteTeamMember(loggedInUserRole);
        if (isSiteTeamMember) {
            util.createLog(event, `Site team member logged in`);
            if (loggedInUserSiteIds && loggedInUserSiteIds.length > 0) {
                loggedInUserSiteId = loggedInUserSiteIds[0]
            } else {
                throw 'User is not associated with any site';
            }
        }
        util.createLog(event, `User is associated with site: loggedInUserSiteId ${loggedInUserSiteId}, LoggedIn user role is: ${loggedInUserRole}`);

        const models = await dbObj.init(clientId);
        entityModels = models;

        const responseModels = await dbObj.init(clientId, dbName);
        responseEntityModels = responseModels;

        const studyDetails = await validateStudy(event, studyId, clientId);
        if (!studyDetails || studyDetails.length === 0) {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: "Invalid study details or Unauthorized access"
                })
            };
        }

        // Creating deLog
        const logType = (sftpUpload === true) ? constants.enums.deLogs.type.SFTP : constants.enums.deLogs.type.DATAEXPORT;
        const deLog = await createDeLog(event, clientId, studyId, logType, loggedInUserId);
        util.createLog(event, `deLog: ${deLog.id}`);
        if (deLog instanceof Error) {
            util.createLog(event, `unable to insert de log record: ${deLog}`);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Not able to create Delog'
                }),
            };
        }
        deLogId = deLog.id;
        var defaultSiteId;
        let studySitesMap = await getStudySitesMap(event, studyId);
        if (studySitesMap && studySitesMap.length > 0) {
            for (let site of studySitesMap) {
                if (site.is_default && site.is_default === true) {
                    defaultSiteId = site.site_id;
                }
            }
        }
        // Fetching client configuration details
        const clientConfig = await getClientConfig(event, clientId);
        if (clientConfig instanceof Error) {
            util.createLog(event, `clientConfig Error: `, clientConfig)
            let delogPayload = {
                status: constants.enums.deLogs.status.FAILED,
                message: 'Client Config not found',
                completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
            }
            await changeDeLogStatus(event, clientId, deLog.id, delogPayload);
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'Client Config not found'
                }),
            };
        }
        const bucketName = clientConfig.s3_bucket;
        const domain = clientConfig.domain_name;
        if (!bucketName) {
            util.createLog(event, `S3 Bucket not Found`);
            let delogPayload = {
                status: constants.enums.deLogs.status.FAILED,
                message: 'S3 Bucket not Found',
                completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
            }
            await changeDeLogStatus(event, clientId, deLog.id, delogPayload);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'S3 Bucket not Found'
                }),
            };
        }
        let executionId = util.generateUniqueCode();
        if (event.executionId) {
            executionId = event.executionId;
        }
        let studyBaseUrl = `/tmp/${constants.tempExportFolder}/${studyId}_${executionId}`;
        if (event.studyBaseUrl) {
            studyBaseUrl = event.studyBaseUrl;
        }
        let zipFileName = studyId;
        if (studyDetails && studyDetails.name) {
            util.createLog(event, `Study Name ${studyDetails.name}`)
            zipFileName = studyDetails.name;
        }
        util.createLog(event, `zipFileName `, zipFileName);
        const dateNow = new Date();
        const timeStamp = util.changeDateFormat(dateNow, 'YYYYMMDDHHmm');
        zipFileNameWithTimeStamp = `${zipFileName}_${timeStamp}`;
        if (event.zipFileNameWithTimeStamp) {
            zipFileNameWithTimeStamp = event.zipFileNameWithTimeStamp;
        }
        util.createLog(event, `zipFileNameWithTimeStamp `, zipFileNameWithTimeStamp);
        let participantAppointmentsBaseUrl = studyBaseUrl + "/Telehealth/TelehealthAppointmentData";

        // Update Delog Status
        const payloadInProgress = { status: constants.enums.deLogs.status.INPROGRESS, message: 'telehealth-appointment' }
        util.createLog(event, `deLog status `, payloadInProgress);
        const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
        if (delogInProgress instanceof Error) {
            util.createLog(event, `delogInProgress Error  `, delogInProgress);
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'Error occured in Updating deLog'
                }),
            };
        }
        let telehealthResponse;
        let siteDetails;
        if (isSiteTeamMember) {
            if (!loggedInUserSiteId) {
                //For all user other than site team members
                util.createLog(event, `!loggedInUserSiteId`);
                telehealthResponse = await getTelehealthResponseByStudyIdAndSiteId(event, studyId, defaultSiteId, clientId);
                //siteDetails = await getSiteDetails(event, defaultSiteId);
            } else if (defaultSiteId && defaultSiteId === loggedInUserSiteId) {
                //For default site's site team members this is for handling null values of site_is column
                util.createLog(event, `defaultSiteId && defaultSiteId === loggedInUserSiteId`);
                telehealthResponse = await getTelehealthResponseByStudyIdAndSiteId(event, studyId, defaultSiteId, clientId);
                //siteDetails = await getSiteDetails(event, defaultSiteId);
            } else {
                //For non-default site's site team members
                util.createLog(event, `For non-default sites site team members`);
                telehealthResponse = await getTelehealthResponseByStudyIdAndSiteId(event, studyId, loggedInUserSiteId, clientId);
                //siteDetails = await getSiteDetails(event, loggedInUserSiteId);
            }
        }
        if (constants.userRoles.studyAdmin.toLowerCase() === loggedInUserRole.toLowerCase() || constants.userRoles.admin.toLowerCase() === loggedInUserRole.toLowerCase()) {
            telehealthResponse = await getTelehealthResponseByStudyId(event, studyId, clientId);
            //siteDetails = await getSiteDetails(event, defaultSiteId);
        }
        if (telehealthResponse && telehealthResponse.length > 0) {
            const piParticipantApptData = [];
            for (let telehealth of telehealthResponse) {
                let participantId = telehealth.participant_id;
                let piId = telehealth.pi_id;
                let id = telehealth.id;
                // Getting Participant Details
                let participant = await getParticipantDetails(event, participantId);
                if (participant && participant.user_defined_participant_id)
                    Object.assign(telehealth.dataValues, { userDefinedParticipantId: participant.user_defined_participant_id });

                // Getting pi user detail
                let piUserData = await getPiUserDetail(event, piId);
                // Getting telehealth session data
                let telehealthSessionData = await getTelehealthSessionData(event, id, clientId);
                if (piUserData && piUserData.first_name && piUserData.last_name) {
                    let piName = piUserData.first_name + " " + piUserData.last_name;
                    telehealth.dataValues["piName"] = piName;
                }
                if (piUserData && piUserData.user_role) {
                    telehealth.dataValues["user_role"] = piUserData.user_role
                }
                if (telehealth && telehealth.site_id) {
                    siteDetails = await getSiteDetails(event, telehealth.site_id);
                    if (siteDetails && siteDetails.siteId) {
                        telehealth.dataValues["siteId"] = siteDetails.siteId
                    } else {
                        telehealth.dataValues["siteId"] = "";
                    }
                    if (siteDetails && siteDetails.name) {
                        telehealth.dataValues["SiteName"] = siteDetails.name
                    } else {
                        telehealth.dataValues["SiteName"] = "";
                    }
                } else {
                    telehealth.dataValues["siteId"] = "";
                    telehealth.dataValues["SiteName"] = "";
                }
                // if (siteDetails.siteId) {
                //     telehealth.dataValues["siteId"] = siteDetails.siteId
                // }
                // if (siteDetails.name) {
                //     telehealth.dataValues["SiteName"] = siteDetails.name
                // }
                if (telehealthSessionData && telehealthSessionData.start_time) {
                    telehealth.dataValues["session_start_time"] = telehealthSessionData.start_time
                }
                if (telehealthSessionData && telehealthSessionData.end_time) {
                    telehealth.dataValues["session_end_time"] = telehealthSessionData.end_time
                }
                piParticipantApptData.push(telehealth.dataValues);
            }
            if (piParticipantApptData && piParticipantApptData.length > 0 && loggedInUserRole.toLowerCase() !== constants.userRoles.homeHealth.toLowerCase()) {
                util.createLog(event, 'Creating Excel Payload');
                const mappedTelehealth = await createExcelPayload(event, piParticipantApptData);
                // Getting Header for Excel File
                const headers = util.getExcelKeys(mappedTelehealth[0]);
                util.createLog(event, `creating file ${participantAppointmentsBaseUrl}/Participant_Telehealth_Appointments.xlsx`);
                // Creating Excel File
                const isExcelCreated = await files.createExcelFile(`${participantAppointmentsBaseUrl}/Participant_Telehealth_Appointments.xlsx`, headers, mappedTelehealth, "Participant_Telehealth_Appointm");
                if (isExcelCreated instanceof Error) {
                    util.createLog(event, `Excel create error: ${isExcelCreated}`);
                    let delogPayload = {
                        status: constants.enums.deLogs.status.FAILED,
                        message: 'Not able to create excel file',
                        completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
                    }
                    await changeDeLogStatus(event, clientId, deLog.id, delogPayload);
                    return {
                        statusCode: constants.statusCodes.BadRequest,
                        body: JSON.stringify({
                            error: 'Not able to create excel file'
                        }),
                    };
                }
                util.createLog(event, `${participantAppointmentsBaseUrl}/Participant_Telehealth_Appointments.xlsx Created Successfully`);
            }
        }
        //download tasks_and_schedules.json from S3 bucket.
        const scheduleWrapper = await downloadTelehealthScheduleFromS3(event, studyId, bucketName);
        var telehealthSchedules = [];
        if (scheduleWrapper && scheduleWrapper.schedules && scheduleWrapper.schedules.length > 0) {
            let scheduleWrapperObj = scheduleWrapper;
            for (const s3Schedule of scheduleWrapperObj.schedules) {
                if (s3Schedule.tasks) {
                    for (const task of s3Schedule.tasks) {
                        if (task.taskType.toLowerCase() === 'telehealth') {
                            let scheduleObj = util.includeNotNull(convertToObj(event, s3Schedule));
                            telehealthSchedules.push(scheduleObj);
                        }
                    }
                }
            }
        }
        if (telehealthSchedules && telehealthSchedules.length > 0) {
            // Creating Json File
            const isJsonCreated = await files.createJsonFile(`${participantAppointmentsBaseUrl}/Telehealth_Settings.json`, telehealthSchedules);
            if (isJsonCreated instanceof Error) {
                util.createLog(event, `isJsonCreated error:  `, isJsonCreated);
                let delogPayload = {
                    status: constants.enums.deLogs.status.FAILED,
                    message: 'Not able to create json',
                    completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
                }
                await changeDeLogStatus(event, clientId, deLog.id, delogPayload);
                return {
                    statusCode: constants.statusCodes.BadRequest,
                    body: JSON.stringify({
                        error: 'Not able to create json'
                    }),
                };
            }
            util.createLog(event, `Json File ${participantAppointmentsBaseUrl}/Telehealth_Settings.json Created Successfully`);
        }
        if (telehealthResponse && telehealthResponse.length > 0 || telehealthSchedules && telehealthSchedules.length > 0) {
            util.createLog(event, `uploading files to s3 in temp folder`);
            // Creating Zip File
            const zipFile = await files.createZip(`${participantAppointmentsBaseUrl}`, `${studyBaseUrl}/telehealthAppoinment.zip`, studyBaseUrl);
            // Convert Zip file to Binary file for S3 Upload
            const binaryZip = files.readFile(`${studyBaseUrl}/telehealthAppoinment.zip`);

            // Uploading Zip File to S3 Bucket
            const isUploadedToS3 = await s3Services.uploadFile(bucketName, `studies/${studyId}/tmp/${studyId}_${executionId}/telehealthAppoinment.zip`, binaryZip, 'application/zip');
            util.createLog(event, `is zipFile Uploaded To S3 `, isUploadedToS3);
            if (isUploadedToS3) {
                util.createLog(event, `Deleting Temp Files`);
                // files.removeFile(studyBaseUrl);
            }

            // Update Delog Status
            const payloadInProgress = {
                status: constants.enums.deLogs.status.SUCCEEDED,
                message: 'TeleHealth Data export Done',
                completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
            }
            util.createLog(event, `deLog status `, payloadInProgress);
            const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
            if (delogInProgress instanceof Error) {
                util.createLog(event, `delogInProgress Error  `, delogInProgress);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Updating deLog'
                    }),
                };
            }
        } else {
            // Update Delog Status
            const payloadInProgress = {
                status: constants.enums.deLogs.status.SUCCEEDED,
                message: 'No Data for Telehealth Export',
                completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
            }
            util.createLog(event, `deLog status `, payloadInProgress);
            const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
            if (delogInProgress instanceof Error) {
                util.createLog(event, `delogInProgress Error  `, delogInProgress);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Updating deLog'
                    }),
                };
            }
        }
        return {
            statusCode: 200,
            body: JSON.stringify({
                success: "telehealth export data!!!"
            })
        };
    } catch (error) {
        util.createLog(event, `Error in exporting telehealth data: `, error);
        console.log('Error', error, 'deLogId', deLogId);

        const payloadFailed = {
            status: constants.enums.deLogs.status.FAILED,
            message: error && error.stack ? error.stack : error && error.message ? error.message : 'failed',
            completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss'),
        }

        const isStatusUpdated = await changeDeLogStatus(event, event.clientId, deLogId, payloadFailed);
        throw error;
    } finally {

    }
};