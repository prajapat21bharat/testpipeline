'use strict';

// const AWS = require('aws-sdk');

const awsXRay = require('aws-xray-sdk');
const AWS = awsXRay.captureAWS(require('aws-sdk'));

const constants = require('../../constants').constants;
const { util, db, stepFunctions } = require('../../utility');
const dbObj = new db();
const dbName = constants.database.dbResearchResnpnse;
//const Sequelize = require('sequelize');
const uuid = require('uuid/v4');

const _ = require('lodash');

let entityModels, clientModels, responeEntityModel;
async function getClients(event) {
    util.createLog(event, 'getClients Called')
    try {
        let filter = {};
        const clients = await dbObj.findAll(filter, [], entityModels.ClientConfig, ['id']);
        return clients;
    } catch (error) {
        util.createLog(event, `Error in getClients : `, error);
        return error;
    }
}
async function getClientsSftpMeta(event, clientId) {
    util.createLog(event, 'getClientsSftpMeta Called')
    try {
        var query = `SELECT sf1.* FROM ${constants.researchDbTables.deSftpMeta} sf1 INNER JOIN (SELECT id,study_id, MAX(sftp_version) AS MaxSftpVersion FROM ${constants.researchDbTables.deSftpMeta} GROUP BY study_id) sf2 ON sf1.study_id = sf2.study_id AND sf1.sftp_version = sf2.MaxSftpVersion`;
        const sftpMetaData = await dbObj.rawQueryWithConn(clientId, query, clientModels.DeSftpMeta);
        return sftpMetaData;
    } catch (error) {
        util.createLog(event, `Error in getClientsSftpMeta : `, error);
        return error;
    }
}
async function getUserDetail(event, clientId) {
    util.createLog(event, 'getUserDetail Called');
    try {
        let filter = {
            user_role: constants.userRoles.admin,
            client_id: clientId,
            status: 'Active'
        };
        const user = await dbObj.findOneWithAttr(filter, [], clientModels.UserData, ['id', 'user_role']);
        return user;
    } catch (error) {
        util.createLog(event, `Error in getUserDetail: `, error);
        return error;
    }
}
async function getClientFeature(event, clientId) {
    util.createLog(event, 'check serverless export enabled or not');
    try {
        let filter = {
            feature_key: 'SERVERLESS_EXPORT',
            client_config_id: clientId,
            is_enabled: 1
        };
        const clientFeature = await dbObj.findOneWithAttr(filter, [], clientModels.ClientFeature, ['is_enabled']);
        return clientFeature;
    } catch {
        util.createLog(event, `Error in getClientFeature: `, error);
        return error;
    }
}
async function exportStudy(event, sftpMetaData, id, payload) {
    util.createLog(event, 'exportStudy Called');
    try {
        var finalResponse = [];
        for (let sftp of sftpMetaData) {
            let executionId = uuid();
            let studyId = sftp.study_id;
            payload['studyId'] = studyId;
            payload['clientId'] = id;
            payload['executionId'] = executionId;
            //console.log(payload);
            var inputParams = payload;
            util.createLog(event, 'StepFunction Payload', inputParams);
            const stepResult = await stepFunctions.triggerStepFunction(inputParams, `data-export-${process.env.ENVIRONMENT}`, executionId);
            if (stepResult instanceof Error) {
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Internal Server Error',
                        errorCode: 11
                    }),
                };
            }
            finalResponse.push({ clientId: id, studyId: studyId, result: stepResult })
        }
        return finalResponse;
    } catch (error) {
        util.createLog(event, `Error in exportStudy: `, error);
        return error;
    }
}
module.exports.list = async(event) => {
    try {
        util.createLog(event, "Lambda sftpJob Execution Started");
        if (process && process.env && process.env.CLIENT_ID) {
            let clientId = process.env.CLIENT_ID;
            const models = await dbObj.init(clientId);
            entityModels = models;
            const clients = await getClients(event);
            if (clients && clients.length > 0) {
                let clientStudies = [];
                let dataExportResponse = [];
                //const payload = { "triggeredBy": null };
                for (let client of clients) {
                    let id = client.id;
                    let sftpMetaResponse = {}
                    clientModels = await dbObj.init(id);
                    let clientFeature = await getClientFeature(event, id);
                    util.createLog(event, `clientFeature`, JSON.stringify(clientFeature));

                    if (clientFeature && clientFeature.is_enabled && clientFeature.is_enabled === true) {
                        let sftpMeta = await getClientsSftpMeta(event, id);
                        if (sftpMeta && sftpMeta.length > 0) {
                            const user = await getUserDetail(event, id);

                            //create payload for data-export
                            const payload = {
                                triggeredBy: {}
                            };
                            if (user) {
                                payload.triggeredBy.user = {
                                    id: user.id,
                                    role: user.user_role
                                };
                            }
                            payload["sftpUpload"] = true;
                            //export study data//
                            let exportStudyData = await exportStudy(event, sftpMeta, id, payload);
                            sftpMetaResponse[id] = exportStudyData;
                            dataExportResponse.push(sftpMetaResponse);
                        }
                    }
                }
                return {
                    statusCode: constants.statusCodes.Success,
                    body: JSON.stringify({ "sftpResponse": dataExportResponse })
                }
            }
        }
    } catch (error) {
        util.createLog(event, `Error in sftpJob Execution is : `, error);
        console.log("Error: ", error);
    } finally {

    }
};