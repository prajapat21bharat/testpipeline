'use strict';
// const AWS = require('aws-sdk');
const awsXRay = require('aws-xray-sdk');
const AWS = awsXRay.captureAWS(require('aws-sdk'));
const constants = require('../../constants').constants;
const { util, db, files, s3Services, csvMappers, mappers, sendgridMail } = require('../../utility');
const dbObj = new db();
const dbName = constants.database.dbResearchResnpnse;
const _ = require('lodash');
let entityModels, responeEntityModels;

function checkIfSiteTeamMember(userRole) {
    if (!userRole ||
        userRole.toLowerCase() === constants.userRoles.pi.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.subPi.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.studyCoordinator.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.homeHealth.toLowerCase()) {
        return true;
    } else {
        return false;
    }
}
async function getStudyDetails(event, studyId) {
    util.createLog(event, ` ========= getStudyDetails called ========= `);
    try {
        let filter = {
            id: studyId,
        };
        const studyDetails = await dbObj.findOne(filter, [], entityModels.StudyMetaData);
        return studyDetails;
    } catch (error) {
        util.createLog(event, `getStudyDetails Error: `, error);
        return error;
    }
}
async function getSiteDetails(event, siteId) {
    util.createLog(event, `========= getSiteDetails called =========`);
    try {
        let filter = {
            id: siteId
        };
        const site = await dbObj.findOne(filter, [], entityModels.Sites);
        return site;
    } catch (error) {
        util.createLog(event, `getSiteDetails Error: `, error);
        return error;
    }
}
async function getStudySiteDetails(event, studyId) {
    util.createLog(event, `getStudySiteDetails called`);
    try {
        let filter = {
            study_id: studyId,
        };
        const studySiteDetails = await dbObj.findAll(filter, [], entityModels.StudySite, '', ['study_id', 'ASC']);
        return studySiteDetails;
    } catch (error) {
        util.createLog(event, `getStudySiteDetails Error: `, error);
        return error;
    }
}
async function getClientConfig(event, clientId) {
    util.createLog(event, `========= getClientConfig called =========`);
    try {
        let filter = {
            id: clientId
        };
        const clientConfig = await dbObj.findOne(filter, [], entityModels.ClientConfig);
        return clientConfig;
    } catch (error) {
        util.createLog(event, `getClientConfig Error: `, error);
        return error;
    }
}
async function getSFTPDetails(event, studyId) {
    util.createLog(event, `getSFTPDetails called`);
    try {
        let filter = {
            study_id: studyId,
        };
        const sftpDetails = await dbObj.findOneWithOrderBy(filter, [], entityModels.DeSftpMeta, ['sftp_version', 'DESC']);
        return sftpDetails;
    } catch (error) {
        util.createLog(event, `Error in getSFTPDetails : `, error)
        return error;
    }
}
async function createDeLog(event, clientId, studyId, logType, loggedInUserId) {
    util.createLog(event, `========= createDeLog called =========`);
    try {
        let deSftpMetaId = null;
        if (logType === 'SFTP') {
            const SFTPSecreteManagerDetails = await getSFTPDetails(event, studyId);
            if (SFTPSecreteManagerDetails instanceof Error) {
                util.createLog(event, `getSFTPDetails Error: `, SFTPSecreteManagerDetails);
                await updateDeLogFailed(event, clientId, deLog, SFTPSecreteManagerDetails);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Upload file on sftp'
                    }),
                };
            }
            util.createLog(event, `getSFTPDetails fetched successfully `);
            deSftpMetaId = (SFTPSecreteManagerDetails && SFTPSecreteManagerDetails.id) ? SFTPSecreteManagerDetails.id : null;
        }
        const data = {
            study_id: studyId,
            de_sftp_meta_id: deSftpMetaId,
            trigger_datetime: util.changeDateToUtc(new Date()),
            completed_datetime: null,
            message: null,
            status: constants.enums.deLogs.status.STARTED,
            aws_job_id: (event && event.executionId) ? event.executionId : null,
            aws_log_path: null,
            type: logType,
            s3_location: null,
            triggered_by: loggedInUserId ? loggedInUserId : null
        };
        const payload = mappers.map.deLogDataMapper(data);
        const deLog = await dbObj.create(payload, entityModels.DeLog);
        return deLog;
    } catch (error) {
        util.createLog(event, `createDeLog Error: `, error);
        return error;
    }
}
async function changeDeLogStatus(event, clientId, logId, payload) {
    util.createLog(event, `========= changeDeLogStatus called =========`);
    try {
        const deLog = await dbObj.updateById(logId, payload, entityModels.DeLog);
        return deLog;
    } catch (error) {
        util.createLog(event, `changeDeLogStatus Error: `, error);
        return error;
    }
}
async function updateDeLogFailed(event, clientId, deLog) {
    // Update Delog Status
    const payloadFailed = { status: constants.enums.deLogs.status.FAILED }
    util.createLog(event, `deLog status ${payloadFailed}`);
    const delogFailed = await changeDeLogStatus(event, clientId, deLog.id, payloadFailed);
    if (delogFailed instanceof Error) {
        return {
            statusCode: constants.statusCodes.InternalServerError,
            body: JSON.stringify({
                error: 'Error occured in Updating deLog'
            }),
        };
    }
}
var findByStudyId = async(event, clientId, studyId, defaultSiteId) => {
    util.createLog(event, 'findByStudyId called');
    try {
        let filter = {
            studyId: studyId
        };
        const validicData = await dbObj.findAllWithGroup(filter, [], responeEntityModels.ValidicData, '', ['id', 'ASC'], ['data_capture_end_time', 'data_capture_start_time', 'dataObjectType', 'participant_id']);
        for (const data of validicData) {
            data.dataValues.siteId = data.dataValues.siteId ? data.dataValues.siteId : defaultSiteId;
        }
        return validicData;
    } catch (error) {
        util.createLog(event, `findByStudyId status `, error);
        return error;
    }
}
var findByStudyIdAndDefaultSiteId = async(event, clientId, studyId, defaultSiteId) => {
    util.createLog(event, 'findByStudyIdAndDefaultSiteId called');
    try {
        let filter = {
            studyId: studyId,
            site_id: defaultSiteId
        };
        const validicData = await dbObj.findAllWithGroup(filter, [], responeEntityModels.ValidicData, '', ['id', 'ASC'], ['data_capture_end_time', 'data_capture_start_time', 'dataObjectType', 'participant_id']);
        for (const data of validicData) {
            data.dataValues.siteId = data.dataValues.siteId ? data.dataValues.siteId : defaultSiteId;
        }
        return validicData;
    } catch (error) {
        util.createLog(event, `findByStudyIdAndDefaultSiteId status `, error);
        return error;
    }
}
var findByStudyIdAndSiteId = async(event, clientId, studyId, loggedInUserSiteId) => {
    util.createLog(event, 'findByStudyIdAndSiteId called');
    try {
        let filter = {
            studyId: studyId,
            siteId: loggedInUserSiteId
        };
        const validicData = await dbObj.findAllWithGroup(filter, [], responeEntityModels.ValidicData, '', ['id', 'ASC'], ['data_capture_end_time', 'data_capture_start_time', 'dataObjectType', 'participant_id']);
        for (const data of validicData) {
            data.dataValues.offset = data.dataValues.offset ? data.dataValues.offset : '+00:00';
        }
        return validicData;
    } catch (error) {
        util.createLog(event, `findByStudyIdAndSiteId status `, error);
        return error;
    }
}
var getUserDefinedParticipantId = async(event, ParticipantID) => {
    try {
        let filter = {
            id: ParticipantID
        };
        if (ParticipantID !== null) {
            const participantId = await dbObj.findOne(filter, [], entityModels.Participant);
            return (participantId && participantId.user_defined_participant_id) ? participantId.user_defined_participant_id : ''
        } else {
            return null;
        }
    } catch (error) {
        util.createLog(event, `getUserDefinedParticipantId status `, error);
        return error;
    }
}
var createValidicResponseCsvFile = async(event, validicResponseBaseUrl, validicResponses) => {
    var validicSubFolder = validicResponseBaseUrl + "Sensors_Data/";
    const ValidicResponses = await createCsvPayload(event, validicResponses, validicSubFolder);
    // Getting Header for CSV File
    const headers = await util.getKeys(ValidicResponses[0]);
    // Creating CSV File
    const isCsvCreated = await files.createCsvFile(`${validicResponseBaseUrl}/Sensors_Data.csv`, headers, ValidicResponses);
    if (isCsvCreated instanceof Error) {
        return {
            statusCode: constants.statusCodes.BadRequest,
            body: JSON.stringify({
                error: 'Not able to create json'
            }),
        };
    }
    util.createLog(event, `Validic Data isCsvCreated `, isCsvCreated);
    util.createLog(event, `validicResponseBaseUrl ${validicResponseBaseUrl}`);
}
async function createCsvPayload(event, data, validicSubFolder) {
    util.createLog(event, `========= Creating CSV Payload For Validic Data =========`);
    const payload = [];
    for (const ValidicResponse of data) {
        // creating JSON dataObjectType
        const isJsonCreated = await files.createJsonFile(`${validicSubFolder}/${util.toTitleCase(ValidicResponse.dataObjectType)}/${ValidicResponse.id}.json`, JSON.parse(ValidicResponse.data));
        if (isJsonCreated instanceof Error) {
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Not able to create json'
                }),
            };
        }
        util.createLog(event, `Validic Data isJsonCreated  `, isJsonCreated);
    }

    for (const ValidicResponse of data) {
        for (const ValidicResponse_ of data) {
            if (new Date(ValidicResponse.dataCaptureStartTime).valueOf() === new Date(ValidicResponse_.dataCaptureStartTime).valueOf() && new Date(ValidicResponse.dataCaptureEndTime).valueOf() === new Date(ValidicResponse_.dataCaptureEndTime).valueOf() && ValidicResponse.id !== ValidicResponse_.id && ValidicResponse.dataObjectType !== ValidicResponse_.dataObjectType && ValidicResponse.participantId === ValidicResponse_.participantId) {
                ValidicResponse["Summaries"] = ValidicResponse_.dataObjectType.toLowerCase() === "summaries" ? ValidicResponse_.id : '';
                ValidicResponse["Workouts"] = ValidicResponse_.dataObjectType.toLowerCase() === "workouts" ? ValidicResponse_.id : '';
                ValidicResponse["Measurements"] = ValidicResponse_.dataObjectType.toLowerCase() === "measurements" ? ValidicResponse_.id : '';
                ValidicResponse["Nutrition"] = ValidicResponse_.dataObjectType.toLowerCase() === "nutrition" ? ValidicResponse_.id : '';
                ValidicResponse["Sleep"] = ValidicResponse_.dataObjectType.toLowerCase() === "sleep" ? ValidicResponse_.id : '';
                util.removeObjectByAttr(data, 'id', ValidicResponse_.id);
            }
        }
        const mappedValidicResponse = csvMappers.map.validicCsvMapper(ValidicResponse);
        payload.push(mappedValidicResponse);
    }
    return payload;
}
var findByStudyIdProfileData = async(event, clientId, studyId, defaultSiteId) => {
    try {
        util.createLog(event, `called findByStudyIdProfileData`);
        var query = `SELECT v1.participant_id, v1.study_id, IFNULL(v1.site_id, '${defaultSiteId}') AS site_id, v1.data_capture_time, v1.profile_data FROM validic_user_profile v1 JOIN ( SELECT participant_id,study_id, MAX(data_capture_time) AS data_capture_time
        FROM validic_user_profile WHERE study_id = '${studyId}' GROUP BY participant_id,study_id ) r ON r.participant_id = v1.participant_id AND r.study_id = v1.study_id AND r.data_capture_time = v1.data_capture_time`;
        const validicProfileData = await dbObj.rawQueryWithConn(clientId, query, responeEntityModels.ValidicProfileData, dbName);
        return validicProfileData;
    } catch (error) {
        util.createLog(event, `findByStudyIdProfileData Error: `, error);
        return error;
    }
}
var findByStudyIdAndDefaultSiteIdProfileData = async(event, clientId, studyId, defaultSiteId) => {
    try {
        util.createLog(event, `called findByStudyIdAndDefaultSiteIdProfileData`);
        var query = `SELECT v1.participant_id, v1.study_id, IFNULL(v1.site_id, '${defaultSiteId}') AS site_id, v1.data_capture_time, v1.profile_data
        FROM validic_user_profile v1 JOIN ( SELECT participant_id,study_id, MAX(data_capture_time) AS data_capture_time FROM validic_user_profile WHERE study_id = '${studyId}' AND site_id = '${defaultSiteId}' OR site_id IS NULL GROUP BY participant_id,study_id ) r ON r.participant_id = v1.participant_id AND r.study_id = v1.study_id AND r.data_capture_time = v1.data_capture_time;`
        const validicProfileData = await dbObj.rawQueryWithConn(clientId, query, responeEntityModels.Sequelize_, dbName);
        return validicProfileData;
    } catch (error) {
        util.createLog(event, `findByStudyIdAndDefaultSiteIdProfileData Error: `, error);
        return error;
    }
}
var findByStudyIdAndSiteIdProfileData = async(event, clientId, studyId, loggedInUserSiteId) => {
    try {
        util.createLog(event, `called findByStudyIdAndSiteIdProfileData`);
        var query = `SELECT v1.participant_id, v1.study_id, site_id, v1.data_capture_time, v1.profile_data FROM validic_user_profile v1 JOIN ( SELECT participant_id,study_id, MAX(data_capture_time) AS data_capture_time FROM validic_user_profile WHERE study_id = '${studyId}' AND site_id = '${loggedInUserSiteId}' GROUP BY participant_id,study_id ) r ON r.participant_id = v1.participant_id AND r.study_id = v1.study_id AND r.data_capture_time = v1.data_capture_time;`
        const validicProfileData = await dbObj.rawQueryWithConn(clientId, query, responeEntityModels.ValidicProfileData, dbName);
        return validicProfileData;
    } catch (error) {
        util.createLog(event, `findByStudyIdAndSiteIdProfileData Error: `, error);
        return error;
    }
}
var createValidicUserProfileCsvFile = async(event, validicResponseBaseUrl, validicProfileData) => {
    const ValidicProfileData = await createValidicUserProfileCsvPayload(event, validicProfileData);
    // Getting Header for CSV File
    const headers = await util.getKeys(ValidicProfileData[0]);
    // Creating CSV File
    const isCsvCreated = await files.createCsvFile(`${validicResponseBaseUrl}/Participant_Connected_Sensors.csv`, headers, ValidicProfileData);
    if (isCsvCreated instanceof Error) {
        return {
            statusCode: constants.statusCodes.BadRequest,
            body: JSON.stringify({
                error: 'Not able to create json'
            }),
        };
    }
    util.createLog(event, `Validic User Profile isCsvCreated `, isCsvCreated);
    util.createLog(event, `validicResponseBaseUrl ${validicResponseBaseUrl}`);
}
async function createValidicUserProfileCsvPayload(event, data) {
    util.createLog(event, `========= Creating CSV Payload ValidicUserProfileCsv =========`);
    const payload = [];
    for (const ValidicResponse of data) {
        const mappedValidicResponse = csvMappers.map.validicUserProfileCsvMapper(ValidicResponse);
        payload.push(mappedValidicResponse);
    }
    return payload;
}
async function validateStudy(event, studyId, clientId) {
    try {
        let filter = {
            id: studyId,
            client_id: clientId
        };
        const study = await dbObj.findOne(filter, [], entityModels.StudyMetaData);
        return study;
    } catch (error) {
        util.createLog(event, `Error in validateStudy: `, error);
        return error;
    }
}
module.exports.export = async event => {
    let deLogId;
    try {
        util.createLog(event, `Lambda Visit Note Data Execution Started`);
        let sftpUpload = false;
        let clientId, studyId, zipFileNameWithTimeStamp, authorization, isDataForNonPI;
        let loggedInUserId = (event && event.triggeredBy && event.triggeredBy.user && event.triggeredBy.user.id) ? event.triggeredBy.user.id : null;
        let hasAreas = [];
        if (event && event.areas) {
            hasAreas = event.areas.split(',');
        }
        //Checked area to execute edcResponse lambda
        if (hasAreas && hasAreas.length > 0 && hasAreas.includes(constants.exportAreas.validic) === false) {
            util.createLog(event, `Skipped lambda Validic Note`);
            return;
        }
        if (event && event.clientId) {
            clientId = event.clientId;
        } else {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: 'Missing clientId'
                }),
            };
        }
        // checking for sftpupload parameter in payload
        if (event && event.sftpUpload && event.sftpUpload === true) {
            sftpUpload = event.sftpUpload;
        }
        // checking for studyId in paylod
        if (event && event.studyId) {
            studyId = event.studyId;
            util.createLog(event, `studyId is `, studyId);
        } else {
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    message: 'Bad Request',
                    error: 'studyId is required'
                }),
            };
        }
        const models = await dbObj.init(clientId);
        entityModels = models;
        const responseModels = await dbObj.init(clientId, dbName);
        responeEntityModels = responseModels;
        const validateStudyDetails = await validateStudy(event, studyId, clientId);
        if (!validateStudyDetails || validateStudyDetails.length === 0) {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: "Invalid study details or Unauthorized access"
                })
            };
        }
        const loggedInUserRole = (event && event.triggeredBy && event.triggeredBy.user && event.triggeredBy.user.role) ? event.triggeredBy.user.role : null;
        util.createLog(event, `loggedInUserRole is `, loggedInUserRole);
        const loggedInUserSiteIds = event.siteIds;
        let loggedInUserSiteId;
        // if ((!loggedInUserSiteIds || !loggedInUserSiteIds.length > 0) && !loggedInUserRole.toLowerCase() === constants.userRoles.admin.toLowerCase()) {
        //     return {
        //         statusCode: constants.statusCodes.Unauthorized,
        //         body: JSON.stringify({
        //             message: 'User is not associated with any site'
        //         }),
        //     };
        // }
        // if (loggedInUserSiteIds && loggedInUserSiteIds.length > 0) {
        //     loggedInUserSiteId = loggedInUserSiteIds[0];
        //     util.createLog(event, `User is associated with site: loggedInUserSiteId ${loggedInUserSiteId}, LoggedIn user role is: ${loggedInUserRole}`);
        // }
        const isSiteTeamMember = checkIfSiteTeamMember(loggedInUserRole);
        if (isSiteTeamMember) {
            util.createLog(event, `Site team member logged in`);
            if (loggedInUserSiteIds && loggedInUserSiteIds.length > 0) {
                loggedInUserSiteId = loggedInUserSiteIds[0]
            } else {
                throw 'User is not associated with any site';
            }
        }
        // if (!isSiteTeamMember) {
        //     return {
        //         statusCode: constants.statusCodes.Unauthorized,
        //         body: JSON.stringify({
        //             message: 'User is not authorized user'
        //         }),
        //     };
        // }
        util.createLog(event, `User is associated with site: loggedInUserSiteId ${loggedInUserSiteId}, LoggedIn user role is: ${loggedInUserRole}`);
        const studyDetails = await getStudyDetails(event, studyId);
        if (studyDetails instanceof Error) {
            util.createLog(event, `studyDetails Error: `, studyDetails);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Study Details not found'
                }),
            };
        }
        util.createLog(event, `studyDetails called successfull`);
        const studySiteDetails = await getStudySiteDetails(event, studyId);
        let defaultSiteId;
        for (const studySiteDetail of studySiteDetails) {
            if (studySiteDetail && studySiteDetail.is_default) {
                defaultSiteId = studySiteDetail.site_id;
            }
        }
        util.createLog(event, `defaultSiteId:`, defaultSiteId);
        // Creating deLog
        const logType = (sftpUpload === true) ? constants.enums.deLogs.type.SFTP : constants.enums.deLogs.type.DATAEXPORT;
        const deLog = await createDeLog(event, clientId, studyId, logType, loggedInUserId);
        if (deLog instanceof Error) {
            util.createLog(event, `unable to insert de log record Error:  ${deLog}`);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Not able to create Delog'
                }),
            };
        }
        util.createLog(event, `deLog: `, deLog.id);
        deLogId = deLog.id;
        // Fetching site details
        const siteDetails = await getSiteDetails(event, (loggedInUserSiteId) ? loggedInUserSiteId : defaultSiteId);
        if (siteDetails instanceof Error) {
            util.createLog(event, `siteDetails Error:  `, siteDetails);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Site Details not found'
                }),
            };
        }
        util.createLog(event, `sites details called successfully`);
        const siteId = siteDetails.site_id;
        util.createLog(event, `siteId ${siteId}`);
        // Fetching client configuration details
        const clientConfig = await getClientConfig(event, clientId);
        if (clientConfig instanceof Error) {
            util.createLog(event, `clientConfig Error:  `, clientConfig);
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'Client Config not found'
                }),
            };
        }
        util.createLog(event, `Client Config called successfully`);
        const bucketName = clientConfig.s3_bucket;
        const domain = clientConfig.domain_name;
        if (!bucketName) {
            util.createLog(event, `S3 Bucket not Found`);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'S3 Bucket not Found'
                }),
            };
        }
        let executionId = util.generateUniqueCode();
        if (event.executionId) {
            executionId = event.executionId;
        }
        let studyBaseUrl = `/tmp/${constants.tempExportFolder}/${studyId}_${executionId}`;
        var validicResponseBaseUrl = studyBaseUrl + "/Sensors/";
        // Update Delog Status
        const payloadInProgress = { status: constants.enums.deLogs.status.INPROGRESS, message: 'validic' }
        util.createLog(event, `========= deLog status ${payloadInProgress} =========`);
        const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
        if (delogInProgress instanceof Error) {
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'Error occured in Updating deLog'
                }),
            };
        }
        //#region ValidicData
        var validicDatas = [];
        if (!loggedInUserSiteId) {
            //For all user other than site team members
            validicDatas = await findByStudyId(event, clientId, studyId, defaultSiteId);
        } else if (defaultSiteId && defaultSiteId === loggedInUserSiteId) {
            //For default site's site team members this is for handling null values of site_is
            validicDatas = await findByStudyIdAndDefaultSiteId(event, clientId, studyId, defaultSiteId);
        } else {
            //For non-default site's site team members
            validicDatas = await findByStudyIdAndSiteId(event, clientId, studyId, loggedInUserSiteId);
        }
        if (validicDatas instanceof Error) {
            await updateDeLogFailed(event, clientId, deLog);
            util.createLog(event, `========= deLog failed ${deLog.id} =========`);
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'error in Participant Events Data Responses'
                }),
            };
        }
        if (validicDatas && validicDatas.length > 0) {
            var validicDataArr = [];
            for (const data of validicDatas) {
                if (data.siteId) {
                    const siteInfo = await getSiteDetails(event, data.siteId);
                    data.dataValues["siteId"] = siteInfo.siteId;
                    data.dataValues["siteName"] = siteInfo.name;
                } else {
                    data.dataValues["siteId"] = siteDetails.siteId;
                    data.dataValues["siteName"] = siteDetails.name;
                }
                data.dataValues["userDefinedParticipantId"] = await getUserDefinedParticipantId(event, data.dataValues.participantId);
                validicDataArr.push(data.dataValues)
            }
            util.createLog(event, `Study Validic Data size ====> ${validicDatas.length}`);
            // var dataDtos = generateValidicDtos(validicDatas);
            var dataDtos = validicDataArr;
            var validicResponseCsvFile = await createValidicResponseCsvFile(event, validicResponseBaseUrl, dataDtos);
            // , studySitesMap, participantUserDefinedIdMap
            if (validicResponseCsvFile instanceof Error) {
                await updateDeLogFailed(event, clientId, deLog);
                util.createLog(event, `============ deLog failed ${deLog.id} ============`);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'error in Validic Response Csv File'
                    }),
                };
            }
            util.createLog(event, `Finished creating Validic Responses Folder for study id ====> ${studyId}`);
        } else {
            util.createLog(event, `No Validic data to export for studyId==> ${studyId}`);
        }
        //#endregion
        //#region validic profile data
        var validicProfileDatas;
        if (!loggedInUserSiteId) {
            //For all user other than site team members
            validicProfileDatas = await findByStudyIdProfileData(event, clientId, studyId, defaultSiteId);
        } else if (defaultSiteId && defaultSiteId === loggedInUserSiteId) {
            //For default site's site team members this is for handling null values of site_is column
            validicProfileDatas = await findByStudyIdAndDefaultSiteIdProfileData(event, clientId, studyId, defaultSiteId);
        } else {
            //For non-default site's site team members
            validicProfileDatas = await findByStudyIdAndSiteIdProfileData(event, clientId, studyId, loggedInUserSiteId);
        }
        if (validicProfileDatas instanceof Error) {
            await updateDeLogFailed(event, clientId, deLog);
            util.createLog(event, `============ deLog failed ${validicProfileDatas} ============`);
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'error in Participant Events Data Responses'
                }),
            };
        }
        var validicProfileDataArr = [];
        if (validicProfileDatas && validicProfileDatas.length > 0) {
            for (var data of validicProfileDatas) {
                var profileDataSources = JSON.parse(data.profile_data);
                const tempParticipantId = data.participant_id;
                const tempStudyId = data.participant_id;
                profileDataSources = JSON.parse(JSON.stringify(profileDataSources));
                if (profileDataSources.sources.length > 0) {
                    for (const pd of profileDataSources.sources) {
                        console.log('siteDetails', JSON.stringify(siteDetails));
                        const siteInfo = await getSiteDetails(event, data.site_id ? data.site_id : siteDetails.siteId);
                        data["siteId"] = siteInfo.siteId;
                        data["siteName"] = siteInfo.name;

                        data["userDefinedParticipantId"] = await getUserDefinedParticipantId(event, tempParticipantId);
                        data["participantId"] = tempParticipantId;
                        data["studyId"] = tempStudyId;
                        data["deviceType"] = pd.type;
                        data["connectedAt"] = pd.connected_at;
                        data["LastProcessedAt"] = pd.last_processed_at;
                        validicProfileDataArr.push(data);
                        data = {};
                    }
                }
            }
            util.createLog(event, `Validic User Profile data entry size=====> ${validicProfileDatas.length}`);
            // var dataDtos = generateValidicDtos(validicProfileDatas);
            var validicUserProfileCsvPath = studyBaseUrl + "/Sensors";
            if (validicProfileDataArr && validicProfileDataArr.length > 0) {
                var validicUserProfileCsvFile = await createValidicUserProfileCsvFile(event, validicUserProfileCsvPath, validicProfileDataArr);
                if (validicUserProfileCsvFile instanceof Error) {
                    await updateDeLogFailed(event, clientId, deLog);
                    util.createLog(event, `============ deLog failed ${deLog.id}============`);
                    return {
                        statusCode: constants.statusCodes.InternalServerError,
                        body: JSON.stringify({
                            error: 'error in Validic User Profile Csv File'
                        }),
                    };
                }
                // , studySitesMap, participantUserDefinedIdMap
                util.createLog(event, `Finished creating Validic User Profile data folder for study id =====> ${studyId}`);
            } else {
                util.createLog(event, `No Validic Profile Data Sources to export for studyId==> ${studyId}`);
            }
        } else {
            util.createLog(event, `No Validic profile data to export for studyId==> ${studyId}`);
        }
        //#endregion
        //#region create zip file validic // validicProfileDatas
        if ((validicProfileDataArr && validicProfileDataArr.length > 0) || (validicDatas && validicDatas.length > 0)) {
            util.createLog(event, `uploading files to s3 in temp folder`);
            // Creating Zip File
            const zipFile = await files.createZip(`${validicResponseBaseUrl}`, `${studyBaseUrl}/Sensors.zip`, studyBaseUrl);
            // Convert Zip file to Binary file for S3 Upload
            const binaryZip = files.readFile(`${studyBaseUrl}/Sensors.zip`);
            // Uploading Zip File to S3 Bucket
            const isUploadedToS3 = await s3Services.uploadFile(bucketName, `studies/${studyId}/tmp/${studyId}_${executionId}/Sensors.zip`, binaryZip, 'application/zip');
            util.createLog(event, `is zipFile Uploaded To S3 `, isUploadedToS3);
            if (isUploadedToS3) {
                util.createLog(event, `Deleting Temp Files`);
                // files.removeFile(studyBaseUrl);
            }
            // Update Delog Status
            const payloadInProgress = {
                status: constants.enums.deLogs.status.SUCCEEDED,
                message: 'Validic Data export Done',
                completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
            }
            util.createLog(event, `deLog status `, payloadInProgress);
            const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
            if (delogInProgress instanceof Error) {
                util.createLog(event, `delogInProgress Error  `, delogInProgress);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Updating deLog'
                    }),
                };
            }
        } else {
            // Update Delog Status
            const payloadInProgress = {
                status: constants.enums.deLogs.status.SUCCEEDED,
                message: 'No Data for Validic Export',
                completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
            }
            util.createLog(event, `deLog status `, payloadInProgress);
            const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
            if (delogInProgress instanceof Error) {
                util.createLog(event, `delogInProgress Error  `, delogInProgress);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Updating deLog'
                    }),
                };
            }
        }
        //#endregion
        return {
            statusCode: 200,
            body: JSON.stringify({
                    success: 'true validic',
                },
                null,
                2
            ),
        };
    } catch (error) {
        util.createLog(event, `Error: `, error);
        console.log('Error', error, 'deLogId', deLogId);

        const payloadFailed = {
            status: constants.enums.deLogs.status.FAILED,
            message: error && error.stack ? error.stack : error && error.message ? error.message : 'failed',
            completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss'),
        }

        const isStatusUpdated = await changeDeLogStatus(event, event.clientId, deLogId, payloadFailed);
        throw error;

    } finally {}
};