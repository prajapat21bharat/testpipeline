'use strict';

// const AWS = require('aws-sdk');

const awsXRay = require('aws-xray-sdk');
const AWS = awsXRay.captureAWS(require('aws-sdk'));

const constants = require('../../constants').constants;
const { util, db, files, s3Services, mappers, sendgridMail, tokenizer } = require('../../utility');
const dbObj = new db();
const dbName = constants.database.dbResearchResnpnse;

let entityModels;

async function getClientConfig(event, clientId) {
    util.createLog(event, `getClientConfig called`);
    try {
        let filter = {
            id: clientId
        };
        const clientConfig = await dbObj.findOne(filter, [], entityModels.ClientConfig);
        return clientConfig;
    } catch (error) {
        util.createLog(event, `Error in getClientConfig : `, error);
        return error;
    }
}

async function sendEMail(event, templateId, emailDataObject) {
    try {
        const mailObject = {
            to: emailDataObject.to,
            from: emailDataObject.from,
            subject: emailDataObject.subject
        }
        let substitutions = {
            firstName: emailDataObject.name,
            study: emailDataObject.study,
            studyName: emailDataObject.studyName,
            downloadUrl: emailDataObject.downloadUrl
        };
        const isSent = await sendgridMail.sendMail(mailObject, substitutions, templateId);
        if (isSent) {
            util.createLog(event, `sendgrid triggered Successfull`);
        } else {
            util.createLog(event, `sendgrid trigger fail`);
        }
    } catch (error) {
        util.createLog(event, `error occured while sending email : `, error);
        return error;
    }
}

async function getPiDetails(event, piId) {
    util.createLog(event, `getPiDetails called`);
    try {
        let filter = {
            id: piId
        };
        const result = await dbObj.findOne(filter, [], entityModels.UserData);
        return result;
    } catch (error) {
        util.createLog(event, `Error in getPiDetails : `, error);
        return error;
    }
}

async function getStudyDetails(event, studyId) {
    util.createLog(event, `getStudyDetails called`);
    try {
        let filter = {
            id: studyId,
        };
        const studyDetails = await dbObj.findOne(filter, [], entityModels.StudyMetaData);
        return studyDetails;
    } catch (error) {
        util.createLog(event, `Error in getStudyDetails : `, error)
        return error;
    }
}

async function getSFTPDetails(event, studyId) {
    util.createLog(event, `getSFTPDetails called`);
    try {
        let filter = {
            study_id: studyId,
        };
        const sftpDetails = await dbObj.findOneWithOrderBy(filter, [], entityModels.DeSftpMeta, ['sftp_version', 'DESC']);
        return sftpDetails;
    } catch (error) {
        util.createLog(event, `Error in getSFTPDetails : `, error)
        return error;
    }
}

async function createDeLog(event, clientId, studyId, logType, loggedInUserId) {
    util.createLog(event, `createDeLog called`);
    try {
        let deSftpMetaId = null;
        if (logType === 'SFTP') {
            const SFTPSecreteManagerDetails = await getSFTPDetails(event, studyId);
            if (SFTPSecreteManagerDetails instanceof Error) {
                util.createLog(event, `getSFTPDetails Error: `, SFTPSecreteManagerDetails);
                await updateDeLogFailed(event, clientId, deLog, SFTPSecreteManagerDetails);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Upload file on sftp'
                    }),
                };
            }
            if (SFTPSecreteManagerDetails && SFTPSecreteManagerDetails.enabled && SFTPSecreteManagerDetails.enabled === 1) {
                util.createLog(event, `getSFTPDetails fetched successfully `);
                deSftpMetaId = (SFTPSecreteManagerDetails && SFTPSecreteManagerDetails.id) ? SFTPSecreteManagerDetails.id : null;
            }
        }
        const data = {
            study_id: studyId,
            de_sftp_meta_id: deSftpMetaId,
            trigger_datetime: util.changeDateToUtc(new Date()),
            completed_datetime: null,
            message: null,
            status: constants.enums.deLogs.status.STARTED,
            aws_job_id: (event && event.executionId) ? event.executionId : null,
            aws_log_path: null,
            type: logType,
            s3_location: null,
            triggered_by: loggedInUserId ? loggedInUserId : null
        };
        const payload = mappers.map.deLogDataMapper(data);

        const deLog = await dbObj.create(payload, entityModels.DeLog);
        return deLog;
    } catch (error) {
        util.createLog(event, `Error in createDeLog : `, error)
        return error;
    }
}

async function changeDeLogStatus(event, clientId, logId, payload) {
    util.createLog(event, `changeDeLogStatus called`);
    try {
        const deLog = await dbObj.updateById(logId, payload, entityModels.DeLog);
        return deLog;
    } catch (error) {
        util.createLog(event, `Error in changeDeLogStatus : `, error);
        return error;
    }
}

async function updateDeLogFailed(event, clientId, deLog, logDetails) {
    // Update Delog Status
    const payloadFailed = {
        status: constants.enums.deLogs.status.FAILED,
        message: logDetails && logDetails.stack ? logDetails.stack : logDetails && logDetails.message ? logDetails.message : logDetails
    };
    util.createLog(event, `deLog status:`, payloadFailed);
    const delogFailed = await changeDeLogStatus(event, clientId, deLog.id, payloadFailed);
    if (delogFailed instanceof Error) {
        return {
            statusCode: constants.statusCodes.InternalServerError,
            body: JSON.stringify({
                error: 'Error occured in Updating deLog'
            }),
        };
    }
}

async function deTokenize(key, value) {
    return new Promise(async(resolve) => {
        const tokenizerSessionToken = await tokenizer.getTokenizerSessionToken();
        const payload = {
            SessionToken: tokenizerSessionToken,
            TokenName: constants.tokenizer.TokenName,
            TokenValue: value
        }
        let deTokenizedData = await tokenizer.deTokenize(payload);
        resolve(deTokenizedData);
    });
}
async function validateStudy(event, studyId, clientId) {
    try {
        let filter = {
            id: studyId,
            client_id: clientId
        };
        const study = await dbObj.findOne(filter, [], entityModels.StudyMetaData);
        return study;
    } catch (error) {
        util.createLog(event, `Error in validateStudy: `, error);
        return error;
    }
}

async function removeFiles(event, studyId, executionId, bucketName) {

    //'===== Removing Local Temp Files =====';
    // files.removeFile(studyBaseUrl);
    util.createLog(event, `Removing Temp Files From S3: studies/${studyId}/tmp/${studyId}_${executionId}/`);

    await s3Services.deleteFolder(bucketName, `studies/${studyId}/tmp/${studyId}_${executionId}/`);
    files.removeFile(`/tmp/${studyId}_${executionId}`);
}

async function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function deLogSuccess(event, clientId, deLog, bucketPath) {
    // Update Delog Status
    const payloadInSuccess = {
        status: constants.enums.deLogs.status.SUCCEEDED,
        message: 'Successfully Completed',
        completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss'),
        s3_location: bucketPath
    }
    util.createLog(event, `deLog status `, payloadInSuccess);
    const isStatusUpdated = await changeDeLogStatus(event, clientId, deLog.id, payloadInSuccess);
    if (isStatusUpdated instanceof Error) {
        util.createLog(event, `isStatusUpdated Error: `, isStatusUpdated);
        await updateDeLogFailed(event, clientId, deLog, isStatusUpdated);
        return {
            statusCode: constants.statusCodes.InternalServerError,
            body: JSON.stringify({
                error: 'Error occured in Upload file on sftp'
            }),
        };
    }
}

async function getNotifierEmail(event) {
    util.createLog(event, `getNotifierEmail called `);
    let email;
    const environment = process.env.ENVIRONMENT.replace(/^\w/, c => c.toLowerCase());
    let key = `/thread/job/${environment}/sftpNotifyToEmail`;

    util.createLog(event, `ssm Key`, key);
    const ssmData = await s3Services.getSsmValueByKey(key, process.env.CLIENT_ID);
    if (ssmData instanceof Error) {
        console.log('Unable to fetch SSM Details', ssmData);
        email = constants.email.sftpFailedFrom;
    } else {
        email = (ssmData && ssmData.Parameter && ssmData.Parameter.Value) ? ssmData.Parameter.Value : constants.email.sftpFailedFrom;
    }
    return email;
}

module.exports.export = async event => {
    let deLogId, emailOrSftpFileName;
    try {
        util.createLog(event, `Lambda sendmail-or-sftp-upload Execution Started`);
        let sftpUpload = false;
        if (event && !event.clientId) {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: 'Missing clientId'
                }),
            };
        }
        if (event && !event.studyId) {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: 'Missing studyId'
                }),
            };
        }
        if (event && (!(event.triggeredBy) || !(event.triggeredBy.user) || !(event.triggeredBy.user.id))) {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: 'Missing loggedInUserId'
                }),
            };
        }

        const clientId = event.clientId;
        const executionId = event.executionId;
        const studyId = event.studyId;
        const loggedInUserId = (event && event.triggeredBy && event.triggeredBy.user && event.triggeredBy.user.id) ? event.triggeredBy.user.id : null;


        const models = await dbObj.init(clientId);
        entityModels = models;
        const validateStudyDetails = await validateStudy(event, studyId, clientId);
        if (!validateStudyDetails || validateStudyDetails.length === 0) {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: "Invalid study details or Unauthorized access"
                })
            };
        }

        if (event && event.sftpUpload && event.sftpUpload === true) {
            sftpUpload = true;
        }
        // Creating deLog
        const logType = (sftpUpload) ? constants.enums.deLogs.type.SFTP : constants.enums.deLogs.type.DATAEXPORT;
        const deLog = await createDeLog(event, clientId, studyId, logType, loggedInUserId);
        util.createLog(event, `deLog: ${deLog.id}`);
        if (deLog instanceof Error) {
            util.createLog(event, `unable to insert de log record: ${deLog}`);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Not able to create Delog'
                }),
            };
        }
        deLogId = deLog.id;
        // Fetching client configuration details
        const clientConfig = await getClientConfig(event, clientId);
        if (clientConfig instanceof Error) {
            util.createLog(event, `clientConfig Error: `, clientConfig);
            await updateDeLogFailed(event, clientId, deLog, clientConfig);
            util.createLog(event, `========= deLog failed ${deLog.id} =========`);
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'Client Config not found'
                }),
            };
        }

        const bucketName = clientConfig.s3_bucket;
        const domain = clientConfig.domain_name;
        if (!bucketName) {
            util.createLog(event, `S3 Bucket not Found`);
            await updateDeLogFailed(event, clientId, deLog, `S3 Bucket not Found`);
            util.createLog(event, `========= deLog failed ${deLog.id} =========`);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'S3 Bucket not Found'
                }),
            };
        }

        // Fetching pi details
        const piDetails = await getPiDetails(event, loggedInUserId);
        if (piDetails instanceof Error) {
            util.createLog(event, `piDetails Error: `, piDetails);
            await updateDeLogFailed(event, clientId, deLog, piDetails);
            util.createLog(event, `========= deLog failed ${deLog.id} =========`);
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'pi Details not found'
                }),
            };
        }

        // Fetching Study details
        const studyDetails = await getStudyDetails(event, studyId);
        if (studyDetails instanceof Error) {
            util.createLog(event, `studyDetails Error: `, siteDetails);
            await updateDeLogFailed(event, clientId, deLog, studyDetails);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Study Details not found'
                }),
            };
        }

        await files.checkAndCreateFile(`/tmp/${studyId}_${executionId}`);
        await files.checkAndCreateFile(`/tmp/${studyId}_${executionId}/data`);

        const s3Downloads = await s3Services.downloadMultipleS3Files(bucketName, studyId, executionId);
        if (s3Downloads instanceof Error) {
            util.createLog(event, `s3Downloads Error: `, s3Downloads);
            await updateDeLogFailed(event, clientId, deLog, s3Downloads);
            util.createLog(event, `========= deLog failed ${deLog.id} =========`);
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'Failed to download temp files'
                }),
            };
        }
        if (s3Downloads) {
            // Update Delog Status
            const payloadInProgress = { status: constants.enums.deLogs.status.INPROGRESS, message: 'Send Mail or SFTP Upload' };
            util.createLog(event, `deLog status `, payloadInProgress);
            const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
            if (delogInProgress instanceof Error) {
                util.createLog(event, `Updating deLog. Error: `, delogInProgress);
                await updateDeLogFailed(event, clientId, deLog, delogInProgress);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Updating deLog'
                    }),
                };
            }

            let zipFileName = studyId;

            util.createLog(event, `Study Name `, studyDetails);

            if (studyDetails && studyDetails.name) {
                util.createLog(event, `Study Name ${studyDetails.name}`);
                zipFileName = studyDetails.name;
            }
            const dateNow = new Date();
            const timeStamp = util.changeDateFormat(dateNow, 'YYYYMMDDHHmmsssss');
            const zipFileNameWithTimeStamp = `${zipFileName}_${timeStamp}`;
            emailOrSftpFileName = (sftpUpload) ? `${zipFileName}_${executionId}_${timeStamp}` : `${zipFileName}`;

            const zipS3TempFiles = await files.extractAllFiles(studyId, executionId);

            for (const s3TempFile of zipS3TempFiles) {
                await files.unzipFiles(studyId, executionId, s3TempFile);
            }
            // Creating Zip File
            const zipFile = await files.createZip(`/tmp/${studyId}_${executionId}/data`, `/tmp/${studyId}_${executionId}/data/${zipFileNameWithTimeStamp}.zip`, `/tmp/${studyId}_${executionId}/data`);

            util.createLog(event, `Uploading ${zipFileNameWithTimeStamp}.zip to s3 on ${bucketName} bucket`);
            const bucketPath = `studies/${studyId}/dataexport/${zipFileNameWithTimeStamp}.zip`;

            // Convert Zip file to Binary file for S3 Upload
            const binaryZip = files.readFile(`/tmp/${studyId}_${executionId}/data/${zipFileNameWithTimeStamp}.zip`);

            // Uploading Zip File to S3 Bucket
            const isUploadedToS3 = await s3Services.uploadFile(bucketName, bucketPath, binaryZip, 'application/zip');
            util.createLog(event, `isUploadedToS3: ${isUploadedToS3}`);

            if (isUploadedToS3) {
                if (sftpUpload) {
                    // Upload Response to sftp
                    util.createLog(event, `Fetching SFTP Secrete Key Details`);
                    const SFTPSecreteManagerDetails = await getSFTPDetails(event, studyId);
                    if (SFTPSecreteManagerDetails instanceof Error) {
                        util.createLog(event, `SFTPSecreteManagerDetails Error: `, SFTPSecreteManagerDetails);
                        await updateDeLogFailed(event, clientId, deLog, SFTPSecreteManagerDetails);
                        return {
                            statusCode: constants.statusCodes.InternalServerError,
                            body: JSON.stringify({
                                error: 'Error occured in Upload file on sftp'
                            }),
                        };
                    }
                    util.createLog(event, `SFTPSecreteManagerDetails`, SFTPSecreteManagerDetails);
                    if (SFTPSecreteManagerDetails && SFTPSecreteManagerDetails.enabled && SFTPSecreteManagerDetails.enabled === 1) {
                        if (SFTPSecreteManagerDetails && SFTPSecreteManagerDetails.secret_manager_key) {
                            const sftpSecreteKey = SFTPSecreteManagerDetails.secret_manager_key;
                            util.createLog(event, `Uploading response on sftp`);
                            const zipFile = `/tmp/${studyId}_${executionId}/data/${zipFileNameWithTimeStamp}.zip`;
                            const isFileUploaded = await s3Services.sftpUpload(clientId, zipFile, `${emailOrSftpFileName}.zip`, sftpSecreteKey);

                            if (isFileUploaded instanceof Error) {
                                console.log('isFileUploaded Error', isFileUploaded);
                                let notifyTo = await getNotifierEmail(event);
                                const emailObject = {
                                    to: notifyTo,
                                    from: constants.email.from,
                                    subject: constants.email.subjects.dataExportFailed,
                                    study: emailOrSftpFileName
                                }
                                const templateId = constants.email.templateIds.sftpFailed;
                                const isMailSent = await sendEMail(event, templateId, emailObject);
                                util.createLog(event, `Email Sent for SFTP Failed`, isMailSent);
                                await updateDeLogFailed(event, clientId, deLog, `Email Sent for SFTP Failed`);
                                throw isFileUploaded;
                            }

                            util.createLog(event, `isFileUploaded on sftpUpload :`, isFileUploaded);
                            // await timeout(65000);
                            await deLogSuccess(event, clientId, deLog, bucketPath);
                            if (isFileUploaded) {
                                await removeFiles(event, studyId, executionId, bucketName)
                            }
                        } else {
                            util.createLog(event, `no sftp enabled for ${studyId}`);
                        }
                    } else {
                        util.createLog(event, 'SFTP is not enabled', studyId);
                    }
                } else {
                    // Send Email for Response // template id ff219972-f319-4507-a680-af718db3b537

                    // console.log(' ========= Finding Template By Client and Template Type ========= ');
                    // const templateType = await findTemplateByClientIdAndTemplateType(studyId, clientId);
                    // if (templateType instanceof Error) {
                    //     console.log(templateType);
                    //     return {
                    //         statusCode: constants.statusCodes.InternalServerError,
                    //         body: JSON.stringify({
                    //             error: 'Internal Server Error',
                    //             errorCode: 11
                    //         }),
                    //     };
                    // }
                    // const templateId = templateType.template_id;
                    util.createLog(event, `piDetails: ${piDetails}`);
                    util.createLog(event, `Download Url: http://${domain}/#/studies/${studyId}/data-export/${deLog.id}`);
                    if (piDetails) {
                        util.createLog(event, 'pi Details fetched successfully');
                        // let deTokenizedEmail = await deTokenize('email', piDetails.username);
                        // console.log('deTokenizedEmail', deTokenizedEmail);
                        let piEmail = piDetails.username;

                        const emailObject = {
                            to: piEmail,
                            from: constants.email.from,
                            subject: constants.email.subjects.dataExport,
                            downloadUrl: `http://${domain}/#/studies/${studyId}/data-export/${deLog.id}`,
                            study: emailOrSftpFileName,
                            name: (piDetails.first_name) ? piDetails.first_name : ""
                        }
                        const templateId = constants.email.templateIds.dataExport;
                        const isMailSent = await sendEMail(event, templateId, emailObject);

                        if (isMailSent instanceof Error) {
                            util.createLog(event, `Email Sent`, isMailSent);
                            await updateDeLogFailed(event, clientId, deLog, isMailSent);
                            return {
                                statusCode: constants.statusCodes.InternalServerError,
                                body: JSON.stringify({
                                    error: 'Internal Server Error',
                                    errorCode: 11
                                }),
                            };
                        }
                        util.createLog(event, `Email Sent`, isMailSent);

                        // Update Delog Status
                        const payloadInSuccess = {
                            status: constants.enums.deLogs.status.SUCCEEDED,
                            message: 'Successfully Completed',
                            completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss'),
                            s3_location: bucketPath
                        }
                        util.createLog(event, `deLog status `, payloadInSuccess);
                        const isStatusUpdated = await changeDeLogStatus(event, clientId, deLog.id, payloadInSuccess);
                        if (isStatusUpdated instanceof Error) {
                            util.createLog(event, `isStatusUpdated Error: `, isStatusUpdated);
                            await updateDeLogFailed(event, clientId, deLog, isStatusUpdated);
                            return {
                                statusCode: constants.statusCodes.InternalServerError,
                                body: JSON.stringify({
                                    error: 'Error occured in Upload file on sftp'
                                }),
                            };
                        }
                        await deLogSuccess(event, clientId, deLog, bucketPath);
                        if (isMailSent) {
                            await timeout(65000);
                            await removeFiles(event, studyId, executionId, bucketName);
                        }
                    }
                }
            }

            return {
                statusCode: 200,
                body: JSON.stringify({
                        success: 'true sendmail-or-sftp-upload',
                    },
                    null,
                    2
                ),
            };
        } else {
            util.createLog(event, `Not able to download temp files`);
        }
    } catch (error) {
        util.createLog(event, `deLogId is ${deLogId}, Error: `, error);
        console.log("Error: ", error);

        const payloadFailed = {
            status: constants.enums.deLogs.status.FAILED,
            message: error && error.stack ? error.stack : error && error.message ? error.message : 'failed',
            completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss'),
        }

        const isStatusUpdated = await changeDeLogStatus(event, event.clientId, deLogId, payloadFailed);

        let notifyTo = await getNotifierEmail(event);
        const emailObject = {
            to: notifyTo,
            from: constants.email.from,
            subject: constants.email.subjects.dataExportFailed,
            studyName: emailOrSftpFileName
        }

        const templateId = constants.email.templateIds.sftpFailed;
        const isMailSent = await sendEMail(event, templateId, emailObject);

        util.createLog(event, `deLog status `, payloadFailed);
        throw error;
    } finally {

    }
};