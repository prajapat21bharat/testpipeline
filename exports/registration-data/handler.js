'use strict';

// const AWS = require('aws-sdk');

const awsXRay = require('aws-xray-sdk');
const AWS = awsXRay.captureAWS(require('aws-sdk'));

const constants = require('../../constants').constants;
const { util, db, files, s3Services, csvMappers, mappers, tokenizer } = require('../../utility');
const dbObj = new db();
const dbName = constants.database.dbResearchResnpnse;

const _ = require('lodash');

let entityModels, responeEntityModels;

function checkIfSiteTeamMember(userRole) {
    if (!userRole ||
        userRole.toLowerCase() === constants.userRoles.pi.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.subPi.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.studyCoordinator.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.homeHealth.toLowerCase()) {
        return true;
    } else {
        return false;
    }
}

async function getStudyDetails(event, studyId) {
    util.createLog(event, `getStudyDetails called`);
    try {
        let filter = {
            id: studyId,
        };
        const studyDetails = await dbObj.findAll(filter, [], entityModels.StudyMetaData);
        return studyDetails;
    } catch (error) {
        util.createLog(event, `getStudyDetails Error: `, error);
        return error;
    }
}


async function getStudySiteDetails(event, studyId) {
    util.createLog(event, `getStudySiteDetails called`);
    try {
        let filter = {
            study_id: studyId,
        };
        const studySiteDetails = await dbObj.findAll(filter, [], entityModels.StudySite, '', ['study_id', 'ASC']);
        return studySiteDetails;
    } catch (error) {
        util.createLog(event, `getStudySiteDetails Error: `, error);
        return error;
    }
}

async function getSiteDetails(event, siteId) {
    util.createLog(event, `getSiteDetails called`);
    try {
        let filter = {
            id: siteId
        };
        const site = await dbObj.findOne(filter, [], entityModels.Sites);
        return site;
    } catch (error) {
        util.createLog(event, `getSiteDetails Error: `, error);
        return error;
    }
}

async function getParticipantsByStudyId(event, studyId, clientId) {
    util.createLog(event, `getParticipantsByStudyId called`);
    try {
        let filter = {
            study_id: studyId
        };
        const participants = await dbObj.findAll(filter, [
            { model: entityModels.ParticipantMeta }
        ], entityModels.Participant, ['id', 'email', 'status', 'site_id', 'disqualificationReason', 'user_defined_participant_id', 'study_id', 'registrationDate', 'invitationDate', 'createdTime', 'modified_time', 'registration_status', 'createdBy', 'role', 'referenceId', 'referenceSource', 'validicAccessToken', 'profile_image', 'validicId'], ['createdTime', 'ASC']);
        return participants;
    } catch (error) {
        util.createLog(event, `getParticipantsByStudyId Error: `, error);
        return error;
    }
}

async function getParticipantsByStudyIdAndLoggedInUserSiteId(event, studyId, clientId, loggedInUserSiteId) {
    util.createLog(event, `getParticipantsByStudyIdAndLoggedInUserSiteId called`);
    try {
        let filter = {
            study_id: studyId,
            site_id: loggedInUserSiteId
        };
        const participants = await dbObj.findAll(filter, [
            { model: entityModels.ParticipantMeta }
        ], entityModels.Participant, ['id', 'email', 'status', 'site_id', 'disqualificationReason', 'user_defined_participant_id', 'study_id', 'registrationDate', 'invitationDate', 'createdTime', 'modified_time', 'registration_status', 'createdBy', 'role', 'referenceId', 'referenceSource', 'validicAccessToken', 'profile_image', 'validicId'], ['createdTime', 'ASC']);
        return participants;
    } catch (error) {
        util.createLog(event, `getParticipantsByStudyIdAndLoggedInUserSiteId Error: `, error);
        return error;
    }
}

async function getClientConfig(event, clientId) {
    util.createLog(event, `getClientConfig called`);
    try {
        let filter = {
            id: clientId
        };
        const clientConfig = await dbObj.findOne(filter, [], entityModels.ClientConfig);
        return clientConfig;
    } catch (error) {
        util.createLog(event, `getClientConfig Error: `, error);
        return error;
    }
}

async function getParticipantDetails(event, participantId) {
    util.createLog(event, `getParticipantDetails called`);
    try {
        let filter = {
            id: participantId
        };
        const participant = await dbObj.findOne(filter, [], entityModels.Participant);
        return participant;
    } catch (error) {
        util.createLog(event, `getParticipantDetails Error: `, error);
        return error;
    }
}

async function downloadMoreFeedBackJsonFromS3(event, studyId, bucketName, s3Path) {
    util.createLog(event, `get FeedBack Json from s3 for study: ${studyId}`);
    try {
        const s3Object = s3Services.readS3Object(s3Path, bucketName);
        return s3Object;
    } catch (error) {
        util.createLog(event, `downloadMoreFeedBackJsonFromS3 Error: `, error);
        return error;
    }
}

async function createCsvPayload(event, data) {
    util.createLog(event, `Creating CSV Payload called`);
    const payload = [];
    for (const participantData of data) {
        const mappedResponse = csvMappers.map.participantRegistrationCsvMapper(participantData);
        payload.push(mappedResponse);
    }
    return payload;
}

async function getSFTPDetails(event, studyId) {
    util.createLog(event, `getSFTPDetails called`);
    try {
        let filter = {
            study_id: studyId,
        };
        const sftpDetails = await dbObj.findOneWithOrderBy(filter, [], entityModels.DeSftpMeta, ['sftp_version', 'DESC']);
        return sftpDetails;
    } catch (error) {
        util.createLog(event, `Error in getSFTPDetails : `, error)
        return error;
    }
}

async function createDeLog(event, clientId, studyId, logType, loggedInUserId) {
    util.createLog(event, `createDeLog called`);
    try {
        let deSftpMetaId = null;
        if (logType === 'SFTP') {
            const SFTPSecreteManagerDetails = await getSFTPDetails(event, studyId);
            if (SFTPSecreteManagerDetails instanceof Error) {
                util.createLog(event, `getSFTPDetails Error: `, SFTPSecreteManagerDetails);
                await updateDeLogFailed(event, clientId, deLog, SFTPSecreteManagerDetails);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Upload file on sftp'
                    }),
                };
            }
            util.createLog(event, `getSFTPDetails fetched successfully `);
            deSftpMetaId = (SFTPSecreteManagerDetails && SFTPSecreteManagerDetails.id) ? SFTPSecreteManagerDetails.id : null;
        }

        const data = {
            study_id: studyId,
            de_sftp_meta_id: deSftpMetaId,
            trigger_datetime: util.changeDateToUtc(new Date()),
            completed_datetime: null,
            message: null,
            status: constants.enums.deLogs.status.STARTED,
            aws_job_id: (event && event.executionId) ? event.executionId : null,
            aws_log_path: null,
            type: logType,
            s3_location: null,
            triggered_by: loggedInUserId ? loggedInUserId : null
        };
        const payload = mappers.map.deLogDataMapper(data);

        const deLog = await dbObj.create(payload, entityModels.DeLog);
        return deLog;
    } catch (error) {
        util.createLog(event, `createDeLog Error: `, error);
        return error;
    }
}

async function changeDeLogStatus(event, clientId, logId, payload) {
    util.createLog(event, `changeDeLogStatus called`);
    try {
        const deLog = await dbObj.updateById(logId, payload, entityModels.DeLog);
        return deLog;
    } catch (error) {
        util.createLog(event, `changeDeLogStatus Error: `, error);
        return error;
    }
}

async function getStatusChangeDates(event, studyId, participantId, status) {
    util.createLog(event, `getStatusChangeDates called`);
    try {
        let filter = {
            study_id: studyId,
            new_status: status,
            participant_id: participantId
        };
        const historyData = await dbObj.findOne(filter, [], entityModels.ParticipantStatusHistory);
        return historyData;
    } catch (error) {
        util.createLog(event, `getStatusChangeDates Error: `, error);
        return error;
    }
}

async function deTokenize(key, value) {
    return new Promise(async(resolve) => {
        const tokenizerSessionToken = await tokenizer.getTokenizerSessionToken();
        const payload = {
            SessionToken: tokenizerSessionToken,
            TokenName: constants.tokenizer.TokenName,
            TokenValue: value
        }
        let deTokenizedData = await tokenizer.deTokenize(payload);
        resolve(deTokenizedData);
    });
}
async function fetchPaticipantLanguageId(event, participantId) {
    util.createLog(event, `fetchPaticipantLanguageId called`);
    try {
        let filter = {
            participant_id: participantId,
            active: 1
        };
        const participantLanguage = await dbObj.findOne(filter, [], entityModels.ParticipantLanguage);
        return participantLanguage;
    } catch (error) {
        util.createLog(event, `Error in fetchPaticipantLanguageId: `, error);
        return error;
    }
}
async function fetchPaticipantLanguageDetail(event, languageId) {
    util.createLog(event, `fetchPaticipantLanguageDetail called`);
    try {
        let filter = {
            language_id: languageId
        };
        const language = await dbObj.findOne(filter, [], entityModels.Language);
        return language;
    } catch (error) {
        util.createLog(event, `Error in fetchPaticipantLanguageDetail: `, error);
        return error;
    }
}

async function fetchDefaultLanguage(event) {
    util.createLog(event, `fetchDefaultLanguage called`);
    try {
        let filter = {
            is_default: 1
        };
        const language = await dbObj.findOne(filter, [], entityModels.Language);
        return language;
    } catch (error) {
        util.createLog(event, `Error in fetchDefaultLanguage: `, error);
        return error;
    }
}

async function fetchParticipantOnBoardingEvent(event, participantId, studyId) {
    util.createLog(event, `fetchParticipantOnBoardingEvent called`);
    try {
        let filter = {
            participant_id: participantId,
            study_id: studyId,
        };
        const participantOnBoardingEvent = await dbObj.findOne(filter, [], responeEntityModels.ParticipantOnBoardingEvent);
        return participantOnBoardingEvent;
    } catch (error) {
        util.createLog(event, `Error in fetchParticipantOnBoardingEvent: `, error);
        return error;
    }
}
async function createPayloadForExport(event, participants, isDataForNonPI, siteId, siteName) {
    util.createLog(event, `createPayloadForExport called`);
    const participantPayload = [];
    let isPi = (isDataForNonPI) ? false : true;
    let piFields = ['firstName', 'lastName', 'phoneNumber', 'country', 'nonMobilePhoneNumber', 'address', 'city', 'zipcode', 'state', 'street'];
    let sysAdminFields = ['email', 'firstName', 'lastName', 'phoneNumber', 'nonMobilePhoneNumber', 'address', 'city', 'state', 'zipcode'];
    for (const participant of participants) {
        let modifiedTime, modifiedLocalTime;
        let deTokenizedEmail = '';
        if (participant.email) {
            deTokenizedEmail = await deTokenize('email', participant.email);
        }
        let statusHistory;
        if (participant.status === constants.participant.status.DISQUALIFIED || participant.status === constants.participant.status.WITHDRAWSTUDY) {
            statusHistory = await getStatusChangeDates(event, participant.study_id, participant.id, participant.status);
            modifiedTime = (statusHistory.modified_time) ? util.changeDateToUtc(statusHistory.modified_time) : '';
            modifiedLocalTime = (statusHistory.modified_time) ? util.changeDateFormat(util.changeDateToUtc(statusHistory.modified_time), 'YYYY-MM-DD HH:mm:ss') : '';
        }
        const registrationStatus = await getStatusChangeDates(event, participant.study_id, participant.id, constants.participant.status.REGISTERED);
        let siteData;
        if (participant && participant.site_id) {
            siteData = await getSiteDetails(event, participant.site_id);
        }
        let payload = {
            isPi: isPi,
            id: participant.id,
            role: participant.referenceSource,
            status: participant.status,
            siteId: (siteData && siteData.siteId) ? siteData.siteId : '',
            siteName: (siteData && siteData.name) ? siteData.name : '',
            invitationDate: (participant.invitationDate) ? util.changeDateToUtc(participant.invitationDate) : '',
            registrationDate: (participant.registrationDate) ? util.changeDateToUtc(participant.registrationDate) : '',
            localRegistrationDate: (registrationStatus && registrationStatus.participant_local_time) ? registrationStatus.participant_local_time : (participant && participant.registrationDate) ? participant.registrationDate : '',
            participantLocalTime: participant.registrationDate,
            //offset: (participant.registrationDate) ? util.changeDateFormat(new Date(participant.registrationDate), 'Z') : '',
            withdrawDate: (participant.status === constants.participant.status.WITHDRAWSTUDY) ? modifiedTime : '',
            disqualifyDate: (participant.status === constants.participant.status.DISQUALIFIED) ? modifiedTime : '',
            localWithdrawDate: modifiedLocalTime,
            disqualificationReason: (participant.disqualificationReason) ? participant.disqualificationReason : null,
            studyId: participant.study_id,
            userDefinedParticipantId: participant.user_defined_participant_id
        };
        if (statusHistory && statusHistory.participant_local_time_offset) {
            payload.offset = statusHistory.participant_local_time_offset;
        }
        if (registrationStatus && registrationStatus.participant_local_time_offset) {
            payload.offset = registrationStatus.participant_local_time_offset;
        }
        //fetch language detail
        let defaultLanguageData = await fetchDefaultLanguage(event);
        let participantLanguage = await fetchPaticipantLanguageId(event, participant.id);
        payload.app_language_id = "";
        payload.display_language = "";
        if (participantLanguage) {
            if (participantLanguage.language_id) {
                let languageId = participantLanguage.language_id;
                let languageInfo = await fetchPaticipantLanguageDetail(event, languageId);
                if (languageInfo && languageInfo.language_id) {
                    payload.app_language_id = languageInfo.language_id;
                }
                if (languageInfo && languageInfo.language_culture) {
                    payload.display_language = languageInfo.language_culture;
                }
            } else if (defaultLanguageData) {
                if (defaultLanguageData.language_id) {
                    payload.app_language_id = defaultLanguageData.language_id;
                }
                if (defaultLanguageData.language_culture) {
                    payload.display_language = defaultLanguageData.language_culture;
                }
            }
        }
        //fetch offset
        // let participantOnBoardingEvent = await fetchParticipantOnBoardingEvent(event, participant.id, participant.study_id);
        // if (participantOnBoardingEvent && participantOnBoardingEvent.offset) {
        //     payload.offset = participantOnBoardingEvent.offset;
        // }
        if (!isDataForNonPI) {
            if (deTokenizedEmail) {
                payload.email = deTokenizedEmail;
            }
            payload.createdBy = participant.createdBy,
                payload.notes = (participant.notes) ? participant.notes : null,
                payload.profileImage = (participant.profileImage) ? participant.profileImage : null,
                payload.referenceId = (participant.referenceId) ? participant.referenceId : null,
                payload.referenceSource = (participant.referenceSource) ? participant.referenceSource : null
        }
        payload.validicAccessToken = (participant.validicAccessToken) ? participant.validicAccessToken : null;
        payload.validicId = (participant.validicId) ? participant.validicId : null
        if (participant && participant.participant_meta && participant.participant_meta.length > 0) {
            const participantMeta = participant.participant_meta;
            for (const metaData of participantMeta) {
                const metaValues = util.getMetaKeyValues(metaData);
                if (isDataForNonPI && _.includes(sysAdminFields, metaValues.key)) {
                    continue;
                } else {
                    if (_.includes(piFields, metaValues.key) && metaValues.value) {
                        let deTokenizedValue = await deTokenize(metaValues.key, metaValues.value);
                        if (deTokenizedValue)
                            payload[`${metaValues.key}`] = deTokenizedValue;
                    } else {
                        payload[`${metaValues.key}`] = metaValues.value;
                    }
                }
            }
        }
        participantPayload.push(payload);
    }
    return participantPayload;

}
async function validateStudy(event, studyId, clientId) {
    try {
        let filter = {
            id: studyId,
            client_id: clientId
        };
        const study = await dbObj.findOne(filter, [], entityModels.StudyMetaData);
        return study;
    } catch (error) {
        util.createLog(event, `Error in validateStudy: `, error);
        return error;
    }
}
module.exports.export = async(event, context) => {
    try {
        util.createLog(event, `Lambda Participant Registration Data Execution Started`);
        let sftpUpload = false;
        let clientId, studyId, zipFileNameWithTimeStamp, isDataForNonPI;
        if (event && event.clientId) {
            clientId = event.clientId;
        } else {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: 'Missing clientId'
                }),
            };
        }
        // checking for sftpupload parameter in payload
        if (event && event.sftpUpload && event.sftpUpload === true) {
            sftpUpload = event.sftpUpload;
        }
        // checking for studyId in paylod
        if (event && event.studyId) {
            studyId = event.studyId;
            util.createLog(event, `studyId is `, studyId);
        } else {
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    message: 'Bad Request',
                    error: 'Url Parameter studyId is required'
                }),
            };
        }
        const models = await dbObj.init(clientId);
        entityModels = models;
        const responseModels = await dbObj.init(clientId, dbName);
        responeEntityModels = responseModels;
        const validateStudyDetails = await validateStudy(event, studyId, clientId);
        if (!validateStudyDetails || validateStudyDetails.length === 0) {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: "Invalid study details or Unauthorized access"
                })
            };
        }
        const loggedInUserRole = (event && event.triggeredBy && event.triggeredBy.user && event.triggeredBy.user.role) ? event.triggeredBy.user.role : null;
        const loggedInUserSiteIds = event.siteIds;
        let loggedInUserSiteId;

        util.createLog(event, `LoggedIn user role is: ${loggedInUserRole}`);
        let loggedInUserId = (event && event.triggeredBy && event.triggeredBy.user && event.triggeredBy.user.id) ? event.triggeredBy.user.id : null;

        const isSiteTeamMember = checkIfSiteTeamMember(loggedInUserRole);
        // if (!isSiteTeamMember) {
        //     return {
        //         statusCode: constants.statusCodes.Unauthorized,
        //         body: JSON.stringify({
        //             message: 'User is not authorized user'
        //         }),
        //     };
        // }

        // if ((!loggedInUserSiteIds || !loggedInUserSiteIds.length > 0) && !loggedInUserRole.toLowerCase() === constants.userRoles.admin.toLowerCase()) {
        //     return {
        //         statusCode: constants.statusCodes.Unauthorized,
        //         body: JSON.stringify({
        //             message: 'User is not associated with any site'
        //         }),
        //     };
        // }
        // if (loggedInUserSiteIds && loggedInUserSiteIds.length > 0) {
        //     loggedInUserSiteId = loggedInUserSiteIds[0];
        //     util.createLog(event, `User is associated with site: loggedInUserSiteId ${loggedInUserSiteId}, LoggedIn user role is: ${loggedInUserRole}`);
        // }
        // // const loggedInUserSiteId = null;

        // util.createLog(event, `User is associated with site: loggedInUserSiteId: ${loggedInUserSiteId}`);

        if (isSiteTeamMember) {
            util.createLog(event, `Site team member logged in`);
            if (loggedInUserSiteIds && loggedInUserSiteIds.length > 0) {
                loggedInUserSiteId = loggedInUserSiteIds[0]
            } else {
                throw 'User is not associated with any site';
            }
        }
        util.createLog(event, `User is associated with site: loggedInUserSiteId ${loggedInUserSiteId}, LoggedIn user role is: ${loggedInUserRole}`);

        const studyDetails = await getStudyDetails(event, studyId);
        if (studyDetails instanceof Error) {
            util.createLog(event, `studyDetails Error: ${studyDetails}`);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Study Details not found'
                }),
            };
        }

        // Creating deLog
        const logType = (sftpUpload === true) ? constants.enums.deLogs.type.SFTP : constants.enums.deLogs.type.DATAEXPORT;
        const deLog = await createDeLog(event, clientId, studyId, logType, loggedInUserId);
        if (deLog instanceof Error) {
            util.createLog(event, `unable to insert de log record Error: ${deLog}`);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Not able to create Delog'
                }),
            };
        }

        const studySiteDetails = await getStudySiteDetails(event, studyId);

        let defaultSiteId;
        for (const studySiteDetail of studySiteDetails) {
            if (studySiteDetail && studySiteDetail.is_default) {
                defaultSiteId = studySiteDetail.site_id;
            }
        }
        util.createLog(event, `defaultSiteId:`, defaultSiteId);

        // Fetching site details
        const siteDetails = await getSiteDetails(event, (loggedInUserSiteId) ? loggedInUserSiteId : defaultSiteId);
        if (siteDetails instanceof Error) {
            util.createLog(event, `siteDetails Error: `, siteDetails);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Site Details not found'
                }),
            };
        }

        const siteId = siteDetails.site_id;
        util.createLog(event, `siteId: ${siteId}`);

        // Fetching client configuration details
        const clientConfig = await getClientConfig(event, clientId);
        if (clientConfig instanceof Error) {
            util.createLog(event, `clientConfig Error: `, clientConfig);
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'Client Config not found'
                }),
            };
        }
        const bucketName = clientConfig.s3_bucket;
        const domain = clientConfig.domain_name;
        if (!bucketName) {
            util.createLog(event, `S3 Bucket not Found`);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'S3 Bucket not Found'
                }),
            };
        }
        let executionId = util.generateUniqueCode();
        if (event.executionId) {
            executionId = event.executionId;
        }
        let studyBaseUrl = `/tmp/${constants.tempExportFolder}/${studyId}_${executionId}`;
        if (event.studyBaseUrl) {
            studyBaseUrl = event.studyBaseUrl;
        }
        let zipFileName = studyId;
        if (studyDetails && studyDetails.name) {
            util.createLog(event, `Study Name: ${studyDetails.name}`);
            zipFileName = studyDetails.name;
        }
        util.createLog(event, `zipFileName: ${zipFileName}`);
        const dateNow = new Date();
        const timeStamp = util.changeDateFormat(dateNow, 'YYYYMMDDHHmm');
        zipFileNameWithTimeStamp = `${zipFileName}_${timeStamp}`;
        if (event.zipFileNameWithTimeStamp) {
            zipFileNameWithTimeStamp = event.zipFileNameWithTimeStamp;
        }
        util.createLog(event, `zipFileNameWithTimeStamp: ${zipFileNameWithTimeStamp}`);
        const participantsPIIDataBaseUrl = studyBaseUrl + "/ParticipantRegistrationData";


        // Update Delog Status
        const payloadInProgress = { status: constants.enums.deLogs.status.INPROGRESS, message: 'registration-data' };
        util.createLog(event, `deLog status: ${payloadInProgress}`);
        const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
        if (delogInProgress instanceof Error) {
            util.createLog(event, `delogInProgress Error: `, delogInProgress);
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'Error occured in Updating deLog'
                }),
            };
        }

        let participantsPIIData, participants;
        if (isSiteTeamMember && !(loggedInUserRole.toLowerCase() === constants.userRoles.admin.toLowerCase())) {
            util.createLog(event, 'inside getParticipantsByStudyIdAndLoggedInUserSiteId');
            participants = await getParticipantsByStudyIdAndLoggedInUserSiteId(event, studyId, clientId, (loggedInUserSiteId) ? loggedInUserSiteId : defaultSiteId);
        } else {
            util.createLog(event, 'inside getParticipantsByStudyId');
            participants = await getParticipantsByStudyId(event, studyId, clientId);
        }

        if (loggedInUserRole.toLowerCase() === constants.userRoles.pi.toLowerCase() || loggedInUserRole.toLowerCase() === constants.userRoles.cra.toLowerCase()) {
            isDataForNonPI = false;
        } else {
            isDataForNonPI = true;
        }

        participantsPIIData = await createPayloadForExport(event, participants, isDataForNonPI, siteDetails.siteId, siteDetails.name);
        if (participantsPIIData && participantsPIIData.length > 0) {
            util.createLog(event, `Study Participants Registration Data size: ${participantsPIIData.length}`);

            // Creating CSV payload and mappings for data
            const mappedresponse = await createCsvPayload(event, participantsPIIData);

            // Getting Header for CSV File
            const headers = util.getKeys(mappedresponse[0]);

            // Creating CSV File
            const isCsvCreated = await files.createCsvFile(`${participantsPIIDataBaseUrl}/data.csv`, headers, mappedresponse);
            if (isCsvCreated instanceof Error) {
                util.createLog(event, `isCsvCreated Error: `, isCsvCreated);
                return {
                    statusCode: constants.statusCodes.BadRequest,
                    body: JSON.stringify({
                        error: 'Not able to create CSV'
                    }),
                };
            }
            util.createLog(event, `data.csv file created successfully`);
            let jsonData = [];
            const participantJsonData = participantsPIIData;
            for (const participants of participantJsonData) {
                let participant = participants;
                if (!participant.offset) {
                    if (participant.status === 'INVITED' || participant.status === 'NOTINVITED') {
                        participant.offset = "";
                    } else {
                        participant.offset = "+00:00";
                    }
                }
                if (participant && participant.localRegistrationDate) {
                    participant.localRegistrationDate = util.stringDateFormatter(participant.localRegistrationDate, participant.offset, 'YYYY-MM-DD HH:mm:ss');
                }
                if (participant && participant.disqualifyDate) {
                    participant.disqualificationDate = participant.disqualifyDate;
                }
                if (participant && !participant.withdrawDate) {
                    delete participant.withdrawDate;
                }
                if (participant && !participant.localWithdrawDate) {
                    delete participant.localWithdrawDate;
                }
                if (!participant.isPi) {
                    delete participant.disqualificationDate;
                    delete participant.email;
                    delete participant.firstName;
                }
                if (participant && participant.localWithdrawDate) {
                    participant.localWithdrawDate = util.stringDateFormatter(participant.localWithdrawDate, participant.offset, 'YYYY-MM-DD HH:mm:ss');
                }

                delete participant.disqualifyDate;
                delete participant.app_language_id;
                delete participant.display_language;
                delete participant.participantLocalTime;
                delete participant.isPi;
                jsonData.push(participants);
            }
            // Creating Json File
            const isJsonCreated = await files.createJsonFile(`${participantsPIIDataBaseUrl}/data.json`, jsonData);
            if (isJsonCreated instanceof Error) {
                util.createLog(event, `isJsonCreated Error:  `, isJsonCreated);
                return {
                    statusCode: constants.statusCodes.BadRequest,
                    body: JSON.stringify({
                        error: 'Not able to create json'
                    }),
                };
            }
            util.createLog(event, `data.json file created successfully`);

            util.createLog(event, `uploading files to s3 in temp folder`);
            // Creating Zip File
            const zipFile = await files.createZip(`${participantsPIIDataBaseUrl}`, `${studyBaseUrl}/participantRegistrationData.zip`, studyBaseUrl);
            // Convert Zip file to Binary file for S3 Upload
            const binaryZip = files.readFile(`${studyBaseUrl}/participantRegistrationData.zip`);

            // Uploading Zip File to S3 Bucket
            const isUploadedToS3 = await s3Services.uploadFile(bucketName, `studies/${studyId}/tmp/${studyId}_${executionId}/participantRegistrationData.zip`, binaryZip, 'application/zip');
            util.createLog(event, `is zipFile Uploaded To S3 `, isUploadedToS3);
            if (isUploadedToS3) {
                util.createLog(event, `Deleting Temp Files`);
                // files.removeFile(studyBaseUrl);
            }
            // Update Delog Status
            const payloadInProgress = {
                status: constants.enums.deLogs.status.SUCCEEDED,
                message: 'Participants Registration Data export Done',
                completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
            }
            util.createLog(event, `deLog status `, payloadInProgress);
            const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
            if (delogInProgress instanceof Error) {
                util.createLog(event, `delogInProgress Error  `, delogInProgress);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Updating deLog'
                    }),
                };
            }
        } else {
            util.createLog(event, `No Participants Registration Data to export for studyId: ${studyId}`);
            // Update Delog Status
            const payloadInProgress = {
                status: constants.enums.deLogs.status.SUCCEEDED,
                message: 'No Data for Participants Registration Export',
                completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
            }
            util.createLog(event, `deLog status `, payloadInProgress);
            const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
            if (delogInProgress instanceof Error) {
                util.createLog(event, `delogInProgress Error  `, delogInProgress);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Updating deLog'
                    }),
                };
            }
        }
        return {
            statusCode: 200,
            body: JSON.stringify({
                    success: 'true participant-registration-data'
                },
                null,
                2
            ),
        };
    } catch (error) {
        util.createLog(event, `Error: `, error);
        console.log('Error: ', error);
        return {
            statusCode: constants.statusCodes.InternalServerError,
            body: JSON.stringify({
                    error: error,
                },
                null,
                2
            ),
        };
    } finally {

    }
};