'use strict';

// const AWS = require('aws-sdk');

const awsXRay = require('aws-xray-sdk');
const AWS = awsXRay.captureAWS(require('aws-sdk'));

const constants = require('../../constants').constants;
const { util, db, files, s3Services, csvMappers, mappers, sendgridMail, excelMappers } = require('../../utility');
const dbObj = new db();
const dbName = constants.database.dbResearchResnpnse;

const _ = require('lodash');

let entityModels, responeEntityModels;

function checkIfSiteTeamMember(userRole) {
    if (!userRole ||
        userRole.toLowerCase() === constants.userRoles.pi.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.subPi.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.studyCoordinator.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.homeHealth.toLowerCase()) {
        return true;
    } else {
        return false;
    }
}

async function getStudyDetails(event, studyId) {
    util.createLog(event, `getStudyDetails called`);
    try {
        let filter = {
            id: studyId,
        };
        const studyDetails = await dbObj.findAll(filter, [], entityModels.StudyMetaData);
        return studyDetails;
    } catch (error) {
        util.createLog(event, `getStudyDetails Error: `, error);
        return error;
    }
}

async function getStudySiteDetails(event, studyId) {
    util.createLog(event, `getStudySiteDetails called`);
    try {
        let filter = {
            study_id: studyId,
        };
        const studySiteDetails = await dbObj.findAll(filter, [], entityModels.StudySite, '', ['study_id', 'ASC']);
        return studySiteDetails;
    } catch (error) {
        util.createLog(event, `getStudySiteDetails Error: `, error);
        return error;
    }
}

async function getSiteDetails(event, siteId) {
    util.createLog(event, `getSiteDetails called`);
    try {
        let filter = {
            id: siteId
        };
        const site = await dbObj.findOne(filter, [], entityModels.Sites);
        return site;
    } catch (error) {
        util.createLog(event, `getSiteDetails Error: `, error);
        return error;
    }
}

async function getClientConfig(event, clientId) {
    util.createLog(event, `getClientConfig called`);
    try {
        let filter = {
            id: clientId
        };
        const clientConfig = await dbObj.findOne(filter, [], entityModels.ClientConfig);
        return clientConfig;
    } catch (error) {
        util.createLog(event, `getClientConfig Error: `, error);
        return error;
    }
}

async function getParticipantDetails(event, participantId) {
    util.createLog(event, `getParticipantDetails called`);
    try {
        let filter = {
            id: participantId
        };
        const participant = await dbObj.findOne(filter, [], entityModels.Participant);
        return participant;
    } catch (error) {
        util.createLog(event, `getParticipantDetails Error: `, error);
        return error;
    }
}

async function createCsvPayload(event, data) {
    util.createLog(event, `Creating Csv Payload called`);
    const payload = [];
    for (const activityData of data) {
        const mappedResponse = csvMappers.map.activityResponseCsvMapper(activityData);
        payload.push(mappedResponse);
    }
    return payload;
}


async function getSFTPDetails(event, studyId) {
    util.createLog(event, `getSFTPDetails called`);
    try {
        let filter = {
            study_id: studyId,
        };
        const sftpDetails = await dbObj.findOneWithOrderBy(filter, [], entityModels.DeSftpMeta, ['sftp_version', 'DESC']);
        return sftpDetails;
    } catch (error) {
        util.createLog(event, `Error in getSFTPDetails : `, error)
        return error;
    }
}


async function createDeLog(event, clientId, studyId, logType, loggedInUserId) {
    util.createLog(event, `createDeLog called`);
    try {
        let deSftpMetaId = null;
        if (logType === 'SFTP') {
            const SFTPSecreteManagerDetails = await getSFTPDetails(event, studyId);
            if (SFTPSecreteManagerDetails instanceof Error) {
                util.createLog(event, `getSFTPDetails Error: `, SFTPSecreteManagerDetails);
                await updateDeLogFailed(event, clientId, deLog, SFTPSecreteManagerDetails);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Upload file on sftp'
                    }),
                };
            }
            util.createLog(event, `getSFTPDetails fetched successfully `);
            deSftpMetaId = (SFTPSecreteManagerDetails && SFTPSecreteManagerDetails.id) ? SFTPSecreteManagerDetails.id : null;
        }
        const data = {
            study_id: studyId,
            de_sftp_meta_id: deSftpMetaId,
            trigger_datetime: util.changeDateToUtc(new Date()),
            completed_datetime: null,
            message: null,
            status: constants.enums.deLogs.status.STARTED,
            aws_job_id: (event && event.executionId) ? event.executionId : null,
            aws_log_path: null,
            type: logType,
            s3_location: null,
            triggered_by: loggedInUserId ? loggedInUserId : null
        };
        const payload = mappers.map.deLogDataMapper(data);

        const deLog = await dbObj.create(payload, entityModels.DeLog);
        return deLog;
    } catch (error) {
        util.createLog(event, `createDeLog Error: `, error);
        return error;
    }
}

async function changeDeLogStatus(event, clientId, logId, payload) {
    util.createLog(event, `changeDeLogStatus called`);
    try {
        const deLog = await dbObj.updateById(logId, payload, entityModels.DeLog);
        return deLog;
    } catch (error) {
        util.createLog(event, `changeDeLogStatus Error: `, error);
        return error;
    }
}

async function getActivityDataByStudyId(event, studyId, clientId, defaultSiteId) {
    util.createLog(event, `getActivityDataByStudyId called`);
    try {
        let filter = {
            studyId: studyId
        };
        const activityResponse = await dbObj.findAll(filter, [], responeEntityModels.ActivityResponse);
        return activityResponse;
    } catch (error) {
        util.createLog(event, `getActivityDataByStudyId Error: `, error);
        return error;
    }
}

async function getActivityDataByStudyIdAndDefaultSiteId(event, studyId, defaultSiteId, clientId) {
    util.createLog(event, `getActivityDataByStudyIdAndDefaultSiteId called`);
    try {
        let filter = {
            studyId: studyId,
            siteId: defaultSiteId
        };
        const activityResponse = await dbObj.findAll(filter, [], responeEntityModels.ActivityResponse);
        return activityResponse;
    } catch (error) {
        util.createLog(event, `getActivityDataByStudyIdAndDefaultSiteId Error: `, error);
        return error;
    }
}

async function getActivityDataByStudyIdAndSiteId(event, studyId, loggedInUserSiteId, clientId) {
    util.createLog(event, `getActivityDataByStudyIdAndSiteId called`);
    try {
        let filter = {
            studyId: studyId,
            siteId: loggedInUserSiteId
        };
        const activityResponse = await dbObj.findAll(filter, [], responeEntityModels.ActivityResponse);
        return activityResponse;
    } catch (error) {
        util.createLog(event, `getActivityDataByStudyIdAndSiteId Error: `, error);
        return error;
    }
}

async function getActivityJsonFromS3(event, studyId, bucketName, s3Path) {
    util.createLog(event, `get Activity Json from s3 for studyId `, studyId);
    try {
        const s3Object = s3Services.readS3Object(s3Path, bucketName);
        return s3Object;
    } catch (error) {
        util.createLog(event, `getActivityJsonFromS3 Error: `, error);
        return error;
    }
}
async function validateStudy(event, studyId, clientId) {
    try {
        let filter = {
            id: studyId,
            client_id: clientId
        };
        const study = await dbObj.findOne(filter, [], entityModels.StudyMetaData);
        return study;
    } catch (error) {
        util.createLog(event, `Error in validateStudy: `, error);
        return error;
    }
}
module.exports.export = async(event, context) => {
    let deLogId;
    try {
        util.createLog(event, "Lambda Activity Response Data Execution Started ");
        let sftpUpload = false;
        let clientId, studyId, zipFileNameWithTimeStamp, authorization, isDataForNonPI;
        let loggedInUserId = (event && event.triggeredBy && event.triggeredBy.user && event.triggeredBy.user.id) ? event.triggeredBy.user.id : null;
        let hasAreas = [];
        if (event && event.areas) {
            hasAreas = event.areas.split(',');
        }
        //Checked area to execute Activity Response lambda
        if (hasAreas && hasAreas.length > 0 && hasAreas.includes(constants.exportAreas.activity) === false) {
            util.createLog(event, `Skipped lambda Activity Response Data`);
            return;
        }
        //Checking for client id in payload
        if (event && event.clientId) {
            clientId = event.clientId;
        } else {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: 'Missing clientId'
                }),
            };
        }
        // checking for sftpupload parameter in payload
        if (event && event.sftpUpload && event.sftpUpload === true) {
            sftpUpload = event.sftpUpload;
        }
        // checking for studyId in paylod
        if (event && event.studyId) {
            studyId = event.studyId;
            util.createLog(event, `studyId is `, studyId);
        } else {
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    message: 'Bad Request',
                    error: 'studyId is required'
                }),
            };
        }
        const models = await dbObj.init(clientId);
        entityModels = models;

        const responseModels = await dbObj.init(clientId, dbName);
        responeEntityModels = responseModels;
        const validateStudyDetails = await validateStudy(event, studyId, clientId);
        if (!validateStudyDetails || validateStudyDetails.length === 0) {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: "Invalid study details or Unauthorized access"
                })
            };
        }
        const loggedInUserRole = (event && event.triggeredBy && event.triggeredBy.user && event.triggeredBy.user.role) ? event.triggeredBy.user.role : null;
        util.createLog(event, `LoggedIn user role is: ${loggedInUserRole}`);
        const loggedInUserSiteIds = event.siteIds;
        let loggedInUserSiteId;

        // if ((!loggedInUserSiteIds || !loggedInUserSiteIds.length > 0) && !loggedInUserRole.toLowerCase() === constants.userRoles.admin.toLowerCase()) {
        //     return {
        //         statusCode: constants.statusCodes.Unauthorized,
        //         body: JSON.stringify({
        //             message: 'User is not associated with any site'
        //         }),
        //     };
        // }
        // if (loggedInUserSiteIds && loggedInUserSiteIds.length > 0) {
        //     loggedInUserSiteId = loggedInUserSiteIds[0];
        //     util.createLog(event, `User is associated with site: loggedInUserSiteId ${loggedInUserSiteId}, LoggedIn user role is: ${loggedInUserRole}`);
        // }

        const isSiteTeamMember = checkIfSiteTeamMember(loggedInUserRole);
        if (isSiteTeamMember) {
            util.createLog(event, `Site team member logged in`);
            if (loggedInUserSiteIds && loggedInUserSiteIds.length > 0) {
                loggedInUserSiteId = loggedInUserSiteIds[0]
            } else {
                throw 'User is not associated with any site';
            }
        }
        util.createLog(event, `User is associated with site: loggedInUserSiteId ${loggedInUserSiteId}, LoggedIn user role is: ${loggedInUserRole}`);
        // if (!isSiteTeamMember) {
        //     return {
        //         statusCode: constants.statusCodes.Unauthorized,
        //         body: JSON.stringify({
        //             message: 'User is not authorized user'
        //         }),
        //     };
        // }

        const studyDetails = await getStudyDetails(event, studyId);
        if (studyDetails instanceof Error) {
            util.createLog(event, `studyDetails Error: `, studyDetails);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Study Details not found'
                }),
            };
        }

        const studySiteDetails = await getStudySiteDetails(event, studyId);

        let defaultSiteId;
        for (const studySite of studySiteDetails) {
            if (studySite && studySite.is_default) {
                defaultSiteId = studySite.site_id;
            }
        }
        util.createLog(event, `defaultSiteId:`, defaultSiteId);

        // Creating deLog
        const logType = (sftpUpload === true) ? constants.enums.deLogs.type.SFTP : constants.enums.deLogs.type.DATAEXPORT;
        const deLog = await createDeLog(event, clientId, studyId, logType, loggedInUserId);
        if (deLog instanceof Error) {
            util.createLog(event, `unable to insert de log record: `, deLog);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Not able to create Delog'
                }),
            };
        }
        util.createLog(event, `deLog: ${deLog.id}`);
        deLogId = deLog.id;

        // Fetching site details
        let siteDetails = await getSiteDetails(event, (loggedInUserSiteId) ? loggedInUserSiteId : defaultSiteId);
        if (siteDetails instanceof Error) {
            util.createLog(event, `siteDetails Error: `, siteDetails);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Site Details not found'
                }),
            };
        }
        util.createLog(event, `sites details: `, siteDetails);

        const siteId = siteDetails.site_id;
        util.createLog(event, `siteId: ${siteId}`);

        // Fetching client configuration details
        const clientConfig = await getClientConfig(event, clientId);
        if (clientConfig instanceof Error) {
            util.createLog(event, `clientConfig Error: `, clientConfig);
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'Client Config not found'
                }),
            };
        }
        const bucketName = clientConfig.s3_bucket;
        const domain = clientConfig.domain_name;
        if (!bucketName) {
            util.createLog(event, `S3 Bucket not Found`);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'S3 Bucket not Found'
                }),
            };
        }
        let executionId = util.generateUniqueCode();
        if (event.executionId) {
            executionId = event.executionId;
        }
        let studyBaseUrl = `/tmp/${constants.tempExportFolder}/${studyId}_${executionId}`;
        if (event.studyBaseUrl) {
            studyBaseUrl = event.studyBaseUrl;
        }
        let zipFileName = studyId;
        if (studyDetails && studyDetails.length > 0 && studyDetails[0].name) {
            util.createLog(event, `Study Name `, studyDetails[0].name);
            zipFileName = studyDetails[0].name;
        }

        util.createLog(event, `zipFileName `, zipFileName);
        const dateNow = new Date();
        const timeStamp = util.changeDateFormat(dateNow, 'YYYYMMDDHHmm');
        zipFileNameWithTimeStamp = `${zipFileName}_${timeStamp}`;
        if (event.zipFileNameWithTimeStamp) {
            zipFileNameWithTimeStamp = event.zipFileNameWithTimeStamp;
        }
        util.createLog(event, `zipFileNameWithTimeStamp `, zipFileNameWithTimeStamp);
        const activityBaseUrl = studyBaseUrl + "/activityResponses";

        // Update Delog Status
        const payloadInProgress = { status: constants.enums.deLogs.status.INPROGRESS, message: 'activity-response' };
        util.createLog(event, `deLog status `, payloadInProgress);
        const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
        if (delogInProgress instanceof Error) {
            util.createLog(event, `delogInProgress Error  `, delogInProgress);
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'Error occured in Updating deLog'
                }),
            };
        }
        let activityResponseData;
        // loggedInUserSiteId = null;
        if (!loggedInUserSiteId) {
            //For all user other than site team members
            util.createLog(event, `!loggedInUserSiteId`);
            activityResponseData = await getActivityDataByStudyId(event, studyId, clientId, defaultSiteId);

        } else if (defaultSiteId && defaultSiteId === loggedInUserSiteId) {
            //For default site's site team members this is for handling null values of site_is column
            util.createLog(event, `defaultSiteId && defaultSiteId === loggedInUserSiteId`);
            activityResponseData = await getActivityDataByStudyIdAndDefaultSiteId(event, studyId, defaultSiteId, clientId);
        } else {
            //For non-default site's site team members
            util.createLog(event, `For non-default sites site team members`);
            activityResponseData = await getActivityDataByStudyIdAndSiteId(event, studyId, loggedInUserSiteId, clientId);
        }

        util.createLog(event, `Study Activity Response Fetched successfully`);
        util.createLog(event, `Study Activity Response Data size: ${activityResponseData.length}`);

        if (activityResponseData && activityResponseData.length > 0) {
            let count = 1;
            const activityIds = [];

            activityResponseData.filter((obj) => {
                activityIds.push(obj.activityId);
            });
            util.createLog(event, 'activityIds', activityIds);
            let itemCount = 1;
            let isResponse = false;
            for (const activity of activityIds) {
                const activityResponseCsvData = [];
                const activityResponseJsonData = [];

                for (const activityResponse of activityResponseData) {
                    let data = activityResponse;
                    const activityId = activityResponse.activityId;
                    if (activity === activityId) {
                        const activityName = activityResponse.activityName;
                        let leftEyeScore, rightEyeScore, examType;
                        if (activityId) {
                            const s3Folders = constants.userConsts.s3Folders;
                            const s3Files = constants.userConsts.s3Files;
                            const s3Key = `${s3Folders.study}/${studyId}/${s3Folders.activities}/${activityId}.json`;
                            const activityS3Data = await getActivityJsonFromS3(event, studyId, bucketName, s3Key);
                            if (activityS3Data) {
                                isResponse = true;
                                util.createLog(event, `activityS3Data: `, activityS3Data);

                                // Creating Json S3 Identifier File for Response file from s3
                                const s3Identifier = activityS3Data.identifier;
                                const s3ResponseJson = await files.createJsonFile(`${activityBaseUrl}/${activityName}/${s3Identifier}.json`, activityS3Data);

                                if (s3ResponseJson instanceof Error) {
                                    util.createLog(event, `s3ResponseJson error: `, s3ResponseJson);
                                    return {
                                        statusCode: constants.statusCodes.BadRequest,
                                        body: JSON.stringify({
                                            error: 'Not able to create json'
                                        }),
                                    };
                                }

                                util.createLog(event, `Json S3 Identifier File ${activityBaseUrl}/${activityName}/responses.json Created Successfully`);

                                if (activityResponse && activityResponse.responseData) {
                                    let jsonFileData = JSON.parse(activityResponse.responseData);
                                    const isJsonCreated = await files.createJsonFile(`${activityBaseUrl}/${activityName}/data/${activityResponse.id}.json`, jsonFileData);

                                    if (isJsonCreated instanceof Error) {
                                        util.createLog(event, `isJsonCreated error:  `, isJsonCreated);
                                        return {
                                            statusCode: constants.statusCodes.BadRequest,
                                            body: JSON.stringify({
                                                error: 'Not able to create json'
                                            }),
                                        };
                                    }

                                    util.createLog(event, `Json File ${activityBaseUrl}/${activityName}/responses.json Created Successfully`);

                                    if (jsonFileData && jsonFileData.ContrastAcuityExamResults && jsonFileData.ContrastAcuityExamResults.length > 0) {
                                        util.createLog(event, 'ContrastAcuityExam');
                                        let contrastAcuityExamResults = jsonFileData.ContrastAcuityExamResults;
                                        util.createLog(event, 'ContrastAcuityExam', contrastAcuityExamResults);
                                        rightEyeScore = _.result(
                                            _.find(contrastAcuityExamResults, function(i) {
                                                return i.eye === 'Right';
                                            }),
                                            'score'
                                        );

                                        leftEyeScore = _.result(
                                            _.find(contrastAcuityExamResults, function(i) {
                                                return i.eye === 'Left';
                                            }),
                                            'score'
                                        );
                                        examType = 'ContrastAcuityExamResults';
                                    } else if (jsonFileData && jsonFileData.VisualAcuityExamResults && jsonFileData.VisualAcuityExamResults.length > 0) {
                                        util.createLog(event, 'VisualAcuityExam');
                                        let visualAcuityExamResults = jsonFileData.VisualAcuityExamResults;
                                        util.createLog(event, 'VisualAcuityExam Data', visualAcuityExamResults);
                                        rightEyeScore = _.result(
                                            _.find(visualAcuityExamResults, function(i) {
                                                return i.eye === 'Right';
                                            }),
                                            'score'
                                        );

                                        leftEyeScore = _.result(
                                            _.find(visualAcuityExamResults, function(i) {
                                                return i.eye === 'Left';
                                            }),
                                            'score'
                                        );
                                        examType = 'VisualAcuityExamResults';
                                    }
                                    data.leftEyeScore = leftEyeScore;
                                    data.rightEyeScore = rightEyeScore;
                                    data.examType = examType;
                                }

                                const participant = await getParticipantDetails(event, activityResponse.participantId);
                                data.userDefinedParticipantId = participant.user_defined_participant_id;

                                // getting siteDetails
                                let siteData;
                                if (activityResponse && activityResponse.siteId) {
                                    let activitySiteId = activityResponse.siteId;
                                    siteData = await getSiteDetails(event, activitySiteId);
                                }
                                if (siteData && siteData.siteId) {
                                    data.siteId = siteData.siteId;
                                }
                                if (siteData && siteData.name) {
                                    data.siteName = siteData.name;
                                }
                                if (data && !data.participantLocalStartTime && data.startTime) {
                                    data.participantLocalStartTime = data.startTime;
                                }
                                if (data && !data.participantLocalEndTime && data.endTime) {
                                    data.participantLocalEndTime = data.endTime;
                                }
                                if (data && !data.participantLocalTimeOffset) {
                                    data.participantLocalTimeOffset = "+00:00";
                                }
                                activityResponseCsvData.push(data);

                                delete data.dataValues.responseData;

                                // Creating CSV payload and mappings for data
                                const mappedResponse = await createCsvPayload(event, activityResponseCsvData);

                                // Getting Header for CSV File
                                const headers = util.getKeys(mappedResponse[0]);

                                // Creating CSV File
                                const isCsvCreated = await files.createCsvFile(`${activityBaseUrl}/${activityName}/response.csv`, headers, mappedResponse);
                                if (isCsvCreated instanceof Error) {
                                    util.createLog(event, `Csv create error: `, isCsvCreated);
                                    return {
                                        statusCode: constants.statusCodes.BadRequest,
                                        body: JSON.stringify({
                                            error: 'Not able to create CSV'
                                        }),
                                    };
                                }

                                // Creating Json payload and mappings for data
                                const jsonResponse = {
                                    responseId: activityResponse.id,
                                    studyId: activityResponse.studyId,
                                    siteId: (data && data.siteId) ? data.siteId : '',
                                    siteName: (data && data.siteName) ? data.siteName : '',
                                    participantId: activityResponse.participantId,
                                    activityId: activityId,
                                    activityName: activityName,
                                    timeTaken: activityResponse.timeTaken,
                                    startTime: (activityResponse.startTime) ? util.changeDateToUtc(activityResponse.startTime) : '',
                                    endTime: (activityResponse.endTime) ? util.changeDateToUtc(activityResponse.endTime) : '',
                                    startTimeLocal: (activityResponse && activityResponse.participantLocalStartTime && activityResponse.participantLocalTimeOffset) ? util.stringDateFormatter(activityResponse.participantLocalStartTime, activityResponse.participantLocalTimeOffset, 'YYYY-MM-DD HH:mm:ss') : '',
                                    endTimeLocal: (activityResponse && activityResponse.participantLocalEndTime && activityResponse.participantLocalTimeOffset) ? util.stringDateFormatter(activityResponse.participantLocalEndTime, activityResponse.participantLocalTimeOffset, 'YYYY-MM-DD HH:mm:ss') : '',
                                    localTimeOffset: (activityResponse.participantLocalTimeOffset) ? activityResponse.participantLocalTimeOffset : '+00:00',
                                    status: activityResponse.status,
                                    studyCode: activityResponse.studyCode,
                                }
                                if (jsonResponse && !jsonResponse.startTimeLocal && jsonResponse.startTime) {
                                    jsonResponse.startTimeLocal = jsonResponse.startTime;
                                }
                                if (jsonResponse && !jsonResponse.endTimeLocal && jsonResponse.endTime) {
                                    jsonResponse.endTimeLocal = jsonResponse.endTime;
                                }
                                if (participant.user_defined_participant_id) {
                                    jsonResponse.userDefinedParticipantId = participant.user_defined_participant_id;
                                }
                                activityResponseJsonData.push(jsonResponse);
                                const isResponseJsonCreated = await files.createJsonFile(`${activityBaseUrl}/${activityName}/response.json`, activityResponseJsonData);

                                if (isResponseJsonCreated instanceof Error) {
                                    util.createLog(event, `isResponseJsonCreated error: `, isResponseJsonCreated);
                                    return {
                                        statusCode: constants.statusCodes.BadRequest,
                                        body: JSON.stringify({
                                            error: 'Not able to create json'
                                        }),
                                    };
                                }
                                util.createLog(event, `Json File ${activityBaseUrl}/${activityName}/responses.json Created Successfully`);

                                // Update Delog Status
                                const payloadInProgress = {
                                    status: constants.enums.deLogs.status.SUCCEEDED,
                                    message: 'Activity Data export Done',
                                    completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
                                }
                                util.createLog(event, `deLog status `, payloadInProgress);
                                const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
                                if (delogInProgress instanceof Error) {
                                    util.createLog(event, `delogInProgress Error  `, delogInProgress);
                                    return {
                                        statusCode: constants.statusCodes.InternalServerError,
                                        body: JSON.stringify({
                                            error: 'Error occured in Updating deLog'
                                        }),
                                    };
                                }
                            }
                        }
                    }
                }

                if (itemCount === activityResponseData.length && isResponse) {
                    util.createLog(event, `uploading files to s3 in temp folder`);
                    // Creating Zip File
                    const zipFile = await files.createZip(`${activityBaseUrl}`, `${studyBaseUrl}/activityResponses.zip`, studyBaseUrl);
                    // Convert Zip file to Binary file for S3 Upload
                    const binaryZip = files.readFile(`${studyBaseUrl}/activityResponses.zip`);

                    // Uploading Zip File to S3 Bucket
                    const isUploadedToS3 = await s3Services.uploadFile(bucketName, `studies/${studyId}/tmp/${studyId}_${executionId}/activityResponses.zip`, binaryZip, 'application/zip');
                    util.createLog(event, `is zipFile Uploaded To S3 `, isUploadedToS3);
                }

                itemCount++;
            }

        } else {
            util.createLog(event, `No Activity Response Data to export for studyId `, studyId);
            // Update Delog Status
            const payloadInProgress = {
                status: constants.enums.deLogs.status.SUCCEEDED,
                message: 'No Data for Activity Export',
                completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
            }
            util.createLog(event, `deLog status `, payloadInProgress);
            const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
            if (delogInProgress instanceof Error) {
                util.createLog(event, `delogInProgress Error  `, delogInProgress);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Updating deLog'
                    }),
                };
            }
        }
        return {
            statusCode: 200,
            body: JSON.stringify({
                    success: 'Activity Response successfull'
                },
                null,
                2
            ),
        };
    } catch (error) {
        util.createLog(event, `Error: `, error);
        console.log('Error', error, 'deLogId', deLogId);
        const payloadFailed = {
            status: constants.enums.deLogs.status.FAILED,
            message: error && error.stack ? error.stack : error && error.message ? error.message : 'failed',
            completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss'),
        }

        const isStatusUpdated = await changeDeLogStatus(event, event.clientId, deLogId, payloadFailed);
        throw error;
    } finally {

    }
};