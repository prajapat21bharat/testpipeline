'use strict';

// const AWS = require('aws-sdk');

const awsXRay = require('aws-xray-sdk');
const AWS = awsXRay.captureAWS(require('aws-sdk'));

const constants = require('../../constants').constants;
const { util, db, files, s3Services, csvMappers, mappers, sendgridMail } = require('../../utility');
const dbObj = new db();
const dbName = constants.database.dbResearchResnpnse;

const _ = require('lodash');

let entityModels, responeEntityModels;

function checkIfSiteTeamMember(userRole) {
    if (!userRole ||
        userRole.toLowerCase() === constants.userRoles.pi.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.subPi.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.studyCoordinator.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.homeHealth.toLowerCase()) {
        return true;
    } else {
        return false;
    }
}

async function getStudyDetails(event, studyId) {
    util.createLog(event, `getStudyDetails called`);
    try {
        let filter = {
            id: studyId,
        };
        const studyDetails = await dbObj.findOne(filter, [], entityModels.StudyMetaData);
        return studyDetails;
    } catch (error) {
        util.createLog(event, `getStudyDetails error: `, error);
        return error;
    }
}

async function getStudySiteDetails(event, studyId) {
    util.createLog(event, `getStudySiteDetails called`);
    try {
        let filter = {
            study_id: studyId,
        };
        const studySiteDetails = await dbObj.findAll(filter, [], entityModels.StudySite, '', ['study_id', 'ASC']);
        return studySiteDetails;
    } catch (error) {
        util.createLog(event, `getStudySiteDetails Error: `, error);
        return error;
    }
}

async function getSiteDetails(event, siteId) {
    util.createLog(event, `getSiteDetails called`);
    try {
        let filter = {
            id: siteId
        };
        const site = await dbObj.findOne(filter, [], entityModels.Sites);
        return site;
    } catch (error) {
        util.createLog(event, `getSiteDetails error: `, error);
        return error;
    }
}

async function getFeedBackResponseByStudyId(event, studyId, clientId) {
    util.createLog(event, `getFeedBackResponseByStudyId called`);
    try {
        let filter = {
            study_id: studyId
        };
        const feedbackResponse = await dbObj.findAll(filter, [], responeEntityModels.FeedbackResponse);
        return feedbackResponse;
    } catch (error) {
        util.createLog(event, `getFeedBackResponseByStudyId error: `, error);
        return error;
    }
}

async function getFeedBackResponseByStudyIdAndDefaultSiteId(event, studyId, defaultSiteId, clientId) {
    util.createLog(event, `getFeedBackResponseByStudyIdAndDefaultSiteId called`);
    try {
        let filter = {
            study_id: studyId,
            site_id: defaultSiteId
        };
        const feedbackResponse = await dbObj.findAll(filter, [], responeEntityModels.FeedbackResponse);
        return feedbackResponse;
    } catch (error) {
        util.createLog(event, `getFeedBackResponseByStudyIdAndDefaultSiteId error: `, error);
        return error;
    }
}

async function getFeedBackResponseByStudyIdAndSiteId(event, studyId, loggedInUserSiteId, clientId) {
    util.createLog(event, `getFeedBackResponseByStudyIdAndSiteId called`);
    try {

        let filter = {
            study_id: studyId,
            site_id: loggedInUserSiteId
        };
        const feedbackResponse = await dbObj.findAll(filter, [], responeEntityModels.FeedbackResponse);
        return feedbackResponse;
    } catch (error) {
        util.createLog(event, `getFeedBackResponseByStudyIdAndSiteId error: `, error);
        return error;
    }
}

async function getClientConfig(event, clientId) {
    util.createLog(event, `getClientConfig called`);
    try {
        let filter = {
            id: clientId
        };
        const clientConfig = await dbObj.findOne(filter, [], entityModels.ClientConfig);
        return clientConfig;
    } catch (error) {
        util.createLog(event, `getClientConfig error: `, error);
        return error;
    }
}

async function getParticipantDetails(event, participantId) {
    util.createLog(event, `getParticipantDetails called`);
    try {
        let filter = {
            id: participantId
        };
        const participant = await dbObj.findOne(filter, [], entityModels.Participant);
        return participant;
    } catch (error) {
        util.createLog(event, `getParticipantDetails error: `, error);
        return error;
    }
}

async function downloadMoreFeedBackJsonFromS3(event, studyId, bucketName, s3Path) {
    util.createLog(event, `get FeedBack Json from s3 for ${studyId}`);
    try {
        const s3Object = s3Services.readS3Object(s3Path, bucketName);
        return s3Object;
    } catch (error) {
        util.createLog(event, `downloadMoreFeedBackJsonFromS3 error: `, error);
        return error;
    }
}

async function createCsvPayload(event, data) {
    util.createLog(event, `Creating CSV Payload`);
    const payload = [];
    for (const feedback of data) {
        const mappedFeedback = csvMappers.map.feedbackCsvMapper(feedback);
        payload.push(mappedFeedback);
    }
    return payload;
}

async function findTemplateByClientIdAndTemplateType(event, studyId, clientId) {
    util.createLog(event, `findTemplateByClientIdAndTemplateType called`);
    try {
        let filter = {
            client_id: clientId,
            template_type: constants.email.templateType.dataExport
        };
        const emailTemplate = await dbObj.findOne(filter, [], entityModels.EmailTemplate);
        return emailTemplate;
    } catch (error) {
        util.createLog(event, `findTemplateByClientIdAndTemplateType error: `, error);
        return error;
    }
}

async function getSFTPDetails(event, studyId) {
    util.createLog(event, `getSFTPDetails called`);
    try {
        let filter = {
            study_id: studyId,
        };
        const sftpDetails = await dbObj.findOneWithOrderBy(filter, [], entityModels.DeSftpMeta, ['sftp_version', 'DESC']);
        return sftpDetails;
    } catch (error) {
        util.createLog(event, `Error in getSFTPDetails : `, error)
        return error;
    }
}

async function createDeLog(event, clientId, studyId, logType, loggedInUserId) {
    util.createLog(event, `createDeLog called`);
    try {
        let deSftpMetaId = null;
        if (logType === 'SFTP') {
            const SFTPSecreteManagerDetails = await getSFTPDetails(event, studyId);
            if (SFTPSecreteManagerDetails instanceof Error) {
                util.createLog(event, `getSFTPDetails Error: `, SFTPSecreteManagerDetails);
                await updateDeLogFailed(event, clientId, deLog, SFTPSecreteManagerDetails);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Upload file on sftp'
                    }),
                };
            }
            util.createLog(event, `getSFTPDetails fetched successfully `);
            deSftpMetaId = (SFTPSecreteManagerDetails && SFTPSecreteManagerDetails.id) ? SFTPSecreteManagerDetails.id : null;
        }
        const data = {
            study_id: studyId,
            de_sftp_meta_id: deSftpMetaId,
            trigger_datetime: util.changeDateToUtc(new Date()),
            completed_datetime: null,
            message: null,
            status: constants.enums.deLogs.status.STARTED,
            aws_job_id: (event && event.executionId) ? event.executionId : null,
            aws_log_path: null,
            type: logType,
            s3_location: null,
            triggered_by: loggedInUserId ? loggedInUserId : null
        };
        const payload = mappers.map.deLogDataMapper(data);

        const deLog = await dbObj.create(payload, entityModels.DeLog);
        return deLog;
    } catch (error) {
        util.createLog(event, `createDeLog error: `, error);
        return error;
    }
}

async function changeDeLogStatus(event, clientId, logId, payload) {
    util.createLog(event, `changeDeLogStatus called`);
    try {
        const deLog = await dbObj.updateById(logId, payload, entityModels.DeLog);
        return deLog;
    } catch (error) {
        util.createLog(event, `changeDeLogStatus error: `, error);
        return error;
    }
}
async function validateStudy(event, studyId, clientId) {
    try {
        let filter = {
            id: studyId,
            client_id: clientId
        };
        const study = await dbObj.findOne(filter, [], entityModels.StudyMetaData);
        return study;
    } catch (error) {
        util.createLog(event, `Error in validateStudy: `, error);
        return error;
    }
}
module.exports.export = async(event, context) => {
    let deLogId;
    try {
        util.createLog(event, `Lambda feedback-response Execution Started`);
        let sftpUpload = false;
        let clientId, studyId, zipFileNameWithTimeStamp;
        let loggedInUserId = (event && event.triggeredBy && event.triggeredBy.user && event.triggeredBy.user.id) ? event.triggeredBy.user.id : null;
        let hasAreas = [];
        if (event && event.areas) {
            hasAreas = event.areas.split(',');
        }
        //Checked area to execute feedbackResponse lambda
        if (hasAreas && hasAreas.length > 0 && hasAreas.includes(constants.exportAreas.feedback) === false) {
            util.createLog(event, "Skipped lambda feedback Response execution");
            return;
        }
        //Checking for client id in payload
        if (event && event.clientId) {
            clientId = event.clientId;
        } else {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: 'Missing clientId'
                }),
            };
        }
        // checking for sftpupload parameter in payload
        if (event && event.sftpUpload && event.sftpUpload === true) {
            sftpUpload = event.sftpUpload;
        }
        // checking for studyId in paylod
        if (event && event.studyId) {
            studyId = event.studyId;
            util.createLog(event, `studyId is `, studyId);
        } else {
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    message: 'Bad Request',
                    error: 'studyId is required'
                }),
            };
        }
        const models = await dbObj.init(clientId);
        entityModels = models;

        const responseModels = await dbObj.init(clientId, dbName);
        responeEntityModels = responseModels;
        const validateStudyDetails = await validateStudy(event, studyId, clientId);
        if (!validateStudyDetails || validateStudyDetails.length === 0) {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: "Invalid study details or Unauthorized access"
                })
            };
        }
        const loggedInUserRole = (event && event.triggeredBy && event.triggeredBy.user && event.triggeredBy.user.role) ? event.triggeredBy.user.role : null;
        const loggedInUserSiteIds = event.siteIds;
        let loggedInUserSiteId;

        // util.createLog(event, `loggedInUserSiteId ${loggedInUserSiteId}`);
        // if ((!loggedInUserSiteIds || !loggedInUserSiteIds.length > 0) && !loggedInUserRole.toLowerCase() === constants.userRoles.admin.toLowerCase()) {
        //     return {
        //         statusCode: constants.statusCodes.Unauthorized,
        //         body: JSON.stringify({
        //             message: 'User is not associated with any site'
        //         }),
        //     };
        // }

        // if (loggedInUserSiteIds && loggedInUserSiteIds.length > 0) {
        //     loggedInUserSiteId = loggedInUserSiteIds[0];
        //     util.createLog(event, ` LoggedIn user role is: ${loggedInUserRole}`);
        // }

        const isSiteTeamMember = checkIfSiteTeamMember(loggedInUserRole);
        if (isSiteTeamMember) {
            util.createLog(event, `Site team member logged in`);
            if (loggedInUserSiteIds && loggedInUserSiteIds.length > 0) {
                loggedInUserSiteId = loggedInUserSiteIds[0]
            } else {
                throw 'User is not associated with any site';
            }
        }
        util.createLog(event, `User is associated with site: loggedInUserSiteId ${loggedInUserSiteId}, LoggedIn user role is: ${loggedInUserRole}`);
        // if (!isSiteTeamMember) {
        //     return {
        //         statusCode: constants.statusCodes.Unauthorized,
        //         body: JSON.stringify({
        //             message: 'User is not authorized user'
        //         }),
        //     };
        // }

        util.createLog(event, `clientId from lambda: ${clientId}`);
        const studyDetails = await getStudyDetails(event, studyId);
        if (studyDetails instanceof Error) {
            util.createLog(event, `Error studyDetails: ${studyDetails}`);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Study Details not found'
                }),
            };
        }

        util.createLog(event, `studyDetails details: ${studyDetails}`);
        // Creating deLog
        const logType = (sftpUpload === true) ? constants.enums.deLogs.type.SFTP : constants.enums.deLogs.type.DATAEXPORT;
        const deLog = await createDeLog(event, clientId, studyId, logType, loggedInUserId);
        if (deLog instanceof Error) {
            util.createLog(event, `deLog Error : ${deLog}`);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Not able to create Delog'
                }),
            };
        }
        deLogId = deLog.id;
        util.createLog(event, `deLog: ${deLog.id}`);
        const studySiteDetails = await getStudySiteDetails(event, studyId);

        let defaultSiteId;
        for (const studySiteDetail of studySiteDetails) {
            if (studySiteDetail && studySiteDetail.is_default) {
                defaultSiteId = studySiteDetail.site_id;
            }
        }
        util.createLog(event, `defaultSiteId:`, defaultSiteId);

        // Fetching site details
        let siteDetails = await getSiteDetails(event, (loggedInUserSiteId) ? loggedInUserSiteId : defaultSiteId);
        if (siteDetails instanceof Error) {
            util.createLog(event, `deLog siteDetails : `, siteDetails);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Site Details not found'
                }),
            };
        }

        const siteId = siteDetails.site_id;
        util.createLog(event, `siteId : ${siteId}`);

        // Fetching client configuration details
        const clientConfig = await getClientConfig(event, clientId);
        if (clientConfig instanceof Error) {
            util.createLog(event, `clientConfig Error : `, clientConfig);
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'Client Config not found'
                }),
            };
        }
        const bucketName = clientConfig.s3_bucket;
        const domain = clientConfig.domain_name;
        if (!bucketName) {
            util.createLog(event, `S3 Bucket not Found `);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'S3 Bucket not Found'
                }),
            };
        }
        let executionId = util.generateUniqueCode();
        if (event.executionId) {
            executionId = event.executionId;
        }
        let studyBaseUrl = `/tmp/${constants.tempExportFolder}/${studyId}_${executionId}`;
        if (event.studyBaseUrl) {
            studyBaseUrl = event.studyBaseUrl;
        }
        let zipFileName = studyId;
        if (studyDetails && studyDetails.name) {
            zipFileName = studyDetails.name;
        }
        util.createLog(event, `zipFileName : ${zipFileName}`);
        const dateNow = new Date();
        const timeStamp = util.changeDateFormat(dateNow, 'YYYYMMDDHHmm');
        zipFileNameWithTimeStamp = `${zipFileName}_${timeStamp}`;
        if (event.zipFileNameWithTimeStamp) {
            zipFileNameWithTimeStamp = event.zipFileNameWithTimeStamp;
        }
        util.createLog(event, `zipFileNameWithTimeStamp : ${zipFileNameWithTimeStamp}`);
        const feedBackResponsesBaseUrl = studyBaseUrl + "/feedbacks";

        // Update Delog Status
        const payloadInProgress = { status: constants.enums.deLogs.status.INPROGRESS, message: 'feedback-response' };
        util.createLog(event, `deLog status : ${payloadInProgress}`);
        const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
        if (delogInProgress instanceof Error) {
            util.createLog(event, `delogInProgress Error : ${payloadInProgress}`);
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'Error occured in Updating deLog'
                }),
            };
        }

        let studyFeedBackResponses;
        if (!loggedInUserSiteId) {
            //For all user other than site team members
            util.createLog(event, `!loggedInUserSiteId `);
            studyFeedBackResponses = await getFeedBackResponseByStudyId(event, studyId, clientId);
            util.createLog(event, `studyFeedBackResponses`);

        } else if (defaultSiteId && defaultSiteId === loggedInUserSiteId) {
            //For default site's site team members this is for handling null values of site_is column
            util.createLog(event, `defaultSiteId && defaultSiteId === loggedInUserSiteId`);
            studyFeedBackResponses = await getFeedBackResponseByStudyIdAndDefaultSiteId(event, studyId, defaultSiteId, clientId);
        } else {
            //For non-default site's site team members
            util.createLog(event, `For non-default sites site team members`);
            studyFeedBackResponses = await getFeedBackResponseByStudyIdAndSiteId(event, studyId, loggedInUserSiteId, clientId);
        }

        if (studyFeedBackResponses && studyFeedBackResponses.length > 0) {
            util.createLog(event, `Study Feedback responses size: ${studyFeedBackResponses.length}`);
            const s3Folders = constants.userConsts.s3Folders;
            const s3Files = constants.userConsts.s3Files;
            const s3Key = `${s3Folders.study}/${studyId}/${s3Folders.assets}/${s3Folders.learnmore}/${s3Files.learnmore}.json`;

            // Download Learn More Data from S3 Bucket
            const s3LearnMoreData = await downloadMoreFeedBackJsonFromS3(event, studyId, bucketName, s3Key);
            if (s3LearnMoreData instanceof Error) {
                util.createLog(event, `s3LearnMoreData Error: ${s3LearnMoreData}`);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Data fetch from S3'
                    }),
                };
            }
            util.createLog(event, `s3LearnMore Data called successfully`);
            const feedbackDataFromS3 = s3LearnMoreData;
            if (feedbackDataFromS3 && feedbackDataFromS3.feedback) {
                // Create Json File for Feedback data from S3 Learnmore file
                const isFeedbackJson = await files.createJsonFile(`${feedBackResponsesBaseUrl}/feedback.json`, feedbackDataFromS3.feedback);
                if (isFeedbackJson instanceof Error) {
                    util.createLog(event, `isFeedbackJson error: ${isFeedbackJson}`);
                    return {
                        statusCode: constants.statusCodes.BadRequest,
                        body: JSON.stringify({
                            error: 'Not able to create json'
                        }),
                    };
                }
                util.createLog(event, `feedbackJson created`);
            }

            let feedbacks = studyFeedBackResponses;
            const feedbackData = [];
            for (const feedback of feedbacks) {
                let rawFeedback = JSON.parse(JSON.stringify(feedback));
                var date = util.changeDateToUtc(rawFeedback.responseTime);
                delete rawFeedback.responseTime;
                const participantId = feedback.participantId;
                const questionId = feedback.questionId;
                const questionData = s3LearnMoreData;
                // if (questionData && questionData.feedback && questionData.feedback.quiz) {
                //     const quizData = questionData.feedback.quiz;
                //     // Adding Questions to Feedback response from Learn more S3 Json
                //     quizData.map((learnMore) => {
                //         if (learnMore.identifier === questionId) {
                //             rawFeedback['questionText'] = learnMore.body;
                //             rawFeedback['displayQuestionText'] = learnMore.body;
                //         }
                //     })
                // }

                // getting siteDetails
                let siteData;
                if (feedback && feedback.siteId) {
                    siteData = await getSiteDetails(event, feedback.siteId);
                }
                // Getting Participant Details
                const participant = await getParticipantDetails(event, participantId);
                rawFeedback['siteId'] = (siteData && siteData.siteId) ? siteData.siteId : '';
                rawFeedback['siteName'] = (siteData && siteData.name) ? siteData.name : '';
                rawFeedback['responseTime'] = date;
                rawFeedback['userDefinedParticipantId'] = participant.user_defined_participant_id;
                feedbackData.push(rawFeedback)
            }

            util.createLog(event, `Feedback Data sucees`);
            // Creating Json File
            const isJsonCreated = await files.createJsonFile(`${feedBackResponsesBaseUrl}/response.json`, feedbackData);
            if (isJsonCreated instanceof Error) {
                util.createLog(event, `isJsonCreated error:  `, isJsonCreated);
                return {
                    statusCode: constants.statusCodes.BadRequest,
                    body: JSON.stringify({
                        error: 'Not able to create json'
                    }),
                };
            }
            util.createLog(event, `Json Created`);
            // Creating CSV payload and mappings for data
            const mappedFeedback = await createCsvPayload(event, feedbackData);

            // Getting Header for CSV File
            const headers = util.getKeys(mappedFeedback[0]);

            // Creating CSV File
            const isCsvCreated = await files.createCsvFile(`${feedBackResponsesBaseUrl}/response.csv`, headers, mappedFeedback);
            if (isCsvCreated instanceof Error) {
                util.createLog(event, `isCsvCreated Error: `, isCsvCreated);
                return {
                    statusCode: constants.statusCodes.BadRequest,
                    body: JSON.stringify({
                        error: 'Not able to create CSV'
                    }),
                };
            }

            util.createLog(event, `Csv Created`);
            util.createLog(event, `uploading files to s3 in temp folder`);
            // Creating Zip File
            const zipFile = await files.createZip(`${feedBackResponsesBaseUrl}`, `${studyBaseUrl}/feedbacks.zip`, studyBaseUrl);
            // Convert Zip file to Binary file for S3 Upload
            const binaryZip = files.readFile(`${studyBaseUrl}/feedbacks.zip`);

            // Uploading Zip File to S3 Bucket
            const isUploadedToS3 = await s3Services.uploadFile(bucketName, `studies/${studyId}/tmp/${studyId}_${executionId}/feedbacks.zip`, binaryZip, 'application/zip');
            util.createLog(event, `is zipFile Uploaded To S3 `, isUploadedToS3);
            if (isUploadedToS3) {
                util.createLog(event, `Deleting Temp Files`);
                // files.removeFile(studyBaseUrl);
            }

            // Update Delog Status
            const payloadInProgress = {
                status: constants.enums.deLogs.status.SUCCEEDED,
                message: 'feedback Data export Done',
                completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
            }
            util.createLog(event, `deLog status `, payloadInProgress);
            const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
            if (delogInProgress instanceof Error) {
                util.createLog(event, `delogInProgress Error  `, delogInProgress);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Updating deLog'
                    }),
                };
            }

        } else {
            util.createLog(event, `No feedback responses to export for studyId`);
            // Update Delog Status
            const payloadInProgress = {
                status: constants.enums.deLogs.status.SUCCEEDED,
                message: 'No Data for feedback Export',
                completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
            }
            util.createLog(event, `deLog status `, payloadInProgress);
            const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
            if (delogInProgress instanceof Error) {
                util.createLog(event, `delogInProgress Error  `, delogInProgress);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Updating deLog'
                    }),
                };
            }
        }
        return {
            statusCode: constants.statusCodes.Success,
            body: JSON.stringify({
                    success: 'true feedback-response',
                },
                null,
                2
            ),
        };
    } catch (error) {
        util.createLog(event, `error: `, error);
        console.log('Error', error, 'deLogId', deLogId);

        const payloadFailed = {
            status: constants.enums.deLogs.status.FAILED,
            message: error && error.stack ? error.stack : error && error.message ? error.message : 'failed',
            completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss'),
        }

        const isStatusUpdated = await changeDeLogStatus(event, event.clientId, deLogId, payloadFailed);
        throw error;
    } finally {

    }
};