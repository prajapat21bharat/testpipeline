'use strict';

// const AWS = require('aws-sdk');

const awsXRay = require('aws-xray-sdk');
const AWS = awsXRay.captureAWS(require('aws-sdk'));

const constants = require('../../constants').constants;
const { util, db, files, s3Services, csvMappers, mappers, sendgridMail, excelMappers } = require('../../utility');
const dbObj = new db();
const dbName = constants.database.dbResearchResnpnse;

const _ = require('lodash');

let entityModels, responeEntityModels;

function checkIfSiteTeamMember(userRole) {
    if (!userRole ||
        userRole.toLowerCase() === constants.userRoles.pi.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.subPi.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.studyCoordinator.toLowerCase() ||
        userRole.toLowerCase() === constants.userRoles.homeHealth.toLowerCase()) {
        return true;
    } else {
        return false;
    }
}

async function getStudyDetails(event, studyId) {
    util.createLog(event, `getStudyDetails called`);
    try {
        let filter = {
            id: studyId,
        };
        const studyDetails = await dbObj.findAll(filter, [], entityModels.StudyMetaData);
        return studyDetails;
    } catch (error) {
        util.createLog(event, `Error in getStudyDetails : `, error)
        return error;
    }
}

async function getSiteDetails(event, siteId) {
    util.createLog(event, `getSiteDetails called`);
    try {
        let filter = {
            id: siteId
        };
        const site = await dbObj.findOne(filter, [], entityModels.Sites);
        return site;
    } catch (error) {
        util.createLog(event, `Error in getSiteDetails : `, error);
        return error;
    }
}

async function getClientConfig(event, clientId) {
    util.createLog(event, `getClientConfig Called`);
    try {
        let filter = {
            id: clientId
        };
        const clientConfig = await dbObj.findOne(filter, [], entityModels.ClientConfig);
        return clientConfig;
    } catch (error) {
        util.createLog(event, `Error in getClientConfig : `, error)
        return error;
    }
}

async function getParticipantDetails(event, participantId) {
    util.createLog(event, `getParticipantDetails called`);
    try {
        let filter = {
            id: participantId
        };
        const participant = await dbObj.findOne(filter, [], entityModels.Participant);
        return participant;
    } catch (error) {
        util.createLog(event, `Error in getParticipantDetails : `, error);
        return error;
    }
}

async function createExcelPayload(event, data) {
    util.createLog(event, `Creating Excel Payload`);
    const payload = [];
    for (const truClinicData of data) {
        const mappedResponse = excelMappers.map.truClinicResponseCsvMapper(truClinicData);
        payload.push(mappedResponse);
    }
    return payload;
}

async function getSFTPDetails(event, studyId) {
    util.createLog(event, `getSFTPDetails called`);
    try {
        let filter = {
            study_id: studyId,
        };
        const sftpDetails = await dbObj.findOneWithOrderBy(filter, [], entityModels.DeSftpMeta, ['sftp_version', 'DESC']);
        return sftpDetails;
    } catch (error) {
        util.createLog(event, `Error in getSFTPDetails : `, error)
        return error;
    }
}

async function createDeLog(event, clientId, studyId, logType, loggedInUserId) {
    util.createLog(event, `createDeLog called`);
    try {
        let deSftpMetaId = null;
        if (logType === 'SFTP') {
            const SFTPSecreteManagerDetails = await getSFTPDetails(event, studyId);
            if (SFTPSecreteManagerDetails instanceof Error) {
                util.createLog(event, `getSFTPDetails Error: `, SFTPSecreteManagerDetails);
                await updateDeLogFailed(event, clientId, deLog, SFTPSecreteManagerDetails);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Upload file on sftp'
                    }),
                };
            }
            util.createLog(event, `getSFTPDetails fetched successfully `);
            deSftpMetaId = (SFTPSecreteManagerDetails && SFTPSecreteManagerDetails.id) ? SFTPSecreteManagerDetails.id : null;
        }
        const data = {
            study_id: studyId,
            de_sftp_meta_id: deSftpMetaId,
            trigger_datetime: util.changeDateToUtc(new Date()),
            completed_datetime: null,
            message: null,
            status: constants.enums.deLogs.status.STARTED,
            aws_job_id: (event && event.executionId) ? event.executionId : null,
            aws_log_path: null,
            type: logType,
            s3_location: null,
            triggered_by: loggedInUserId ? loggedInUserId : null
        };
        const payload = mappers.map.deLogDataMapper(data);

        const deLog = await dbObj.create(payload, entityModels.DeLog);
        return deLog;
    } catch (error) {
        util.createLog(event, `Error in createDeLog : `, error)
        return error;
    }
}

async function changeDeLogStatus(event, clientId, logId, payload) {
    util.createLog(event, `changeDeLogStatus called`);
    try {
        const deLog = await dbObj.updateById(logId, payload, entityModels.DeLog);
        return deLog;
    } catch (error) {
        util.createLog(event, `Error in changeDeLogStatus : `, error);
        return error;
    }
}

async function getTruClinicDataByStudyId(event, studyId, clientId) {
    util.createLog(event, `getTruClinicDataByStudyId called`);
    try {
        let filter = {
            study_id: studyId
        };
        const truClinicResponse = await dbObj.findAll(filter, [], responeEntityModels.ParticipantAppointment);
        return truClinicResponse;
    } catch (error) {
        util.createLog(event, `Error in getTruClinicDataByStudyId : `, error)
        return error;
    }
}

async function getTruClinicDataByStudyIdAndDefaultSiteId(event, studyId, defaultSiteId, clientId) {
    util.createLog(event, `getTruClinicDataByStudyIdAndDefaultSiteId called`)
    try {
        let filter = {
            study_id: studyId,
            site_id: defaultSiteId
        };
        const truClinicResponse = await dbObj.findAll(filter, [], responeEntityModels.ParticipantAppointment);
        return truClinicResponse;
    } catch (error) {
        util.createLog(event, `Error in getTruClinicDataByStudyIdAndDefaultSiteId : `, error);
        return error;
    }
}

async function getTruClinicDataByStudyIdAndSiteId(event, studyId, loggedInUserSiteId, clientId) {
    util.createLog(event, `getTruClinicDataByStudyIdAndSiteId Called`);
    try {
        let filter = {
            study_id: studyId,
            site_id: loggedInUserSiteId
        };
        const truClinicResponse = await dbObj.findAll(filter, [], responeEntityModels.ParticipantAppointment);
        return truClinicResponse;
    } catch (error) {
        util.createLog(event, `Error in getTruClinicDataByStudyIdAndSiteId : `, error);
        return error;
    }
}

async function getStudySiteDetails(event, studyId) {
    util.createLog(event, `getStudySiteDetails called`);
    try {
        let filter = {
            study_id: studyId,
        };
        const studySiteDetails = await dbObj.findAll(filter, [], entityModels.StudySite, '', ['study_id', 'ASC']);
        return studySiteDetails;
    } catch (error) {
        util.createLog(event, `getStudySiteDetails Error: `, error);
        return error;
    }
}
async function validateStudy(event, studyId, clientId) {
    try {
        let filter = {
            id: studyId,
            client_id: clientId
        };
        const study = await dbObj.findOne(filter, [], entityModels.StudyMetaData);
        return study;
    } catch (error) {
        util.createLog(event, `Error in validateStudy: `, error);
        return error;
    }
}
module.exports.export = async(event, context) => {
    try {
        util.createLog(event, `Lambda TruClinic Data Execution Started`);
        let sftpUpload = false;
        let clientId, studyId, zipFileNameWithTimeStamp, authorization, isDataForNonPI;

        let loggedInUserId = (event && event.triggeredBy && event.triggeredBy.user && event.triggeredBy.user.id) ? event.triggeredBy.user.id : null;
        let hasAreas = [];
        if (event && event.areas) {
            hasAreas = event.areas.split(',');
        }
        //Checked area to execute trueClinic lambda
        if (hasAreas && hasAreas.length > 0 && hasAreas.includes(constants.exportAreas.trueClinic) === false) {
            util.createLog(event, `Skipped lambda TruClinic`);
            return;
        }
        if (event && event.clientId) {
            clientId = event.clientId;
        } else {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: 'Missing clientId'
                }),
            };
        }
        // checking for sftpupload parameter in payload
        if (event && event.sftpUpload && event.sftpUpload === true) {
            sftpUpload = event.sftpUpload;
        }
        // checking for studyId in paylod
        if (event && event.studyId) {
            studyId = event.studyId;
            util.createLog(event, `studyId is `, studyId);
        } else {
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    message: 'Bad Request',
                    error: 'studyId is required'
                }),
            };
        }
        const models = await dbObj.init(clientId);
        entityModels = models;

        const responseModels = await dbObj.init(clientId, dbName);
        responeEntityModels = responseModels;
        const validateStudyDetails = await validateStudy(event, studyId, clientId);
        if (!validateStudyDetails || validateStudyDetails.length === 0) {
            return {
                statusCode: constants.statusCodes.Unauthorized,
                body: JSON.stringify({
                    message: "Invalid study details or Unauthorized access"
                })
            };
        }
        const loggedInUserRole = (event && event.triggeredBy && event.triggeredBy.user && event.triggeredBy.user.role) ? event.triggeredBy.user.role : null;
        const loggedInUserSiteIds = event.siteIds;
        let loggedInUserSiteId;
        // if ((!loggedInUserSiteIds || !loggedInUserSiteIds.length > 0) && !loggedInUserRole.toLowerCase() === constants.userRoles.admin.toLowerCase()) {
        //     return {
        //         statusCode: constants.statusCodes.Unauthorized,
        //         body: JSON.stringify({
        //             message: 'User is not associated with any site'
        //         }),
        //     };
        // }
        // if (loggedInUserSiteIds && loggedInUserSiteIds.length > 0) {
        //     loggedInUserSiteId = loggedInUserSiteIds[0];
        //     util.createLog(event, `User is associated with site: loggedInUserSiteId ${loggedInUserSiteId}, LoggedIn user role is: ${loggedInUserRole}`);
        // }
        const isSiteTeamMember = checkIfSiteTeamMember(loggedInUserRole);
        if (isSiteTeamMember) {
            util.createLog(event, `Site team member logged in`);
            if (loggedInUserSiteIds && loggedInUserSiteIds.length > 0) {
                loggedInUserSiteId = loggedInUserSiteIds[0]
            } else {
                throw 'User is not associated with any site';
            }
        }
        // if (!isSiteTeamMember) {
        //     return {
        //         statusCode: constants.statusCodes.Unauthorized,
        //         body: JSON.stringify({
        //             message: 'User is not authorized user'
        //         }),
        //     };
        // }
        util.createLog(event, `User is associated with site: loggedInUserSiteId ${loggedInUserSiteId}, LoggedIn user role is: ${loggedInUserRole}`);
        const studyDetails = await getStudyDetails(event, studyId);
        if (studyDetails instanceof Error) {
            util.createLog(event, `studyDetails Error: `, siteDetails);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Study Details not found'
                }),
            };
        }

        const studySiteDetails = await getStudySiteDetails(event, studyId);

        let defaultSiteId;
        for (const studySiteDetail of studySiteDetails) {
            if (studySiteDetail && studySiteDetail.is_default) {
                defaultSiteId = studySiteDetail.site_id;
            }
        }
        util.createLog(event, `defaultSiteId:`, defaultSiteId);

        // Creating deLog
        const logType = (sftpUpload === true) ? constants.enums.deLogs.type.SFTP : constants.enums.deLogs.type.DATAEXPORT;
        const deLog = await createDeLog(event, clientId, studyId, logType, loggedInUserId);
        util.createLog(event, `deLog: ${deLog.id}`);
        if (deLog instanceof Error) {
            util.createLog(event, `unable to insert de log record: ${deLog}`);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Not able to create Delog'
                }),
            };
        }

        console.log(loggedInUserSiteId, defaultSiteId);
        // Fetching site details
        const siteDetails = await getSiteDetails(event, (loggedInUserSiteId) ? loggedInUserSiteId : defaultSiteId);
        if (siteDetails instanceof Error) {
            util.createLog(event, `siteDetails Error: `, siteDetails);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'Site Details not found'
                }),
            };
        }
        const siteId = siteDetails.site_id;

        // Fetching client configuration details
        const clientConfig = await getClientConfig(event, clientId);
        if (clientConfig instanceof Error) {
            util.createLog(event, `clientConfig Error: `, clientConfig)
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'Client Config not found'
                }),
            };
        }
        const bucketName = clientConfig.s3_bucket;
        const domain = clientConfig.domain_name;
        if (!bucketName) {
            util.createLog(event, `S3 Bucket not Found`);
            return {
                statusCode: constants.statusCodes.BadRequest,
                body: JSON.stringify({
                    error: 'S3 Bucket not Found'
                }),
            };
        }
        let executionId = util.generateUniqueCode();
        if (event.executionId) {
            executionId = event.executionId;
        }
        let studyBaseUrl = `/tmp/${constants.tempExportFolder}/${studyId}_${executionId}`;
        if (event.studyBaseUrl) {
            studyBaseUrl = event.studyBaseUrl;
        }
        let zipFileName = studyId;
        if (studyDetails && studyDetails.length > 0 && studyDetails[0].name) {
            util.createLog(event, `Study Name ${studyDetails.name}`)
            zipFileName = studyDetails[0].name;
        }
        util.createLog(event, `zipFileName `, zipFileName);
        const dateNow = new Date();
        const timeStamp = util.changeDateFormat(dateNow, 'YYYYMMDDHHmm');
        zipFileNameWithTimeStamp = `${zipFileName}_${timeStamp}`;
        if (event.zipFileNameWithTimeStamp) {
            zipFileNameWithTimeStamp = event.zipFileNameWithTimeStamp;
        }
        util.createLog(event, `zipFileNameWithTimeStamp `, zipFileNameWithTimeStamp);
        const truClinicBaseUrl = studyBaseUrl + "/telehealth";

        // Update Delog Status
        const payloadInProgress = { status: constants.enums.deLogs.status.INPROGRESS, message: 'tru-clinic' }
        util.createLog(event, `deLog status `, payloadInProgress);
        const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
        if (delogInProgress instanceof Error) {
            util.createLog(event, `delogInProgress Error  `, delogInProgress);
            return {
                statusCode: constants.statusCodes.InternalServerError,
                body: JSON.stringify({
                    error: 'Error occured in Updating deLog'
                }),
            };
        }
        let truClinicResponseData;
        if (!loggedInUserSiteId) {
            //For all user other than site team members
            util.createLog(event, `!loggedInUserSiteId`)
            truClinicResponseData = await getTruClinicDataByStudyId(event, studyId, clientId);

        } else if (defaultSiteId && defaultSiteId === loggedInUserSiteId) {
            //For default site's site team members this is for handling null values of site_is column
            util.createLog(event, `defaultSiteId && defaultSiteId === loggedInUserSiteId`)
            truClinicResponseData = await getTruClinicDataByStudyIdAndDefaultSiteId(event, studyId, defaultSiteId, clientId);
        } else {
            //For non-default site's site team members
            util.createLog(event, `For non-default sites site team members`)
            truClinicResponseData = await getTruClinicDataByStudyIdAndSiteId(event, studyId, loggedInUserSiteId, clientId);
        }
        if (truClinicResponseData && truClinicResponseData.length > 0) {
            const truClinicResponseXlxData = [];
            for (const truClinicResponse of truClinicResponseData) {
                let data = JSON.parse(JSON.stringify(truClinicResponse));

                const participant = await getParticipantDetails(event, truClinicResponse.participant_id);
                data.userDefinedParticipantId = participant.user_defined_participant_id;

                data.siteId = siteDetails.siteId;
                data.siteName = siteDetails.name;
                const piName = (event.firstName && event.lastName) ? event.firstName + " " + event.lastName : "";
                const piId = (event.loggedInUserId) ? event.loggedInUserId : '';
                data.piName = piName;
                data.piId = piId;
                truClinicResponseXlxData.push(data);
            }

            // Creating Excel payload and mappings for data
            const mappedResponse = await createExcelPayload(event, truClinicResponseXlxData);

            // Getting Header for Excel File
            const headers = util.getExcelKeys(mappedResponse[0]);

            // Creating Excel File;
            const isExcelCreated = await files.createExcelFile(`${truClinicBaseUrl}/appointment.xlsx`, headers, mappedResponse, "Appointments");

            if (isExcelCreated instanceof Error) {
                util.createLog(event, `Excel create error: ${isExcelCreated}`);
                return {
                    statusCode: constants.statusCodes.BadRequest,
                    body: JSON.stringify({
                        error: 'Not able to create Excel File'
                    }),
                };
            }

            util.createLog(event, `uploading files to s3 in temp folder`);
            // Creating Zip File
            const zipFile = await files.createZip(`${truClinicBaseUrl}`, `${studyBaseUrl}/truclinic.zip`, studyBaseUrl);
            // Convert Zip file to Binary file for S3 Upload
            const binaryZip = files.readFile(`${studyBaseUrl}/truclinic.zip`);

            // Uploading Zip File to S3 Bucket
            const isUploadedToS3 = await s3Services.uploadFile(bucketName, `studies/${studyId}/tmp/${studyId}_${executionId}/truclinic.zip`, binaryZip, 'application/zip');
            util.createLog(event, `is zipFile Uploaded To S3 `, isUploadedToS3);
            if (isUploadedToS3) {
                util.createLog(event, `Deleting Temp Files`);
                // files.removeFile(studyBaseUrl);
            }
            // Update Delog Status
            const payloadInDone = {
                status: constants.enums.deLogs.status.SUCCEEDED,
                message: 'Truclinic Data export Done',
                completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
            }
            util.createLog(event, `deLog status ${payloadInDone}`);
            const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInDone);
            if (delogInProgress instanceof Error) {
                util.createLog(event, `delogInProgress Error  `, delogInProgress);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Updating deLog'
                    }),
                };
            }
        } else {
            util.createLog(event, `No TruClinic Data to export for studyId :${studyId}`);
            // Update Delog Status
            const payloadInProgress = {
                status: constants.enums.deLogs.status.SUCCEEDED,
                message: 'No Data for TruClinic Export',
                completed_datetime: util.changeDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss')
            }
            util.createLog(event, `deLog status `, payloadInProgress);
            const delogInProgress = await changeDeLogStatus(event, clientId, deLog.id, payloadInProgress);
            if (delogInProgress instanceof Error) {
                util.createLog(event, `delogInProgress Error  `, delogInProgress);
                return {
                    statusCode: constants.statusCodes.InternalServerError,
                    body: JSON.stringify({
                        error: 'Error occured in Updating deLog'
                    }),
                };
            }
        }
        return {
            statusCode: 200,
            body: JSON.stringify({
                    success: 'true clinic appointment successfull'
                },
                null,
                2
            ),
        };
    } catch (error) {
        util.createLog(event, `Error in exporting TruClinic Data : `, error);
        console.log('Error: ', error);
        return {
            statusCode: constants.statusCodes.InternalServerError,
            body: JSON.stringify({
                    error: error,
                },
                null,
                2
            ),
        };
    } finally {

    }
};